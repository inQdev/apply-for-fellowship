<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160505162234 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE specialist_projects_areas_of_expertise (specialist_project_id INT NOT NULL, area_of_expertise_id INT NOT NULL, INDEX IDX_F7732EDC1BB7C343 (specialist_project_id), INDEX IDX_F7732EDC52D09986 (area_of_expertise_id), PRIMARY KEY(specialist_project_id, area_of_expertise_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE specialist_projects_areas_of_expertise ADD CONSTRAINT FK_F7732EDC1BB7C343 FOREIGN KEY (specialist_project_id) REFERENCES specialist_project (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE specialist_projects_areas_of_expertise ADD CONSTRAINT FK_F7732EDC52D09986 FOREIGN KEY (area_of_expertise_id) REFERENCES base_term (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE fellow_project CHANGE dependent_restrictions_desc dependent_restrictions_desc LONGTEXT DEFAULT NULL, CHANGE cycle_season_desc cycle_season_desc LONGTEXT DEFAULT NULL, CHANGE housing_desc housing_desc LONGTEXT DEFAULT NULL, CHANGE housing_dependent_info housing_dependent_info LONGTEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE fellow_project_budget CHANGE post_funding_source post_funding_source LONGTEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE fellowship_duty CHANGE additional_info_desc additional_info_desc LONGTEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE fellowship_duty_activity CHANGE activity_desc activity_desc LONGTEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE location_host_info CHANGE host_desc host_desc LONGTEXT DEFAULT NULL, CHANGE additional_info_desc additional_info_desc LONGTEXT DEFAULT NULL, CHANGE host_city_description host_city_description LONGTEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE project_general_info CHANGE visa_requirements_desc visa_requirements_desc LONGTEXT DEFAULT NULL, CHANGE medical_restrictions_desc medical_restrictions_desc LONGTEXT DEFAULT NULL, CHANGE degree_requirements_desc degree_requirements_desc LONGTEXT DEFAULT NULL, CHANGE security_info_desc security_info_desc LONGTEXT DEFAULT NULL, CHANGE additional_requirements_desc additional_requirements_desc LONGTEXT DEFAULT NULL, CHANGE post_mission_goals_desc post_mission_goals_desc LONGTEXT DEFAULT NULL, CHANGE sustainability_by_locals_desc sustainability_by_locals_desc LONGTEXT DEFAULT NULL, CHANGE required_vaccinations_desc required_vaccinations_desc LONGTEXT DEFAULT NULL, CHANGE age_restriction_desc age_restriction_desc LONGTEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE specialist_project ADD degree_requirements TINYINT(1) DEFAULT NULL, ADD degree_requirements_desc LONGTEXT DEFAULT NULL, ADD proposed_candidate VARCHAR(255) DEFAULT NULL, ADD proposed_candidate_contacted TINYINT(1) DEFAULT NULL, ADD proposed_candidate_email VARCHAR(255) DEFAULT NULL, CHANGE further_details_desc further_details_desc LONGTEXT DEFAULT NULL');
    }

    /**
     * @param Schema $schema
     */
    public function postUp(Schema $schema)
    {
        $this->connection->executeQuery("INSERT INTO base_term(name, type, created_at, updated_at) VALUES ('Assessment and Testing', 3, '2016-05-05 17:07:39', '2016-05-05 17:07:39'),('Content Based Instruction / Content and Language Integrated Learning', 3, '2016-05-05 17:07:39', '2016-05-05 17:07:39'),('Cross-Cultural Studies and Education', 3, '2016-05-05 17:07:39', '2016-05-05 17:07:39'),('Curriculum Design', 3, '2016-05-05 17:07:39', '2016-05-05 17:07:39'),('Differentiated Instruction', 3, '2016-05-05 17:07:39', '2016-05-05 17:07:39'),('English for Academic Purposes (EAP)', 3, '2016-05-05 17:07:39', '2016-05-05 17:07:39'),('ESP - Aviation ', 3, '2016-05-05 17:07:39', '2016-05-05 17:07:39'),('ESP – Business', 3, '2016-05-05 17:07:39', '2016-05-05 17:07:39'),('ESP - Engineering', 3, '2016-05-05 17:07:39', '2016-05-05 17:07:39'),('ESP - Hospitality', 3, '2016-05-05 17:07:39', '2016-05-05 17:07:39'),('ESP – Journalism', 3, '2016-05-05 17:07:39', '2016-05-05 17:07:39'),('ESP – Law', 3, '2016-05-05 17:07:39', '2016-05-05 17:07:39'),('ESP – Medical', 3, '2016-05-05 17:07:39', '2016-05-05 17:07:39'),('ESP – Science', 3, '2016-05-05 17:07:39', '2016-05-05 17:07:39'),('ESP – Security', 3, '2016-05-05 17:07:39', '2016-05-05 17:07:39'),('ESP – Tourism', 3, '2016-05-05 17:07:39', '2016-05-05 17:07:39'),('ESP - other', 3, '2016-05-05 17:07:39', '2016-05-05 17:07:39'),('Literacy', 3, '2016-05-05 17:07:39', '2016-05-05 17:07:39'),('Materials Design', 3, '2016-05-05 17:07:39', '2016-05-05 17:07:39'),('Multi-level classrooms', 3, '2016-05-05 17:07:39', '2016-05-05 17:07:39'),('Needs Assessment', 3, '2016-05-05 17:07:39', '2016-05-05 17:07:39'),('Research Writing and Publication /Academic Writing ', 3, '2016-05-05 17:07:39', '2016-05-05 17:07:39'),('Resource-limited Teaching', 3, '2016-05-05 17:07:39', '2016-05-05 17:07:39'),('Special Education / Special Needs Populations', 3, '2016-05-05 17:07:39', '2016-05-05 17:07:39'),('Teaching English to Young Learners (TEYL)', 3, '2016-05-05 17:07:39', '2016-05-05 17:07:39'),('Technology in the Classroom/E-learning/Blended learning/CALL', 3, '2016-05-05 17:07:39', '2016-05-05 17:07:39'),('Teacher Training / Training of Trainers', 3, '2016-05-05 17:07:39', '2016-05-05 17:07:39'),('TESOL Methodology', 3, '2016-05-05 17:07:39', '2016-05-05 17:07:39'),('Other', 3, '2016-05-05 17:07:39', '2016-05-05 17:07:39');");
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE specialist_projects_areas_of_expertise');
        $this->addSql('ALTER TABLE fellow_project CHANGE dependent_restrictions_desc dependent_restrictions_desc LONGTEXT DEFAULT NULL COLLATE utf8_unicode_ci, CHANGE cycle_season_desc cycle_season_desc LONGTEXT DEFAULT NULL COLLATE utf8_unicode_ci, CHANGE housing_desc housing_desc LONGTEXT DEFAULT NULL COLLATE utf8_unicode_ci, CHANGE housing_dependent_info housing_dependent_info LONGTEXT DEFAULT NULL COLLATE utf8_unicode_ci');
        $this->addSql('ALTER TABLE fellow_project_budget CHANGE post_funding_source post_funding_source LONGTEXT DEFAULT NULL COLLATE utf8_unicode_ci');
        $this->addSql('ALTER TABLE fellowship_duty CHANGE additional_info_desc additional_info_desc LONGTEXT DEFAULT NULL COLLATE utf8_unicode_ci');
        $this->addSql('ALTER TABLE fellowship_duty_activity CHANGE activity_desc activity_desc LONGTEXT DEFAULT NULL COLLATE utf8_unicode_ci');
        $this->addSql('ALTER TABLE location_host_info CHANGE host_city_description host_city_description LONGTEXT DEFAULT NULL COLLATE utf8_unicode_ci, CHANGE host_desc host_desc LONGTEXT DEFAULT NULL COLLATE utf8_unicode_ci, CHANGE additional_info_desc additional_info_desc LONGTEXT DEFAULT NULL COLLATE utf8_unicode_ci');
        $this->addSql('ALTER TABLE project_general_info CHANGE visa_requirements_desc visa_requirements_desc LONGTEXT DEFAULT NULL COLLATE utf8_unicode_ci, CHANGE medical_restrictions_desc medical_restrictions_desc LONGTEXT DEFAULT NULL COLLATE utf8_unicode_ci, CHANGE required_vaccinations_desc required_vaccinations_desc LONGTEXT DEFAULT NULL COLLATE utf8_unicode_ci, CHANGE age_restriction_desc age_restriction_desc LONGTEXT DEFAULT NULL COLLATE utf8_unicode_ci, CHANGE degree_requirements_desc degree_requirements_desc LONGTEXT DEFAULT NULL COLLATE utf8_unicode_ci, CHANGE security_info_desc security_info_desc LONGTEXT DEFAULT NULL COLLATE utf8_unicode_ci, CHANGE additional_requirements_desc additional_requirements_desc LONGTEXT DEFAULT NULL COLLATE utf8_unicode_ci, CHANGE post_mission_goals_desc post_mission_goals_desc LONGTEXT DEFAULT NULL COLLATE utf8_unicode_ci, CHANGE sustainability_by_locals_desc sustainability_by_locals_desc LONGTEXT DEFAULT NULL COLLATE utf8_unicode_ci');
        $this->addSql('ALTER TABLE specialist_project DROP degree_requirements, DROP degree_requirements_desc, DROP proposed_candidate, DROP proposed_candidate_contacted, DROP proposed_candidate_email, CHANGE further_details_desc further_details_desc VARCHAR(255) DEFAULT NULL COLLATE utf8_unicode_ci');
    }
}
