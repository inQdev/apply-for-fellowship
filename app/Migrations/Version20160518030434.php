<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160518030434 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE project_review_comment (id INT AUTO_INCREMENT NOT NULL, project_review_id INT DEFAULT NULL, comment_by_id INT DEFAULT NULL, action_taken VARCHAR(40) NOT NULL, comment LONGTEXT NOT NULL, comment_by_role VARCHAR(40) NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, INDEX IDX_E62DD2392F12D34C (project_review_id), INDEX IDX_E62DD23937DB8F5 (comment_by_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE project_review_comment ADD CONSTRAINT FK_E62DD2392F12D34C FOREIGN KEY (project_review_id) REFERENCES project_review (id)');
        $this->addSql('ALTER TABLE project_review_comment ADD CONSTRAINT FK_E62DD23937DB8F5 FOREIGN KEY (comment_by_id) REFERENCES user (id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE project_review_comment');
    }
}
