<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160515130753 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE fellow_project CHANGE dependent_restrictions_desc dependent_restrictions_desc LONGTEXT DEFAULT NULL, CHANGE cycle_season_desc cycle_season_desc LONGTEXT DEFAULT NULL, CHANGE housing_desc housing_desc LONGTEXT DEFAULT NULL, CHANGE housing_dependent_info housing_dependent_info LONGTEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE fellow_project_budget CHANGE post_funding_source post_funding_source LONGTEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE fellowship_duty CHANGE additional_info_desc additional_info_desc LONGTEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE fellowship_duty_activity CHANGE activity_desc activity_desc LONGTEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE location_host_info CHANGE host_desc host_desc LONGTEXT DEFAULT NULL, CHANGE additional_info_desc additional_info_desc LONGTEXT DEFAULT NULL, CHANGE host_city_description host_city_description LONGTEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE pre_post_work CHANGE additional_pre_work_desc additional_pre_work_desc LONGTEXT DEFAULT NULL, CHANGE additional_post_work_desc additional_post_work_desc LONGTEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE project_general_info ADD project_review_status_id INT DEFAULT NULL, ADD project_outcome_id INT DEFAULT NULL, CHANGE visa_requirements_desc visa_requirements_desc LONGTEXT DEFAULT NULL, CHANGE medical_restrictions_desc medical_restrictions_desc LONGTEXT DEFAULT NULL, CHANGE degree_requirements_desc degree_requirements_desc LONGTEXT DEFAULT NULL, CHANGE security_info_desc security_info_desc LONGTEXT DEFAULT NULL, CHANGE additional_requirements_desc additional_requirements_desc LONGTEXT DEFAULT NULL, CHANGE post_mission_goals_desc post_mission_goals_desc LONGTEXT DEFAULT NULL, CHANGE sustainability_by_locals_desc sustainability_by_locals_desc LONGTEXT DEFAULT NULL, CHANGE required_vaccinations_desc required_vaccinations_desc LONGTEXT DEFAULT NULL, CHANGE age_restriction_desc age_restriction_desc LONGTEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE project_general_info ADD CONSTRAINT FK_19EDC515418358D5 FOREIGN KEY (project_review_status_id) REFERENCES project_review_status (id)');
        $this->addSql('ALTER TABLE project_general_info ADD CONSTRAINT FK_19EDC515FE481651 FOREIGN KEY (project_outcome_id) REFERENCES project_outcome (id)');
        $this->addSql('CREATE INDEX IDX_19EDC515418358D5 ON project_general_info (project_review_status_id)');
        $this->addSql('CREATE INDEX IDX_19EDC515FE481651 ON project_general_info (project_outcome_id)');
        $this->addSql('ALTER TABLE project_review DROP FOREIGN KEY FK_6EE76A8C418358D5');
        $this->addSql('ALTER TABLE project_review DROP FOREIGN KEY FK_6EE76A8CFE481651');
        $this->addSql('DROP INDEX IDX_6EE76A8C418358D5 ON project_review');
        $this->addSql('DROP INDEX IDX_6EE76A8CFE481651 ON project_review');
        $this->addSql('ALTER TABLE project_review DROP project_review_status_id, DROP project_outcome_id');
        $this->addSql('ALTER TABLE specialist_project CHANGE further_details_desc further_details_desc LONGTEXT DEFAULT NULL, CHANGE degree_requirements_desc degree_requirements_desc LONGTEXT DEFAULT NULL, CHANGE subsequent_phases_desc subsequent_phases_desc LONGTEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE specialist_project_phase_budget DROP FOREIGN KEY FK_4DA1487FB6CDBD28');
        $this->addSql('DROP INDEX UNIQ_4DA1487FB6CDBD28 ON specialist_project_phase_budget');
        $this->addSql('ALTER TABLE specialist_project_phase_budget DROP program_activities_allowance_id, CHANGE post_funding_source post_funding_source LONGTEXT DEFAULT NULL');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE fellow_project CHANGE dependent_restrictions_desc dependent_restrictions_desc LONGTEXT DEFAULT NULL COLLATE utf8_unicode_ci, CHANGE cycle_season_desc cycle_season_desc LONGTEXT DEFAULT NULL COLLATE utf8_unicode_ci, CHANGE housing_desc housing_desc LONGTEXT DEFAULT NULL COLLATE utf8_unicode_ci, CHANGE housing_dependent_info housing_dependent_info LONGTEXT DEFAULT NULL COLLATE utf8_unicode_ci');
        $this->addSql('ALTER TABLE fellow_project_budget CHANGE post_funding_source post_funding_source LONGTEXT DEFAULT NULL COLLATE utf8_unicode_ci');
        $this->addSql('ALTER TABLE fellowship_duty CHANGE additional_info_desc additional_info_desc LONGTEXT DEFAULT NULL COLLATE utf8_unicode_ci');
        $this->addSql('ALTER TABLE fellowship_duty_activity CHANGE activity_desc activity_desc LONGTEXT DEFAULT NULL COLLATE utf8_unicode_ci');
        $this->addSql('ALTER TABLE location_host_info CHANGE host_city_description host_city_description LONGTEXT DEFAULT NULL COLLATE utf8_unicode_ci, CHANGE host_desc host_desc LONGTEXT DEFAULT NULL COLLATE utf8_unicode_ci, CHANGE additional_info_desc additional_info_desc LONGTEXT DEFAULT NULL COLLATE utf8_unicode_ci');
        $this->addSql('ALTER TABLE pre_post_work CHANGE additional_pre_work_desc additional_pre_work_desc LONGTEXT DEFAULT NULL COLLATE utf8_unicode_ci, CHANGE additional_post_work_desc additional_post_work_desc LONGTEXT DEFAULT NULL COLLATE utf8_unicode_ci');
        $this->addSql('ALTER TABLE project_general_info DROP FOREIGN KEY FK_19EDC515418358D5');
        $this->addSql('ALTER TABLE project_general_info DROP FOREIGN KEY FK_19EDC515FE481651');
        $this->addSql('DROP INDEX IDX_19EDC515418358D5 ON project_general_info');
        $this->addSql('DROP INDEX IDX_19EDC515FE481651 ON project_general_info');
        $this->addSql('ALTER TABLE project_general_info DROP project_review_status_id, DROP project_outcome_id, CHANGE visa_requirements_desc visa_requirements_desc LONGTEXT DEFAULT NULL COLLATE utf8_unicode_ci, CHANGE medical_restrictions_desc medical_restrictions_desc LONGTEXT DEFAULT NULL COLLATE utf8_unicode_ci, CHANGE required_vaccinations_desc required_vaccinations_desc LONGTEXT DEFAULT NULL COLLATE utf8_unicode_ci, CHANGE age_restriction_desc age_restriction_desc LONGTEXT DEFAULT NULL COLLATE utf8_unicode_ci, CHANGE degree_requirements_desc degree_requirements_desc LONGTEXT DEFAULT NULL COLLATE utf8_unicode_ci, CHANGE security_info_desc security_info_desc LONGTEXT DEFAULT NULL COLLATE utf8_unicode_ci, CHANGE additional_requirements_desc additional_requirements_desc LONGTEXT DEFAULT NULL COLLATE utf8_unicode_ci, CHANGE post_mission_goals_desc post_mission_goals_desc LONGTEXT DEFAULT NULL COLLATE utf8_unicode_ci, CHANGE sustainability_by_locals_desc sustainability_by_locals_desc LONGTEXT DEFAULT NULL COLLATE utf8_unicode_ci');
        $this->addSql('ALTER TABLE project_review ADD project_review_status_id INT DEFAULT NULL, ADD project_outcome_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE project_review ADD CONSTRAINT FK_6EE76A8C418358D5 FOREIGN KEY (project_review_status_id) REFERENCES project_review_status (id)');
        $this->addSql('ALTER TABLE project_review ADD CONSTRAINT FK_6EE76A8CFE481651 FOREIGN KEY (project_outcome_id) REFERENCES project_outcome (id)');
        $this->addSql('CREATE INDEX IDX_6EE76A8C418358D5 ON project_review (project_review_status_id)');
        $this->addSql('CREATE INDEX IDX_6EE76A8CFE481651 ON project_review (project_outcome_id)');
        $this->addSql('ALTER TABLE specialist_project CHANGE further_details_desc further_details_desc LONGTEXT DEFAULT NULL COLLATE utf8_unicode_ci, CHANGE degree_requirements_desc degree_requirements_desc LONGTEXT DEFAULT NULL COLLATE utf8_unicode_ci, CHANGE subsequent_phases_desc subsequent_phases_desc LONGTEXT DEFAULT NULL COLLATE utf8_unicode_ci');
        $this->addSql('ALTER TABLE specialist_project_phase_budget ADD program_activities_allowance_id INT DEFAULT NULL, CHANGE post_funding_source post_funding_source LONGTEXT DEFAULT NULL COLLATE utf8_unicode_ci');
        $this->addSql('ALTER TABLE specialist_project_phase_budget ADD CONSTRAINT FK_4DA1487FB6CDBD28 FOREIGN KEY (program_activities_allowance_id) REFERENCES program_activities_allowance (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_4DA1487FB6CDBD28 ON specialist_project_phase_budget (program_activities_allowance_id)');
    }
}
