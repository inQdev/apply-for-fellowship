<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160509082732 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE day_breakdown_activity ADD day_breakdown_activity_type_id INT DEFAULT NULL, DROP day_breakdown_activity_type');
        $this->addSql('ALTER TABLE day_breakdown_activity ADD CONSTRAINT FK_A1A860DCD30867FD FOREIGN KEY (day_breakdown_activity_type_id) REFERENCES base_term (id)');
        $this->addSql('CREATE INDEX IDX_A1A860DCD30867FD ON day_breakdown_activity (day_breakdown_activity_type_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE day_breakdown_activity DROP FOREIGN KEY FK_A1A860DCD30867FD');
        $this->addSql('DROP INDEX IDX_A1A860DCD30867FD ON day_breakdown_activity');
        $this->addSql('ALTER TABLE day_breakdown_activity ADD day_breakdown_activity_type VARCHAR(255) DEFAULT NULL COLLATE utf8_unicode_ci, DROP day_breakdown_activity_type_id');
    }
}
