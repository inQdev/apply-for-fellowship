<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20160502100928 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE in_country_assignment (id INT AUTO_INCREMENT NOT NULL, country_id INT DEFAULT NULL, specialist_project_phase_id INT DEFAULT NULL, host_id INT DEFAULT NULL, city VARCHAR(255) NOT NULL, deliverables_desc LONGTEXT DEFAULT NULL, host_desc LONGTEXT DEFAULT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, INDEX IDX_A9F5D476F92F3E70 (country_id), INDEX IDX_A9F5D4764A1B6C5F (specialist_project_phase_id), INDEX IDX_A9F5D4761FB8D185 (host_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE in_country_assignment_activity (id INT AUTO_INCREMENT NOT NULL, in_country_assignment_id INT DEFAULT NULL, activity_desc LONGTEXT DEFAULT NULL, topic VARCHAR(255) DEFAULT NULL, audience_desc VARCHAR(255) DEFAULT NULL, participants SMALLINT DEFAULT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, INDEX IDX_8B0FDBDB1EBED7F3 (in_country_assignment_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE day_breakdown_activity (id INT AUTO_INCREMENT NOT NULL, in_country_assignment_id INT DEFAULT NULL, activity_date DATE NOT NULL, day_breakdown_activity_type VARCHAR(255) DEFAULT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, INDEX IDX_A1A860DC1EBED7F3 (in_country_assignment_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE in_country_assignment ADD CONSTRAINT FK_A9F5D476F92F3E70 FOREIGN KEY (country_id) REFERENCES country (id)');
        $this->addSql('ALTER TABLE in_country_assignment ADD CONSTRAINT FK_A9F5D4764A1B6C5F FOREIGN KEY (specialist_project_phase_id) REFERENCES specialist_project_phase (id)');
        $this->addSql('ALTER TABLE in_country_assignment ADD CONSTRAINT FK_A9F5D4761FB8D185 FOREIGN KEY (host_id) REFERENCES host (id)');
        $this->addSql('ALTER TABLE in_country_assignment_activity ADD CONSTRAINT FK_8B0FDBDB1EBED7F3 FOREIGN KEY (in_country_assignment_id) REFERENCES in_country_assignment (id)');
        $this->addSql('ALTER TABLE day_breakdown_activity ADD CONSTRAINT FK_A1A860DC1EBED7F3 FOREIGN KEY (in_country_assignment_id) REFERENCES in_country_assignment (id)');
        $this->addSql('ALTER TABLE location_host_info CHANGE host_desc host_desc LONGTEXT DEFAULT NULL, CHANGE additional_info_desc additional_info_desc LONGTEXT DEFAULT NULL, CHANGE host_city_description host_city_description LONGTEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE specialist_project_phase ADD type VARCHAR(45) DEFAULT \'in-country\' NOT NULL, ADD start_date DATE DEFAULT NULL, ADD end_date DATE DEFAULT NULL, ADD date_length_desc LONGTEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE project_general_info CHANGE visa_requirements_desc visa_requirements_desc LONGTEXT DEFAULT NULL, CHANGE medical_restrictions_desc medical_restrictions_desc LONGTEXT DEFAULT NULL, CHANGE degree_requirements_desc degree_requirements_desc LONGTEXT DEFAULT NULL, CHANGE security_info_desc security_info_desc LONGTEXT DEFAULT NULL, CHANGE additional_requirements_desc additional_requirements_desc LONGTEXT DEFAULT NULL, CHANGE post_mission_goals_desc post_mission_goals_desc LONGTEXT DEFAULT NULL, CHANGE sustainability_by_locals_desc sustainability_by_locals_desc LONGTEXT DEFAULT NULL, CHANGE required_vaccinations_desc required_vaccinations_desc LONGTEXT DEFAULT NULL, CHANGE age_restriction_desc age_restriction_desc LONGTEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE fellow_project_budget CHANGE post_funding_source post_funding_source LONGTEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE fellowship_duty_activity CHANGE activity_desc activity_desc LONGTEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE fellow_project CHANGE dependent_restrictions_desc dependent_restrictions_desc LONGTEXT DEFAULT NULL, CHANGE cycle_season_desc cycle_season_desc LONGTEXT DEFAULT NULL, CHANGE housing_desc housing_desc LONGTEXT DEFAULT NULL, CHANGE housing_dependent_info housing_dependent_info LONGTEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE fellowship_duty CHANGE additional_info_desc additional_info_desc LONGTEXT DEFAULT NULL');

        // Adds day breakdown activity types to base terms table
        $this->addSql("INSERT INTO base_term (name, created_at, updated_at, type) VALUES ('Work', '2016-04-28 17:07:39', '2016-04-28 17:07:39', '2')");
        $this->addSql("INSERT INTO base_term (name, created_at, updated_at, type) VALUES('Travel', '2016-04-28 17:07:39', '2016-04-28 17:07:39', '2')");
        $this->addSql("INSERT INTO base_term (name, created_at, updated_at, type) VALUES('Rest', '2016-04-28 17:07:39', '2016-04-28 17:07:39', '2')");
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE in_country_assignment_activity DROP FOREIGN KEY FK_8B0FDBDB1EBED7F3');
        $this->addSql('ALTER TABLE day_breakdown_activity DROP FOREIGN KEY FK_A1A860DC1EBED7F3');
        $this->addSql('DROP TABLE in_country_assignment');
        $this->addSql('DROP TABLE in_country_assignment_activity');
        $this->addSql('DROP TABLE day_breakdown_activity');
        $this->addSql('ALTER TABLE fellow_project CHANGE dependent_restrictions_desc dependent_restrictions_desc LONGTEXT DEFAULT NULL COLLATE utf8_unicode_ci, CHANGE cycle_season_desc cycle_season_desc LONGTEXT DEFAULT NULL COLLATE utf8_unicode_ci, CHANGE housing_desc housing_desc LONGTEXT DEFAULT NULL COLLATE utf8_unicode_ci, CHANGE housing_dependent_info housing_dependent_info LONGTEXT DEFAULT NULL COLLATE utf8_unicode_ci');
        $this->addSql('ALTER TABLE fellow_project_budget CHANGE post_funding_source post_funding_source LONGTEXT DEFAULT NULL COLLATE utf8_unicode_ci');
        $this->addSql('ALTER TABLE fellowship_duty CHANGE additional_info_desc additional_info_desc LONGTEXT DEFAULT NULL COLLATE utf8_unicode_ci');
        $this->addSql('ALTER TABLE fellowship_duty_activity CHANGE activity_desc activity_desc LONGTEXT DEFAULT NULL COLLATE utf8_unicode_ci');
        $this->addSql('ALTER TABLE location_host_info CHANGE host_city_description host_city_description LONGTEXT DEFAULT NULL COLLATE utf8_unicode_ci, CHANGE host_desc host_desc LONGTEXT DEFAULT NULL COLLATE utf8_unicode_ci, CHANGE additional_info_desc additional_info_desc LONGTEXT DEFAULT NULL COLLATE utf8_unicode_ci');
        $this->addSql('ALTER TABLE project_general_info CHANGE visa_requirements_desc visa_requirements_desc LONGTEXT DEFAULT NULL COLLATE utf8_unicode_ci, CHANGE medical_restrictions_desc medical_restrictions_desc LONGTEXT DEFAULT NULL COLLATE utf8_unicode_ci, CHANGE required_vaccinations_desc required_vaccinations_desc LONGTEXT DEFAULT NULL COLLATE utf8_unicode_ci, CHANGE age_restriction_desc age_restriction_desc LONGTEXT DEFAULT NULL COLLATE utf8_unicode_ci, CHANGE degree_requirements_desc degree_requirements_desc LONGTEXT DEFAULT NULL COLLATE utf8_unicode_ci, CHANGE security_info_desc security_info_desc LONGTEXT DEFAULT NULL COLLATE utf8_unicode_ci, CHANGE additional_requirements_desc additional_requirements_desc LONGTEXT DEFAULT NULL COLLATE utf8_unicode_ci, CHANGE post_mission_goals_desc post_mission_goals_desc LONGTEXT DEFAULT NULL COLLATE utf8_unicode_ci, CHANGE sustainability_by_locals_desc sustainability_by_locals_desc LONGTEXT DEFAULT NULL COLLATE utf8_unicode_ci');
        $this->addSql('ALTER TABLE specialist_project_phase DROP type, DROP start_date, DROP end_date, DROP date_length_desc');

        // Removes day breakdown activity types from base terms table
        $this->addSql('DELETE FROM base_term WHERE type=2');
    }
}
