CIED - Proposal system 
======================

A Symfony project created on January 30, 2016, 9:31 am. to manage fellow and specialist projects, review, financial data, reports 

# Installation

You could run this project standalone, but it's recommended to run it as a submodule of `Docker CIED Proposal` repo (https://bitbucket.org/inqbation/docker-cied-proposal)

Once you get the copy of the repo in your local, you must run:

```bash
$ composer install
```

Refer to the README file of Docker `CIED Proposal repo` for more information about the parameters configuration