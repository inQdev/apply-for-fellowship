<?php

namespace AppBundle\Controller;

use AppBundle\Entity\ContributionLivingExpense;
use AppBundle\Entity\ContributionLivingExpenseCostItem;
use AppBundle\Entity\ContributionOneTime;
use AppBundle\Entity\ContributionOneTimeCostItem;
use AppBundle\Entity\ContributionTransportation;
use AppBundle\Entity\ContributionTransportationCostItem;
use AppBundle\Entity\Country;
use AppBundle\Entity\LivingExpense;
use AppBundle\Entity\LivingExpenseCostItem;
use AppBundle\Entity\ProgramActivitiesAllowance;
use AppBundle\Entity\ProgramActivitiesAllowanceCostItem;
use AppBundle\Entity\ProjectGeneralInfo;
use AppBundle\Entity\SpecialistProject;
use AppBundle\Entity\SpecialistProjectPhase;
use AppBundle\Entity\SpecialistProjectPhaseBudget;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;

/**
 * SpecialistProjectBudgetController controller.
 *
 * @package AppBundle\Controller
 * @author  Juan Obando <juan.obando@inqbation.com>
 */
class SpecialistProjectBudgetController extends Controller
{
    /**
     * @Route(
     *     "/project/specialist/{id}/{phase}/budget/funded-by",
     *     requirements={"id": "\d+", "phase": "\d+"},
     *     name="specialist_project_phase_budget_funded_by",
     * )
     * @Method({"GET", "POST"})
     *
     * @param Request           $request
     * @param SpecialistProject $specialistProject
     * @param int               $phase
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function fundedByAction(
      Request $request,
      SpecialistProject $specialistProject,
      $phase
    ) {
        $em = $this->getDoctrine()->getManager();

        /** @var SpecialistProjectPhase $specialistProjectPhase */
        $specialistProjectPhase = $specialistProject->getPhases()->get($phase);

        /** @var ProjectGeneralInfo $projectGeneralInfo */
        $projectGeneralInfo = $specialistProjectPhase->getProjectGeneralInfo();

        /** @var SpecialistProjectPhaseBudget $phaseBudget */
        $phaseBudget = $specialistProjectPhase->getBudget();

        if (null === $phaseBudget) {
            $currentFundedBy = false;

            $contributionLivingCostItems = $em->getRepository('AppBundle:ContributionLivingCostItem')->findAll();

            // Create a budget for the phase.
            $phaseBudget = new SpecialistProjectPhaseBudget(
              $specialistProjectPhase
            );

            $em->persist($phaseBudget);

            // For every country associated in to the phase, a Living Expenses
            // budget must be created.
            /** @var Country $country */
            foreach ($projectGeneralInfo->getCountries() as $country) {
                $contributionLivingExpense = new ContributionLivingExpense(
                  $phaseBudget,
                  $country
                );

                $em->persist($contributionLivingExpense);

                foreach ($contributionLivingCostItems as $contributionLivingCostItem) {
                    $contributionLivingExpenseCostItem = new ContributionLivingExpenseCostItem(
                      $contributionLivingExpense
                    );
                    $contributionLivingExpenseCostItem->setCostItem(
                      $contributionLivingCostItem
                    );

                    $em->persist($contributionLivingExpenseCostItem);
                }
            }

            // Create Transportation contribution for the phase.
            $contributionTransportation = new ContributionTransportation(
              $phaseBudget
            );

            $em->persist($contributionTransportation);

            // Creates Living Expenses basic records, per city/country (InCountryAssignment)
            if ($specialistProjectPhase->getType() == 'in-country' || $specialistProjectPhase->getType() == 'mixed') {
                $inCountryAssignments = $specialistProjectPhase->getInCountryAssignments();
                foreach ($inCountryAssignments as $assignment) {
                    $livingExpense = new LivingExpense();
                    $livingExpense->setInCountryAssignment($assignment);
                    $livingExpense->setPhaseBudget($phaseBudget);
                    $phaseBudget->addLivingExpenseEstimate($livingExpense);

                    $em->persist($livingExpense);
                }
            }

            // Create Settling-in contribution for the phase.
            $contributionOneTime = new ContributionOneTime($phaseBudget);

            $em->persist($contributionOneTime);

            // Create PAA for the phase.
            $paa = new ProgramActivitiesAllowance($phaseBudget);

            $em->persist($paa);

            $em->flush();
        }
        else {
            // Cache this to compare when form is submitted and the Funded By is
            // changed.
            $currentFundedBy = $phaseBudget->getFundedBy();
        }

        $form = $this->createForm(
          'AppBundle\Form\Specialist\FundedByType',
          $phaseBudget
        );
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // If project budget Funded By changes, the totals for the records
            // must be re-calculated.
            if ($currentFundedBy
              && ($currentFundedBy !== $phaseBudget->getFundedBy())
            ) {
                // TODO: this should be handle in an Event Listerner
                /** @var ContributionLivingExpense $contributionLivingExpense */
                foreach ($phaseBudget->getContributionLivingExpenses() as $contributionLivingExpense) {
                    /** @var ContributionLivingExpenseCostItem $contributionLivingExpenseCostItem */
                    foreach ($contributionLivingExpense->getContributionLivingExpenseCostItems() as $contributionLivingExpenseCostItem) {
                        $contributionLivingExpenseCostItem->calculateTotals();
                    }
                }

                // TODO: this should be handle in an Event Listerner
                /** @var ContributionTransportationCostItem $contributionTransportationCostItem */
                foreach ($phaseBudget->getContributionTransportation()->getContributionTransportationCostItems() as $contributionTransportationCostItem) {
                    $contributionTransportationCostItem->calculateTotals();
                }

                // TODO: this should be handle in an Event Listerner
                /** @var ContributionOneTimeCostItem $contributionOneTimeCostItems */
                foreach ($phaseBudget->getContributionOneTime()->getContributionOneTimeCostItems() as $contributionOneTimeCostItem) {
                    $contributionOneTimeCostItem->calculateTotals();
                }

                // TODO: this should be handle in an Event Listerner
                /** @var ProgramActivitiesAllowanceCostItem $paaCostItem */
                foreach ($phaseBudget->getProgramActivitiesAllowance() as $paaCostItem) {
                    $paaCostItem->calculateTotals();
                }
            }

            $em->persist($phaseBudget);
            $em->flush();

            $nextAction = 'specialist_project_phase_admin_panel';

            if ($form->get('next')->isClicked()) {
                $nextAction = 'specialist_project_phase_budget';
            }

            return $this->redirectToRoute(
              $nextAction,
              [
                'id'    => $specialistProject->getId(),
                'phase' => $phase,
              ]
            );
        }

        return $this->render(
          'project/specialist/wizard/budget/funded_by.html.twig',
          [
            'specialistProject'      => $specialistProject,
            'specialistProjectPhase' => $specialistProjectPhase,
            'form'                   => $form->createView(),
            'goBack'                 => null,
          ]
        );
    }

    /**
     * @Route(
     *     "/project/specialist/{id}/{phase}/budget",
     *     requirements={"id": "\d+", "phase": "\d+"},
     *     name="specialist_project_phase_budget",
     * )
     * @Method({"GET"})
     *
     * @param SpecialistProject $specialistProject
     * @param int               $phase
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(
      SpecialistProject $specialistProject,
      $phase
    ) {
        /** @var SpecialistProjectPhase $specialistProjectPhase */
        $specialistProjectPhase = $specialistProject->getPhases()->get($phase);

        /** @var SpecialistProjectPhaseBudget $phaseBudget */
        $phaseBudget = $specialistProjectPhase->getBudget();

        return $this->render(
          'project/specialist/wizard/budget/'. strtolower($phaseBudget->getFundedBy()) .'/index.html.twig',
          [
            'specialistProject'      => $specialistProject,
            'specialistProjectPhase' => $specialistProjectPhase,
            'phaseBudget'            => $phaseBudget,
            'phase'                  => $phase,
            'goBack'                 => null,
          ]
        );
    }

    /**
     * @Route(
     *     "/project/specialist/{id}/{phase}/budget/contribution/living-expenses/{cleid}",
     *     requirements={"id": "\d+", "phase": "\d+", "cleid": "\d+"},
     *     name="specialist_project_phase_budget_living_expenses",
     * )
     * @Method({"GET", "POST"})
     *
     * @ParamConverter(
     *     "contributionLivingExpense",
     *     class="AppBundle:ContributionLivingExpense",
     *     options={"mapping": {"cleid": "id"}}
     * )
     *
     * @param Request                   $request
     * @param SpecialistProject         $specialistProject
     * @param ContributionLivingExpense $contributionLivingExpense
     * @param int                       $phase
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function contributionLivingExpensesAction(
      Request $request,
      SpecialistProject $specialistProject,
      ContributionLivingExpense $contributionLivingExpense,
      $phase
    ) {
        /** @var SpecialistProjectPhase $specialistProjectPhase */
        $specialistProjectPhase = $specialistProject->getPhases()->get($phase);

        /** @var SpecialistProjectPhaseBudget $phaseBudget */
        $phaseBudget = $specialistProjectPhase->getBudget();

        $form = $this->createForm(
          'AppBundle\Form\Specialist\ContributionLivingExpenseType',
          $contributionLivingExpense
        );
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            // TODO: The totals calculation shouldn't be handle in here, but in an Event Listener (prePersist, maybe?)
            /** @var ContributionLivingExpense $livingExpenseCostItem */
            foreach ($contributionLivingExpense->getContributionLivingExpenseCostItems() as $contributionlivingExpenseCostItem) {
                $contributionlivingExpenseCostItem->calculateTotals();
            }

            $em->persist($contributionLivingExpense);
            $em->flush();

            return $this->redirectToRoute(
              'specialist_project_phase_budget',
              [
                'id'    => $specialistProject->getId(),
                'phase' => $phase,
              ]
            );
        }

        return $this->render(
          'project/specialist/wizard/budget/'. strtolower($phaseBudget->getFundedBy()) .'/contribution_living_expenses.html.twig',
          [
            'specialistProject'         => $specialistProject,
            'specialistProjectPhase'    => $specialistProjectPhase,
            'phaseBudget'               => $phaseBudget,
            'contributionLivingExpense' => $contributionLivingExpense,
            'phase'                     => $phase,
            'form'                      => $form->createView(),
            'goBack'                    => null,
          ]
        );
    }

    /**
     * @Route(
     *     "/project/specialist/{id}/{phase}/budget/contribution/transportation",
     *     requirements={"id": "\d+", "phase": "\d+"},
     *     name="specialist_project_phase_budget_contribution_transportation",
     * )
     * @Method({"GET", "POST"})
     *
     * @param Request           $request
     * @param SpecialistProject $specialistProject
     * @param int               $phase
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function contributionTransportationAction(
      Request $request,
      SpecialistProject $specialistProject,
      $phase
    ) {
        $em = $this->getDoctrine()->getManager();

        /** @var SpecialistProjectPhase $specialistProjectPhase */
        $specialistProjectPhase = $specialistProject->getPhases()->get($phase);

        /** @var SpecialistProjectPhaseBudget $phaseBudget */
        $phaseBudget = $specialistProjectPhase->getBudget();

        /** @var ContributionTransportation $contributionTransportation */
        $contributionTransportation = $phaseBudget->getContributionTransportation();

        // One cost item should be added to the collection so it can be
        // rendered in the form.
        if ($contributionTransportation->getContributionTransportationCostItems()->isEmpty()) {
            $contributionTransportationCostItem = new ContributionTransportationCostItem(
              $contributionTransportation
            );

            $contributionTransportation->addContributionTransportationCostItem(
              $contributionTransportationCostItem
            );

            $em->persist($contributionTransportation);
            $em->flush();
        }

        $form = $this->createForm(
          'AppBundle\Form\Specialist\ContributionTransportationType',
          $contributionTransportation
        );
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // Get the ID of the item to delete.
            $costItemToDeleteId = $form->get('costItemToDeleteId')->getData();

            if ($costItemToDeleteId) {
                /** @var ContributionTransportationCostItem $costItem */
                $costItem = $em->getRepository('AppBundle:ContributionTransportationCostItem')->find($costItemToDeleteId);

                if ($costItem) {
                    $contributionTransportation->removeContributionTransportationCostItem(
                      $costItem
                    );

                    $em->remove($costItem);
                }
            }

            // TODO: The totals calculation shouldn't be handle in here, but in an Event Listener (prePersist, maybe?)
            /** @var ContributionTransportationCostItem $contributionTransportationCostItem */
            foreach ($contributionTransportation->getContributionTransportationCostItems() as $contributionTransportationCostItem) {
                // if no description is entered by the user for the added cost
                // item, `Untitled` will be use as default description.
                if (null === $contributionTransportationCostItem->getCostItem()->getDescription()) {
                    $contributionTransportationCostItem->getCostItem()->setDescription(
                      'Untitled'
                    );
                }

                $contributionTransportationCostItem->calculateTotals();
            }

            $nextAction = 'specialist_project_phase_budget';

            if ($form->get('additional')->isClicked()) {
                $contributionTransportationCostItem = new ContributionTransportationCostItem(
                  $contributionTransportation
                );

                $contributionTransportation->addContributionTransportationCostItem(
                  $contributionTransportationCostItem
                );

                $nextAction = 'specialist_project_phase_budget_contribution_transportation';
            }

            $em->persist($contributionTransportation);
            $em->flush();

            return $this->redirectToRoute(
              $nextAction,
              [
                'id'    => $specialistProject->getId(),
                'phase' => $phase,
              ]
            );
        }

        return $this->render(
          'project/specialist/wizard/budget/'. strtolower($phaseBudget->getFundedBy()) .'/contribution_transportation.html.twig',
          [
            'specialistProject'          => $specialistProject,
            'specialistProjectPhase'     => $specialistProjectPhase,
            'phaseBudget'                => $phaseBudget,
            'contributionTransportation' => $contributionTransportation,
            'phase'                      => $phase,
            'form'                       => $form->createView(),
            'goBack'                     => null,
          ]
        );
    }

    /**
     * @Route(
     *     "/project/specialist/{id}/{phase}/budget/living-expenses/{leid}",
     *     requirements={"id": "\d+", "phase": "\d+", "cleid": "\d+"},
     *     name="specialist_project_phase_budget_living_expense_estimates",
     * )
     * @Method({"GET", "POST"})
     *
     * @ParamConverter("livingExpense", class="AppBundle:LivingExpense", options={"mapping": {"leid": "id"}}
     * )
     *
     * @param Request                   $request
     * @param SpecialistProject         $specialistProject
     * @param LivingExpense             $livingExpense
     * @param int                       $phase
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function livingExpensesAction(
        Request $request,
        SpecialistProject $specialistProject,
        LivingExpense $livingExpense,
        $phase
    ) {
        /** @var SpecialistProjectPhase $specialistProjectPhase */
        $specialistProjectPhase = $specialistProject->getPhases()->get($phase);

        /** @var SpecialistProjectPhaseBudget $phaseBudget */
        $phaseBudget = $specialistProjectPhase->getBudget();

        // If no cost items found, create and associate them
        // Take into account days per city from itinerary
        if (!$livingExpense->getLivingExpenseCostItems()->count()) {
            $em = $this->getDoctrine()->getManager();

            $days = $livingExpense->getInCountryAssignment()->getDayBreakdownActivities()->count();
            $contributionLivingCostItems = $em->getRepository('AppBundle:ContributionLivingCostItem')->findAll();

            foreach ($contributionLivingCostItems as $costItem) {
                $livingExpenseCostItem = new LivingExpenseCostItem();
                $livingExpenseCostItem->setNumberOfDays($days);
                $livingExpenseCostItem->setLivingExpense($livingExpense);
                $livingExpenseCostItem->setCostItem($costItem);
                $livingExpense->addLivingExpenseCostItem($livingExpenseCostItem);
            }
        }

        $form = $this->createForm(
            'AppBundle\Form\Specialist\LivingExpenseType',
            $livingExpense
        );

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            // TODO: The totals calculation shouldn't be handle in here, but in an Event Listener (prePersist, maybe?)
            /** @var ContributionLivingExpense $livingExpenseCostItem */
            foreach ($livingExpense->getLivingExpenseCostItems() as $livingExpenseCostItem) {
                if ($livingExpenseCostItem->getCostPerDay() !== null) {
                    $livingExpenseCostItem->calculateTotals();
                }
            }

            $em->persist($livingExpense);
            $em->flush();

            return $this->redirectToRoute(
                'specialist_project_phase_budget',
                [
                    'id'    => $specialistProject->getId(),
                    'phase' => $phase,
                ]
            );
        }

        return $this->render(
            'project/specialist/wizard/budget/living_expenses.html.twig',
            [
                'specialistProject'         => $specialistProject,
                'specialistProjectPhase'    => $specialistProjectPhase,
                'phaseBudget'               => $phaseBudget,
                'livingExpense'             => $livingExpense,
                'phase'                     => $phase,
                'form'                      => $form->createView(),
                'goBack'                    => null,
            ]
        );
    }

    /**
     * @Route(
     *     "/project/specialist/{id}/{phase}/budget/contribution/one-time",
     *     requirements={"id": "\d+", "phase": "\d+"},
     *     name="specialist_project_phase_budget_contribution_one_time",
     * )
     * @Method({"GET", "POST"})
     *
     * @param Request           $request
     * @param SpecialistProject $specialistProject
     * @param int               $phase
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function contributionOneTimeAction(
      Request $request,
      SpecialistProject $specialistProject,
      $phase
    ) {
        $em = $this->getDoctrine()->getManager();

        /** @var SpecialistProjectPhase $specialistProjectPhase */
        $specialistProjectPhase = $specialistProject->getPhases()->get($phase);

        /** @var SpecialistProjectPhaseBudget $phaseBudget */
        $phaseBudget = $specialistProjectPhase->getBudget();

        /** @var ContributionOneTime $contributionOneTime */
        $contributionOneTime = $phaseBudget->getContributionOneTime();

        if ($contributionOneTime->getContributionOneTimeCostItems()->isEmpty()) {
            $contributionOneTimeCostItem = new ContributionOneTimeCostItem(
              $contributionOneTime
            );

            $contributionOneTime->addContributionOneTimeCostItem(
              $contributionOneTimeCostItem
            );

            $em->persist($contributionOneTime);
            $em->flush();
        }

        $form = $this->createForm(
          'AppBundle\Form\Specialist\ContributionOneTimeType',
          $contributionOneTime
        );
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // Get the ID of the item to delete.
            $costItemToDeleteId = $form->get('costItemToDeleteId')->getData();

            if ($costItemToDeleteId) {
                /** @var ContributionOneTimeCostItem $costItem */
                $costItem = $em->getRepository('AppBundle:ContributionOneTimeCostItem')->find($costItemToDeleteId);

                if ($costItem) {
                    $contributionOneTime->removeContributionOneTimeCostItem(
                      $costItem
                    );

                    $em->remove($costItem);
                }
            }

            // TODO: The totals calculation shouldn't be handle in here, but in an Event Listener (prePersist, maybe?)
            /** @var ContributionOneTimeCostItem $contributionOneTimeCostItem */
            foreach ($contributionOneTime->getContributionOneTimeCostItems() as $contributionOneTimeCostItem) {
                // if no description is entered by the user to the added cost
                // item, `Untitled` will be use as default description.
                if (null === $contributionOneTimeCostItem->getCostItem()->getDescription()) {
                    $contributionOneTimeCostItem->getCostItem()->setDescription(
                      'Untitled'
                    );
                }

                $contributionOneTimeCostItem->calculateTotals();
            }

            $nextAction = 'specialist_project_phase_budget';

            if ($form->get('additional')->isClicked()) {
                $contributionOneTimeCostItem = new ContributionOneTimeCostItem(
                  $contributionOneTime
                );

                $contributionOneTime->addContributionOneTimeCostItem(
                  $contributionOneTimeCostItem
                );

                $nextAction = 'specialist_project_phase_budget_contribution_one_time';
            }

            $em->persist($contributionOneTime);
            $em->flush();

            return $this->redirectToRoute(
              $nextAction,
              [
                'id'    => $specialistProject->getId(),
                'phase' => $phase,
              ]
            );
        }

        return $this->render(
            'project/specialist/wizard/budget/'. strtolower($phaseBudget->getFundedBy()) .'/contribution_one_time.html.twig',
            [
                'specialistProject'      => $specialistProject,
                'specialistProjectPhase' => $specialistProjectPhase,
                'phaseBudget'            => $phaseBudget,
                'contributionOneTime'    => $contributionOneTime,
                'phase'                  => $phase,
                'form'                   => $form->createView(),
                'goBack'                 => null,
            ]
        );
    }

    /**
     * @Route(
     *     "/project/specialist/{id}/{phase}/budget/paa",
     *     requirements={"id": "\d+", "phase": "\d+"},
     *     name="specialist_project_phase_budget_paa",
     * )
     * @Method({"GET", "POST"})
     *
     * @param Request           $request
     * @param SpecialistProject $specialistProject
     * @param int               $phase
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function programActivitiesAllowanceAction(
      Request $request,
      SpecialistProject $specialistProject,
      $phase
    ) {
        /** @var SpecialistProjectPhase $specialistProjectPhase */
        $specialistProjectPhase = $specialistProject->getPhases()->get($phase);

        /** @var SpecialistProjectPhaseBudget $phaseBudget */
        $phaseBudget = $specialistProjectPhase->getBudget();

        /** @var ContributionOneTime $contributionOneTime */
        $paa = $phaseBudget->getProgramActivitiesAllowance();

        if ($paa->getProgramActivitiesAllowanceCostItems()->isEmpty()) {
            $paaCostItem = new ProgramActivitiesAllowanceCostItem($paa);

            $paa->addProgramActivitiesAllowanceCostItem($paaCostItem);
        }

        $form = $this->createForm(
          'AppBundle\Form\Specialist\ProgramActivitiesAllowanceType',
          $paa
        );
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            // TODO: The totals calculation shouldn't be handle in here, but in an Event Listener (prePersist, maybe?)
            /** @var ProgramActivitiesAllowanceCostItem $paaCostItem */
            foreach ($paa->getProgramActivitiesAllowanceCostItems() as $paaCostItem) {
                // if no description is entered by the user to the added cost
                // item, `Untitled` will be use as default description.
                if (null === $paaCostItem->getCostItem()->getDescription()) {
                    $paaCostItem->getCostItem()->setDescription('Untitled');
                }

                $paaCostItem->calculateTotals();
            }

            $em->persist($paaCostItem);
            $em->flush();

            return $this->redirectToRoute(
              'specialist_project_phase_budget',
              [
                'id'    => $specialistProject->getId(),
                'phase' => $phase,
              ]
            );
        }

        return $this->render(
          'project/specialist/wizard/budget/'. strtolower($phaseBudget->getFundedBy()) .'/paa.html.twig',
          [
            'specialistProject'      => $specialistProject,
            'specialistProjectPhase' => $specialistProjectPhase,
            'phaseBudget'            => $phaseBudget,
            'ppa'                    => $paa,
            'phase'                  => $phase,
            'form'                   => $form->createView(),
            'goBack'                 => null,
          ]
        );
    }
}
