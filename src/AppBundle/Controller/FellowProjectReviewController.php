<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Country;
use AppBundle\Entity\Fellow\CycleFellow;
use AppBundle\Entity\FellowProject;
use AppBundle\Entity\LocationHostInfo;
use AppBundle\Entity\ProjectGeneralInfo;
use AppBundle\Entity\ProjectOutcome;
use AppBundle\Entity\ProjectReview;
use AppBundle\Entity\ProjectReviewComment;
use AppBundle\Entity\ProjectReviewStatus;
use AppBundle\Entity\Region;
use AppBundle\Entity\ReloLocation;
use AppBundle\Entity\User;
use AppBundle\Form\Type\CustomTextareaType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

/**
 * Fellow Project Review Controller
 *
 * @package AppBundle\Controller
 * @author  Juan Obando <juan.obando@inqbation.com>
 */
class FellowProjectReviewController extends Controller implements RoleSwitcherInterface
{
	/**
	 * @Route("/project/fellow/{id}/submit", name="fellow_project_submit")
	 * @Method("POST")
	 *
	 * @param Request $request
	 * @param FellowProject $fellowProject
	 *
	 * @return \Symfony\Component\HttpFoundation\RedirectResponse
	 */
	public function submitFellowProjectAction(
		Request $request,
		FellowProject $fellowProject
	)
	{
		$authenticatedUser = $this->getUser();

		$form = $this->createForm('AppBundle\Form\SubmitFellowProposalType');
		$form->handleRequest($request);

		// TODO: this needs to be refactor when a emails/notificiatons module is implemented.
		$sendEmails = $this->getParameter('send_emails');

		if ($form->isSubmitted() && $form->isValid()) {
			$em = $this->getDoctrine()->getManager();

			$projectGeneralInfo = $fellowProject->getProjectGeneralInfo();
			$projectGeneralInfo->setSubmissionStatus(
				ProjectGeneralInfo::PROJECT_SUBMITTED
			);

			// Checks the role of the user submitting the proposal, and assign
			// the right status
			if ($this->isGranted('ROLE_RELO')) {
				$reviewStatus = ProjectReviewStatus::UNDER_RPO_REVIEW;
				$submittedRole = 'RELO';
				$notificationAction = 'Submitted to RPO review';
			} else {
				$reviewStatus = ProjectReviewStatus::UNDER_RELO_REVIEW;
				$notificationAction = 'Submitted to RELO review';
				if ($this->isGranted('ROLE_RPO')) {
					$submittedRole = 'RPO';
				} elseif ($this->isGranted('ROLE_STATE_DEPARTMENT')) {
					$submittedRole = 'ECA';
				} elseif ($this->isGranted('ROLE_ADMIN') || $this->isGranted('ROLE_SUPER_ADMIN')) {
					$submittedRole = 'Admin';
				} else {
					$submittedRole = 'Embassy';
				}
			}
			$projectGeneralInfo->setSubmittedByRole($submittedRole);

			$projectGeneralInfo->setProjectReviewStatus(
				$em->getRepository('AppBundle:ProjectReviewStatus')->find($reviewStatus)
			);

			$projectGeneralInfo->setSubmittedBy($authenticatedUser); // Saves the user that submits proposal

			$em->persist($projectGeneralInfo);

			$fellowProjectReview = new ProjectReview();
			// TODO: should relation between FellowProject and ProjectReview be 1:1?
			$fellowProjectReview->addFellowProject($fellowProject);

			// TODO: this needs to be refactor when a emails/notifications module is implemented; all this should happen in it.

//			if ($sendEmails) {
//				$message = \Swift_Message::newInstance()
//					->setSubject('Fellow proposal submitted for your review')
//					->setFrom('proposal-no-reply@elprograms.org')
//					->setTo('support@inqbation.com')
//					->setCc('virginia.alvarez@inqbation.com')
//					->setBody(
//						$this->renderView(
//							'email/proposalreview/relo_review.html.twig', [
//								'fellowProject' => $fellowProject,
//								'recipients' => $recipients,
//								'sendMessage' => $this->getParameter('send_emails'),
//						]),
//						'text/html'
//					);
//
//				$this->get('mailer')->send($message);
//			} else {
//				$emailTemplate = $this->renderView(
//					'email/proposal_submission.html.twig',
//					['fellowProject' => $fellowProject]
//				);
//
//				$this->addFlash('debug-email', $emailTemplate);
//			}

			$em->persist($fellowProjectReview);
			$em->flush();

			$this->sendNotifications($fellowProject, $authenticatedUser, $notificationAction, $submittedRole);

			// Update the Solr index
			// TODO: should be this placed in a EventListener? Service?
			$this->get('solr.client')->updateDocument($fellowProject);

			$this->addFlash(
				'success',
				'The Proposal has been submitted successfully.'
			);
		}

		return $this->redirectToRoute(
			'project_fellow_show',
			['id' => $fellowProject->getId()]
		);
	}

	/**
	 * @Route("/project/fellow/{id}/review", name="fellow_project_review")
	 * @Method({"POST"})
	 *
	 * @param FellowProject $fellowProject
	 *
	 * @return \Symfony\Component\HttpFoundation\Response
	 */
	public function reviewAction(FellowProject $fellowProject, Request $request)
	{
		$authenticatedUser = $this->getUser();
		$reviewComment = new ProjectReviewComment();
		$reviewComment->setCommentBy($authenticatedUser);
		$reviewComment->setProjectReview($fellowProject->getFellowProjectReviews()->first());

		$reviewForm = $this->createReviewCommentForm($reviewComment, $fellowProject->getProjectGeneralInfo());
//		$translator = $this->get('translator');

		if ($reviewForm) {
			$reviewForm->handleRequest($request);

			if ($reviewForm->isSubmitted() && $reviewForm->isValid()) {
				$em = $this->getDoctrine()->getManager();

				$projectGeneralInfo = $fellowProject->getProjectGeneralInfo();

				// Changes things depending on the user role
				$proposalStatus = $role = $commentAction = '';
				$outcome = null;
				$addComment = true;

				if ($this->isGranted('ROLE_EMBASSY')) {
					$role = 'Embassy';
				} else if ($this->isGranted('ROLE_RELO')) {
					$role = 'RELO';
				} else if ($this->isGranted('ROLE_RPO')) {
					$role = 'RPO';
				} else if ($this->isGranted('ROLE_STATE_DEPARTMENT')) {
					$role = 'ECA';
				} else if ($this->isGranted('ROLE_ADMIN') || $this->isGranted('ROLE_SUPER_ADMIN')) {

					// adds a note ONLY if the Admin fills in the comment field
					$role = 'Admin';
//					if ($reviewComment->getComment()) {
					if (!$reviewForm->get('save')->isClicked()) {
						$commentAction = 'Admin Comment'; // This may change below depending on button clicked
					} else {
						$addComment = false;
					}

					// Adjusts values on proposal
					// Outcome
					if ($reviewForm->get('outcome')->getViewData() !== '') {
						$outcome = $reviewForm->get('outcome')->getViewData();
					} else {
						$projectGeneralInfo->setProjectOutcome(null);
					}

					// Review Status
					if ($reviewForm->get('reviewStatus')->getViewData() !== '') {
						$proposalStatus = $reviewForm->get('reviewStatus')->getViewData();
					}

					// Cycle
					$projectGeneralInfo->setCycle(
						$em->getRepository('AppBundle:Fellow\CycleFellow')->find($reviewForm->get('cycle')->getViewData())
					);

					// Submission Status
					$projectGeneralInfo->setSubmissionStatus($reviewForm->get('submissionStatus')->getViewData());

				}

				$currentProposalStatus = $projectGeneralInfo->getProjectReviewStatus()->getId();

				switch ($currentProposalStatus) {

					case ProjectReviewStatus::POST_REQUIRE_ALTERNATE_FUNDING:
						// Submission handler for setting alternate funding.
						if ($reviewForm->has('accept') && $reviewForm->get('accept')->isClicked()) {
							$proposalStatus = ProjectReviewStatus::UNDER_ECA_REVIEW;
							$commentAction = 'Funding changed';
						} // Submission handler when Embassy declines alternate funding.
						elseif ($reviewForm->has('decline') && $reviewForm->get('decline')->isClicked()) {
							$proposalStatus = ProjectReviewStatus::REVIEW_COMPLETE;
							$commentAction = 'Alternate Funding Unavailable';

							// If Embassy declines to modify the budget `Funded by`
							// to Post, it means the proposal will be withdrawn.
							$outcome = ProjectOutcome::ALTERNATE_FUNDING_UNAVAILABLE;
						}
						break;

					case ProjectReviewStatus::UNDER_POST_RE_REVIEW:

						// Submission handler for re-review proposal.
						if ($reviewForm->has('submit') && $reviewForm->get('submit')->isClicked()) {
							$proposalStatus = ProjectReviewStatus::UNDER_RELO_REVIEW;
//							$commentAction = 'Submitted to RELO review';
							$commentAction = 'Re-submitted to RELO review';
						}
						break;

					case ProjectReviewStatus::UNDER_RELO_REVIEW:

						if ($reviewForm->get('approve')->isClicked()) {
							$proposalStatus = ProjectReviewStatus::UNDER_RPO_REVIEW;
							$commentAction = 'Submitted to RPO review';
						} else if ($reviewForm->get('return')->isClicked()) {
							$proposalStatus = ProjectReviewStatus::UNDER_POST_RE_REVIEW;
							$commentAction = 'Returned to Post for revision';
						} else if ($reviewForm->get('reject')->isClicked()) {
							$proposalStatus = ProjectReviewStatus::REVIEW_COMPLETE;
							$commentAction = 'Rejected by RELO';
							$outcome = ProjectOutcome::REJECTED_BY_RELO;
						}
						break;

					case ProjectReviewStatus::UNDER_RPO_REVIEW:

						if ($reviewForm->get('approve')->isClicked()) {
							$proposalStatus = ProjectReviewStatus::UNDER_ECA_REVIEW;
							$commentAction = 'Submitted to ECA review';
						} else if ($reviewForm->get('return_relo')->isClicked()) {
							$proposalStatus = ProjectReviewStatus::UNDER_RELO_REVIEW;
							$commentAction = 'Returned to RELO for revision';
						} else if ($reviewForm->get('return_post')->isClicked()) {
							$proposalStatus = ProjectReviewStatus::UNDER_POST_RE_REVIEW;
							$commentAction = 'Returned to Post for revision';
						} else if ($reviewForm->get('reject')->isClicked()) {
							$proposalStatus = ProjectReviewStatus::REVIEW_COMPLETE;
							$commentAction = 'Rejected by ECA';
							$outcome = ProjectOutcome::REJECTED_BY_ECA;
						}
						break;

					case ProjectReviewStatus::UNDER_ECA_REVIEW:

						if ($reviewForm->get('approve')->isClicked()) {
							$proposalStatus = ProjectReviewStatus::REVIEW_COMPLETE;
							$commentAction = 'Approved';
							$outcome = ProjectOutcome::APPROVED_BY_ECA;
						} else if ($reviewForm->get('return')->isClicked()) {
							$proposalStatus = ProjectReviewStatus::UNDER_RPO_REVIEW;
							$commentAction = 'Returned to RPO for revision';
						} else if ($reviewForm->get('alternate_funding')->isClicked()) {
							$proposalStatus = ProjectReviewStatus::POST_REQUIRE_ALTERNATE_FUNDING;
							$commentAction = 'Alternate funding requested';
						} else if ($reviewForm->get('reject')->isClicked()) {
							$proposalStatus = ProjectReviewStatus::REVIEW_COMPLETE;
							$commentAction = 'Rejected by ECA';
							$outcome = ProjectOutcome::REJECTED_BY_ECA;
						}
						break;
				}

				$projectGeneralInfo->setProjectReviewStatus(
					$em->getRepository('AppBundle:ProjectReviewStatus')->find($proposalStatus)
				);

				if ($outcome !== null) {
					$projectGeneralInfo->setProjectOutcome(
						$em->getRepository('AppBundle:ProjectOutcome')->find($outcome)
					);
				}

				// Saves changes to the DB
				$em->persist($projectGeneralInfo);

				// Notifications (if simple Admin comment, avoid sending email message)
				if ($commentAction && $commentAction != 'Admin Comment') {
					$this->sendNotifications($fellowProject, $authenticatedUser, $commentAction, $role);
				}

				// Sets values for comment and save
				if ($addComment) {
					$reviewComment->setCommentBy($authenticatedUser);
					$reviewComment->setActionTaken($commentAction);
					$reviewComment->setCommentByRole($role);
					$em->persist($reviewComment);
				}
				$em->flush();

				// Update the Solr index
				// TODO: should be this placed in a EventListener? Service?
				$this->get('solr.client')->updateDocument($fellowProject);

				// TODO: The JSON generation should be placed somewhere else (service manager, maybe?)
				// If outcome is `Approved by ECA`, the project should appear
				// in the list of Rosters project list (Application System);
				// the JSON with projects should be updated.
				if (ProjectOutcome::APPROVED_BY_ECA === $outcome) {
					// The project will be added to the JSON for the cycle it
					// has associated.
					/** @var CycleFellow $fellowCycle */
					$fellowCycle = $fellowProject->getProjectGeneralInfo()->getCycle();

					// Projects for the cycle (2016 - 2017) were added manually.
					// This validation will avoid the Proposal System to
					// overwrite the JSON file with those projects.
					if ((null !== $fellowCycle->getTid()) && ($fellowCycle->getTid() > 5147)) {
						$fs = new Filesystem();

						$rosterPath = $this->getParameter(
							'application_system_roster_dir'
						);

						$fellowProjects = $em->getRepository('AppBundle:FellowProject')->findAllApprovedInCycle($fellowCycle);

						// Check if the path where the JSON should be placed exits.
						if ($fs->exists($rosterPath)) {
							$approvedFellowProjects = [];

							/** @var FellowProject $approvedFellowProject */
							foreach ($fellowProjects as $approvedFellowProject) {
								/** @var ProjectGeneralInfo $projectGeneralInfo */
								$projectGeneralInfo = $approvedFellowProject->getProjectGeneralInfo();

								/** @var Country $country */
								$country = $projectGeneralInfo->getCountries()->last();
								$countryName = $country->getName();

								/** @var LocationHostInfo $locationHostInfo */
								$locationHostInfo = $approvedFellowProject->getLocationHostInfo();

								// This validation will avoid adding the
								// Country-wide project more than once.
								if (!array_key_exists($countryName, $approvedFellowProjects)) {
									$approvedFellowProjects[$countryName]['name'] = $countryName;
									$approvedFellowProjects[$countryName]['ISO_code'] = $country->getIso2Code();

									// Every country should have a Country-wide
									// project.
									$approvedFellowProjects[$countryName]['projects'][] = [
										'id' => 100,
										'name' => 'Country-wide',
									];
								}

								// Make sure the Host instance has set the `name`
								// property.
								$host = (null !== $locationHostInfo->getHost())
									? $locationHostInfo->getHost()->getName()
									: '';

								$referenceNumber = (null !== $projectGeneralInfo->getReferenceNumber())
									? $projectGeneralInfo->getReferenceNumber()
									: '';

								$relo = '';
								$reloEmail = '';

								// Check if there are RELO users for RELO
								// Location associated to the project.
								if (!$projectGeneralInfo->getReloLocation()->getRelos()->isEmpty()) {
									// There could be more than one user
									// associated to the RELO Location; the
									// first user will be the one included in
									// the JSON file.
									/** @var User $reloUser */
									$reloUser = $projectGeneralInfo->getReloLocation()->getRelos()->first();

									$relo = trim($reloUser->getName());
									$reloEmail = trim($reloUser->getEmail());
								}

								$approvedFellowProjects[$countryName]['projects'][] = [
									'id' => $approvedFellowProject->getId(),
									'name' => trim("$host ($referenceNumber)"),
									'region' => $projectGeneralInfo->getRegion()->getName(),
									'relo' => $relo,
									'relo_email' => $reloEmail,
								];
							}

							// The array cannot be an Associative one: Country names
							// are not expected as keys in the JSON file.
							$approvedFellowProjects = array_values($approvedFellowProjects);

							$jsonFileName = "cied_roster_cycle_{$fellowCycle->getTid()}.json";

							$jsonFilePath = $rosterPath . DIRECTORY_SEPARATOR . $jsonFileName;

							// The JSON file is going to be completely replaced
							// with the content just generated.
							if ($fs->exists($jsonFilePath)) {
								$fs->remove($jsonFilePath);
							}

							$fs->dumpFile(
								$jsonFilePath,
								json_encode(
									['countries' => $approvedFellowProjects]
								)
							);
						}
					}
				}

				return $this->redirectToRoute(
					'project_fellow_show',
					['id' => $fellowProject->getId()]
				);
			}
		}

		// Get Budget Summary info to show in page
		$fellowProjectBudgetTotals = null;
		$monthlyCostItems = $oneTimeCostItems = [];
		$totals = 0;
		if ($fellowProject->getFellowProjectBudget()) {
			// Gets monthly and onetime cost items
			$monthlyCostItems = $this->getDoctrine()->getRepository(
				'AppBundle:CostItemMonthly'
			)->findAll();
			$oneTimeCostItems = $this->getDoctrine()->getRepository(
				'AppBundle:CostItemOneTime'
			)->findAll();

			// calculations for budget section
			$totals = $fellowProject->getFellowProjectBudget()->getSummaryTotals();
			$fellowProjectBudgetTotals = $fellowProject->getFellowProjectBudget()->getFellowProjectBudgetTotals()->first();
		}

		return $this->render(
			'fellowprojectreview/review_form.html.twig',
			[
				'fellowProject' => $fellowProject,
				'fellowProjectReview' => $fellowProject->getFellowProjectReviews()->first(),
				'fellowProjectBudgetTotals' => $fellowProjectBudgetTotals,
				'reviewForm' => ($reviewForm) ? $reviewForm->createView() : null,
				'totals' => $totals,
			]
		);
	}

	/**
	 * Creates the form (or not) depending on the Proposal Status and the user role set on the session
	 *
	 * @param ProjectReviewComment $reviewComment
	 * @param ProjectGeneralInfo $projectGeneralInfo
	 *
	 * @return mixed (Form or null)
	 */
	private function createReviewCommentForm($reviewComment, $projectGeneralInfo)
	{
//		$token = $this->get('security.token_storage')->getToken();
		$projectReviewStatus = $projectGeneralInfo->getProjectReviewStatus()->getId();

		$formBuilder = $form = null;
		$formOptions = array(
			'action' => $this->generateUrl('fellow_project_review',
				['id' => $projectGeneralInfo->getFellowProject()->getId()]
			),
		);

		$commentFieldLabel = $this->get('translator')->trans('Add comment');

		if ($this->isGranted('review', $projectGeneralInfo)) {

			// Creates empty formBuilder
			$formBuilder = $this->createFormBuilder($reviewComment, $formOptions);

			// When the user is ADMIN, creates a special form
			if ($this->isGranted('ROLE_ADMIN') || $this->isGranted('ROLE_SUPER_ADMIN')) {

				$formBuilder
					->add('cycle', EntityType::class, [
						'class' => 'AppBundle:Fellow\CycleFellow',
						'data' => $projectGeneralInfo->getCycle(),
						'choice_label' => function (CycleFellow $cycle) {
							return $cycle->getName();
						},
						'mapped' => false,
						'label' => 'Cycle',
					])
					->add('reviewStatus', EntityType::class, [
						'class' => 'AppBundle:ProjectReviewStatus',
						'choice_label' => function (ProjectReviewStatus $reviewStatus) {
							return $reviewStatus->getName();
						},
						'data' => $projectGeneralInfo->getProjectReviewStatus(),
						'mapped' => false,
						'label' => 'Proposal Review Status',
					])
					->add('outcome', EntityType::class, [
						'class' => 'AppBundle:ProjectOutcome',
						'choice_label' => function (ProjectOutcome $projectOutcome) {
							return $projectOutcome->getName();
						},
						'data' => $projectGeneralInfo->getProjectOutcome(),
						'mapped' => false,
						'required' => false,
						'label' => 'Proposal Outcome',
						'placeholder' => 'Not defined',
					])
					->add('submissionStatus', ChoiceType::class, [
						'choices' => ['Submitted' => 'submitted', 'Not Submitted' => 'not_submitted'],
						'data' => $projectGeneralInfo->getSubmissionStatus(),
						'mapped' => false,
						'label' => 'Proposal Submission Status',
					])
					->add('save', SubmitType::class, [
						'label' => 'Save',
					]);

				// Adds a special Submit comment button for the end of the form
				$formBuilder->add('adminComment', SubmitType::class, [
					'label' => 'Send Admin Comment',
					'attr' => array('class' => 'm-5 btn btn-success'),
				]);
			}

			switch ($projectReviewStatus) {
				case ProjectReviewStatus::UNDER_RELO_REVIEW:
//					$formBuilder = $this->createFormBuilder($reviewComment, $formOptions)
					$formBuilder
						->add('approve', SubmitType::class, [
							'label' => 'Submit to RPO Review',
							'attr' => array('class' => 'm-5 btn btn-success'),
						])
						->add('return', SubmitType::class, [
							'label' => 'Return to Post for revision',
							'attr' => array('class' => 'm-5 btn btn-default '),
						])
						->add('reject', SubmitType::class, [
							'label' => 'Reject',
							'attr' => array('class' => 'm-5 btn btn-danger '),
						]);
					break;

				case ProjectReviewStatus::UNDER_RPO_REVIEW:
					$formBuilder
						->add('approve', SubmitType::class, [
							'label' => 'Submit to ECA Review',
							'attr' => array('class' => 'm-5 btn btn-success'),
						])
						->add('return_relo', SubmitType::class, [
							'label' => 'Return to RELO for revision',
							'attr' => array('class' => 'm-5 btn btn-default '),
						])
						->add('return_post', SubmitType::class, [
							'label' => 'Return to Post for revision',
							'attr' => array('class' => 'm-5 btn btn-default '),
						])
						->add('reject', SubmitType::class, [
							'label' => 'Reject',
							'attr' => array('class' => 'm-5 btn btn-danger '),
						]);
					break;

				case ProjectReviewStatus::UNDER_ECA_REVIEW:
					$formBuilder
						->add('approve', SubmitType::class, [
							'label' => 'Approve',
							'attr' => array('class' => 'm-5 btn btn-success'),
						])
						->add('return', SubmitType::class, [
							'label' => 'Return to RPO for revision',
							'attr' => array('class' => 'm-5 btn btn-default '),
						])
						->add('alternate_funding', SubmitType::class, [
							'label' => 'Request alternate funding',
							'attr' => array('class' => 'm-5 btn btn-default '),
						])
						->add('reject', SubmitType::class, [
							'label' => 'Reject',
							'attr' => array('class' => 'm-5 btn btn-danger '),
						]);
					break;

				case ProjectReviewStatus::UNDER_POST_RE_REVIEW:
					$formBuilder
						->add('submit', SubmitType::class, [
							'label' => 'Submit to RELO review',
							'attr' => array('class' => 'm-5 btn btn-success'),
						]);
					break;

				case ProjectReviewStatus::POST_REQUIRE_ALTERNATE_FUNDING:
					$acceptBtnDisabled = 'ECA' === $projectGeneralInfo->getFellowProject()->getFellowProjectBudget()->getFundedBy();
					$acceptBtnClasses = 'm-7 btn btn-success';

					if ($acceptBtnDisabled) {
						$acceptBtnClasses .= ' disabled';
					}

					$formBuilder
						->add('accept', SubmitType::class, [
							'label' => 'Funding changed, resubmit to ECA',
							'attr' => array('class' => $acceptBtnClasses),
							'disabled' => $acceptBtnDisabled
						])
						->add('decline', SubmitType::class, [
							'label' => 'Decline',
							'attr' => array('class' => 'm-5 btn btn-danger '),
						]);

//					$commentFieldLabel = $this->get('translator')->trans('Specify alternate funding');

					break;

				case ProjectReviewStatus::REVIEW_COMPLETE:
					// Nothing
					break;

			}

			$formBuilder
				->add('comment', CustomTextareaType::class, [
					'label' => $commentFieldLabel,
					'required' => false,
				]);

			$form = $formBuilder
				->getForm();
		}

		return $form;

	}

	/**
	 * @param FellowProject $fellowProject The Fellow project
	 * @param User $user
	 * @param string $action
	 * @param string $role
	 *
	 * @return null|array
	 */
	public function sendNotifications(FellowProject $fellowProject, User $user, $action, $role)
	{
		$projectGeneralInfo = $fellowProject->getProjectGeneralInfo();
		$em = $this->getDoctrine()->getManager();
		$recipients = array();
		$template = 'proposal_review';
		$subject = 'Fellow project proposal - ';

		switch ($action) {
			case 'Rejected by RELO':
				$subject .= 'not approved for funding';
				$template = 'proposal_rejected';

				$recipients[] = $projectGeneralInfo->getCreatedBy()->getEmail();
				$submitter = $projectGeneralInfo->getSubmittedBy()->getEmail();
				if (!in_array($submitter, $recipients)) {
					$recipients[] = $submitter;
				}
				break;

			case 'Rejected by ECA':
				$subject .= 'not approved for funding';
				$template = 'proposal_rejected';

				$recipients[] = $projectGeneralInfo->getCreatedBy()->getEmail();
				$submitter = $projectGeneralInfo->getSubmittedBy()->getEmail();
				if (!in_array($submitter, $recipients)) {
					$recipients[] = $submitter;
				}

				$reloLocation = $projectGeneralInfo->getReloLocation();
				$relos = $reloLocation->getRelos();
				foreach ($relos as $relo) {
					$recipients[] = $relo->getEmail();
				}

				$region = $projectGeneralInfo->getRegion();
				$rpos = $region->getRpos();
				foreach ($rpos as $rpo) {
					$recipients[] = $rpo->getEmail();
				}
				break;

			// This is a failsafe option
			case 'Rejected':
				$subject .= 'not approved for funding';
				$authors = $projectGeneralInfo->getAuthors();
				foreach ($authors as $author) {
					$recipients[] = $author->getEmail();
				}
				$recipients[] = $projectGeneralInfo->getCreatedBy()->getEmail();
				$submitter = $projectGeneralInfo->getSubmittedBy()->getEmail();
				if (!in_array($submitter, $recipients)) {
					$recipients[] = $submitter;
				}
				break;

			case 'Submitted to RELO review':
			case 'Submitted to RPO review':
			case 'Submitted to ECA review':
			case 'Approved':
//				$subject = 'A Fellow proposal has been approved!';
				switch ($reviewStatus = $projectGeneralInfo->getProjectReviewStatus()->getId()) {
					case ProjectReviewStatus::UNDER_ECA_REVIEW:
						$subject .= 'ready for ECA review';
						$template = 'eca_review';

						$recipients[] = $this->getParameter('eca_email_address');
//						$ecas = $em->getRepository('AppBundle:User')->findAllECAs();
//						foreach ($ecas as $eca) {
//							$recipients[] = $eca->getEmail();
//						}
						break;

					case ProjectReviewStatus::UNDER_RPO_REVIEW:
						$subject .= 'ready for RPO review';
						$template = 'rpo_review';

						$region = $projectGeneralInfo->getRegion();
						$rpos = $region->getRpos();
						foreach ($rpos as $rpo) {
							$recipients[] = $rpo->getEmail();
						}
						break;

					case ProjectReviewStatus::UNDER_RELO_REVIEW:
						$subject .= 'ready for RELO review';
						$template = 'relo_review';

						$reloLocation = $projectGeneralInfo->getReloLocation();
						$relos = $reloLocation->getRelos();
						foreach ($relos as $relo) {
							$recipients[] = $relo->getEmail();
						}
						break;

					case ProjectReviewStatus::REVIEW_COMPLETE:
						$subject .= 'approved!';
						$template = 'proposal_approved';

						// Changes on recipients on 7/7/2016
//						$authors = $projectGeneralInfo->getAuthors();
//						foreach ($authors as $author) {
//							$recipients[] = $author->getEmail();
//						}
						$recipients[] = $projectGeneralInfo->getCreatedBy()->getEmail();
						$submitter = $projectGeneralInfo->getSubmittedBy()->getEmail();
						if (!in_array($submitter, $recipients)) {
							$recipients[] = $submitter;
						}

						$reloLocation = $projectGeneralInfo->getReloLocation();
						$relos = $reloLocation->getRelos();
						foreach ($relos as $relo) {
							$recipients[] = $relo->getEmail();
						}

						$region = $projectGeneralInfo->getRegion();
						$rpos = $region->getRpos();
						foreach ($rpos as $rpo) {
							$recipients[] = $rpo->getEmail();
						}
						break;

				}

				break;

			case 'Returned to Post for revision':
			case 'Returned to RELO for revision':
			case 'Returned to RPO for revision':
//				$subject = 'A Fellow proposal has been returned and needs your attention';
				switch ($reviewStatus = $projectGeneralInfo->getProjectReviewStatus()->getId()) {
					case ProjectReviewStatus::UNDER_RPO_REVIEW:
						$subject .= 'returned to RPO for revision';
						$template = 'rpo_revision';

						$region = $projectGeneralInfo->getRegion();
						$rpos = $region->getRpos();
						foreach ($rpos as $rpo) {
							$recipients[] = $rpo->getEmail();
						}
						break;

					case ProjectReviewStatus::UNDER_RELO_REVIEW:
						$subject .= 'returned to RELO for revision';
						$template = 'relo_revision';

						$reloLocation = $projectGeneralInfo->getReloLocation();
						$relos = $reloLocation->getRelos();
						foreach ($relos as $relo) {
							$recipients[] = $relo->getEmail();
						}
						break;

					case ProjectReviewStatus::UNDER_POST_RE_REVIEW:
						$subject .= "returned for revision";
						$template = 'post_revision';

						$authors = $projectGeneralInfo->getAuthors();
						foreach ($authors as $author) {
							$recipients[] = $author->getEmail();
						}
						$recipients[] = $projectGeneralInfo->getCreatedBy()->getEmail();
						$submitter = $projectGeneralInfo->getSubmittedBy()->getEmail();
						if (!in_array($submitter, $recipients)) {
							$recipients[] = $submitter;
						}
						break;

				}
				break;

			case 'Alternate funding requested':
				$subject .= 'alternate funding required';
				$template = 'alternate_funding_request';

				$authors = $projectGeneralInfo->getAuthors();
				foreach ($authors as $author) {
					$recipients[] = $author->getEmail();
				}
				$recipients[] = $projectGeneralInfo->getCreatedBy()->getEmail();
				$submitter = $projectGeneralInfo->getSubmittedBy()->getEmail();
				if (!in_array($submitter, $recipients)) {
					$recipients[] = $submitter;
				}
				break;

			case 'Funding changed':
				$subject .= 'alternate funding set; ready for ECA re-review';
				$template = 'alternate_funding_resubmit';

				$recipients[] = $this->getParameter('eca_email_address');
				break;

			case 'Withdrawn':
				$subject .= 'proposal withdrawn';
				$template = 'proposal_withdrawn';

				$recipients[] = $this->getParameter('eca_email_address');
				$reloLocation = $projectGeneralInfo->getReloLocation();
				$relos = $reloLocation->getRelos();
				foreach ($relos as $relo) {
					$recipients[] = $relo->getEmail();
				}

				$region = $projectGeneralInfo->getRegion();
				$rpos = $region->getRpos();
				foreach ($rpos as $rpo) {
					$recipients[] = $rpo->getEmail();
				}
				break;

			case 'Alternate Funding Unavailable':
				$subject .= 'alternative funding unavailable';
				$template = 'alternate_funding_unavailable';

				$recipients[] = $this->getParameter('eca_email_address');
				break;

			case 'Re-submitted to RELO review':
				$subject .= 'ready for RELO re-review';
				$template = 'relo_re_review';

				$reloLocation = $projectGeneralInfo->getReloLocation();
				$relos = $reloLocation->getRelos();
				foreach ($relos as $relo) {
					$recipients[] = $relo->getEmail();
				}
				break;
		}


		// Render and the content of the email notification to be sent.
		$reviewEmailBody = $this->renderView(
			'email/proposalreview/'. $template .'.html.twig',
			[
				'fellowProject' => $fellowProject,
				'user' => $user,
				'role' => $role,
				'recipients' => $recipients,
				'sendMessage' => $this->getParameter('send_emails'),
			]
		);

		// First, check if emails are allowed to sent in this instance.  Then,
		// check if there recipients for this email.
		if ($this->getParameter('send_emails') && count($recipients)) {
			if ($this->getParameter('send_emails')) {
				foreach ($recipients as $recipient) {
					/** @var \Swift_Message $message */
					$message = \Swift_Message::newInstance()
						->setSubject($subject)
						->setFrom('proposal-no-reply@elprograms.org')
						->setTo($recipient)
//						->setCc('virginia.alvarez@inqbation.com')
						->setBody($reviewEmailBody, 'text/html');

					if ($this->getParameter('bcc_email_address')) {
						$message->setBcc($this->getParameter('bcc_email_address'));
					}
					
					$this->get('mailer')->send($message);
				}
			}
		} else {
			$this->addFlash('debug-email', $reviewEmailBody);
		}
	}

	/**
	 * Generates page to confirm withdrawal of submitted Fellow Project
	 *
	 * @Route("/project/fellow/{id}/withdraw", name="project_fellow_confirm_withdrawal")
	 * @Method("GET")
	 *
	 * @param \AppBundle\Entity\FellowProject $fellowProject
	 *
	 * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
	 */
	public function confirmWithdrawalAction(FellowProject $fellowProject)
	{
		$this->denyAccessUnlessGranted('withdraw', $fellowProject->getProjectGeneralInfo());

		$form = $this->createWithdrawalForm($fellowProject);

		return $this->render('fellowprojectreview/confirm_withdrawal.html.twig', array(
			'fellowProject' => $fellowProject,
			'form' => $form->createView(),
		));

	}

	/**
	 * Creates a form to withdraw a Fellow Project.
	 *
	 * @param FellowProject $fellowProject The FellowProject entity
	 *
	 * @return \Symfony\Component\Form\Form The form
	 */
	private function createWithdrawalForm(FellowProject $fellowProject)
	{
		return $this->createFormBuilder()
			->setAction(
				$this->generateUrl(
					'project_fellow_withdrawal',
					['id' => $fellowProject->getId()]
				)
			)
			->setMethod('DELETE')
			->getForm();
	}

	/**
	 * Withdraws a FellowProject entity.
	 *
	 * @Route("/project/fellow/{id}/withdraw", name="project_fellow_withdrawal")
	 * @Method("DELETE")
	 *
	 * @param Request $request
	 * @param FellowProject           $fellowProject
	 *
	 * @return \Symfony\Component\HttpFoundation\RedirectResponse
	 */
	public function withdrawAction(Request $request, FellowProject $fellowProject)
	{
		/** @var ProjectGeneralInfo $projectGeneralInfo */
		$projectGeneralInfo = $fellowProject->getProjectGeneralInfo();
		$this->denyAccessUnlessGranted('withdraw', $projectGeneralInfo);

		$form = $this->createWithdrawalForm($fellowProject);
		$form->handleRequest($request);

		if ($form->isSubmitted() && $form->isValid()) {
			$em = $this->getDoctrine()->getManager();

			// Closes the Proposal Review/Sets it as Complete
			$projectGeneralInfo->setProjectReviewStatus(
				$em->getRepository('AppBundle:ProjectReviewStatus')->find(ProjectReviewStatus::REVIEW_COMPLETE)
			);

			// Sets the outcome
			$projectGeneralInfo->setProjectOutcome(
				$em->getRepository('AppBundle:ProjectOutcome')->find(ProjectOutcome::WITHDRAWN)
			);

			// Gets the project review and adds a comment to the Comments History
			/** @var ProjectReview $projectReview */
			$projectReview = $fellowProject->getFellowProjectReviews()->first();

			$authenticatedUser = $this->getUser();
			$reviewComment = new ProjectReviewComment();
			$reviewComment->setCommentBy($authenticatedUser);
			$reviewComment->setProjectReview($projectReview);
			$reviewComment->setActionTaken('Withdrawn');

			if ($this->isGranted('ROLE_RELO')) {
				$role = 'RELO';
			} elseif ($this->isGranted('ROLE_RPO')) {
				$role = 'RPO';
			} elseif ($this->isGranted('ROLE_STATE_DEPARTMENT')) {
				$role = 'ECA';
			} elseif ($this->isGranted('ROLE_ADMIN') || $this->isGranted('ROLE_SUPER_ADMIN')) {
				$role = 'Admin';
			} else {
				$role = 'Embassy';
			}

			$reviewComment->setCommentByRole($role);
			$em->persist($reviewComment);

			$em->flush();

			$this->sendNotifications($fellowProject, $authenticatedUser, 'Withdrawn', $role);
		}

		return $this->redirectToRoute('proposal_fellow_index');
	}


	/**
	 * TODO: this method should be placed in the model, no in a controller.
	 *
	 * @param \AppBundle\Entity\FellowProjectBudget $fellowProjectBudget
	 * @param object $item
	 *
	 * @return object $item
	 */
	public function calculateRowTotals($fellowProjectBudget, $item = null)
	{
		if (!$item->getPostContribution()) {
			$item->setPostContribution(0);
		}

		if (!$item->getHostContributionMonetary()) {
			$item->setHostContributionMonetary(0);
		}

		if (!$item->getHostContributionInKind()) {
			$item->setHostContributionInKind(0);
		}

		if ($fellowProjectBudget->getFundedBy() == 'ECA') { // ECA funded
			$item->setPostHostTotalContribution($item->getPostContribution() + $item->getHostContributionMonetary() + $item->getHostContributionInKind());
			$item->setPostTotalContribution($item->getPostContribution());
			if ($item->getFullCost()) {
				$item->setECATotalContribution($item->getFullCost() - $item->getPostHostTotalContribution());
			} else {
				$item->setECATotalContribution(0);
			}
		} else { // Post funded
			$item->setPostHostTotalContribution($item->getHostContributionMonetary() + $item->getHostContributionInKind());
			if ($item->getFullCost()) {
				$item->setPostTotalContribution($item->getFullCost() - $item->getPostHostTotalContribution());
			} else {
				$item->setPostTotalContribution(0);
			}
			$item->setPostContribution($item->getPostTotalContribution());
			$item->setECATotalContribution(0);
		}

		return $item;
	}
}