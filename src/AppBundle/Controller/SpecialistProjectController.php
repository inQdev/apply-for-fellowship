<?php

namespace AppBundle\Controller;

use AppBundle\Entity\ContactPoint;
use AppBundle\Entity\ContactPointAdditional;
use AppBundle\Entity\ContactPointSecondary;
use AppBundle\Entity\DayBreakdownActivity;
use AppBundle\Entity\InCountryAssignmentActivity;
use AppBundle\Entity\PrePostWork;
use AppBundle\Entity\ProjectGeneralInfo;
use AppBundle\Entity\SpecialistProject;
use AppBundle\Entity\SpecialistProjectPhase;
use AppBundle\Entity\User;
use AppBundle\Entity\InCountryAssignment;
use AppBundle\Entity\Host;
use AppBundle\Entity\VirtualAssignment;
use AppBundle\Entity\VirtualAssignmentActivity;
use AppBundle\Form\Specialist\DayBreakdownActivityType;
use AppBundle\Form\Specialist\InCountryAssignmentCityType;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

/**
 * SpecialistProject controller.
 *
 * @package AppBundle\Controller
 * @author  Juan Obando <juan.obando@inqbation.com>
 * @author  Felipe Ceballos <felipe.ceballos@inqbation.com>
 *
 * @Route("/project/specialist")
 */
class SpecialistProjectController extends Controller implements RoleSwitcherInterface
{
    /**
     * @Route("/", name="specialist_project_index")
     * @Method({"GET"})
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction()
    {
        /** @var User $authenticatedUser */
        $authenticatedUser = $this->getUser();
        $em = $this->getDoctrine()->getManager();

        $specialistProposalsGeneralInfo = $em
          ->getRepository('AppBundle:ProjectGeneralInfo')
          ->findAllSpecialistProposalsByUser($authenticatedUser);

        // This will retrieve all proposals (both Fellow and Specialist).
        $sharedProposals = $authenticatedUser->getProjects()->toArray();

        $sharedProposals = array_filter(
          $sharedProposals,
          function ($sharedProposal) {
              return null !== $sharedProposal->getSpecialistProjectPhase();
          }
        );

        return $this->render(
          'project/specialist/index.html.twig',
          [
            'specialistProposalsGeneralInfo' => $specialistProposalsGeneralInfo,
            'sharedProposals'                => $sharedProposals,
          ]
        );
    }

    /**
     * Creates a new SpecialistProject entity and save the record in the
     * database.
     *
     * @Route("/new", name="specialist_project_new")
     * @Method({"GET"})
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function newAction()
    {
        $em = $this->getDoctrine()->getManager();

        $currentSpecialistCycle = $em->getRepository('AppBundle:Specialist\CycleSpecialist')->getCurrentCycle();

        $specialistProject = new SpecialistProject();
        $em->persist($specialistProject);
        $em->flush();

        $projectGeneralInfo = new ProjectGeneralInfo($currentSpecialistCycle);
        $em->persist($projectGeneralInfo);
        $em->flush();

        $specialistProjectPhase = new SpecialistProjectPhase(
          $specialistProject,
          $projectGeneralInfo
        );

        $em->persist($specialistProjectPhase);
        $em->flush();

        return $this->redirectToRoute(
          'specialist_project_phase_admin_panel',
          [
            'id'    => $specialistProject->getId(),
            'phase' => 0,
          ]
        );
    }

    /**
     * @Route(
     *     "/{id}/{phase}/admin-panel",
     *     requirements={"id": "\d+", "phase": "\d+"},
     *     name="specialist_project_phase_admin_panel",
     * )
     * @Method({"GET"})
     *
     * @param SpecialistProject $specialistProject
     * @param int               $phase
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function adminPanelAction(
      SpecialistProject $specialistProject,
      $phase
    ) {
        $em = $this->getDoctrine()->getManager();

        $submitProjectForm = $this->createForm(
          'AppBundle\Form\Specialist\SubmitSpecialistProposalType',
          null,
          [
            'action' => $this->generateUrl(
              'specialist_project_submit',
              [
                'id' => $specialistProject->getId(),
                'phase' => $phase
              ]
            ),
          ]
        );

        /** @var SpecialistProjectPhase $specialistProjectPhase */
        $specialistProjectPhase = $specialistProject->getPhases()->get($phase);

        /** @var ProjectGeneralInfo $projectGeneralInfo */
        $projectGeneralInfo = $specialistProjectPhase->getProjectGeneralInfo();

        $shareProjectForm = $this->createFormBuilder()
          ->setMethod('POST')
          ->setAction(
            $this->generateUrl(
              'specialist_project_phase_share',
              [
                'id'    => $specialistProject->getId(),
                'phase' => $phase,
              ]
            )
          )
          ->add('shareProject', SubmitType::class, ['label' => 'Save'])
          ->getForm();

        $authors = $projectGeneralInfo->getAuthors()->toArray();

        // Include creator user in the authors list
        $authors[] = $projectGeneralInfo->getCreatedBy();

        // Get all users with RELO and EMBASSY roles, excluding those who are
        // authors of this project already.
        $embassyRELOUsers = $em->getRepository('AppBundle:User')
          ->findAllByRoles(['ROLE_EMBASSY', 'ROLE_RELO'], $authors);

        // The data should be mapped in a way the JS auto-complete plugin
        // understands it.
        $embassyRELOUsers = array_map(
          function ($user) {
              return [
                'id'       => $user['id'],
                'name'     => trim($user['name'].' <'.$user['email'].'>'),
                'fullname' => $user['name'],
                'email'    => $user['email'],
              ];
          },
          $embassyRELOUsers
        );

        return $this->render(
          'project/specialist/admin_panel.html.twig',
          [
            'specialistProject'      => $specialistProject,
            'specialistProjectPhase' => $specialistProjectPhase,
            'projectGeneralInfo'     => $projectGeneralInfo,
            'phase'                  => $phase,
            'embassyRELOUsers'       => json_encode($embassyRELOUsers),
            'shareProjectForm'       => $shareProjectForm->createView(),
            'submitProjectForm'     => $submitProjectForm->createView()
          ]
        );
    }

    /**
     * @Route(
     *     "/{id}/{phase}/title-region",
     *     requirements={"id": "\d+", "phase": "\d+"},
     *     name="specialist_project_title_region"
     * )
     * @Method({"GET", "POST"})
     *
     * @param Request           $request
     * @param SpecialistProject $specialistProject
     * @param int               $phase
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function titleRegionAction(
      Request $request,
      SpecialistProject $specialistProject,
      $phase
    ) {
        /** @var SpecialistProjectPhase $specialistProjectPhase */
        $specialistProjectPhase = $specialistProject->getPhases()->get($phase);

        /** @var ProjectGeneralInfo $projectGeneralInfo */
        $projectGeneralInfo = $specialistProjectPhase->getProjectGeneralInfo();

        $form = $this->createForm(
          'AppBundle\Form\Specialist\TitleRegionType',
          $projectGeneralInfo
        );
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($projectGeneralInfo);
            $em->flush();

            $nextAction = 'specialist_project_phase_admin_panel';

            if ($form->get('next')->isClicked()) {
                if ('' === $form->get('region')->getViewData()) {
                    $this->addFlash('no_region', 'Please select a Region');
                    $nextAction = 'specialist_project_title_region';
                } else {
                    $nextAction = 'specialist_project_phase_countries';
                }
            }

            return $this->redirectToRoute(
              $nextAction,
              [
                'id'    => $specialistProject->getId(),
                'phase' => $phase,
              ]
            );
        }

        return $this->render(
          'project/specialist/wizard/title_region.html.twig',
          [
            'specialistProject'      => $specialistProject,
            'specialistProjectPhase' => $specialistProjectPhase,
            'form'                   => $form->createView(),
            'goBack'                 => null,
          ]
        );
    }

    /**
     * @Route(
     *     "/{id}/{phase}/countries",
     *     requirements={"id": "\d+", "phase": "\d+"},
     *     name="specialist_project_phase_countries"
     * )
     * @Method({"GET", "POST"})
     *
     * @param Request           $request
     * @param SpecialistProject $specialistProject
     * @param int               $phase
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function countriesAction(
      Request $request,
      SpecialistProject $specialistProject,
      $phase
    ) {
        /** @var SpecialistProjectPhase $specialistProjectPhase */
        $specialistProjectPhase = $specialistProject->getPhases()->get($phase);

        /** @var ProjectGeneralInfo $projectGeneralInfo */
        $projectGeneralInfo = $specialistProjectPhase->getProjectGeneralInfo();

        $form = $this->createForm(
          'AppBundle\Form\Specialist\CountriesType',
          $projectGeneralInfo
        );
        $form->handleRequest($request);

        $prevAction = 'specialist_project_title_region';

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $postData = $request->request->get('countries');
            $countryId = $postData['country'];

            if ('' !== $countryId) {
                $country = $em->getRepository('AppBundle:Country')
                  ->findOneBy(['id' => $countryId]);

                $projectGeneralInfo->addCountry($country);

                $em->persist($projectGeneralInfo);
                $em->flush();
            }

            if ($form->get('save')->isClicked()) {
                return $this->redirectToRoute(
                  'specialist_project_phase_admin_panel',
                  [
                    'id'    => $specialistProject->getId(),
                    'phase' => $phase,
                  ]
                );
            } elseif ($form->get('next')->isClicked()) {
                return $this->redirectToRoute(
                  'specialist_project_phase_posts',
                  [
                    'id'    => $specialistProject->getId(),
                    'phase' => $phase,
                  ]
                );
            } elseif ($form->get('back')->isClicked()) {
                return $this->redirectToRoute(
                  $prevAction,
                  [
                    'id'    => $specialistProject->getId(),
                    'phase' => $phase,
                  ]
                );
            } elseif ($form->get('addCountry')->isClicked()) {
                return $this->redirectToRoute(
                  'specialist_project_phase_countries',
                  [
                    'id'    => $specialistProject->getId(),
                    'phase' => $phase,
                  ]
                );
            }
        }

        return $this->render(
          'project/specialist/wizard/countries.html.twig',
          [
            'specialistProject'      => $specialistProject,
            'specialistProjectPhase' => $specialistProjectPhase,
            'form'                   => $form->createView(),
            'goBack'                 => $prevAction,
          ]
        );
    }

    /**
     * @Route(
     *     "/{id}/{phase}/posts",
     *     requirements={"id": "\d+", "phase": "\d+"},
     *     name="specialist_project_phase_posts"
     * )
     *
     * @param Request           $request
     * @param SpecialistProject $specialistProject
     * @param int               $phase
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function postsAction(
      Request $request,
      SpecialistProject $specialistProject,
      $phase
    ) {
        /** @var SpecialistProjectPhase $specialistProjectPhase */
        $specialistProjectPhase = $specialistProject->getPhases()->get($phase);

        /** @var ProjectGeneralInfo $projectGeneralInfo */
        $projectGeneralInfo = $specialistProjectPhase->getProjectGeneralInfo();

        $form = $this->createForm(
          'AppBundle\Form\Specialist\PostsType',
          $projectGeneralInfo
        );
        $form->handleRequest($request);

        $prevAction = 'specialist_project_phase_countries';

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $projectGeneralInfo->getPosts()->clear();

            $postsIds = $request->request->get('posts')['posts'];

            if (!empty($postsIds)) {
                foreach ($postsIds as $postId) {
                    if ('' !== $postId) {
                        $post = $em->getRepository('AppBundle:Post')->find($postId);

                        if ($post) {
                            $projectGeneralInfo->addPost($post);
                        }
                    }
                }
            }

            $em->persist($projectGeneralInfo);
            $em->flush();

            $nextAction = 'specialist_project_phase_admin_panel';

            if ($form->get('back')->isClicked()) {
                $nextAction = $prevAction;
            }

            return $this->redirectToRoute(
              $nextAction,
              [
                'id'    => $specialistProject->getId(),
                'phase' => $phase,
              ]
            );
        }

        return $this->render(
          'project/specialist/wizard/posts.html.twig',
          [
            'specialistProject'      => $specialistProject,
            'specialistProjectPhase' => $specialistProjectPhase,
            'form'                   => $form->createView(),
            'goBack'                 => $prevAction,
          ]
        );
    }

    /**
     * @Route(
     *     "/{id}/{phase}/contact-points/add",
     *     requirements={"id": "\d+", "phase": "\d+"},
     *     name="specialist_project_phase_contact_points_add"
     * )
     * @Method({"GET"})
     *
     * @param SpecialistProject $specialistProject
     * @param int               $phase
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function addContactPointsAction(
      SpecialistProject $specialistProject,
      $phase
    ) {
        /** @var SpecialistProjectPhase $specialistProjectPhase */
        $specialistProjectPhase = $specialistProject->getPhases()->get($phase);

        /** @var ProjectGeneralInfo $projectGeneralInfo */
        $projectGeneralInfo = $specialistProjectPhase->getProjectGeneralInfo();

        if ($projectGeneralInfo->getContactPoints()->isEmpty()) {
            $contactPointPrimary = new ContactPoint();
            $contactPointPrimary->setProjectGeneralInfo($projectGeneralInfo);

            $contactPointSecondary = new ContactPointSecondary();
            $contactPointSecondary->setProjectGeneralInfo($projectGeneralInfo);

            $em = $this->getDoctrine()->getManager();
            $em->persist($contactPointSecondary);
            $em->persist($contactPointPrimary);
            $em->flush();
        }

        return $this->redirectToRoute(
          'specialist_project_phase_contact_points',
          [
            'id'    => $specialistProject->getId(),
            'phase' => $phase,
          ]
        );
    }

    /**
     * @Route(
     *     "/{id}/{phase}/contact-points/{cid}/delete",
     *     name="specialist_project_phase_contact_points_delete"
     * )
     * @Method("DELETE")
     *
     * @ParamConverter(
     *     "contactPointAdditional",
     *     class="AppBundle:ContactPointAdditional",
     *     options={"mapping": {"cid": "id"}}
     * )
     *
     * @param Request                $request
     * @param SpecialistProject      $specialistProject
     * @param ContactPointAdditional $contactPointAdditional
     * @param int                    $phase
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteContactPointAction(
      Request $request,
      SpecialistProject $specialistProject,
      ContactPointAdditional $contactPointAdditional,
      $phase
    ) {
        $form = $this->createDeleteContactPointForm(
          $contactPointAdditional,
          $specialistProject,
          $phase
        );
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($contactPointAdditional);
            $em->flush();
        }

        return $this->redirectToRoute(
          'specialist_project_phase_contact_points',
          [
            'id'    => $specialistProject->getId(),
            'phase' => $phase,
          ]
        );
    }

    /**
     * @Route(
     *     "/{id}/{phase}/contact-points",
     *     requirements={"id": "\d+", "phase": "\d+"},
     *     name="specialist_project_phase_contact_points"
     * )
     * @Method({"GET", "POST"})
     *
     * @param Request           $request
     * @param SpecialistProject $specialistProject
     * @param int               $phase
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function contactPointsAction(
      Request $request,
      SpecialistProject $specialistProject,
      $phase
    ) {
        /** @var SpecialistProjectPhase $specialistProjectPhase */
        $specialistProjectPhase = $specialistProject->getPhases()->get($phase);

        /** @var ProjectGeneralInfo $projectGeneralInfo */
        $projectGeneralInfo = $specialistProjectPhase->getProjectGeneralInfo();

        $form = $this->createForm(
          'AppBundle\Form\Common\ContactPointsType',
          $projectGeneralInfo
        );
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($projectGeneralInfo);
            $em->flush();

            $nextAction = 'specialist_project_phase_admin_panel';

            if ($form->get('additional')->isClicked()) {
                $nextAction = 'specialist_project_phase_contact_points';

                $contactPointAdditional = new ContactPointAdditional();
                $contactPointAdditional->setProjectGeneralInfo(
                  $projectGeneralInfo
                );

                $em->persist($contactPointAdditional);
                $em->flush();
            }

            return $this->redirectToRoute(
              $nextAction,
              [
                'id'    => $specialistProject->getId(),
                'phase' => $phase,
              ]
            );
        }

        // Get last Contact Point entry and check if it is Additional.  If it
        // is, a delete form for Contact Points needs to be created.  This
        // delete form will handle the deletion of all Additional Contact
        // Points through JavaScript.
        $contactPoint = $projectGeneralInfo->getContactPoints()->last();

        // to have something to pass to the template
        $deleteForm = false;

        if ($contactPoint instanceof ContactPointAdditional) {
            $deleteForm = $this->createDeleteContactPointForm(
              $contactPoint,
              $specialistProject,
              $phase
            );
        }

        return $this->render(
          'project/specialist/wizard/contact_points.html.twig',
          [
            'specialistProject'      => $specialistProject,
            'specialistProjectPhase' => $specialistProjectPhase,
            'phase'                  => $phase,
            'form'                   => $form->createView(),
            'deleteForm'             => $deleteForm ? $deleteForm->createView()
              : false,
            'goBack'                 => null,
          ]
        );
    }

    /**
     * Creates a form to delete an additional contact point
     *
     * @param ContactPointAdditional $contactPoint
     * @param SpecialistProject      $specialistProject
     * @param int                    $phase
     *
     * @return \Symfony\Component\Form\Form The form.
     */
    private function createDeleteContactPointForm(
      ContactPointAdditional $contactPoint,
      SpecialistProject $specialistProject,
      $phase
    ) {
        return $this->createFormBuilder()
          ->setAction(
            $this->generateUrl(
              'specialist_project_phase_contact_points_delete',
              [
                'id'    => $specialistProject->getId(),
                'phase' => $phase,
                'cid'   => $contactPoint->getId(),
              ]
            )
          )
          ->setMethod('DELETE')
          ->getForm();
    }

    /**
     * In Country Assignment Duties
     *
     * @Route(
     *     "/{id}/{phase}/country-assignment-duties",
     *     requirements={"id": "\d+", "phase": "\d+"},
     *     name="specialist_project_phase_country_assignment_duties"
     * )
     * @Method({"GET", "POST"})
     *
     * @param SpecialistProject $specialistProject
     * @param int $phase
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function inCountryAssignmentAction(SpecialistProject $specialistProject, $phase, Request $request)
    {
        $specialistProjectPhase = $specialistProject->getPhases()->get($phase);
        $inCountryAssignments = $specialistProjectPhase->getInCountryAssignments();

        // verifies that at least one country is selected
        if (!$specialistProjectPhase->getProjectGeneralInfo()->getCountries()) {

            $errorMsg = printf('Please, make sure you have selected a region and at least one country from the <a class="alert-link" href="">General Information section</a>',
                $this->generateUrl(
                    'specialist_project_title_region',
                    [   'id' => $specialistProject->getId(),
                        'phase' => $phase,
                    ]
                ));

            $this->addFlash('danger', $errorMsg);

        } else {

            // preparing data for populate "anonymous" form, this just creates empty fields to add cities
            $formData = new \stdClass();
            $formData->inCountryAssignmentCity = new ArrayCollection();

            foreach ($specialistProjectPhase->getProjectGeneralInfo()->getCountries() as $country) {
                $inCountryAssignment = new InCountryAssignment();
                $inCountryAssignment->setCountry($country);
                $formData->inCountryAssignmentCity->add($inCountryAssignment);
            }
        }

        // creates "anonymous" form
        $form = $this->createFormBuilder($formData)
            ->add('inCountryAssignmentCity', CollectionType::class, [
                'entry_type' => InCountryAssignmentCityType::class,
            ])
            ->add('deleteId', HiddenType::class, [
                'mapped' => false,
                'data' => '',
            ])
            ->add('save', SubmitType::class, ['label' => 'Save and return to Panel'])
            ->add('next', SubmitType::class, ['label' => 'Save and continue'])
            ->add('addCity', SubmitType::class, ['label' => 'Add City'])
            ->add('here', SubmitType::class)
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            foreach ($formData->inCountryAssignmentCity as $inCountryAssignment) {
                if ($inCountryAssignment->getCity() !== null) {
                    $country = $em->getRepository('AppBundle:Country')->findOneByName($inCountryAssignment->getCountry());
                    $inCountryAssignment->setCountry($country);
                    $inCountryAssignment->setSpecialistProjectPhase($specialistProjectPhase);
                    $em->persist($inCountryAssignment);
                }
            }

            if ($form->get('deleteId')->getViewData()) {
                $assignmentToDelete = $em->getRepository('AppBundle:InCountryAssignment')->find($form->get('deleteId')->getViewData());
                $em->remove($assignmentToDelete);
            }

            $em->flush();

            $nextAction = 'specialist_project_phase_country_assignment_duties';
            $urlParameters = array(
                'id' => $specialistProject->getId(),
                'phase' => $phase,
            );

            if ($form->get('save')->isClicked()) {
                $nextAction = 'specialist_project_phase_admin_panel';
            }

            return $this->redirectToRoute($nextAction, $urlParameters);
        }

        return $this->render(
            'project/specialist/wizard/country_assignments_duties.html.twig',
            [
                'specialistProject' => $specialistProject,
                'specialistProjectPhase' => $specialistProjectPhase,
                'inCountryAssignments' => $inCountryAssignments,
                'phase' => $phase,
                'form' => $form->createView(),
                'goBack' => null,
            ]
        );

    }

    /**
     * In Country Assignment Duty Edit
     *
     * @Route(
     *     "/{id}/{phase}/country-assignment-duties/{icaid}/edit",
     *     requirements={"id": "\d+", "phase": "\d+", "icaid": "\d+"},
     *     name="specialist_project_phase_country_assignment_duty_edit"
     * )
     * @ParamConverter("specialistProject", class="AppBundle:SpecialistProject", options={"mapping": {"id": "id"}})
     * @ParamConverter("inCountryAssignment", class="AppBundle:InCountryAssignment", options={"mapping": {"icaid": "id"}})
     * @Method({"GET", "POST"})
     *
     * @param SpecialistProject $specialistProject
     * @param int $phase
     * @param InCountryAssignment $inCountryAssignment
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function inCountryAssignmentEditAction(SpecialistProject $specialistProject, $phase,
                                                  InCountryAssignment $inCountryAssignment, Request $request)
    {
        $specialistProjectPhase = $specialistProject->getPhases()->get($phase);

        // creates empty activities to show in the form and assigns it to inCountryAssignment, if empty set
        $activitiesNumber = $inCountryAssignment->getAssignmentActivities()->count();
        $activitiesDefault = 2;
        for ($i=$activitiesNumber; $i < $activitiesDefault; $i++) {
            $assignmentActivity = new InCountryAssignmentActivity();
            $assignmentActivity->setInCountryAssignment($inCountryAssignment);
            $inCountryAssignment->addAssignmentActivity($assignmentActivity);
        }
//        dump($inCountryAssignment);

        $form = $this->createForm(
            'AppBundle\Form\Specialist\InCountryAssignmentType',
            $inCountryAssignment
        );

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $nextAction =  'specialist_project_phase_country_assignment_duties';
            $urlParameters = array(
                'id'    => $specialistProject->getId(),
                'phase' => $phase,
            );

            if ('' !== $form->get('newHost')->getViewData()) {
                $host = new Host();
                $host->setName($form->get('newHost')->getViewData());
                $host->setCountry($inCountryAssignment->getCountry());

                $em->persist($host);
                $em->flush();

                $inCountryAssignment->setHost($host);
            }

            if ('' !== $form->get('deleteId')->getViewData()) {
//                dump($form->get('deleteId')->getViewData()); exit;
                if ($form->get('deleteId')->getViewData()) {
                    $activityToDelete = $em->getRepository('AppBundle:InCountryAssignmentActivity')->find($form->get('deleteId')->getViewData());
                    $em->remove($activityToDelete);
                    $em->flush();
                } else {
                    $activityToDelete = $inCountryAssignment->getAssignmentActivities()->last();
                }
                $inCountryAssignment->removeAssignmentActivity($activityToDelete);
            }

            if ($form->get('here')->isClicked()) {
                $nextAction = 'specialist_project_phase_country_assignment_duty_edit';
                $urlParameters['icaid'] = $inCountryAssignment->getId();
            }

            // Adds a new (empty) activity if the blue button is clicked
            if ($form->get('add')->isClicked()) {
                $assignmentActivity = new InCountryAssignmentActivity();
                $assignmentActivity->setInCountryAssignment($inCountryAssignment);
                $inCountryAssignment->addAssignmentActivity($assignmentActivity);

                $nextAction = 'specialist_project_phase_country_assignment_duty_edit';
                $urlParameters['icaid'] = $inCountryAssignment->getId();
            }

            $em->persist($inCountryAssignment);
            $em->flush();

            // default save button
            return $this->redirectToRoute($nextAction, $urlParameters);
        }

        $prevAction = 'specialist_project_phase_country_assignment_duties';

        return $this->render(
            'project/specialist/wizard/country_assignments_duty_edit.html.twig',
            [
                'specialistProject' => $specialistProject,
                'specialistProjectPhase' => $specialistProjectPhase,
                'inCountryAssignment' => $inCountryAssignment,
                'phase' => $phase,
                'form' => $form->createView(),
                'goBack' => null,
            ]
        );

    }

    /**
     * In Country Assignment Duty Delete
     *
     * @Route(
     *     "/{id}/{phase}/country-assignment-duties/{icaid}/delete",
     *     requirements={"id": "\d+", "phase": "\d+", "icaid": "\d+"},
     *     name="specialist_project_phase_country_assignment_duty_delete"
     * )
     * @ParamConverter("specialistProject", class="AppBundle:SpecialistProject", options={"mapping": {"id": "id"}})
     * @ParamConverter("inCountryAssignment", class="AppBundle:InCountryAssignment", options={"mapping": {"icaid": "id"}})
     * @Method({"GET", "POST"})
     *
     * @param SpecialistProject $specialistProject
     * @param int $phase
     * @param InCountryAssignment $inCountryAssignment
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function inCountryAssignmentDeleteAction(SpecialistProject $specialistProject, $phase,
                                                  InCountryAssignment $inCountryAssignment, Request $request)
    {
        $specialistProjectPhase = $specialistProject->getPhases()->get($phase);

        // creates empty activity to show in the form and assigns it to inCountryAssignment
        $assignmentActivity = new InCountryAssignmentActivity();
        $assignmentActivity->setInCountryAssignment($inCountryAssignment);
        $inCountryAssignment->addAssignmentActivity($assignmentActivity);

//        dump($inCountryAssignment);

        $form = $this->createForm(
            'AppBundle\Form\Specialist\InCountryAssignmentType',
            $inCountryAssignment
        );

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($inCountryAssignment);
            $em->flush();

            return $this->redirectToRoute(
                'specialist_project_phase_country_assignment_duties',
                [
                    'id'    => $specialistProject->getId(),
                    'phase' => $phase,
                ]
            );
        }

        $prevAction = 'specialist_project_phase_country_assignment_duties';

        return $this->render(
            'project/specialist/wizard/country_assignments_duty_edit.html.twig',
            [
                'specialistProject' => $specialistProject,
                'specialistProjectPhase' => $specialistProjectPhase,
                'inCountryAssignment' => $inCountryAssignment,
                'phase' => $phase,
                'form' => $form->createView(),
                'goBack' => $prevAction,
            ]
        );

    }

    /**
     * In Country Assignment Duty Activity Delete
     *
     * @Route(
     *     "/{id}/{phase}/country-assignment-duties/activity/{icaaid}/delete",
     *     requirements={"id": "\d+", "phase": "\d+", "icaaid": "\d+"},
     *     name="specialist_project_phase_country_assignment_duty_activity_delete"
     * )
     * @ParamConverter("specialistProject", class="AppBundle:SpecialistProject", options={"mapping": {"id": "id"}})
     * @ParamConverter("inCountryAssignmentActivity", class="AppBundle:InCountryAssignmentActivity", options={"mapping": {"icaaid": "id"}})
     * @Method({"GET", "POST"})
     *
     * @param SpecialistProject $specialistProject
     * @param int $phase
     * @param InCountryAssignmentActivity $inCountryAssignmentActivity
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function inCountryAssignmentActivityDeleteAction(SpecialistProject $specialistProject, $phase,
                                                  InCountryAssignmentActivity $inCountryAssignmentActivity, Request $request)
    {
        $specialistProjectPhase = $specialistProject->getPhases()->get($phase);

        // creates empty activity to show in the form and assigns it to inCountryAssignment
        $assignmentActivity = new InCountryAssignmentActivity();
        $assignmentActivity->setInCountryAssignment($inCountryAssignmentActivity);
        $inCountryAssignmentActivity->addAssignmentActivity($assignmentActivity);

//        dump($inCountryAssignment);

        $form = $this->createForm(
            'AppBundle\Form\Specialist\InCountryAssignmentType',
            $inCountryAssignmentActivity
        );

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($inCountryAssignmentActivity);
            $em->flush();

            return $this->redirectToRoute(
                'specialist_project_phase_country_assignment_duties',
                [
                    'id'    => $specialistProject->getId(),
                    'phase' => $phase,
                ]
            );
        }

        $prevAction = 'specialist_project_phase_country_assignment_duties';

        return $this->render(
            'project/specialist/wizard/country_assignments_duty_edit.html.twig',
            [
                'specialistProject' => $specialistProject,
                'specialistProjectPhase' => $specialistProjectPhase,
                'inCountryAssignmentActivity' => $inCountryAssignmentActivity,
                'phase' => $phase,
                'form' => $form->createView(),
                'goBack' => $prevAction,
            ]
        );

    }

    /**
     * @Route(
     *     "/{id}/{phase}/requirements",
     *     requirements={"id": "\d+", "phase": "\d+"},
     *     name="specialist_project_phase_requirements"
     * )
     * @Method({"GET", "POST"})
     *
     * @param Request           $request
     * @param SpecialistProject $specialistProject
     * @param int               $phase
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function requirementsAction(
      Request $request,
      SpecialistProject $specialistProject,
      $phase
    ) {
        /** @var SpecialistProjectPhase $specialistProjectPhase */
        $specialistProjectPhase = $specialistProject->getPhases()->get($phase);

        /** @var ProjectGeneralInfo $projectGeneralInfo */
        $projectGeneralInfo = $specialistProjectPhase->getProjectGeneralInfo();

        $form = $this->createForm(
          'AppBundle\Form\Specialist\RequirementsType',
          $projectGeneralInfo
        );
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($projectGeneralInfo);
            $em->flush();

            return $this->redirectToRoute(
              'specialist_project_phase_admin_panel',
              [
                'id'    => $specialistProject->getId(),
                'phase' => $phase,
              ]
            );
        }

        return $this->render(
          'project/specialist/wizard/requirements.html.twig',
          [
            'specialistProject'      => $specialistProject,
            'specialistProjectPhase' => $specialistProjectPhase,
            'phase'                  => $phase,
            'form'                   => $form->createView(),
            'goBack'                 => null,
          ]
        );
    }

    /**
     * @Route(
     *     "/{id}/{phase}/certifications",
     *     requirements={"id": "\d+", "phase": "\d+"},
     *     name="specialist_project_phase_certifications"
     * )
     * @Method({"GET", "POST"})
     *
     * @param Request           $request
     * @param SpecialistProject $specialistProject
     * @param int               $phase
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function certificationsAction(
      Request $request,
      SpecialistProject $specialistProject,
      $phase
    ) {
        /** @var SpecialistProjectPhase $specialistProjectPhase */
        $specialistProjectPhase = $specialistProject->getPhases()->get($phase);

        /** @var ProjectGeneralInfo $projectGeneralInfo */
        $projectGeneralInfo = $specialistProjectPhase->getProjectGeneralInfo();

        $form = $this->createForm(
          'AppBundle\Form\Common\CertificationsType',
          $projectGeneralInfo
        );
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($projectGeneralInfo);
            $em->flush();

            return $this->redirectToRoute(
              'specialist_project_phase_admin_panel',
              [
                'id'    => $specialistProject->getId(),
                'phase' => $phase,
              ]
            );
        }

        return $this->render(
          'project/specialist/wizard/certifications.html.twig',
          [
            'specialistProject'      => $specialistProject,
            'specialistProjectPhase' => $specialistProjectPhase,
            'phase'                  => $phase,
            'form'                   => $form->createView(),
            'goBack'                 => null,
          ]
        );
    }

    /**
     * @Route(
     *     "/{id}/{phase}/candidate-info",
     *     requirements={"id": "\d+", "phase": "\d+"},
     *     name="specialist_project_phase_candidate_info"
     * )
     * @Method({"GET", "POST"})
     *
     * @param Request           $request
     * @param SpecialistProject $specialistProject
     * @param int               $phase
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function candidateInfoAction(
      Request $request,
      SpecialistProject $specialistProject,
      $phase
    ) {
        /** @var SpecialistProjectPhase $specialistProjectPhase */
        $specialistProjectPhase = $specialistProject->getPhases()->get($phase);

        $form = $this->createForm(
          'AppBundle\Form\Specialist\CandidateInfoType',
          $specialistProject
        );
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($specialistProject);
            $em->flush();

            // User shouldn't select more than five Areas of Expertise.
            if (count($form->get('areasOfExpertise')->getViewData()) <= 5) {
                return $this->redirectToRoute(
                  'specialist_project_phase_admin_panel',
                  [
                    'id'    => $specialistProject->getId(),
                    'phase' => $phase,
                  ]
                );
            }
            else {
                $this->addFlash(
                  'danger',
                  'You must check at least one Area of Expertise and no more than five.'
                );
            }
        }

        return $this->render(
          'project/specialist/wizard/candidate_info.html.twig',
          [
            'specialistProject'      => $specialistProject,
            'specialistProjectPhase' => $specialistProjectPhase,
            'phase'                  => $phase,
            'form'                   => $form->createView(),
            'goBack'                 => null,
          ]
        );
    }

    /**
     * @Route(
     *     "/{id}/{phase}/pre-post-work",
     *     requirements={"id": "\d+", "phase": "\d+"},
     *     name="specialist_project_phase_pre_post_work"
     * )
     * @Method({"GET", "POST"})
     *
     * @param Request           $request
     * @param SpecialistProject $specialistProject
     * @param int               $phase
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function prePostWorkAction(
      Request $request,
      SpecialistProject $specialistProject,
      $phase
    ) {
        /** @var SpecialistProjectPhase $specialistProjectPhase */
        $specialistProjectPhase = $specialistProject->getPhases()->get($phase);

        /** @var PrePostWork $prePostWork */
        $prePostWork = $specialistProjectPhase->getPrePostWork();

        if (null === $prePostWork) {
            $prePostWork = new PrePostWork($specialistProjectPhase);
        }

        $form = $this->createForm(
          'AppBundle\Form\Specialist\PrePostWorkType',
          $prePostWork
        );
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($prePostWork);
            $em->flush();

            return $this->redirectToRoute(
              'specialist_project_phase_admin_panel',
              [
                'id'    => $specialistProject->getId(),
                'phase' => $phase,
              ]
            );
        }

        return $this->render(
          'project/specialist/wizard/pre_post_work.html.twig',
          [
            'specialistProject'      => $specialistProject,
            'specialistProjectPhase' => $specialistProjectPhase,
            'phase'                  => $phase,
            'form'                   => $form->createView(),
            'goBack'                 => null,
          ]
        );
    }

    /**
     * @Route(
     *     "/{id}/{phase}/goals-sustainability",
     *     requirements={"id": "\d+", "phase": "\d+"},
     *     name="specialist_project_phase_goals_sustainability"
     * )
     * @Method({"GET", "POST"})
     *
     * @param Request           $request
     * @param SpecialistProject $specialistProject
     * @param int               $phase
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function goalsAndSustainabilityAction(
      Request $request,
      SpecialistProject $specialistProject,
      $phase
    ) {

        /** @var SpecialistProjectPhase $specialistProjectPhase */
        $specialistProjectPhase = $specialistProject->getPhases()->get($phase);

        /** @var ProjectGeneralInfo $projectGeneralInfo */
        $projectGeneralInfo = $specialistProjectPhase->getProjectGeneralInfo();

        $form = $this->createForm(
          'AppBundle\Form\Specialist\GoalsSustainabilityType',
          $projectGeneralInfo
        );
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($projectGeneralInfo);
            $em->flush();

            return $this->redirectToRoute(
              'specialist_project_phase_admin_panel',
              [
                'id'    => $specialistProject->getId(),
                'phase' => $phase,
              ]
            );
        }

        return $this->render(
          'project/specialist/wizard/goals_sustainability.html.twig',
          [
            'specialistProject'      => $specialistProject,
            'specialistProjectPhase' => $specialistProjectPhase,
            'phase'                  => $phase,
            'form'                   => $form->createView(),
            'goBack'                 => null,
          ]
        );

    }

    /**
     * @Route(
     *     "/{id}/{phase}/share",
     *     requirements={"id": "\d+", "phase": "\d+"},
     *     name="specialist_project_phase_share"
     * )
     * @Method({"POST"})
     *
     * @param Request                                   $request
     *
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @param \AppBundle\Entity\SpecialistProject       $specialistProject
     * @param                                           $phase
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function shareAction(
      Request $request,
      SpecialistProject $specialistProject,
      $phase
    ) {
        $em = $this->getDoctrine()->getManager();
        $userRepo = $em->getRepository('AppBundle:User');

        /** @var SpecialistProjectPhase $specialistProjectPhase */
        $specialistProjectPhase = $specialistProject->getPhases()->get($phase);

        /** @var ProjectGeneralInfo $projectGeneralInfo */
        $projectGeneralInfo = $specialistProjectPhase->getProjectGeneralInfo();

        $postData = $request->request->get('form');

        $shareWith = array_key_exists('shareWith', $postData)
          ? $postData['shareWith']
          : [];

        $unShareFrom = array_key_exists('unShareFrom', $postData)
          ? $postData['unShareFrom']
          : [];

        // Check if there are users to share project with
        if (!empty($shareWith)) {
            $sharedWithList = '<ul class="list-unstyled">';

            foreach ($shareWith as $userId) {
                $user = $userRepo->findOneBy(['id' => $userId]);

                // this is the way ManyToMany should be persisted
                $projectGeneralInfo->addAuthor($user);
                $user->addProject($projectGeneralInfo);

                $em->persist($user);

                $sharedWithList .= "<li>{$user->getName()} &lt;{$user->getEmail()}&gt;</li>";
            }

            $em->persist($projectGeneralInfo);
            $em->flush();

            $sharedWithList .= '</ul>';

            $this->addFlash(
              'success',
              '<p>Project has been shared successfully with the following users:</p>'
              .$sharedWithList
            );
        }

        // Check if there are users to un-share project from
        if (!empty($unShareFrom)) {
            $sharedWithList = '<ul class="list-unstyled">';

            foreach ($unShareFrom as $userId) {
                $user = $userRepo->findOneBy(['id' => $userId]);

                // this is the way ManyToMany should be removed
                $projectGeneralInfo->getAuthors()->removeElement($user);
                $user->getProjects()->removeElement($projectGeneralInfo);

                $em->persist($user);

                $sharedWithList .= "<li>{$user->getName()} &lt;{$user->getEmail()}&gt;</li>";
            }

            $em->persist($projectGeneralInfo);
            $em->flush();

            $sharedWithList .= '</ul>';

            $this->addFlash(
              'success',
              '<p>Project has been un-shared successfully from the following users:</p>'
              .$sharedWithList
            );
        }

        return $this->redirectToRoute(
          'specialist_project_phase_admin_panel',
          [
            'id'    => $specialistProject->getId(),
            'phase' => $phase,
          ]
        );
    }

    /**
     * @Route(
     *     "/{id}/{phase}",
     *     requirements={"id": "\d+", "phase": "\d+"},
     *     name="specialist_project_phase_show"
     * )
     *
     * @param SpecialistProject $specialistProject
     * @param int               $phase
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showAction(
      SpecialistProject $specialistProject,
      $phase
    ) {
        /** @var SpecialistProjectPhase $specialistProjectPhase */
        $specialistProjectPhase = $specialistProject->getPhases()->get($phase);

        return $this->render(
          'project/specialist/show.html.twig',
          [
            'specialistProject'      => $specialistProject,
            'specialistProjectPhase' => $specialistProjectPhase,
            'phase'                  => $phase,
          ]
        );
    }


    /**
     * Itinerary Dates
     *
     * @Route(
     *     "/{id}/{phase}/itinerary/dates",
     *     requirements={"id": "\d+", "phase": "\d+"},
     *     name="specialist_project_phase_itinerary_dates"
     * )
     * @Method({"GET", "POST"})
     *
     * @param SpecialistProject $specialistProject
     * @param int $phase
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function itineraryDatesAction(SpecialistProject $specialistProject, $phase, Request $request)
    {
        $specialistProjectPhase = $specialistProject->getPhases()->get($phase);

        $form = $this->createForm(
            'AppBundle\Form\Specialist\ItineraryDatesType',
            $specialistProjectPhase
        );
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();

//            dump($specialistProjectPhase->getStartDate()); exit;

            $dates = $this->dateRange($specialistProjectPhase->getStartDate(), $specialistProjectPhase->getEndDate());
            // Creates the DayBreakdownActivities records, or remove them according to dates
            if ($specialistProjectPhase->getDayBreakdownActivities()->count()) {
                // TODO: Compare, create and remove accordingly

            } else { // Just create itinerary
                foreach ($dates as $date) {
                    $dayBreakdownActivity = new DayBreakdownActivity();
                    $dayBreakdownActivity->setActivityDate($date);
                    $dayBreakdownActivity->setSpecialistProjectPhase($specialistProjectPhase);
                    $specialistProjectPhase->addDayBreakdownActivity($dayBreakdownActivity);
                }

            }
//            dump($specialistProjectPhase);
//            exit;

            $em->persist($specialistProjectPhase);
            $em->flush();

            $nextAction = 'specialist_project_phase_admin_panel';
            $urlParameters = array(
                'id' => $specialistProject->getId(),
                'phase' => $phase,
            );

            if ($form->get('next')->isClicked()) {
                $nextAction = 'specialist_project_phase_itinerary_activities';
            }

            return $this->redirectToRoute($nextAction, $urlParameters);
        }

        return $this->render(
            'project/specialist/wizard/itinerary_dates.html.twig',
            [
                'specialistProject'      => $specialistProject,
                'specialistProjectPhase' => $specialistProjectPhase,
                'phase'                  => $phase,
                'form'                   => $form->createView(),
                'goBack'                 => null,
            ]
        );

    }

    /**
     * Itinerary Activities
     *
     * @Route(
     *     "/{id}/{phase}/itinerary/activities",
     *     requirements={"id": "\d+", "phase": "\d+"},
     *     name="specialist_project_phase_itinerary_activities"
     * )
     * @Method({"GET", "POST"})
     *
     * @param SpecialistProject $specialistProject
     * @param int $phase
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function itineraryActivitiesAction(SpecialistProject $specialistProject, $phase, Request $request)
    {
        $specialistProjectPhase = $specialistProject->getPhases()->get($phase);

        // creates "anonymous" form
        // notice that using ItineraryActivitiesType (form) didn't work well
        $form = $this->createFormBuilder($specialistProjectPhase)
            ->add('dayBreakdownActivities', CollectionType::class, [
                'entry_type' => DayBreakdownActivityType::class,
            ])
            ->add('back', SubmitType::class, ['label' => 'Save and go back'])
            ->add('save', SubmitType::class, ['label' => 'Save and return to Panel'])
            ->add('next', SubmitType::class, ['label' => 'Save and continue'])
            ->getForm();

        $form->handleRequest($request);

        $prevStep = 'specialist_project_phase_itinerary_dates';

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($specialistProjectPhase);
            $em->flush();


            $nextAction = 'specialist_project_phase_admin_panel';
            $urlParameters = array(
                'id' => $specialistProject->getId(),
                'phase' => $phase,
            );

            if ($form->get('back')->isClicked()) {
                $nextAction = $prevStep;
            }

            return $this->redirectToRoute($nextAction, $urlParameters);
        }

        return $this->render(
            'project/specialist/wizard/itinerary_activities.html.twig',
            [
                'specialistProject'      => $specialistProject,
                'specialistProjectPhase' => $specialistProjectPhase,
                'phase'                  => $phase,
                'form'                   => $form->createView(),
                'goBack'                 => $prevStep,
            ]
        );

    }

    /**
     * Virtual Assignment Country
     *
     * @Route(
     *     "/{id}/{phase}/virtual-assignment/country",
     *     requirements={"id": "\d+", "phase": "\d+"},
     *     name="specialist_project_phase_virtual_assignment_country"
     * )
     * @Method({"GET", "POST"})
     *
     * @param SpecialistProject $specialistProject
     * @param int $phase
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function virtualAssignmentCountryAction(SpecialistProject $specialistProject, $phase, Request $request)
    {
        $specialistProjectPhase = $specialistProject->getPhases()->get($phase);
        $virtualAssignment = $specialistProjectPhase->getVirtualAssignments();

        if (!$virtualAssignment->count()) {
            $virtualAssignment = new VirtualAssignment();
            $virtualAssignment->setSpecialistProjectPhase($specialistProjectPhase);
            $specialistProjectPhase->addVirtualAssignment($virtualAssignment);
        } else {
            $virtualAssignment = $virtualAssignment->first();
        }

        $form = $this->createForm(
            'AppBundle\Form\Specialist\VirtualAssignmentCountryType',
            $virtualAssignment
        );

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $nextAction =  'specialist_project_phase_virtual_assignment_activities';
            $urlParameters = array(
                'id'    => $specialistProject->getId(),
                'phase' => $phase,
            );

            if ($form->get('save')->isClicked()) {
                $nextAction = 'specialist_project_phase_admin_panel';
            }

            $em->persist($virtualAssignment);
            $em->flush();

            // default save button
            return $this->redirectToRoute($nextAction, $urlParameters);
        }

        return $this->render(
            'project/specialist/wizard/virtual_assignment_country.html.twig',
            [
                'specialistProject' => $specialistProject,
                'specialistProjectPhase' => $specialistProjectPhase,
                'phase' => $phase,
                'form' => $form->createView(),
                'goBack' => null,
            ]
        );

    }

    /**
     * Virtual Assignment Duties
     *
     * @Route(
     *     "/{id}/{phase}/virtual-assignment/activities",
     *     requirements={"id": "\d+", "phase": "\d+"},
     *     name="specialist_project_phase_virtual_assignment_activities"
     * )
     * @Method({"GET", "POST"})
     *
     * @param SpecialistProject $specialistProject
     * @param int $phase
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function virtualAssignmentActivitiesAction(SpecialistProject $specialistProject, $phase, Request $request)
    {
        $specialistProjectPhase = $specialistProject->getPhases()->get($phase);
        $virtualAssignment = $specialistProjectPhase->getVirtualAssignments()->first();

        // creates empty activities to show in the form and assigns it to inCountryAssignment, if empty set
        $activitiesNumber = $virtualAssignment->getVirtualAssignmentActivities()->count();
        $activitiesDefault = 2;
        for ($i = $activitiesNumber; $i < $activitiesDefault; $i++) {
            $assignmentActivity = new VirtualAssignmentActivity();
            $assignmentActivity->setVirtualAssignment($virtualAssignment);
            $virtualAssignment->addVirtualAssignmentActivity($assignmentActivity);
        }

        $form = $this->createForm(
            'AppBundle\Form\Specialist\VirtualAssignmentType',
            $virtualAssignment
        );

        $form->handleRequest($request);

        $prevStep = 'specialist_project_phase_virtual_assignment_country';

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $nextAction =  'specialist_project_phase_virtual_assignment_activities';

            $urlParameters = array(
                'id'    => $specialistProject->getId(),
                'phase' => $phase,
            );

            if ('' !== $form->get('newHost')->getViewData()) {
                $host = new Host();
                $host->setName($form->get('newHost')->getViewData());
                $host->setCountry($virtualAssignment->getCountry());

                $em->persist($host);
                $em->flush();

                $virtualAssignment->setHost($host);
            }

            // If an activity item was selected for deletion, proceed
            if ('' !== $form->get('deleteId')->getViewData()) {
                if ($form->get('deleteId')->getViewData()) {
                    $activityToDelete = $em->getRepository('AppBundle:VirtualAssignmentActivity')->find($form->get('deleteId')->getViewData());
                    $em->remove($activityToDelete);
                    $em->flush();
                } else {
                    $activityToDelete = $virtualAssignment->getVirtualAssignmentActivities()->last();
                }
                $virtualAssignment->removeVirtualAssignmentActivity($activityToDelete);
            }

            // Adds a new (empty) activity if the blue button is clicked
            if ($form->get('add')->isClicked()) {
                $assignmentActivity = new VirtualAssignmentActivity();
                $assignmentActivity->setVirtualAssignment($virtualAssignment);
                $virtualAssignment->addVirtualAssignmentActivity($assignmentActivity);
            }

            if ($form->get('back')->isClicked()) {
                $nextAction = $prevStep;
            }

            if ($form->get('save')->isClicked() || $form->get('next')->isClicked()) {
                $nextAction = 'specialist_project_phase_admin_panel';
            }

            $em->persist($virtualAssignment);
            $em->flush();

            // default save button
            return $this->redirectToRoute($nextAction, $urlParameters);
        }

        return $this->render(
            'project/specialist/wizard/virtual_assignment_activities.html.twig',
            [
                'specialistProject' => $specialistProject,
                'specialistProjectPhase' => $specialistProjectPhase,
                'phase' => $phase,
                'form' => $form->createView(),
                'goBack' => $prevStep,
            ]
        );

    }

    /**
     * Generates page to confirm removal of virtual Assignment activity
     *
     * @Route(
     *     "/{id}/{phase}/virtual-assignment/activity/{aid}/delete",
     *     requirements={"id": "\d+", "phase": "\d+", "aid": "\d+"},
     *     name="specialist_project_phase_virtual_assignment_activity_confirm_delete"
     * )
     * @Method("GET")
     *
     * @ParamConverter("specialistProject", class="AppBundle:SpecialistProject", options={"mapping": {"id": "id"}})
     * @ParamConverter("virtualAssignmentActivity", class="AppBundle:VirtualAssignmentActivity", options={"mapping": {"aid": "id"}})
     *
     * @param SpecialistProject $specialistProject
     * @param int $phase
     * @param VirtualAssignmentActivity $virtualAssignmentActivity
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function virtualAssignmentActivityConfirmDeleteAction($specialistProject, $phase, $virtualAssignmentActivity)
    {
        // TODO: Validate that user has access to project Budget and that cost item belongs to it
        // TODO: Review the parameters and refactor if possible

        $deleteForm = $this->createDeleteForm('VirtualAssignmentActivity', [
            $specialistProject->getId(),
            $phase,
            $virtualAssignmentActivity->getId(),
        ]);

        return $this->render('project/specialist/wizard/virtual_assignment_activity_confirm_delete.html.twig', array(
            'specialistProject' => $specialistProject,
            'phase' => $phase,
            'virtualAssignmentActivity' => $virtualAssignmentActivity,
            'delete_form' => $deleteForm->createView(),
        ));

    }

    /**
     * Deletes a Custom cost item.
     *
     * @Route(
     *     "/{id}/{phase}/virtual-assignment/activity/{aid}/delete",
     *     requirements={"id": "\d+", "phase": "\d+", "aid": "\d+"},
     *     name="specialist_project_phase_virtual_assignment_activity_delete"
     * )
     * @Method("DELETE")
     *
     * @ParamConverter("specialistProject", class="AppBundle:SpecialistProject", options={"mapping": {"id": "id"}})
     * @ParamConverter("virtualAssignmentActivity", class="AppBundle:VirtualAssignmentActivity", options={"mapping": {"aid": "id"}})
     *
     * @param SpecialistProject $specialistProject
     * @param int $phase
     * @param VirtualAssignmentActivity $virtualAssignmentActivity
     *
     * @return null
     *
     */
    public function virtualAssignmentActivityDeleteAction($specialistProject, $phase, $virtualAssignmentActivity, Request $request)
    {
        $form =  $this->createDeleteForm('VirtualAssignmentActivity', [
            $specialistProject->getId(),
            $phase,
            $virtualAssignmentActivity->getId(),
        ]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($virtualAssignmentActivity);

            $em->flush();
        }

        return $this->redirectToRoute('specialist_project_phase_virtual_assignment_activities', [
            'id' => $specialistProject->getId(),
            'phase' => $phase,
        ]);
    }



    /**
     * Creates a form to delete an item.
     *
     * @param string
     * @param mixed
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($type, $parameters)
    {
        switch ($type) {
            case 'VirtualAssignmentActivity':
                $action = 'specialist_project_phase_virtual_assignment_activity_delete';
                $urlParameters = [
                    'id' => $parameters[0],
                    'phase' => $parameters[1],
                    'aid' => $parameters[2],
                ];
                break;

        }

        return $this->createFormBuilder()
            ->setAction(
                $this->generateUrl($action, $urlParameters)
            )
            ->setMethod('DELETE')
            ->getForm();
    }


    /**
     * Helper function to create a data range array
     *
     * @param \DateTime $first
     * @param \DateTime $last
     * @param string $step
     * @param string $format
     * @return array
     */
    protected function dateRange($first, $last, $step = '+1 day', $format = 'Y-m-d' ) {
        $last->modify($step);
        $periodInterval = new \DateInterval("P1D");
        $dates = new \DatePeriod($first, $periodInterval, $last);

        return $dates;
    }

}
