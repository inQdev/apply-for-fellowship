<?php

namespace AppBundle\Controller;

use AppBundle\Controller\RoleSwitcherInterface;
use AppBundle\Entity\SpecialistProject;
use AppBundle\Entity\ProjectGeneralInfo;
use AppBundle\Entity\ProjectReview;
use AppBundle\Entity\ProjectReviewStatus;
use AppBundle\Entity\SpecialistProjectPhase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

/**
 * Specialist Project Review Controller
 *
 * @package AppBundle\Controller
 */
class SpecialistProjectReviewController extends Controller implements RoleSwitcherInterface
{
    /**
     * @Route(
     *   "/project/specialist/{id}/{phase}/submit",
     *   requirements={"id": "\d+", "phase": "\d+"},
     *   name="specialist_project_submit"
     * )
     * @Method("POST")
     *
     * @param Request           $request
     * @param SpecialistProject $specialistProject
     * @param int               $phase
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function submitSpecialistProjectAction(
      Request $request,
      SpecialistProject $specialistProject,
      $phase
    ) {

        $authenticatedUser = $this->getUser();

        $form = $this->createForm('AppBundle\Form\Specialist\SubmitSpecialistProposalType');
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            /** @var SpecialistProjectPhase $specialistProjectPhase */
            $specialistProjectPhase = $specialistProject->getPhases()->get($phase);

            /** @var ProjectGeneralInfo $projectGeneralInfo */
            $projectGeneralInfo = $specialistProjectPhase->getProjectGeneralInfo();

            $projectGeneralInfo->setSubmissionStatus(
              ProjectGeneralInfo::PROJECT_SUBMITTED
            );
            $projectGeneralInfo->setSubmittedBy($authenticatedUser); // Saves the user that submits proposal

            $em->persist($projectGeneralInfo);

            $specialistProjectReview = new ProjectReview();
            // TODO: should relation between FellowProject and ProjectReview be 1:1?
            $specialistProjectReview->addSpecialistProject($specialistProjectPhase);

	        // Checks the role of the user submitting the proposal, and assign the right status
            $reviewStatus = ProjectReviewStatus::UNDER_RELO_REVIEW;
            if ($this->isGranted('ROLE_RELO')) {
                $reviewStatus = ProjectReviewStatus::UNDER_RPO_REVIEW;
            }

            $specialistProjectReview->setProjectReviewStatus($em->getRepository('AppBundle:ProjectReviewStatus')->find($reviewStatus));
/*
            $message = \Swift_Message::newInstance()
              ->setSubject('Specialist proposal submitted for your review')
              ->setFrom('proposal-no-reply@elprograms.org')
              ->setTo('flynn@inqbation.com')
              ->setCc('virginia.alvarez@inqbation.com')
              ->setBody(
                $this->renderView(
                  'email/proposal_submission.html.twig',
                  ['specialistProject' => $specialistProject]
                ),
                'text/html'
              );

            $this->get('mailer')->send($message);
*/
            $em->persist($specialistProjectReview);
            $em->flush();

            $this->addFlash(
              'success',
              'The Proposal has been submitted successfully.'
            );
        }

        return $this->redirectToRoute(
          'specialist_project_phase_admin_panel',
          [
            'id' => $specialistProject->getId(),
             'phase' => $phase
          ]
        );
    }

    /**
     * @Route("/project/fellow/{id}/review", name="fellow_project_review_index")
     * @Method("GET")
     *
     * @param \AppBundle\Entity\FellowProject $fellowProject
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(FellowProject $fellowProject)
    {
        return $this->render(
          'fellowprojectreview/index.html.twig',
          [
            'fellowProject'       => $fellowProject,
            'fellowProjectReview' => $fellowProject->getFellowProjectReviews()->first()
          ]
        );
    }
}