<?php

namespace AppBundle\Controller;

use HWI\Bundle\OAuthBundle\Controller\ConnectController as BaseConnectController;
use Symfony\Component\HttpFoundation\Request;

/**
 * ConnectController controller.
 *
 * @package AppBundle\Controller
 * @author  Juan Obando <juan.obando@inqbation.com>
 */
class ConnectController extends BaseConnectController
{
    /**
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function connectAction(Request $request)
    {
        return $this->redirectToRoute(
          'hwi_oauth_service_redirect',
          ['service' => 'cied']
        );
    }
}
