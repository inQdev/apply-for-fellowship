<?php

namespace AppBundle\Controller;

use AppBundle\Controller\RoleSwitcherInterface;
use AppBundle\Form\GlobalValueType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

/**
 * GlobalValue controller.
 *
 * @Route("/admin/global-value")
 */
class GlobalValueController extends Controller implements RoleSwitcherInterface
{
	/**
	 * Lists all GlobalValueFellow entities.
	 *
	 * @Route("/", name="proposal_global_value_index")
	 * @Method("GET")
	 *
	 * @return \Symfony\Component\HttpFoundation\Response
	 */
	public function indexAction()
	{
		$em = $this->getDoctrine()->getManager();

		$fellowGlobalValues = $em->getRepository('AppBundle:GlobalValueFellow')->findAll();
		$specialistGlobalValues = $em->getRepository('AppBundle:GlobalValueSpecialist')->findAll();

		return $this->render(
			'global_value/index.html.twig',
			[
				'fellowGlobalValues' => $fellowGlobalValues,
				'specialistGlobalValues' => $specialistGlobalValues,
			]
		);
	}

	/**
	 * Displays a form to edit an existing GlobalValueFellow entities.
	 *
	 * @Route("/fellow/edit", name="proposal_global_value_fellow_edit")
	 * @Method({"GET", "POST"})
	 *
	 * @param \Symfony\Component\HttpFoundation\Request $request
	 *
	 * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
	 */
	public function editFellowAction(Request $request)
	{
		$em = $this->getDoctrine()->getManager();

		$formData = new \stdClass();
		$formData->globalValues = $em->getRepository('AppBundle:GlobalValueFellow')->findAll();

		$form = $this->createFormBuilder($formData)
			->add('globalValues', CollectionType::class, [
				'entry_type' => GlobalValueType::class,
			])
			->add('submit', SubmitType::class, ['label' => 'Save'])
			->getForm();

		$form->handleRequest($request);

		if ($form->isSubmitted() && $form->isValid()) {
			foreach ($formData->globalValues as $globalValue) {
				$em->persist($globalValue);
			}
			$em->flush();

			return $this->redirectToRoute('proposal_global_value_index');
		}

		return $this->render(
			'global_value/edit.html.twig',
			[
				'form' => $form->createView(),
				'type' => 'Fellow'
			]
		);
	}

	/**
	 * Displays a form to edit an existing GlobalValueSpecialist entities.
	 *
	 * @Route("/specialist/edit", name="proposal_global_value_specialist_edit")
	 * @Method({"GET", "POST"})
	 *
	 * @param \Symfony\Component\HttpFoundation\Request $request
	 *
	 * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
	 */
	public function editSpecialistAction(Request $request)
	{
		$em = $this->getDoctrine()->getManager();

		$formData = new \stdClass();
		$formData->globalValues = $em->getRepository('AppBundle:GlobalValueSpecialist')->findAll();

		$form = $this->createFormBuilder($formData)
			->add('globalValues', CollectionType::class, [
				'entry_type' => GlobalValueType::class,
			])
			->add('submit', SubmitType::class, ['label' => 'Save'])
			->getForm();

		$form->handleRequest($request);

		if ($form->isSubmitted() && $form->isValid()) {
			foreach ($formData->globalValues as $globalValue) {
				$em->persist($globalValue);
			}
			$em->flush();

			return $this->redirectToRoute('proposal_global_value_index');
		}

		return $this->render(
			'global_value/edit.html.twig',
			[
				'form' => $form->createView(),
				'type' => 'Specialist'
			]
		);
	}
}
