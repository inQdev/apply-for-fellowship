<?php

namespace AppBundle\Controller;

use AppBundle\Entity\ContactPoint;
use AppBundle\Entity\ContactPointAdditional;
use AppBundle\Entity\ContactPointSecondary;
use AppBundle\Entity\CostItemCustom;
use AppBundle\Entity\CostItemCustomInKind;
use AppBundle\Entity\Country;
use AppBundle\Entity\FellowProject;
use AppBundle\Entity\Fellow\CycleFellow;
use AppBundle\Entity\Fellow\DutyActivityPrimary;
use AppBundle\Entity\Fellow\DutyActivitySecondary;
use AppBundle\Entity\FellowProjectBudgetCostItem;
use AppBundle\Entity\FellowProjectBudgetTotals;
use AppBundle\Entity\Host;
use AppBundle\Entity\LocationHostInfo;
use AppBundle\Entity\Post;
use AppBundle\Entity\ProjectGeneralInfo;
use AppBundle\Entity\ProjectReview;
use AppBundle\Entity\Region;
use AppBundle\Entity\User;
use AppBundle\Entity\WizardStepStatus;
use AppBundle\Form\Fellow\CreateProjectType as CreateFellowProjectType;
use AppBundle\Form\FellowProjectFilterType;
use AppBundle\Presenter\FellowProjectPresenter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Solarium\QueryType\Select\Result\FacetSet;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

/**
 * FellowProject controller.
 *
 * @package AppBundle\Controller
 * @author  Juan Obando <juan.obando@inqbation.com>
 * @author  Felipe Ceballos <felipe.ceballos@inqbation.com>
 *
 * @Route("/project/fellow")
 */
class FellowProjectController extends Controller implements RoleSwitcherInterface
{
    /**
     * @Route("/{id}/admin-panel", name="project_fellow_admin_menu")
     * @Method({"GET"})
     *
     * @param FellowProject $fellowProject
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function adminPanelAction(FellowProject $fellowProject) {

        $this->denyAccessUnlessGranted('edit', $fellowProject->getProjectGeneralInfo());

        $em = $this->getDoctrine()->getManager();

        $submitProjectForm = $this->createForm(
            'AppBundle\Form\SubmitFellowProposalType',
            null,
            [
                'action' => $this->generateUrl(
                    'fellow_project_submit',
                    ['id' => $fellowProject->getId()]
                ),
            ]
        );

        $shareProjectForm = $this->createFormBuilder()
            ->setMethod('POST')
            ->setAction(
                $this->generateUrl(
                    'project_fellow_share',
                    ['id' => $fellowProject->getId()]
                )
            )
            ->add('shareProject', SubmitType::class, ['label' => 'Save'])
            ->getForm();

        $authors = $fellowProject->getProjectGeneralInfo()->getAuthors()->toArray();

        // Include creator user in the authors list
        $authors[] = $fellowProject->getProjectGeneralInfo()->getCreatedBy();

        // Get all users with RELO and EMBASSY roles, excluding those who are
        // authors of this project already.
        $embassyRELOUsers = $em->getRepository('AppBundle:User')
            ->findAllByRoles(['ROLE_EMBASSY', 'ROLE_RELO'], $authors);

        // The data should be mapped in a way the JS auto-complete plugin
        // understands it.
        $embassyRELOUsers = array_map(
            function ($user) {
                return [
                    'id' => $user['id'],
                    'name' => trim($user['name'] . ' <' . $user['email'] . '>'),
                    'fullname' => $user['name'],
                    'email' => $user['email'],
                ];
            },
            $embassyRELOUsers
        );

        return $this->render(
            'fellowproject/admin_panel.html.twig',
            [
                'fellowProject' => $fellowProject,
                'fellowProjectPresenter' => new FellowProjectPresenter(
                    $fellowProject
                ),
                'form' => $submitProjectForm->createView(),
                'embassyRELOUsers' => json_encode($embassyRELOUsers),
                'shareProjectForm' => $shareProjectForm->createView(),
            ]
        );
    }

    /**
     * @Route("/", name="proposal_fellow_index")
     * @Method({"GET"})
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction()
    {
        $authenticatedUser = $this->getUser();
        $em = $this->getDoctrine()->getManager();

        // Retrieve all Fellow Projects created/own by the authenticated user.
        // TODO: This info should be retrieved from FellowProject, not ProjectGeneralInfo one.
        $fellowProposalsGeneralInfo = $em
            ->getRepository('AppBundle:ProjectGeneralInfo')
            ->findAllFellowProposalsByUser($authenticatedUser);

        $sharedProposals = $authenticatedUser->getProjects()->toArray();

        // Projects associated to user include both Fellow and Specialist ones.
        // For this action, only Fellow ones are required.
        $sharedProposals = array_filter(
            $sharedProposals,
            function ($sharedProposal) {
                return null !== $sharedProposal->getFellowProject();
            }
        );

        return $this->render(
            'fellowproject/index.html.twig',
            [
                'fellowProposalsGeneralInfo' => $fellowProposalsGeneralInfo,
                'sharedProposals' => $sharedProposals,
            ]
        );
    }

    /**
     * Browse Fellow projects.  The projects will be retrieved based on the
     * authenticated user active role.
     *
     * @Route("/browse", name="fellow_project_browse")
     * @Method({"GET"})
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function browseAction(Request $request)
    {
        /** @var User $authenticatedUser */
        $authenticatedUser = $this->getUser();
        $em = $this->getDoctrine()->getManager();
        $currentCycle = $em->getRepository(CycleFellow::class)->getCurrentCycle();

        $cycleForm = $this->get('form.factory')
            ->createNamedBuilder('cycle', EntityType::class, $currentCycle, [
                'method' => 'GET',
                'class' => CycleFellow::class,
                'choice_label' => 'name',
            ])
            ->getForm();

        $cycleForm->handleRequest($request);

        $fellowProjectRepo = $this->get('proposal.solr_repository.fellow_project');

        $allowedSorts = [
            'reference_number_s',
            'proposal_status_s',
            'proposal_outcome_s',
            'region_s',
            'relo_location_s',
            'country_s',
            'host_institution_s',
            'cycle_season_s',
        ];

        $formData = $request->query->get('fellow_project_filter', []);
        $criteria = [
            'cycle_id_i' => [$cycleForm->getData()->getId()],
        ];
        $facetFields = ['country_s', 'cycle_season_s', 'proposal_status_s', 'proposal_outcome_s'];
        $results = null;
        $totalECAContribution = null;

        // If user's active role is RELO, projects should be filtered by his/her
        // RELO location associated.
        if ($this->isGranted('ROLE_RELO')) {
            $terms = $authenticatedUser->getReloLocations()->map(function ($reloLocation) {
                return (string) $reloLocation;
            })->toArray();

            if (count($terms)) {
                $criteria['relo_location_s'] = $terms;
            } else {
                $results = [
                    'facet_set' => new FacetSet([]),
                    'fellow_projects' => [],
                ];
            }
        } elseif ($this->isGranted('ROLE_RPO')) {
            $terms = $authenticatedUser->getRegions()->map(function ($region) {
                return $region->getAcronym();
            })->toArray();

            if (count($terms)) {
                $criteria['region_s'] = $terms;
            } else {
                $results = [
                    'facet_set' => new FacetSet([]),
                    'fellow_projects' => [],
                ];
            }

            $facetFields[] = 'relo_location_s';

            $totalECAContribution = $em
              ->getRepository('AppBundle:FellowProjectBudget')
              ->getTotalECAContributionInRegionsInCycle($authenticatedUser->getRegions(), $cycleForm->getData());
        } elseif ($this->isGranted('ROLE_STATE_DEPARTMENT') || $this->isGranted('ROLE_ADMIN')) {
            $facetFields[] = 'relo_location_s';
            $facetFields[] = 'region_s';

            $totalECAContribution = $em
              ->getRepository('AppBundle:FellowProjectBudget')
              ->getTotalECAContributionInCycle($cycleForm->getData());
        } else {
            $results = [
                'facet_set' => new FacetSet([]),
                'fellow_projects' => [],
            ];
        }

        foreach ($facetFields as $filter) {
            if (isset($formData[$filter])) {
                $criteria[$filter] = $formData[$filter];
            }
        }

        if (!$results) {
            $results = $fellowProjectRepo->getFacetedResultsForSearch($criteria, $facetFields, $allowedSorts, 1, $request->query->get('sort'), $request->query->get('sortDirection'));
        }

        $form = $this->createForm(FellowProjectFilterType::class, [], [
            'facet_set' => $results['facet_set'],
        ]);

        $form->handleRequest($request);

        return $this->render(
          'fellowproject/browse.html.twig',
          [
            'totalECAContribution' => $totalECAContribution,
            'fellowProjects'       => $results['fellow_projects'],
            'cycleForm'            => $cycleForm->createView(),
            'form'                 => $form->createView(),
          ]
        );
    }

    /**
     * @Route("/test", name="project_fellow_test")
     */
    public function testAction()
    {
        $api_request = $this->get('proposal.drupal_data_consumer');
        $response = $api_request->sendRequest(
            'GET',
            '/user-data?email=luis.cuellar@inqbation.com'
        );

        return $this->render(
            'fellowproject/test.html.twig',
            [
                'output' => $response,
            ]
        );
    }

    /**
     * @Route("/testpdf", name="project_fellow_testpdf")
     */
    public function testPdfAction(Request $request)
    {
        $pageUrl = $this->generateUrl('proposal_fellow_index', array(), UrlGeneratorInterface::ABSOLUTE_URL);
        //$knp_snappy_pdf = $this->get('knp_snappy.pdf');
        //$request = Request::createFromGlobals();
        //$session = $request->getSession();
        //$session->save();
        //$cookie = 'PHPSESSID='.$request->cookies->get('PHPSESSID');
        //$PHPSESSID = $session->getName().'='.$session->getId();

        $session = $this->get('session');
        $session->save();
        session_write_close();

        $PHPSESSID = $session->getId();

        $output = $this->get('knp_snappy.pdf')->getOutput($pageUrl, array(
            'cookie' => array(
                'PHPSESSID' => $PHPSESSID,
            ),
            'zoom' => 0.8,
        )
        );

        return new Response(
            $output,
            200,
            array(
                'Content-Type' => 'application/pdf',
                'Content-Disposition' => sprintf('attachment; filename="file.pdf"'),
            )
        );
        // $knp_snappy_pdf->setOption(
        //     'custom-header',
        //         array(
        //           'cookie' => $cookie
        //           )
        // );

        // return new Response(
        //   $knp_snappy_pdf->getOutput($pageUrl,
        //     array(
        //       // 'cookie' => array($session->getName() => $session->getId()),
        //       // 'disable-javascript' => true
        //     )
        //    ), 200,
        //     array(
        //       'Content-Type' => 'application/pdf',
        //       'Content-Disposition' => 'attachment; filename="my_name.pdf"'
        //     )
        //   );

        // $knp_snappy_pdf->getOutput(
        //   'http://proposal-local.elprograms.org:8000/app_dev.php/project/fellow/',
        //   array(),
        // '/./test3.pdf');

    }

    /**
     * Export the fellow form in pdf format.
     *
     * @Route(
     *     "/{id}/export-pdf",
     *     requirements={"id": "\d+"},
     *     name="project_fellow_export_pdf"
     * )
     * @Method("GET")
     *
     * @param FellowProject $fellowProject
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function exportPdfAction(Request $request, FellowProject $fellowProject)
    {

        $id = $fellowProject->getId();

        $pageUrl = $this->generateUrl('project_fellow_show', array('id' => $id), UrlGeneratorInterface::ABSOLUTE_URL);
        $pageUrl .= '?print=1';

        $session = $this->get('session');
        $session->save();
        session_write_close();

        $PHPSESSID = $session->getId();

        $output = $this->get('knp_snappy.pdf')->getOutput($pageUrl, array(
            'cookie' => array(
                'PHPSESSID' => $PHPSESSID,
            ),
            'zoom' => 0.8,
            'run-script' => "javascript:(\$(function(){ \$('#header, #review-navigation').hide()   ;}))",
        )
        );

        return new Response(
            $output,
            200,
            array(
                'Content-Type' => 'application/pdf',
                'Content-Disposition' => sprintf('attachment; filename="fellow-proposal.pdf"'),
            )
        );

    }

    /**
     * @Route("/new", name="project_fellow_new")
     * @Method({"GET", "POST"})
     *
     * @param Request $request The request.
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function newAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $regions = $em->getRepository(Region::class)->findAll();
        $regionsData = [];

        // This chunck of code will store the data to build dynamically the
        // dependent drop-downs.
        /** @var Region $region */
        foreach ($regions as $region) {
            $regionsData[] = [
                'id'        => $region->getId(),
                'name'      => $region->getName(),
                'countries' => $region->getCountries()
                    ->filter(
                        function (Country $country) {
                            return !$country->getReloLocations()->isEmpty();
                        }
                    )->map(
                        function (Country $country) {
                            return [
                                'id'    => $country->getId(),
                                'name'  => str_replace(['\\', "'"], ['\\\\', ''], $country->getName()),
                                'posts' => $country->getPosts()
                                    ->map(
                                        function (Post $post) {
                                            return [
                                                'id'   => $post->getId(),
                                                'name' => str_replace(['\\', "'"], ['\\\\', ''], $post->getName()),
                                            ];
                                        }
                                    )
                                    ->toArray(),
                            ];
                        }
                    )
                    ->toArray(),
            ];
        }

        /** @var CycleFellow $currentFellowCycle */
        $currentFellowCycle = $em->getRepository(CycleFellow::class)->getCurrentCycle();

        $projectGeneralInfo = new ProjectGeneralInfo($currentFellowCycle);
        $fellowProject = new FellowProject($projectGeneralInfo);

        // Statement needed to render a the form without a persisted entity.
        $projectGeneralInfo->setFellowProject($fellowProject);

        $form = $this->createForm(
            CreateFellowProjectType::class,
            $projectGeneralInfo
        );
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var Country $country */
            $country = $form->get('country')->getData();

            $projectGeneralInfo->addCountry($country);
            $projectGeneralInfo->setReloLocation(
                $country->getReloLocations()->first()
            );
            $projectGeneralInfo->addPost($form->get('post')->getData());

            $fellowProject->getLocationHostInfo()->setCountry($country);

            $em->persist($fellowProject);
            $em->flush();

            // Validate `General Information` section
            // TODO: should be this placed in a EventListener? Service?
            $projectGeneralInfo->setProjectGeneralInfoStepStatus($em);

            // Update the Solr index
            // TODO: should be this placed in a EventListener? Service?
            $this->get('solr.client')->updateDocument($fellowProject);

            return $this->redirectToRoute(
                'project_fellow_admin_menu',
                ['id' => $fellowProject->getId()]
            );
        }

        return $this->render(
            ':fellowproject/wizard:new.html.twig',
            [
                'form'               => $form->createView(),
                'currentFellowCycle' => $currentFellowCycle,
                'regionsData'        => json_encode($regionsData),
                'goBack'             => null,
            ]
        );
    }

    /**
     * @Route("/{id}/general-info/add", name="project_fellow_add_general_info")
     * @Method({"GET"})
     *
     * @param FellowProject $fellowProject
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function addProjectGeneralInfoAction(FellowProject $fellowProject)
    {
        if (null === $fellowProject->getProjectGeneralInfo()) {
            $fellowProject->setProjectGeneralInfo(new ProjectGeneralInfo());

            $em = $this->getDoctrine()->getManager();
            $em->persist($fellowProject);
            $em->flush();
        }

//        $this->get('logger')->info("Fellow Proposal {$fellowProject->getId()} ({$projectGeneralInfo->getReferenceNumber()}) created.");
        return $this->redirectToRoute(
            'project_fellow_region',
            ['id' => $fellowProject->getId()]
        );
    }

    /**
     * @Route("/{id}/contact-points/add", name="project_fellow_add_contact_points")
     * @Method({"GET"})
     *
     * @param FellowProject $fellowProject
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function addContactPointsAction(FellowProject $fellowProject)
    {
        if (null === $fellowProject->getProjectGeneralInfo()) {
            $fellowProject->setProjectGeneralInfo(new ProjectGeneralInfo());
        }

        $projectGeneralInfo = $fellowProject->getProjectGeneralInfo();

        if ($projectGeneralInfo->getContactPoints()->isEmpty()) {
            $contactPointPrimary = new ContactPoint();
            $contactPointPrimary->setProjectGeneralInfo($projectGeneralInfo);

            $contactPointSecondary = new ContactPointSecondary();
            $contactPointSecondary->setProjectGeneralInfo($projectGeneralInfo);

            $em = $this->getDoctrine()->getManager();
            $em->persist($contactPointSecondary);
            $em->persist($contactPointPrimary);
            $em->flush();
        }

        return $this->redirectToRoute(
            'project_fellow_contact_points',
            ['id' => $fellowProject->getId()]
        );
    }

    /**
     * Deletes an additional contact point
     *
     * @Route("/{pid}/contactpoint/{cid}/delete", name="fellow_project_contact_point_delete")
     * @Method("DELETE")
     *
     * @ParamConverter("fellowProject", class="AppBundle:FellowProject", options={"mapping": {"pid": "id"}})
     * @ParamConverter("contactPointAdditional", class="AppBundle:ContactPointAdditional", options={"mapping": {"cid":
     *                                           "id"}})
     *
     * @param Request                $request
     * @param FellowProject          $fellowProject
     * @param ContactPointAdditional $contactPointAdditional
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteContactPointAction(
        Request $request,
        FellowProject $fellowProject,
        ContactPointAdditional $contactPointAdditional
    ) {
        $form = $this->createDeleteContactPointForm($contactPointAdditional);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($contactPointAdditional);
            $em->flush();
        }

        return $this->redirectToRoute(
            'project_fellow_contact_points',
            ['id' => $fellowProject->getId()]
        );
    }

    /**
     * Creates a form to delete an additional contact point
     *
     * @param ContactPointAdditional $contactPointAdditional
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteContactPointForm($contactPointAdditional)
    {
        return $this->createFormBuilder()
            ->setAction(
                $this->generateUrl(
                    'fellow_project_contact_point_delete',
                    [
                        'pid' => $contactPointAdditional->getProjectGeneralInfo()
                            ->getFellowProject()
                            ->getId(),
                        'cid' => $contactPointAdditional->getId(),
                    ]
                )
            )
            ->setMethod('DELETE')
            ->getForm();
    }

    /**
     * Deletes a Fellowship Duty
     *
     * @Route("/{pid}/fellowship_duty/{cid}/delete", name="fellow_project_fellowship_duty_delete")
     * @Method("DELETE")
     *
     * @ParamConverter("fellowProject", class="AppBundle:FellowProject", options={"mapping": {"pid": "id"}})
     * @ParamConverter("fellowshipDutyActivity", class="AppBundle:FellowshipDutyActivity", options={"mapping": {"cid":
     *                                           "id"}})
     *
     * @param Request                $request
     * @param FellowProject          $fellowProject
     * @param FellowshipDutyActivity $fellowshipDutyActivity
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteFellowshipDutyActivityAction(
        Request $request,
        FellowProject $fellowProject,
        FellowshipDutyActivity $fellowshipDutyActivity
    ) {
        $form = $this->createDeleteFellowshipDutyActivityForm(
            $fellowshipDutyActivity
        );
        $form->handleRequest($request);

        if (!$fellowshipDutyActivity->getFellowshipDuty()->getType() == 1) {
            $routeToRedirect = 'project_fellow_fellowship_duty_primary';
        } else {
            $routeToRedirect = 'project_fellow_fellowship_duty_secondary';
        }

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($fellowshipDutyActivity);
            $em->flush();
        }

        return $this->redirectToRoute(
            $routeToRedirect,
            ['id' => $fellowProject->getId()]
        );
    }

    /**
     * Creates a form to delete a fellowship duty
     *
     * @param FellowshipDutyActivity $fellowshipDutyActivity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteFellowshipDutyActivityForm(
        $fellowshipDutyActivity
    ) {
        return $this->createFormBuilder()
            ->setAction(
                $this->generateUrl(
                    'fellow_project_fellowship_duty_delete',
                    [
                        'pid' => $fellowshipDutyActivity->getFellowshipDuty()
                            ->getFellowProject()
                            ->getId(),
                        'cid' => $fellowshipDutyActivity->getId(),
                    ]
                )
            )
            ->setMethod('DELETE')
            ->getForm();
    }

    /**
     * @Route("/{id}/region", name="project_fellow_region")
     * @Method({"GET", "POST"})
     *
     * @param Request       $request
     * @param FellowProject $fellowProject
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function regionAction(Request $request, FellowProject $fellowProject)
    {
        /** @var ProjectGeneralInfo $projectGeneralInfo */
        $projectGeneralInfo = $fellowProject->getProjectGeneralInfo();

        $this->denyAccessUnlessGranted('edit', $projectGeneralInfo);

        $form = $this->createForm(
            'AppBundle\Form\RegionType',
            $projectGeneralInfo
        );
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($projectGeneralInfo);
            $em->flush();

            // Validate `General Information` section
            // TODO: should be this placed in a EventListener? Service?
            $projectGeneralInfo->setProjectGeneralInfoStepStatus($em);

            // Update the Solr index
            // TODO: should be this placed in a EventListener? Service?
            $this->get('solr.client')->updateDocument($fellowProject);

            $nextAction = 'project_fellow_admin_menu';

            if ($form->get('next')->isClicked()) {
                // A Region must be selected to continue with the wizard
                if ('' === $form->get('region')->getViewData()) {
                    $this->addFlash('no_region', 'Please select a Region');

                    $nextAction = 'project_fellow_region';
                } else {
                    $nextAction = 'project_fellow_country';
                }
            }

            return $this->redirectToRoute(
                $nextAction,
                ['id' => $fellowProject->getId()]
            );
        }

        return $this->render(
            'fellowproject/wizard/region.html.twig',
            [
                'fellowProject' => $fellowProject,
                'form' => $form->createView(),
                'goBack' => null,
            ]
        );
    }

    /**
     * @Route("/{id}/country", name="project_fellow_country")
     * @Method({"GET", "POST"})
     *
     * @param Request       $request
     * @param FellowProject $fellowProject
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function countryAction(Request $request, FellowProject $fellowProject)
    {
        /** @var ProjectGeneralInfo $projectGeneralInfo */
        $projectGeneralInfo = $fellowProject->getProjectGeneralInfo();

        $this->denyAccessUnlessGranted('edit', $projectGeneralInfo);

        $form = $this->createForm('AppBundle\Form\CountryType', $projectGeneralInfo);
        $form->handleRequest($request);

        // If no region has been selected for the project, set flash message
        // notifying user that one must be selected
        if (null === $projectGeneralInfo->getRegion()) {
            $errorMsg = 'Please, select a Region from the <a class="alert-link" href="'
            . $this->generateUrl(
                'project_fellow_region',
                ['id' => $fellowProject->getId()]
            )
                . '">the previous step</a>';

            $this->addFlash('danger', $errorMsg);
        }

        if ($form->isSubmitted() && $form->isValid()) {
            $nextAction = 'project_fellow_admin_menu';

            // Country submitted?
            $newCountryId = $request->request->get('country')['countries'];

            // Check if country has been selected from the Countries drop down
            // list.
            if ($newCountryId) {
                $em = $this->getDoctrine()->getManager();

                // Fellow Proposal should be associated to one country only.
                // Clearing countries array will ensure no more than one country
                // is associated to it.
                $projectGeneralInfo->getCountries()->clear();

                $country = $em->getRepository('AppBundle:Country')->find($newCountryId);

                $projectGeneralInfo->addCountry($country);

                $em->persist($projectGeneralInfo);
                $em->flush();

                // Validate `General Information` section
                // TODO: should be this placed in a EventListener? Service?
                $projectGeneralInfo->setProjectGeneralInfoStepStatus($em);

                // Update the Solr index
                // TODO: should be this placed in a EventListener? Service?
                $this->get('solr.client')->updateDocument($fellowProject);

                if ($form->get('next')->isClicked()) {
                    $nextAction = 'project_fellow_post_relo';
                } elseif ($form->get('back')->isClicked()) {
                    $nextAction = 'project_fellow_region';
                }
            }
            // The first time ever users enter to this section, they'll be
            // allowed to go back or return to Admin Panel without selecting a
            // Country from the drop down list.
            elseif ($projectGeneralInfo->getCountries()->isEmpty()) {
                if ($form->get('next')->isClicked()) {
                    $this->addFlash('no_country', 'Please select a Country');

                    $nextAction = 'project_fellow_country';
                } elseif ($form->get('back')->isClicked()) {
                    $nextAction = 'project_fellow_region';
                }
            }
            // After the first time, users will always be requested to select a
            // country from the drop down list, no matter the button they
            // clicked.
            else {
                $this->addFlash('no_country', 'Please select a Country');

                $nextAction = 'project_fellow_country';
            }

            return $this->redirectToRoute(
                $nextAction,
                ['id' => $fellowProject->getId()]
            );
        }

        return $this->render(
            'fellowproject/wizard/country.html.twig',
            [
                'fellowProject' => $fellowProject,
                'form' => $form->createView(),
                'goBack' => 'project_fellow_region',
            ]
        );
    }

    /**
     * @Route("/{id}/post-relo", name="project_fellow_post_relo")
     * @Method({"GET", "POST"})
     *
     * @param Request       $request
     * @param FellowProject $fellowProject
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function postReloAction(Request $request, FellowProject $fellowProject)
    {
        $em = $this->getDoctrine()->getManager();

        /** @var ProjectGeneralInfo $projectGeneralInfo */
        $projectGeneralInfo = $fellowProject->getProjectGeneralInfo();

        $this->denyAccessUnlessGranted('edit', $projectGeneralInfo);

        /** @var CycleFellow $currentFellowCycle */
        $currentFellowCycle = $em->getRepository('AppBundle:Fellow\CycleFellow')->getCurrentCycle();

        $form = $this->createForm(
            'AppBundle\Form\PostReloType',
            $projectGeneralInfo
        );
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // Post submitted?
            $newPostId = $request->request->get('post_relo')['post'];

            // Check if post has been selected from the Countries drop down
            // list.
            if ($newPostId) {
                // Fellow Proposal should be associated to one Post only.
                // Clearing countries array will ensure no more than one country
                // is associated to it.
                $projectGeneralInfo->getPosts()->clear();

                $post = $em->getRepository('AppBundle:Post')->find($newPostId);

                if ($post) {
                    $projectGeneralInfo->addPost($post);
                }
            }

            $em->persist($fellowProject);
            $em->flush();

            $projectGeneralInfo->setProjectGeneralInfoStepStatus($em);

            $nextAction = 'project_fellow_admin_menu';

            if ($form->get('next')->isClicked()) {
                if ('' === $form->get('fellowProject')->get('proposedStartDate')->getViewData()) {
                    $this->addFlash(
                        'no_proposal_start_date',
                        'Please enter the start date for the project'
                    );

                    $nextAction = 'project_fellow_post_relo';
                } else {
                    $nextAction = 'project_fellow_date_comments';
                }
            } elseif ($form->get('back')->isClicked()) {
                $nextAction = 'project_fellow_country';
            }

            return $this->redirectToRoute(
                $nextAction,
                ['id' => $fellowProject->getId()]
            );
        }

        return $this->render(
            'fellowproject/wizard/post_relo.html.twig',
            [
                'fellowProject' => $fellowProject,
                'currentFellowCycle' => $currentFellowCycle,
                'form' => $form->createView(),
                'goBack' => 'project_fellow_country',
            ]
        );
    }

    /**
     * @Route(
     *     "/{id}/date-comments",
     *     requirements={"id": "\d+"},
     *     name="project_fellow_date_comments"
     * )
     * @Method({"GET", "POST"})
     *
     * @param Request       $request
     * @param FellowProject $fellowProject
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function dateCommentsAction(
        Request $request,
        FellowProject $fellowProject
    ) {
        $this->denyAccessUnlessGranted('edit', $fellowProject->getProjectGeneralInfo());

        $form = $this->createForm('AppBundle\Form\DateCommentsType', $fellowProject);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($fellowProject);
            $em->flush();

            // Validate `General Information` section
            // TODO: should be this placed in a EventListener? Service?
            $fellowProject->getProjectGeneralInfo()->setProjectGeneralInfoStepStatus($em);

            $nextAction = $form->get('back')->isClicked()
              ? 'project_fellow_post_relo'
              : 'project_fellow_admin_menu';

            return $this->redirectToRoute(
                $nextAction,
                ['id' => $fellowProject->getId()]
            );
        }

        return $this->render(
            'fellowproject/wizard/date_comments.html.twig',
            [
                'fellowProject' => $fellowProject,
                'form' => $form->createView(),
                'goBack' => 'project_fellow_post_relo',
            ]
        );
    }

    /**
     * @Route("/{id}/contact-points", name="project_fellow_contact_points")
     * @Method({"GET", "POST"})
     *
     * @param Request $request
     * @param FellowProject           $fellowProject
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function contactPointsAction(Request $request, FellowProject $fellowProject)
    {
        /** @var ProjectGeneralInfo $projectGeneralInfo */
        $projectGeneralInfo = $fellowProject->getProjectGeneralInfo();

        $this->denyAccessUnlessGranted('edit', $projectGeneralInfo);

        $form = $this->createForm('AppBundle\Form\ContactPointsType', $projectGeneralInfo);
        $form->handleRequest($request);

        $deleteForm = false; // to have something to pass to the template

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $nextAction = 'project_fellow_admin_menu';

            if ($form->get('additional')->isClicked()) {
                $nextAction = 'project_fellow_contact_points';

                $contactPointAdditional = new ContactPointAdditional($projectGeneralInfo);
                $projectGeneralInfo->addContactPoint($contactPointAdditional);
            }

            $em->persist($projectGeneralInfo);
            $em->flush();

            // Validate `Points of Contact` section
            // TODO: should be this placed in a EventListener? Service?
            $projectGeneralInfo->setContactPointsSectionStatus($em);

            return $this->redirectToRoute(
                $nextAction,
                ['id' => $fellowProject->getId()]
            );
        }

        $contacPointsQty = count(
            $fellowProject->getProjectGeneralInfo()->getContactPoints()
        );

        if (is_a(
            $fellowProject->getProjectGeneralInfo()->getContactPoints(
            )[$contacPointsQty - 1],
            'AppBundle\Entity\ContactPointAdditional'
        )) {
            $deleteForm = $this->createDeleteContactPointForm(
                $fellowProject->getProjectGeneralInfo()->getContactPoints(
                )[$contacPointsQty - 1]
            );
        }

        return $this->render(
          'fellowproject/wizard/contact_points.html.twig',
          [
            'fellowProject'  => $fellowProject,
            'form'           => $form->createView(),
            'goBack'         => null,
            'delete_form'    => ($deleteForm) ? $deleteForm->createView() : false,
            'fellow_project' => $fellowProject,
          ]
        );
    }

    /**
     * @Route(
     *     "/{id}/requirements-restrictions",
     *     requirements={"id": "\d+"},
     *     name="project_fellow_requirements"
     * )
     * @Method({"GET", "POST"})
     *
     * @param Request       $request
     * @param FellowProject $fellowProject
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function requirementsAction(Request $request, FellowProject $fellowProject)
    {
        /** @var $projectGeneralInfo ProjectGeneralInfo */
        $projectGeneralInfo = $fellowProject->getProjectGeneralInfo();

        $this->denyAccessUnlessGranted('edit', $projectGeneralInfo);

        $form = $this->createForm('AppBundle\Form\Fellow\RequirementsType', $projectGeneralInfo);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($projectGeneralInfo);
            $em->flush();

            return $this->redirectToRoute(
              'project_fellow_admin_menu',
              ['id' => $fellowProject->getId()]
            );
        }

        return $this->render(
          'fellowproject/wizard/requirements.html.twig',
          [
            'fellowProject' => $fellowProject,
            'form'          => $form->createView(),
            'goBack'        => null,
          ]
        );
    }

    /**
     * @Route(
     *     "/{id}/dependents",
     *     requirements={"id": "\d+"},
     *     name="project_fellow_project_house_info"
     * )
     * @Method({"GET", "POST"})
     *
     * @param Request $request
     * @param FellowProject           $fellowProject
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function dependentsAction(Request $request, FellowProject $fellowProject)
    {
        $this->denyAccessUnlessGranted('edit', $fellowProject->getProjectGeneralInfo());

        return $this->wizardStepHandler(
            [
                $request,
                $fellowProject,
                $fellowProject,
                'Fellow\DependentsType',
                'dependents',
                null,
                'project_fellow_admin_menu',
                [],
            ]
        );
    }

    /**
     * @Route("/{id}/certifications", name="project_fellow_certifications")
     * @Method({"GET", "POST"})
     *
     * @param Request $request
     * @param FellowProject           $fellowProject
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function certificationsAction(
        Request $request,
        FellowProject $fellowProject
    ) {
        $this->denyAccessUnlessGranted('edit', $fellowProject->getProjectGeneralInfo());

        return $this->wizardStepHandler(
            [
                $request,
                $fellowProject,
                $fellowProject->getProjectGeneralInfo(),
                'CertificationsType',
                'certifications',
                null,
                'project_fellow_admin_menu',
                [],
            ]
        );
    }

    /**
     * @Route(
     *     "/{id}/location-host",
     *     requirements={"id": "\d+"},
     *     name="project_fellow_location_host_info"
     * )
     * @Method({"GET", "POST"})
     *
     * @param Request       $request
     * @param FellowProject $fellowProject
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function locationHostInfoAction(Request $request, FellowProject $fellowProject)
    {
        $this->denyAccessUnlessGranted('edit', $fellowProject->getProjectGeneralInfo());

        /** @var LocationHostInfo $locationHostInfo */
        $locationHostInfo = $fellowProject->getLocationHostInfo();

        $em = $this->getDoctrine()->getManager();

        /** @var CycleFellow $currentFellowCycle */
        $currentFellowCycle = $em->getRepository('AppBundle:Fellow\CycleFellow')->getCurrentCycle();

        $form = $this->createForm('AppBundle\Form\LocationHostInfoType', $locationHostInfo);
        $form->handleRequest($request);

        // If no country hasn't been selected for the project, set flash message
        // notifying user that one must be selected
        if (null === $locationHostInfo->getCountry()) {
            $errorMsg = 'Please, select a Country from <a class="alert-link" href="'
            . $this->generateUrl(
                'project_fellow_region',
                ['id' => $fellowProject->getId()]
            )
                . '">General Information</a> in the Administration Panel.';

            $this->addFlash('danger', $errorMsg);
        }

        if ($form->isSubmitted() && $form->isValid()) {
            // Check if user entered a custom Host instead of select one from
            // the Hosts dropdown list.
            if ('' !== $form->get('hostCustom')->getViewData()) {
                $host = new Host();
                $host->setName($form->get('hostCustom')->getViewData());
                $host->setCountry($locationHostInfo->getCountry());

                $em->persist($host);
                $em->flush();

                $locationHostInfo->setHost($host);
            }

            $em->persist($locationHostInfo);
            $em->flush();

            // Update the Solr index
            // TODO: should be this placed in a EventListener? Service?
            $this->get('solr.client')->updateDocument($fellowProject);

            return $this->redirectToRoute(
                'project_fellow_admin_menu',
                ['id' => $fellowProject->getId()]
            );
        }

        return $this->render(
          'fellowproject/wizard/location_host_info.html.twig',
          [
            'fellowProject'      => $fellowProject,
            'currentFellowCycle' => $currentFellowCycle,
            'form'               => $form->createView(),
            'goBack'             => null,
          ]
        );
    }

    /**
     * @Route(
     *     "/{id}/fellowship-duties",
     *     requirements={"id": "\d+"},
     *     name="fellow_project_fellowship_duties"
     * )
     * @Method({"GET", "POST"})
     *
     * @param Request       $request
     * @param FellowProject $fellowProject
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function fellowshipDutiesActivity(
        Request $request,
        FellowProject $fellowProject
    ) {
        $this->denyAccessUnlessGranted('edit', $fellowProject->getProjectGeneralInfo());

        $em = $this->getDoctrine()->getManager();

        if ($fellowProject->getDutyPrimaryActivities()->isEmpty()) {
            $dutyActivityPrimary = new DutyActivityPrimary($fellowProject);
            $fellowProject->addDutyPrimaryActivity($dutyActivityPrimary);
        }

        if ($fellowProject->getDutySecondaryActivities()->isEmpty()) {
            $secondaryActivityPrimary = new DutyActivitySecondary(
                $fellowProject
            );
            $fellowProject->addDutySecondaryActivity($secondaryActivityPrimary);

            $em->persist($fellowProject);
            $em->flush();
        }

        $form = $this->createForm('AppBundle\Form\Fellow\DutiesType', $fellowProject);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $nextAction = 'fellow_project_fellowship_duties_mission_goals';

            if ($form->get('addPrimary')->isClicked()) {
                $dutyActivityPrimary = new DutyActivityPrimary($fellowProject);
                $fellowProject->addDutyPrimaryActivity($dutyActivityPrimary);

                $nextAction = 'fellow_project_fellowship_duties';
            } elseif ($form->get('addSecondary')->isClicked()) {
                $secondaryActivityPrimary = new DutyActivitySecondary($fellowProject);
                $fellowProject->addDutySecondaryActivity($secondaryActivityPrimary);

                $nextAction = 'fellow_project_fellowship_duties';
            } elseif ($form->get('save')->isClicked()) {
                $nextAction = 'project_fellow_admin_menu';
            }

            // Get the IDs of the item to remove.
            $activityPrimaryIdToRemove = $form->get('activityPrimaryIdToRemove')->getData();
            $activitySecondaryIdToRemove = $form->get('activitySecondaryIdToRemove')->getData();

            // Check if there is a Primary Duty to remove.
            if ($activityPrimaryIdToRemove) {
                $dutyActivityPrimary = $em->getRepository('AppBundle:Fellow\DutyActivityPrimary')->find($activityPrimaryIdToRemove);

                if ($dutyActivityPrimary) {
                    $fellowProject->removeDutyPrimaryActivity($dutyActivityPrimary);
                    $em->remove($dutyActivityPrimary);

                    $nextAction = 'fellow_project_fellowship_duties';
                }
            }

            // Check if there is a Secondary Duty to remove.
            if ($activitySecondaryIdToRemove) {
                $dutyActivitySecondary = $em->getRepository('AppBundle:Fellow\DutyActivitySecondary')->find($activitySecondaryIdToRemove);

                if ($dutyActivitySecondary) {
                    $fellowProject->removeDutySecondaryActivity($dutyActivitySecondary);
                    $em->remove($dutyActivitySecondary);

                    $nextAction = 'fellow_project_fellowship_duties';
                }
            }

            $em->persist($fellowProject);
            $em->flush();

            // Validate `Fellowship Duties and Mission Goals` section
            // TODO: should be this placed in a EventListener? Service?
            $fellowProject->setFellowshipDutiesAndMissiongGoalsSectionStatus($em);

            /** @var DutyActivityPrimary $dutyActivity */
            foreach ($form->get('dutyPrimaryActivities')->getViewData() as $dutyActivity) {
                if ($dutyActivity->getActivitiesFocus()->count() > 3) {
                    $this->addFlash(
                        'danger',
                        '<strong>Primary Activities:</strong> You must check at least one Activty Focus and no more than three.'
                    );

                    $nextAction = 'fellow_project_fellowship_duties';

                    break 1;
                }
            }

            /** @var DutyActivitySecondary $dutyActivity */
            foreach ($form->get('dutySecondaryActivities')->getViewData() as $dutyActivity) {
                if ($dutyActivity->getActivitiesFocus()->count() > 3) {
                    $this->addFlash(
                        'danger',
                        '<strong>Secondary Activities:</strong> You must check at least one Activty Focus and no more than three.'
                    );

                    $nextAction = 'fellow_project_fellowship_duties';

                    break 1;
                }
            }

            return $this->redirectToRoute(
                $nextAction,
                ['id' => $fellowProject->getId()]
            );
        }

        return $this->render(
            'fellowproject/wizard/fellowship_duties.html.twig',
            [
                'form' => $form->createView(),
                'goBack' => false,
                'fellowProject' => $fellowProject,
            ]
        );
    }

    /**
     * @Route(
     *     "/{id}/fellowship-duties/mission-goals",
     *     requirements={"id": "\d+"},
     *     name="fellow_project_fellowship_duties_mission_goals"
     * )
     * @Method({"GET", "POST"})
     *
     * @param Request       $request
     * @param FellowProject $fellowProject
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function missionGoalsAction(Request $request, FellowProject $fellowProject)
    {
        /** @var ProjectGeneralInfo $projectGeneralInfo */
        $projectGeneralInfo = $fellowProject->getProjectGeneralInfo();

        $this->denyAccessUnlessGranted('edit', $projectGeneralInfo);

        $form = $this->createForm('AppBundle\Form\Fellow\MissionGoalsType', $projectGeneralInfo);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var \Doctrine\Common\Persistence\ObjectManager $em */
            $em = $this->getDoctrine()->getManager();
            $em->persist($projectGeneralInfo);
            $em->flush();

            $fellowProject->setFellowshipDutiesAndMissiongGoalsSectionStatus($em);

            $nextAction = 'project_fellow_admin_menu';

            if ($form->get('next')->isClicked()) {
                $nextAction = 'project_fellow_admin_menu';
            } elseif ($form->get('back')->isClicked()) {
                $nextAction = 'fellow_project_fellowship_duties';
            }

            return $this->redirectToRoute(
                $nextAction,
                ['id' => $fellowProject->getId()]
            );
        }

        return $this->render(
            'fellowproject/wizard/mission_goals.html.twig',
            [
                'fellowProject' => $fellowProject,
                'form' => $form->createView(),
                'goBack' => 'fellow_project_fellowship_duties',
            ]
        );
    }

    /**
     * Finds and displays a FellowProject entity.
     *
     * @Route(
     *     "/{id}",
     *     requirements={"id": "\d+"},
     *     name="project_fellow_show"
     * )
     * @Method("GET")
     *
     * @param FellowProject $fellowProject
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showAction(FellowProject $fellowProject)
    {
        $deleteForm = $this->createDeleteForm($fellowProject);

        $form = $this->createForm(
            'AppBundle\Form\SubmitFellowProposalType',
            null,
            [
                'action' => $this->generateUrl(
                    'fellow_project_submit',
                    ['id' => $fellowProject->getId()]
                ),
            ]
        );

        $monthlyCostItems = $oneTimeCostItems = [];
        $totals = 0;
        $fellowProjectBudgetTotals = null;
        if ($fellowProject->getFellowProjectBudget()) {
            // Gets monthly and onetime cost items
            $monthlyCostItems = $this->getDoctrine()->getRepository(
                'AppBundle:CostItemMonthly'
            )->findAll();
            $oneTimeCostItems = $this->getDoctrine()->getRepository(
                'AppBundle:CostItemOneTime'
            )->findAll();

            // calculations for budget section
            $fellowProjectBudgetTotals = $fellowProject->getFellowProjectBudget()->getFellowProjectBudgetTotals();
            // TODO: felipe - modify this after all budgets totals have been created
            if (!count($fellowProjectBudgetTotals)) {
                $fellowProject->getFellowProjectBudget()->setSummaryTotals($this->getDoctrine()->getManager(), true);
            }
            $fellowProjectBudgetTotals = $fellowProject->getFellowProjectBudget()->getFellowProjectBudgetTotals()->first();
            $totals = $fellowProject->getFellowProjectBudget()->getSummaryTotals();

        }

        return $this->render(
          'fellowproject/show.html.twig',
          [
            'fellowProject'                => $fellowProject,
            'fellowProjectPresenter'       => new FellowProjectPresenter($fellowProject),
            'monthlyCostItems'             => $monthlyCostItems,
            'oneTimeCostItems'             => $oneTimeCostItems,
            'totals'                       => $totals,
            'fellowProjectBudgetTotals'    => $fellowProjectBudgetTotals,
            'delete_form'                  => $deleteForm->createView(),
            'form'                         => $form->createView(),
          ]
        );
    }

    /**
     * Generates page to confirm removal of Fellow Project
     *
     * @Route("/{id}/delete", name="project_fellow_confirm_delete")
     * @Method("GET")
     *
     * @param \AppBundle\Entity\FellowProject $fellowProject
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function confirmDeleteAction(FellowProject $fellowProject)
    {
        $this->denyAccessUnlessGranted('delete', $fellowProject->getProjectGeneralInfo());

        $deleteForm = $this->createDeleteForm($fellowProject);

        return $this->render('fellowproject/confirm_delete.html.twig', array(
            'fellowProject' => $fellowProject,
            'deleteForm' => $deleteForm->createView(),
        ));

    }

    /**
     * Deletes a FellowProject entity.
     *
     * @Route("/{id}", name="project_fellow_delete")
     * @Method("DELETE")
     *
     * @param Request $request
     * @param FellowProject           $fellowProject
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteAction(Request $request, FellowProject $fellowProject)
    {
        /** @var ProjectGeneralInfo $projectGeneralInfo */
        $projectGeneralInfo = $fellowProject->getProjectGeneralInfo();
        $this->denyAccessUnlessGranted('delete', $projectGeneralInfo);

        $form = $this->createDeleteForm($fellowProject);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            // Gets the project review (if exists) to remove it later
            $reviews = $fellowProject->getFellowProjectReviews();

            $projectReview = null;
            if ($reviews->count()) {
                /** @var ProjectReview $projectReview */
                $projectReview = $reviews->first();
            }

            // Gets costs items to remove them later
            $costItemsToRemove = array();
            if ($fellowProject->getFellowProjectBudget()) {
                $costItems = $fellowProject->getFellowProjectBudget()->getBudgetCostItems();
                $costItemsToRemove = array();
                /** @var FellowProjectBudgetCostItem $item */
                foreach ($costItems as $item) {
                    if ($item->getCostItem() instanceof CostItemCustom || $item->getCostItem() instanceof CostItemCustomInKind) {
                        $costItemsToRemove[] = $item->getCostItem();
                    }
                }
            }

            $em->remove($fellowProject);
//            $em->flush();

            // Removes those entities that can not be removed by DB cascade:
            // Project Review and comments, custom cost items
            if ($projectReview) {
                $em->remove($projectReview);
            }
            foreach($costItemsToRemove as $item) {
                $em->remove($item);
            }
            $em->flush();

        }

        return $this->redirectToRoute('proposal_fellow_index');
    }

    /**
     * @Route(
     *     "/{id}/share",
     *     requirements={"id": "\d+"},
     *     name="project_fellow_share"
     * )
     * @Method({"POST"})
     *
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function shareAction(Request $request, FellowProject $fellowProject)
    {
        $em = $this->getDoctrine()->getManager();
        $userRepo = $em->getRepository('AppBundle:User');

        $projectGeneralInfo = $fellowProject->getProjectGeneralInfo();

        $postData = $request->request->get('form');

        $shareWith = array_key_exists('shareWith', $postData)
        ? $postData['shareWith']
        : [];

        $unShareFrom = array_key_exists('unShareFrom', $postData)
        ? $postData['unShareFrom']
        : [];

        // Check if there are users to share project with
        if (!empty($shareWith)) {
            $sharedWithList = '<ul class="list-unstyled">';

            foreach ($shareWith as $userId) {
                $user = $userRepo->findOneBy(['id' => $userId]);

                // this is the way ManyToMany should be persisted
                $projectGeneralInfo->addAuthor($user);
                $user->addProject($projectGeneralInfo);

                $em->persist($user);

                $sharedWithList .= "<li>{$user->getName()} &lt;{$user->getEmail()}&gt;</li>";
            }

            $em->persist($projectGeneralInfo);
            $em->flush();

            $sharedWithList .= '</ul>';

            $this->addFlash(
                'success',
                '<p>Project has been shared successfully with the following users:</p>'
                . $sharedWithList
            );
        }

        // Check if there are users to un-share project from
        if (!empty($unShareFrom)) {
            $sharedWithList = '<ul class="list-unstyled">';

            foreach ($unShareFrom as $userId) {
                $user = $userRepo->findOneBy(['id' => $userId]);

                // this is the way ManyToMany should be removed
                $projectGeneralInfo->getAuthors()->removeElement($user);
                $user->getProjects()->removeElement($projectGeneralInfo);

                $em->persist($user);

                $sharedWithList .= "<li>{$user->getName()} &lt;{$user->getEmail()}&gt;</li>";
            }

            $em->persist($projectGeneralInfo);
            $em->flush();

            $sharedWithList .= '</ul>';

            $this->addFlash(
                'success',
                '<p>Project has been un-shared successfully from the following users:</p>'
                . $sharedWithList
            );
        }

        return $this->redirectToRoute(
            'project_fellow_admin_menu',
            ['id' => $fellowProject->getId()]
        );
    }

    /**
     * @Route("/ranking/relo", name="fellow_project_relo_ranking")
     * @Method({"GET", "POST"})
     *
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function reloRankingAction(Request $request) {
        /** @var User $user */
        $user = $this->getUser();

        $em = $this->getDoctrine()->getManager();

        $cycleId = $request->get('cycle');

        /** @var CycleFellow $cycleFellow */
        $cycleFellow = (null === $cycleId)
          ? $em->getRepository(CycleFellow::class)->getCurrentCycle()
          : $em->getRepository(CycleFellow::class)->find($cycleId);

        $cycleForm = $this->get('form.factory')
          ->createNamedBuilder(
            'cycle',
            EntityType::class,
            $cycleFellow,
            [
              'method'       => 'GET',
              'class'        => CycleFellow::class,
              'choice_label' => 'name',
            ]
          )
          ->getForm();
        $cycleForm->handleRequest($request);

        $rankingForm = $this
          ->createFormBuilder(null, ['attr' => ['class' => 'ranking-form']])
          ->add('ranking', HiddenType::class)
          ->add('submitRanking', SubmitType::class, ['label' => 'Save Ranking'])
          ->getForm();
        $rankingForm->handleRequest($request);

        $fellowProjects = $em->getRepository('AppBundle:FellowProject')->findAllByRELOLocationsAndCycle($user->getReloLocations(), $cycleForm->getData());

        if ($rankingForm->isSubmitted() && $rankingForm->isValid()) {
            $fellowProjectsRanking = $rankingForm->getData()['ranking'];

            $fellowProjectsIds = explode(',', $fellowProjectsRanking);

            /** @var FellowProject $fellowProject */
            foreach ($fellowProjects as $fellowProject) {
                $ranking = array_search($fellowProject->getId(), $fellowProjectsIds);

                if (FALSE !== $ranking) {
                    /** @var ProjectGeneralInfo $projectGeneralInfo */
                    $projectGeneralInfo = $fellowProject->getProjectGeneralInfo();

                    // Indexes are zero-based. Ranking starts from 1.
                    $ranking += 1;

                    // Modify the RELO ranking if different from the current one.
                    if ($projectGeneralInfo->getReloRanking() !== $ranking) {
                        $projectGeneralInfo->setReloRanking($ranking);

                        $em->persist($projectGeneralInfo);
                    }
                }
            }

            $em->flush();

            return $this->redirectToRoute(
              'fellow_project_relo_ranking',
              ['cycle' => $cycleForm->getData()->getId()]
            );
        }

        return $this->render(
          ':fellowproject/ranking:relo.html.twig',
          [
            'cycleForm'      => $cycleForm->createView(),
            'rankingForm'    => $rankingForm->createView(),
            'fellowProjects' => $fellowProjects,
          ]
        );
    }

    /**
     * Creates a form to delete a FellowProject entity.
     *
     * @param FellowProject $fellowProject The FellowProject entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(FellowProject $fellowProject)
    {
        return $this->createFormBuilder()
            ->setAction(
                $this->generateUrl(
                    'project_fellow_delete',
                    ['id' => $fellowProject->getId()]
                )
            )
            ->setMethod('DELETE')
            ->getForm();
    }

    /**
     * This function handles those steps share the same logic
     *
     * @param array $wizardStepParams
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    private function wizardStepHandler($wizardStepParams)
    {
        /** @var FellowProject $fellowProject */
        list(
            $request,
            $fellowProject,
            $formEntity,
            $formType,
            $twigTemplate,
            $prevStepRoute,
            $nextStepRoute,
            $routeExtraParams
        ) = $wizardStepParams;

        $form = $this->createForm("AppBundle\\Form\\$formType", $formEntity);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($fellowProject);
            $em->flush();

            $nextAction = 'project_fellow_admin_menu';

            if ($form->get('next')->isClicked()) {
                $nextAction = $nextStepRoute;
            } elseif ($form->get('back')->isClicked()) {
                $nextAction = $prevStepRoute;
            }

            $routeParams = array_merge(
                ['id' => $fellowProject->getId()],
                $routeExtraParams
            );

            return $this->redirectToRoute($nextAction, $routeParams);
        }

        return $this->render(
            "fellowproject/wizard/$twigTemplate.html.twig",
            [
                'fellowProject' => $fellowProject,
                'form' => $form->createView(),
                'goBack' => $prevStepRoute,
            ]
        );
    }
}
