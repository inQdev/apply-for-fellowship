<?php

namespace AppBundle\Controller;

use AppBundle\Controller\RoleSwitcherInterface;
use AppBundle\Entity\ProjectReviewStatus;
use AppBundle\Entity\CostItemCustom;
use AppBundle\Entity\CostItemCustomInKind;
use AppBundle\Entity\FellowProject;
use AppBundle\Entity\FellowProjectBudget;
use AppBundle\Entity\FellowProjectBudgetCostItem;
use AppBundle\Entity\ProjectGlobalValue;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

//  * @Route("/project/fellow/budget")

/**
 * FellowProject controller.
 *
 */
class FellowProjectBudgetController extends Controller implements RoleSwitcherInterface
{

	/**
	 * Given a FellowProject, allows to select the organization funding the project
	 *
	 * @Route("/project/fellow/{id}/budget/fundedby", name="fellow_project_budget_funded")
	 * @Method({"GET", "POST"})
	 *
	 * @param \Symfony\Component\HttpFoundation\Request $request
	 * @param \AppBundle\Entity\FellowProject $fellowProject
	 *
	 * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
	 */
	public function fundedByAction(FellowProject $fellowProject, Request $request)
	{

		// Checks if fellowProject is valid
//		$fellowProject = $this->getDoctrine()->getRepository('AppBundle:FellowProject');
//		if (!$fellowProject->find($fpid)) {
//			throw $this->createNotFoundException();
//		}

		// Tries to load the Budget from the Project, if none found, create a new one
		/** @var FellowProjectBudget $projectBudget */
		$projectBudget = $fellowProject->getFellowProjectBudget();
		if (!$projectBudget) {
			$projectBudget = new FellowProjectBudget();
			$projectBudget->setFellowProject($fellowProject);
		}

		$this->denyAccessUnlessGranted('edit', $projectBudget);

		$form = $this->createForm('AppBundle\Form\FellowProjectBudgetFundedType', $projectBudget, [
			'role' => $this->get('security.token_storage')->getToken()->getRoles(),
		]);

		$form->handleRequest($request);

		if ($form->isValid()) {

			// checks version
			if (!$form->get('revision_id')->getData()) {
				$projectBudget->setRevisionId(1);
			}

			// Associates the project with the budget
//			$projectBudget->setFellowProject($fellowProject);

			// If the funding entity changes, values must be recalculated
			$oldProjectBudget = $fellowProject->getFellowProjectBudget();
			if ($oldProjectBudget) { // && $oldProjectBudget->getFundedBy() != $projectBudget->getFundedBy()) {

				if (count($projectBudget->getBudgetCostItems())) {

					foreach ($projectBudget->getBudgetCostItems() as &$item) {
						$item = $this->calculateRowTotals($projectBudget, $item);
					}

				}
			}

			$em = $this->getDoctrine()->getManager();
			$em->persist($projectBudget);
			$em->flush();

			// Saves the status, if in this step, consider started but not completed
			if ($projectBudget->getBudgetStepStatus($fellowProject, $em) == 0) {
				$projectBudget->setBudgetStepStatus($fellowProject, $em, 1);
			}

			$redirect_route = $this->redirectToRoute('fellow_project_budget_costs', [
				'pid' => $fellowProject->getId(),
				'bid' => $projectBudget->getId(),
			]);

			if ($form->get('save')->isClicked()) {
				$projectGeneralInfo = $fellowProject->getProjectGeneralInfo();
				$pathName =  'project_fellow_admin_menu';

				if ($projectGeneralInfo->getProjectReviewStatus()) {
					if ($projectGeneralInfo->getProjectReviewStatus()->getId() == ProjectReviewStatus::POST_REQUIRE_ALTERNATE_FUNDING
						&& $this->isGranted('ROLE_EMBASSY')) {
						$pathName = 'project_fellow_show';
					}
				}

				$redirect_route = $this->redirectToRoute($pathName, [
					'id' => $fellowProject->getId()
				]);
			}

			return $redirect_route;
		}

		return $this->render('fellowprojectbudget/funded_by.html.twig', [
			'fellowProject' => $fellowProject,
			'form' => $form->createView(),
			'projectBudget' => $projectBudget,
		]);
	}

	/**
	 * Given a FellowProject and FellowProjectBudget, shows the table to enter or modify values
	 *
	 * @Route("/project/fellow/{pid}/budget/{bid}/costitems", name="fellow_project_budget_costs")
	 * @ParamConverter("fellowProject", class="AppBundle:FellowProject", options={"mapping": {"pid": "id"}})
	 * @ParamConverter("fellowProjectBudget", class="AppBundle:FellowProjectBudget", options={"mapping": {"bid": "id"}})
	 * @Method({"GET", "POST"})
	 *
	 * @param \AppBundle\Entity\FellowProject $fellowProject
	 * @param \AppBundle\Entity\FellowProjectBudget $fellowProjectBudget
	 * @param \Symfony\Component\HttpFoundation\Request $request
	 *
	 * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
	 */
	public function costItemsAction(FellowProject $fellowProject, FellowProjectBudget $fellowProjectBudget, Request $request)
	{

		$this->denyAccessUnlessGranted('edit', $fellowProjectBudget);

		// prepares projectBudget and subitems
		$monthlyCostItems = $this->getDoctrine()->getRepository('AppBundle:CostItemMonthly')->findAll();
		$oneTimeCostItems = $this->getDoctrine()->getRepository('AppBundle:CostItemOneTime')->findAll();

		if (!count($fellowProjectBudget->getBudgetCostItems())) {
			$budgetCostItems = array();
			$i = 0;
			foreach ($monthlyCostItems as $item) {
				$budgetCostItems[$i] = new FellowProjectBudgetCostItem();
				$budgetCostItems[$i]->setCostItem($item);
				$budgetCostItems[$i]->setFellowProjectBudget($fellowProjectBudget);
				$fellowProjectBudget->getBudgetCostItems()->add($budgetCostItems[$i]);
				$i++;
			}

			foreach ($oneTimeCostItems as $item) {
				$budgetCostItems[$i] = new FellowProjectBudgetCostItem();
				$budgetCostItems[$i]->setCostItem($item);
				$budgetCostItems[$i]->setFellowProjectBudget($fellowProjectBudget);
				$fellowProjectBudget->getBudgetCostItems()->add($budgetCostItems[$i]);
				$i++;
			}
		}

		$form = $this->createForm('AppBundle\Form\FellowProjectBudgetType', $fellowProjectBudget, [
			'role' => $this->get('security.token_storage')->getToken()->getRoles(),
		]);

		$form->handleRequest($request);

		// Processes the request when the form is submitted
		if ($form->isValid()) {

			$em = $this->getDoctrine()->getManager();

			// processes any deletion requested
			if ($form->get('delete_id')->getData()) {
				$projectCostItem = $this->getDoctrine()->getRepository('AppBundle:FellowProjectBudgetCostItem')->find($form->get('delete_id')->getData());
				$em->remove($projectCostItem);

				// there's not cascade delete of cost item, so delete it manually
				$em->remove($projectCostItem->getCostItem());
				$em->flush();

			}

			$revision_id = 1; // TODO: Handle change on this value according to workflow

			// creates project global values if needed, if none found, create new records by copying the template from global values
			if (!count($fellowProjectBudget->getProjectGlobalValues())) {
				$globalValueFellow = $this->getDoctrine()->getRepository('AppBundle:GlobalValueFellow')->findAll();

				foreach ($globalValueFellow as $globalValue) {
					$projectGlobalValue = new ProjectGlobalValue();
					$projectGlobalValue->setActualValue($globalValue->getDefaultValue());
					$projectGlobalValue->setGlobalValue($globalValue);
					$projectGlobalValue->setFellowProjectBudget($fellowProjectBudget);
					$fellowProjectBudget->getProjectGlobalValues()->add($projectGlobalValue);
				}
			}

			// Now, processes the Budget items submitted
			foreach ($fellowProjectBudget->getBudgetCostItems() as &$item) {

				$item->setRevisionId($revision_id);

				if (true) {
					$item = $this->calculateRowTotals($fellowProjectBudget, $item);
				}

				// Adds default description when needed
				if ($item->getCostItem()->getDescription() == '') {
					$item->getCostItem()->setDescription('Untitled');
				}

			}

			// Saves the Summary Totals
			$fellowProjectBudget->setSummaryTotals();

			// controls where to redirect after processing

            if ($form->get('save')->isClicked()) {
	            $projectGeneralInfo = $fellowProject->getProjectGeneralInfo();
	            $pathName =  'project_fellow_admin_menu';

	            if ($projectGeneralInfo->getProjectReviewStatus()) {
		            if ($projectGeneralInfo->getProjectReviewStatus()->getId() == ProjectReviewStatus::POST_REQUIRE_ALTERNATE_FUNDING
			            && $this->isGranted('ROLE_EMBASSY')) {
			            $pathName = 'project_fellow_show';
		            }
	            }

	            $redirect_route = $this->redirectToRoute($pathName, [
		            'id' => $fellowProject->getId()
	            ]);
            }
            elseif ($form->get('calculate1')->isClicked() || $form->get('calculate2')->isClicked()) {
				$redirect_route = $this->redirectToRoute('fellow_project_budget_costs', [
					'pid' => $fellowProject->getId(),
					'bid' => $fellowProjectBudget->getId(),
				]);

			} else if ($form->get('additional')->isClicked()) { // Adds a cost item

				$costItemCustom = new FellowProjectBudgetCostItem();
				$costItem = new CostItemCustom();
				$costItemCustom->setCostItem($costItem);
				$costItemCustom->setRevisionId($revision_id);
				$costItemCustom->setFellowProjectBudget($fellowProjectBudget);
				$fellowProjectBudget->getBudgetCostItems()->add($costItemCustom);

				$redirect_route = $this->redirectToRoute('fellow_project_budget_costs', [
					'pid' => $fellowProject->getId(),
					'bid' => $fellowProjectBudget->getId(),
				]);

			} else if ($form->get('additional2')->isClicked()) { // Adds a cost item

				$costItemCustom = new FellowProjectBudgetCostItem();
				$costItem = new CostItemCustomInKind();
				$costItemCustom->setCostItem($costItem);
				$costItemCustom->setRevisionId($revision_id);
				$costItemCustom->setFellowProjectBudget($fellowProjectBudget);
				$fellowProjectBudget->getBudgetCostItems()->add($costItemCustom);

				$redirect_route = $this->redirectToRoute('fellow_project_budget_costs', [
					'pid' => $fellowProject->getId(),
					'bid' => $fellowProjectBudget->getId(),
				]);

			} elseif ($form->get('back')->isClicked()) {

				$redirect_route = $this->redirectToRoute('fellow_project_budget_funded', [
					'id' => $fellowProject->getId()
				]);

			}
			else { // Goes to review

				$redirect_route = $this->redirectToRoute('fellow_project_budget_review', [
					'pid' => $fellowProject->getId(),
					'bid' => $fellowProjectBudget->getId(),
				]);

			}

			$em->persist($fellowProjectBudget);
			$em->flush();

			return $redirect_route;

		}

		if ($fellowProjectBudget->getFundedBy() == 'ECA') {
			$viewToRender = 'cost_items_eca.html.twig';
		} else {
			$viewToRender = 'cost_items_post.html.twig';
		}

		return $this->render('fellowprojectbudget/' . $viewToRender, [
			'fellowProject' => $fellowProject,
			'form' => $form->createView(),
			'projectBudget' => $fellowProjectBudget,
			'monthlyCostItems' => $monthlyCostItems,
			'oneTimeCostItems' => $oneTimeCostItems,
		]);
	}

	/**
	 * @Route("/project/fellow/{pid}/budget/{bid}/reviewcosts", name="fellow_project_budget_review")
	 * @ParamConverter("fellowProject", class="AppBundle:FellowProject", options={"mapping": {"pid": "id"}})
	 * @ParamConverter("fellowProjectBudget", class="AppBundle:FellowProjectBudget", options={"mapping": {"bid": "id"}})
	 *
	 * @Method({"GET", "POST"})
	 *
	 * @param \AppBundle\Entity\FellowProject $fellowProject
	 * @param \AppBundle\Entity\FellowProjectBudget $fellowProjectBudget
	 * @param \Symfony\Component\HttpFoundation\Request $request
	 *
	 * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
	 */
	public function reviewCostsAction(FellowProject $fellowProject, FellowProjectBudget $fellowProjectBudget, Request $request)
	{
		$this->denyAccessUnlessGranted('edit', $fellowProjectBudget);

		// Gets monthly and onetime cost items
		$monthlyCostItems = $this->getDoctrine()->getRepository('AppBundle:CostItemMonthly')->findAll();
		$oneTimeCostItems = $this->getDoctrine()->getRepository('AppBundle:CostItemOneTime')->findAll();

		$totals = $fellowProjectBudget->getSummaryTotals();

		// addition to define on the save button label
		/** @var ProjectGeneralInfo $projectGeneralInfo */
		$projectGeneralInfo = $fellowProject->getProjectGeneralInfo();

		// Defines the label depending on the status of the proposal
		$label = 'Save and return to Panel';
		if ($projectGeneralInfo->getProjectReviewStatus()) {
			if ($projectGeneralInfo->getProjectReviewStatus()->getId() == ProjectReviewStatus::POST_REQUIRE_ALTERNATE_FUNDING
				&& $this->isGranted('ROLE_EMBASSY')) {
				$label = 'Save and return to Review';
			}
		}

		$form = $this->createFormBuilder()
          ->add('save', SubmitType::class, ['label' => $label])
			->add('back', submitType::class, array('label' => 'Save and go back'))
			->add('next', submitType::class, array('label' => 'Save and continue'))
			->getForm();

		$form->handleRequest($request);

		// Processes the request when the form is submitted
		if ($form->isValid()) {
			$em = $this->getDoctrine()->getManager();

//            if ($form->get('save')->isClicked()) {
//                $redirect_route = $this->redirectToRoute(
//                  'project_fellow_admin_menu',
//                  ['id' => $fellowProject->getId()]
//                );
//            }
			if ($form->get('back')->isClicked()) {
				$redirect_route = $this->redirectToRoute('fellow_project_budget_costs', [
					'pid' => $fellowProject->getId(),
					'bid' => $fellowProjectBudget->getId(),
				]);
			}
			elseif ($form->get('next')->isClicked() || $form->get('save')->isClicked()) {
				// Checks if all fullCost values were set (not empty)
				$status = 2;
				foreach($fellowProjectBudget->getBudgetCostItems() as $item) {
					if ($item->getFullCost() === null && !is_a($item->getCostItem(), 'AppBundle\Entity\CostItemCustomInKind')) {
						$status = 1;
						break;
					}
				}

				// Sets the budget section completed if going forward in this step
				$fellowProjectBudget->setBudgetStepStatus($fellowProject, $em, $status);

				$projectGeneralInfo = $fellowProject->getProjectGeneralInfo();
				$pathName =  'project_fellow_admin_menu';

				if ($projectGeneralInfo->getProjectReviewStatus()) {
					if ($projectGeneralInfo->getProjectReviewStatus()->getId() == ProjectReviewStatus::POST_REQUIRE_ALTERNATE_FUNDING
						&& $this->isGranted('ROLE_EMBASSY')) {
						$pathName = 'project_fellow_show';
					}
				}

				$redirect_route = $this->redirectToRoute($pathName, [
					'id' => $fellowProject->getId()
				]);
			}

			return $redirect_route;

		}

		// When accessing the form
		if ($fellowProjectBudget->getFundedBy() == 'ECA') {
			$viewToRender = 'review_costs_eca.html.twig';
		} else {
			$viewToRender = 'review_costs_post.html.twig';
		}

		return $this->render('fellowprojectbudget/' . $viewToRender, array(
			'fellowProject' => $fellowProject,
			'projectBudget' => $fellowProjectBudget,
			'monthlyCostItems' => $monthlyCostItems,
			'oneTimeCostItems' => $oneTimeCostItems,
			'totals' => $totals,
			'fellowProjectBudgetTotals' => $fellowProjectBudget->getFellowProjectBudgetTotals()->first(),
			'form' => $form->createView(),
		));

	}

	/**
	 * @Route("/project/fellow/{project_id}/budget/{budget_id}/compareversions")
	 */
	public function compareVersionsAction()
	{
		return $this->render('fellowprojectbudget/compare_versions.html.twig', array(// ...
		));
	}


	/**
	 * @param \AppBundle\Entity\FellowProjectBudget $fellowProjectBudget
	 * @param object $item
	 *
	 * @return object $item
	 *
	 */
	public function calculateRowTotals($fellowProjectBudget, $item = null)
	{

//		if (!$item->getFullCost()) {
//			$item->setFullCost(0);
//		}

		if (!$item->getPostContribution()) {
			$item->setPostContribution(0);
		}

		if (!$item->getHostContributionMonetary()) {
			$item->setHostContributionMonetary(0);
		}

		if (!$item->getHostContributionInKind()) {
			$item->setHostContributionInKind(0);
		}

		if ($fellowProjectBudget->getFundedBy() == 'ECA') { // ECA funded

			$item->setPostHostTotalContribution($item->getPostContribution() + $item->getHostContributionMonetary());
			$item->setPostTotalContribution($item->getPostContribution());
			if ($item->getFullCost()) {
				$item->setECATotalContribution($item->getFullCost() - $item->getPostHostTotalContribution());
			} else {
				$item->setECATotalContribution(0);
			}
		} else { // Post funded

			$item->setPostHostTotalContribution($item->getHostContributionMonetary());
			if ($item->getFullCost()) {
				$item->setPostTotalContribution($item->getFullCost() - $item->getPostHostTotalContribution());
			} else {
				$item->setPostTotalContribution(0);
			}
			$item->setPostContribution($item->getPostTotalContribution());
			$item->setECATotalContribution(0);

		}

		return $item;

	} // End calculateRowTotals


	/**
	 * Generates page to confirm removal of project custom cost item
	 *
	 * @Route("/project/fellow/{pid}/costitems/{cid}/delete", name="fellow_project_cost_item_confirm_delete")
	 * @Method("GET")
	 *
	 * @ParamConverter("fellowProject", class="AppBundle:FellowProject", options={"mapping": {"pid": "id"}})
	 * @ParamConverter("projectCostItem", class="AppBundle:FellowProjectBudgetCostItem", options={"mapping": {"cid": "id"}})
	 *
	 * @param \AppBundle\Entity\FellowProject $fellowProject
	 * @param \AppBundle\Entity\FellowProjectBudgetCostItem $projectCostItem
	 *
	 * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
	 */
	public function confirmDeleteCustomCostAction($fellowProject, $projectCostItem)
	{

		$this->denyAccessUnlessGranted('edit', $fellowProject->getFellowProjectBudget());

		// TODO: Validate that cost item belongs to it Budget
		// TODO: Review the parameters and refactor if possible

		if (!(is_a($projectCostItem->getCostItem(), '\AppBundle\Entity\CostItemCustom'))) {
			$this->addFlash('error', 'This item can not be deleted!');
			return $this->redirectToRoute('fellow_project_budget_costs', [
				'pid' => $fellowProject->getId(),
				'bid' => $projectCostItem->getFellowProjectBudget()->getId(),
			]);
		}

		$deleteForm = $this->createDeleteForm($projectCostItem);

		return $this->render('fellowprojectbudget/confirm_delete_cost_item.html.twig', array(
			'projectCostItem' => $projectCostItem,
			'delete_form' => $deleteForm->createView(),
		));

	}

	/**
	 * Deletes a Custom cost item.
	 *
	 * @Route("/project/fellow/{pid}/costitems/{cid}/delete", name="fellow_project_cost_item_delete")
	 * @Method("DELETE")
	 *
	 * @ParamConverter("fellowProject", class="AppBundle:FellowProject", options={"mapping": {"pid": "id"}})
	 * @ParamConverter("projectCostItem", class="AppBundle:FellowProjectBudgetCostItem", options={"mapping": {"cid": "id"}})
	 *
	 * @param \AppBundle\Entity\FellowProject $fellowProject
	 * @param \AppBundle\Entity\FellowProjectBudgetCostItem $projectCostItem
	 *
	 * @return null
	 *
	 */
	public function deleteAction($fellowProject, $projectCostItem, Request $request)
	{
		$form = $this->createDeleteForm($projectCostItem);
		$form->handleRequest($request);

		if ($form->isSubmitted() && $form->isValid()) {
			$em = $this->getDoctrine()->getManager();
			$em->remove($projectCostItem);

			// there's not cascade delete of cost item, so delete it manually
			$em->remove($projectCostItem->getCostItem());

			$em->flush();
		}
//		$this->addFlash('error', 'This item can not be deleted!');

		return $this->redirectToRoute('fellow_project_budget_costs', [
			'pid' => $fellowProject->getId(),
			'bid' => $projectCostItem->getFellowProjectBudget()->getId(),
		]);
	}


	/**
	 * Creates a form to delete a project custom cost item.
	 *
	 * @param \AppBundle\Entity\FellowProjectBudgetCostItem $projectCostItem
	 *
	 * @return \Symfony\Component\Form\Form The form
	 */
	private function createDeleteForm($projectCostItem)
	{
		return $this->createFormBuilder()
			->setAction(
				$this->generateUrl('fellow_project_cost_item_delete', [
					'pid' => $projectCostItem->getFellowProjectBudget()->getFellowProject()->getId(),
					'cid' => $projectCostItem->getId()
				])
			)
			->setMethod('DELETE')
			->getForm();
	}



}
