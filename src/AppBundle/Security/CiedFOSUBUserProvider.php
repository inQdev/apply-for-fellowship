<?php

namespace AppBundle\Security;

use AppBundle\Entity\User;
use HWI\Bundle\OAuthBundle\OAuth\Response\UserResponseInterface;
use HWI\Bundle\OAuthBundle\Security\Core\User\FOSUBUserProvider as BaseFOSUBProvider;
use Symfony\Bundle\MonologBundle\MonologBundle;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class CiedFOSUBUserProvider
 *
 * @package AppBundle\Security
 */
class CiedFOSUBUserProvider extends BaseFOSUBProvider
{
	/**
	 * {@inheritDoc}
	 *
	 * @param \Symfony\Component\Security\Core\User\UserInterface $user
	 * @param \HWI\Bundle\OAuthBundle\OAuth\Response\UserResponseInterface $response
	 */
	public function connect(UserInterface $user, UserResponseInterface $response)
	{

		// get property from provider configuration by provider name
		$property = $this->getProperty($response);  // ciedId
		$setter = 'set'. ucfirst($property);
		$username = $response->getUsername(); // get the unique user identifier

		//we "disconnect" previously connected users
		$existingUser = $this->userManager->findUserBy(
			[$property => $username]
		);

		if (null !== $existingUser) {
			// set current user id and token to null for disconnect
			$existingUser->$setter(null);
//			$existingUser->setCiedAccessToken(null);
			$this->userManager->updateUser($existingUser);
		}

		//we connect current user, set current user id and token
		$user->$setter($response->getResponse()['sub']);

		$serviceAccessTokenName = $response->getResourceOwner()->getName() . 'AccessToken'; // ciedAccessToken
		$serviceAccessTokenSetter = 'set' . ucfirst($serviceAccessTokenName);

		$user->$serviceAccessTokenSetter($response->getAccessToken());

		// TODO: Get and save roles in DB row


		$this->userManager->updateUser($user);

	}

	/**
	 * {@inheritdoc}
	 *
	 * @param \HWI\Bundle\OAuthBundle\OAuth\Response\UserResponseInterface $response
	 *
	 * @return \AppBundle\Entity\User|\FOS\UserBundle\Model\UserInterface
	 */
	public function loadUserByOAuthUserResponse(UserResponseInterface $response)
	{
//		$logger = $this->get('logger');
//		$logger->info('loadUserByOAuthUserResponse: Response');
//		$logger->info($response);
		// Get access to the request
//		$request = Request::createFromGlobals();
//		$current_role = $request->cookies->get('Drupal_visitor_current_role');

		$userEmail = $response->getEmail();
		$user = $this->userManager->findUserByEmail($userEmail);

		// if null, just create new user and set it properties
		if (null === $user) {
			$rawResponse = $response->getResponse();

			/** @var \AppBundle\Entity\User $user */
			$user = new User();
			$user->setEnabled(true);
			$user->setCiedId($rawResponse['sub']);
			$user->setUsername($rawResponse['preferred_username']);
			$user->setEmail($userEmail);
			$user->setName($rawResponse['name']);
			$user->setPassword('');

			$this->userManager->updateUser($user);

			return $user;
		}
		// TODO: Check if we need to update the roles here
//		if ($current_role) {
//			$current_role = 'ROLE_'. strtoupper(str_replace(' ', '_', $current_role));
//			$user->setRoles([$current_role]);
//		}

		// else update access token of existing user
		$serviceName = $response->getResourceOwner()->getName();
		$setter = 'set' . ucfirst($serviceName) . 'AccessToken';
		$user->$setter($response->getAccessToken()); //update access token

		return $user;
	}
}
