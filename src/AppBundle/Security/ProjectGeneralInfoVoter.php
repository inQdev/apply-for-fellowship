<?php
/**
 * Allows to decide when a user has access to the Project General Info hence its Fellow Project or Specialist Project phase
 * @author Felipe Ceballos <felipe.ceballos@inqbation.com>
 */

namespace AppBundle\Security;

use AppBundle\Entity\ProjectGeneralInfo;
use AppBundle\Entity\ProjectReviewStatus;
use AppBundle\Entity\User;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\AccessDecisionManager;
use Symfony\Component\Security\Core\Authorization\Voter\RoleHierarchyVoter;
use Symfony\Component\Security\Core\Authorization\Voter\RoleVoter;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\Authorization\Voter\VoterInterface;

class ProjectGeneralInfoVoter extends BaseVoter
{
	const VIEW = 'view';
	const EDIT = 'edit';
	const REVIEW = 'review';
	const DELETE = 'delete';
	const WITHDRAW = 'withdraw';

	/**
	 * @param string $attribute
	 * @param mixed $subject
	 * @return bool
	 */
	protected function supports($attribute, $subject)
	{
		// if the attribute isn't one we support, return false
		if (!in_array($attribute, array(self::VIEW, self::EDIT, self::REVIEW, self::DELETE, self::WITHDRAW))) {
			return false;
		}

		// only vote on ProjectGeneralInfo objects inside this voter
		if (!$subject instanceof ProjectGeneralInfo) {
			return false;
		}

		return true;
	}

	/**
	 * @param string $attribute
	 * @param mixed $subject
	 * @param TokenInterface $token
	 * @return bool
	 */
	protected function voteOnAttribute($attribute, $subject, TokenInterface $token)
	{
		$user = $token->getUser();

		if (!$user instanceof User) {
			// the user must be logged in; if not, deny access
			return false;
		}

		// If the user is Super Admin or Admin
		// TODO: Confirm this assumption
		if ($this->decisionManager->decide($token, array('ROLE_SUPER_ADMIN'))) {
			return true;
		}

		if ($this->decisionManager->decide($token, array('ROLE_ADMIN'))) {
			return true;
		}

		// you know $subject is a Post object, thanks to supports
		/** @var ProjectGeneralInfo $projectGeneralInfo */
		$projectGeneralInfo = $subject;

		switch ($attribute) {
			case self::VIEW:
				return $this->canView($projectGeneralInfo, $token);
			case self::EDIT:
				return $this->canEdit($projectGeneralInfo, $token);
			case self::REVIEW:
				return $this->canReview($projectGeneralInfo, $token);
			case self::DELETE:
				return $this->canDelete($projectGeneralInfo, $token);
			case self::WITHDRAW:
				return $this->canWithdraw($projectGeneralInfo, $token);
		}

		throw new \LogicException('This code should not be reached!');
	}

	/**
	 * @param ProjectGeneralInfo $projectGeneralInfo
	 * @param TokenInterface $token
	 * @return bool
	 */
	private function canEdit(ProjectGeneralInfo $projectGeneralInfo, TokenInterface $token) {

		$type = 'Fellow';
		if ($projectGeneralInfo->getSpecialistProjectPhase() !== null) {
			$type = 'Specialist';
		}

		$user = $token->getUser();

		// If proposal hasn't been submitted, check sharing settings
		if ($projectGeneralInfo->getSubmissionStatus() == 'not_submitted') {

			$result = $this->checkDirectAccess($projectGeneralInfo, $user);

		} else {

			$roleHierarchyVoter = $this->setRoleHierarchyVoter();
			$reviewStatus = $projectGeneralInfo->getProjectReviewStatus()->getId();
			$result = false;

			switch ($reviewStatus) {
				case ProjectReviewStatus::UNDER_RELO_REVIEW:
				case ProjectReviewStatus::POST_REQUIRE_ALTERNATE_FUNDING:
					$result = ( $roleHierarchyVoter->vote($token, null, array('ROLE_RELO')) > 0 ? true : false );
					// TODO: Add conditions related to country
					break;

				case ProjectReviewStatus::UNDER_RPO_REVIEW:
					$result = ( $roleHierarchyVoter->vote($token, null, array('ROLE_RPO')) > 0 ? true : false );
					// TODO: Add conditions related to region
					break;

				case ProjectReviewStatus::UNDER_ECA_REVIEW:
					$result = ( $roleHierarchyVoter->vote($token, null, array('ROLE_STATE_DEPARTMENT')) > 0 ? true : false );
					break;

				case ProjectReviewStatus::UNDER_POST_RE_REVIEW:
					$result = ( $roleHierarchyVoter->vote($token, null, array('ROLE_EMBASSY')) > 0 ? true : false );
					$result = $result && $this->checkDirectAccess($projectGeneralInfo, $token->getUser());
					break;
			}

		}

		return $result;

	}

	/**
	 * @param ProjectGeneralInfo $projectGeneralInfo
	 * @param TokenInterface $token
	 * @return bool
	 */
	private function canView(ProjectGeneralInfo $projectGeneralInfo, TokenInterface $token) {
		// Anybody could see (not in use yet)
		return true;
	}

	/**
	 * @param ProjectGeneralInfo $projectGeneralInfo
	 * @param TokenInterface $token
	 * @return bool
	 */
	private function canReview(ProjectGeneralInfo $projectGeneralInfo, TokenInterface $token) {

		if ($projectGeneralInfo->getSubmissionStatus() == 'not_submitted') {

			return false;

		} else {

			$roleHierarchyVoter = $this->setRoleHierarchyVoter();
			$reviewStatus = $projectGeneralInfo->getProjectReviewStatus()->getId();
			$result = false;

			switch ($reviewStatus) {
				case ProjectReviewStatus::UNDER_RELO_REVIEW:
//					return $roleHierarchyVoter->vote($token, null, array('ROLE_RELO'));
					$result = ( $roleHierarchyVoter->vote($token, null, array('ROLE_RELO')) > 0 ? true : false );
					// TODO: Add conditions related to country, it could be implemented in the BaseVoter
					break;

				case ProjectReviewStatus::UNDER_RPO_REVIEW:
//					return $roleHierarchyVoter->vote($token, null, array('ROLE_RPO'));
					$result = ( $roleHierarchyVoter->vote($token, null, array('ROLE_RPO')) > 0 ? true : false );
					// TODO: Add conditions related to region, it could be implemented in the BaseVoter
					break;

				case ProjectReviewStatus::UNDER_ECA_REVIEW:
//					return $roleHierarchyVoter->vote($token, null, array('ROLE_STATE_DEPARTMENT'));
					$result = ( $roleHierarchyVoter->vote($token, null, array('ROLE_STATE_DEPARTMENT')) > 0 ? true : false );
					break;

				case ProjectReviewStatus::UNDER_POST_RE_REVIEW:
				case ProjectReviewStatus::POST_REQUIRE_ALTERNATE_FUNDING:
//					return ($roleHierarchyVoter->vote($token, null, array('ROLE_EMBASSY'))
//						&& $this->checkDirectAccess($projectGeneralInfo, $token->getUser()));
					$result = ( $roleHierarchyVoter->vote($token, null, array('ROLE_EMBASSY')) > 0 ? true : false );
					$result = $result && $this->checkDirectAccess($projectGeneralInfo, $token->getUser());
					break;

//				case ProjectReviewStatus::REVIEW_COMPLETE:
//					return false;
//
			}

		}

		return $result;

	}

	/**
	 * @param ProjectGeneralInfo $projectGeneralInfo
	 * @param TokenInterface $token
	 * @return bool
	 *
	 */
	private function canDelete(ProjectGeneralInfo $projectGeneralInfo, TokenInterface $token) {

		$type = 'Fellow';
		if ($projectGeneralInfo->getSpecialistProjectPhase() !== null) {
			$type = 'Specialist';
		}

		$user = $token->getUser();

		// If proposal has been submitted, it can not be deleted
		if ($projectGeneralInfo->getSubmissionStatus() == 'submitted') {
			return false;
		}

		// If user is ECA, allows him
		if ($this->decisionManager->decide($token, array('ROLE_STATE_DEPARTMENT'))) {
			return true;
		}

		// checks ownership
		if ($user == $projectGeneralInfo->getCreatedBy()) {
			return true;
		}

		return false;

	}

	/**
	 * @param ProjectGeneralInfo $projectGeneralInfo
	 * @param TokenInterface $token
	 * @return bool
	 */
	private function canWithdraw(ProjectGeneralInfo $projectGeneralInfo, TokenInterface $token) {

//		$type = 'Fellow';
//		if ($projectGeneralInfo->getSpecialistProjectPhase() !== null) {
//			$type = 'Specialist';
//		}

		$user = $token->getUser();

		// If proposal hasn't been submitted, this option is not available
		if ($projectGeneralInfo->getSubmissionStatus() == 'not_submitted') {
			return false;
		}

		// Only owner, authors and submitter can withdraw
		return $this->checkDirectAccess($projectGeneralInfo, $user);

	}


}