<?php
/**
 * This class keeps attributes and methods that could be used by other voters
 * @author Felipe Ceballos <felipe.ceballos@inqbation.com>
 */

namespace AppBundle\Security;

use AppBundle\Entity\ProjectGeneralInfo;
use AppBundle\Entity\RoleStructure;
use AppBundle\Entity\User;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\AccessDecisionManager;
use Symfony\Component\Security\Core\Authorization\Voter\RoleHierarchyVoter;
use Symfony\Component\Security\Core\Authorization\Voter\RoleVoter;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\Authorization\Voter\VoterInterface;
use Symfony\Component\Security\Core\Role\RoleHierarchy;

class BaseVoter extends Voter
{
//	const VIEW = 'view';
//	const EDIT = 'edit';

	protected $decisionManager;
	protected $em;

	/**
	 * Constructor - To allow using the Entity manager in the Voter methods
	 * @param AccessDecisionManager $decisionManager
	 * @param EntityManager $em
	 *
	 */
	public function __construct(AccessDecisionManager $decisionManager, EntityManager $em)
	{
		$this->decisionManager = $decisionManager;
		$this->em = $em;
	}

	/**
	 * To be overridden in child classes
	 * @param string $attribute
	 * @param mixed $subject
	 * @return void
	 */
	protected function supports($attribute, $subject)
	{
		// TODO: Implement supports() method.
	}

	/**
	 * This function implementation is mandatory
	 * So, this default one returns false, it must be overridden in child classes
	 */
	protected function voteOnAttribute($attribute, $subject, TokenInterface $token)
	{
		return false;
	}

	/**
	 * Function to check if user is owner, author or if has submitted the proposal
	 * @param ProjectGeneralInfo $projectGeneralInfo
	 * @param User $user
	 * @return bool
	 */
	protected function checkDirectAccess(ProjectGeneralInfo $projectGeneralInfo, User $user) {
		// checks ownership
		if ($user == $projectGeneralInfo->getCreatedBy()) {
			return true;
		}

		// checks authorship
		$authors = $projectGeneralInfo->getAuthors();
		foreach ($authors as $author) {
			if ($user == $author) {
				return true;
			}
		}

		// checks submission
		if ($user == $projectGeneralInfo->getSubmittedBy()) {
			return true;
		}

//		return VoterInterface::ACCESS_DENIED;
		return false;
	}

	/**
	 * Takes the Role Hierarchy defined in DB and creates a voter
	 *
	 * @return RoleHierarchyVoter
	 */
	protected function setRoleHierarchyVoter() {
		// Sets the role hierarchy....
		$hierarchy = $this->em->getRepository('AppBundle:RoleStructure')->getRolesArray();

		$roleHierarchy = new RoleHierarchy($hierarchy);
		$roleHierarchyVoter = new RoleHierarchyVoter($roleHierarchy);

		return $roleHierarchyVoter;
	}

}