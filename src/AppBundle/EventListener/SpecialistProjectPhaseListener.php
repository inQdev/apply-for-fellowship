<?php

namespace AppBundle\EventListener;

use AppBundle\Entity\ProjectGeneralInfo;
use AppBundle\Entity\SpecialistProject;
use AppBundle\Entity\Sequence;
//use AppBundle\Entity\WizardStepStatus;
use AppBundle\Entity\SpecialistProjectPhase;
use Doctrine\ORM\Event\LifecycleEventArgs;

/**
 * Class SpecialistProjectListener
 *
 * @package AppBundle\EventListener
 */
class SpecialistProjectPhaseListener
{
    /**
     * @param LifecycleEventArgs $args
     */
    public function prePersist(LifecycleEventArgs $args)
    {
        /** @var SpecialistProjectPhase $specialistProjectPhase */
        $specialistProjectPhase = $args->getEntity();

        // only acts on some "SpecialistProject" entity
        if (!$specialistProjectPhase instanceof SpecialistProjectPhase) {
            return;
        }

        $em = $args->getEntityManager();

        /** @var Sequence $sequence */
        $sequence = $em->getRepository('AppBundle:Sequence')->findOneByName('SpecialistProject');

        // sets the reference number
        $referencePrefix = $specialistProjectPhase->getProjectGeneralInfo()->getCycle()->getStateDepartmentName();
        $specialistProjectPhase->getProjectGeneralInfo()->setReferenceNumber($referencePrefix .'-'. $sequence->getNextValue());

        // updates the Sequence
        $sequence->setNextValue($sequence->getNextValue()+1);
        $em->persist($sequence);
        $em->flush();
    }
}