<?php

namespace AppBundle\EventListener;

use AppBundle\Entity\Country;
use AppBundle\Entity\FellowProject;
use AppBundle\Entity\LocationHostInfo;
use AppBundle\Entity\ProjectGeneralInfo;
use AppBundle\Entity\ReloLocation;
use AppBundle\Entity\WizardStepStatus;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Event\LifecycleEventArgs;

/**
 * FellowProjectListener listener.
 *
 * @package AppBundle\EventListener
 * @author  Juan Obando <juan.obando@inqbation.com>
 */
class ProjectGeneralInfoListener
{
    /**
     * @param LifecycleEventArgs $args
     */
    public function postUpdate(LifecycleEventArgs $args)
    {
        /** @var ProjectGeneralInfo $projectGeneralInfo */
        $projectGeneralInfo = $args->getEntity();

        // only act on some "ProjectGeneralInfo" entity
        if (!$projectGeneralInfo instanceof ProjectGeneralInfo) {
            return;
        }

        $em = $args->getEntityManager();

        // Apply to Fellow Proposal only
        if (null !== $projectGeneralInfo->getFellowProject()) {
            $this->setReloLocation($projectGeneralInfo, $em);
            $this->setRELORanking($projectGeneralInfo, $em);
            $this->setLocationHostInfoCountry($projectGeneralInfo, $em);
            $this->setRequirementsSectionStatus($projectGeneralInfo, $em);
            $this->setCertificationsSectionStatus($projectGeneralInfo, $em);
        }
    }

    /**
     * Assign the RELO Location to the Project General Info.  It will take
     * the first RELO Location from the list of them associated to the country.
     *
     * @param ProjectGeneralInfo                         $projectGeneralInfo
     * @param \Doctrine\Common\Persistence\ObjectManager $em
     */
    private function setReloLocation($projectGeneralInfo, $em)
    {
        /** @var Country $country */
        $country = $projectGeneralInfo->getCountries()->last();

        if ($country) {
            $reloLocations = $country->getReloLocations();

            if (!$reloLocations->isEmpty()) {
                // It doesn't matter if there are more than one RELO location
                // associated to the country, the first one will be the one
                // selected.
                /** @var ReloLocation $reloLocation */
                $reloLocation = $reloLocations->first();

                if ($reloLocation !== $projectGeneralInfo->getReloLocation()) {
                    $projectGeneralInfo->setReloLocation($reloLocation);

                    $em->persist($projectGeneralInfo);
                    $em->flush();
                }
            }
        }
    }

    /**
     * @param ProjectGeneralInfo $projectGeneralInfo
     * @param \Doctrine\Common\Persistence\ObjectManager $em
     */
    private function setRELORanking($projectGeneralInfo, $em)
    {
        // If the ranking for this Proposal has been already set, ignore and continue.
        if (null !== $projectGeneralInfo->getReloRanking()) {
            return;
        }

        // Set the ranking for the RELO Location associated to the Proposal.
        if (null !== $projectGeneralInfo->getReloLocation()) {
            $ranking = $em
              ->getRepository('AppBundle:FellowProject')
              ->countAllByRELOLocationInCycle(
                new ArrayCollection([$projectGeneralInfo->getReloLocation()]),
                $projectGeneralInfo->getCycle()
              );

            $projectGeneralInfo->setReloRanking($ranking);

            $em->persist($projectGeneralInfo);
            $em->flush();
        }
    }

    /**
     * Assign the Fellow project's country to associated Location and Host
     *
     * @param ProjectGeneralInfo                         $projectGeneralInfo
     * @param \Doctrine\Common\Persistence\ObjectManager $em
     */
    private function setLocationHostInfoCountry(ProjectGeneralInfo $projectGeneralInfo, $em)
    {
        /** @var LocationHostInfo $locationHostInfo */
        $locationHostInfo = $projectGeneralInfo->getFellowProject()->getLocationHostInfo();

        /** @var Country $country */
        $country = $projectGeneralInfo->getCountries()->last();

        if ((null !== $locationHostInfo) && ($country) && ($country !== $locationHostInfo->getCountry())) {
            $locationHostInfo->setCountry($country);

            $em->persist($locationHostInfo);
            $em->flush();
        }
    }

    /**
     * @param ProjectGeneralInfo                         $projectGeneralInfo
     * @param \Doctrine\Common\Persistence\ObjectManager $em
     */
    private function setRequirementsSectionStatus(ProjectGeneralInfo $projectGeneralInfo, $em)
    {
        /** @var FellowProject $fellowProject */
        $fellowProject = $projectGeneralInfo->getFellowProject();

        $newStepStatus = WizardStepStatus::NOT_STARTED;

        // Required fields
        if ((null !== $projectGeneralInfo->getVisaPriorArrival())
          && (null !== $projectGeneralInfo->getRequiredMedicalConditions())
          && (null !== $projectGeneralInfo->getMedicalAccessIssues())
          && (null !== $projectGeneralInfo->getAgeRestriction())
          && (null !== $projectGeneralInfo->getDegreeRequirements())
        ) {
            $newStepStatus = WizardStepStatus::COMPLETED;
        }
        // Required + Optional fields
        elseif ((null !== $projectGeneralInfo->getVisaPriorArrival())
          || (null !== $projectGeneralInfo->getVisaAvgLength())
          || (null !== $projectGeneralInfo->getVisaRequirementsDesc())
          || (null !== $projectGeneralInfo->getRequiredMedicalConditions())
          || (null !== $projectGeneralInfo->getMedicalAccessIssues())
          || (null !== $projectGeneralInfo->getMedicalRestrictionsDesc())
          || (null !== $projectGeneralInfo->getAgeRestriction())
          || (null !== $projectGeneralInfo->getAgeRestrictionDesc())
          || (null !== $projectGeneralInfo->getDegreeRequirements())
          || (null !== $projectGeneralInfo->getDegreeRequirementsDesc())
          || (null !== $projectGeneralInfo->getSecurityInfoDesc())
          || (null !== $projectGeneralInfo->getAdditionalRequirementsDesc())
        ) {
            $newStepStatus = WizardStepStatus::IN_PROGRESS;
        }

        /** @var WizardStepStatus $projectRequirementsStepStatus */
        $projectRequirementsStepStatus = $em->getRepository('AppBundle:WizardStepStatus')
          ->findStepStatusByProject(
            $fellowProject,
            WizardStepStatus::PROJECT_REQUIREMENTS_STEP
          );

        if ($newStepStatus !== $projectRequirementsStepStatus->getStatus()) {
            $projectRequirementsStepStatus->setStatus($newStepStatus);

            $em->persist($projectRequirementsStepStatus);
            $em->flush();
        }
    }

    /**
     * @param ProjectGeneralInfo       $projectGeneralInfo
     * @param \Doctrine\Common\Persistence\ObjectManager $em
     */
    private function setCertificationsSectionStatus($projectGeneralInfo, $em)
    {
        /** @var FellowProject $fellowProject */
        $fellowProject = $projectGeneralInfo->getFellowProject();

        $newStepStatus = WizardStepStatus::NOT_STARTED;

        if ($projectGeneralInfo->getRsoCertification()
          && $projectGeneralInfo->getHousingCertification()
          && $projectGeneralInfo->getVisaCertification()
          && $projectGeneralInfo->getMedicalCertification()
          && $projectGeneralInfo->getPostArrivalCertification()
          && $projectGeneralInfo->getCostSharingCertification()
        ) {
            $newStepStatus = WizardStepStatus::COMPLETED;
        }
        elseif ((null !== $projectGeneralInfo->getRsoCertification())
          || (null !== $projectGeneralInfo->getHousingCertification())
          || (null !== $projectGeneralInfo->getVisaCertification())
          || (null !== $projectGeneralInfo->getMedicalCertification())
          || (null !== $projectGeneralInfo->getPostArrivalCertification())
          || (null !== $projectGeneralInfo->getCostSharingCertification())
        ) {
            $newStepStatus = WizardStepStatus::IN_PROGRESS;
        }

        /** @var WizardStepStatus $certificationsSectionStatus */
        $certificationsSectionStatus = $em->getRepository('AppBundle:WizardStepStatus')
          ->findStepStatusByProject(
            $fellowProject,
            WizardStepStatus::CERTIFICATIONS_STEP
          );

        if ($newStepStatus !== $certificationsSectionStatus->getStatus()) {
            $certificationsSectionStatus->setStatus($newStepStatus);

            $em->persist($certificationsSectionStatus);
            $em->flush();
        }
    }
}
