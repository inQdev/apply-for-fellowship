<?php
/**
 * Listener to switch role in Symfony when changes on Drupal side
 * @author felipe.ceballos@inqbation.com
 * @date 4/19/2016
 */

namespace AppBundle\EventListener;

use HWI\Bundle\OAuthBundle\Security\Core\Authentication\Token\OAuthToken;
use AppBundle\Controller\RoleSwitcherInterface;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\FilterControllerEvent;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;

class RoleSwitcherListener
{
	private $token;

	public function __construct(TokenStorage $token) {
		$this->token = $token;
	}

	public function onKernelController(FilterControllerEvent $event) {
		$controller = $event->getController();

		if (!is_array($controller)) {
			return;
		}

		if ($controller[0] instanceof RoleSwitcherInterface) {

			$request = Request::createFromGlobals();
			// Reads the role set on the Drupal side using the cookie
			$current_role = $request->cookies->get('Application_current_role');

			// for local environments, sets a cookie with a manually set role
			if (!$current_role || $request->query->get('current_role')) {
				$current_role = $request->query->get('current_role')?$request->query->get('current_role'):'Embassy';

//				dump($current_role); exit;
				$cookie = new Cookie('Application_current_role', $current_role, 0, '/');
				$response = new Response();
				$response->headers->setCookie($cookie);
				$response->sendHeaders();
			}

			if ($current_role) {
				$current_role = 'ROLE_'. strtoupper(str_replace(' ', '_', $current_role));
				$token = $this->token->getToken();
				$new_roles = array($current_role, 'ROLE_USER');

				// checks if the role changed
				if (count($token->getRoles()) > 2 || $current_role != $token->getRoles()[0]) {

					// We have to create a token from scratch to be able to rebuild the roles attribute
					$new_token = new OAuthToken($token->getRawToken(), $new_roles);
					$new_token->setResourceOwnerName($token->getResourceOwnerName());
					$new_token->setUser($token->getUser());
					$new_token->setCreatedAt($token->getCreatedAt());
					$new_token->setTokenSecret($token->getTokenSecret());
					$new_token->setAuthenticated(true);

					// Finally, replaces the existing token
					$this->token->setToken($new_token);
				}
			}

			return;
		}

	}

} // end class