<?php

namespace AppBundle\EventListener;

use AppBundle\Entity\Country;
use AppBundle\Entity\ProjectGeneralInfo;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;

/**
 * AddPostFieldSubscriber event listener.
 *
 * @package AppBundle\EventListener
 * @author  Juan Obando <juan.obando@inqbation.com>
 * @author  Felipe Ceballos <felipe.ceballos@inqbation.com>
 */
class AddPostReloFieldsSubscriber implements EventSubscriberInterface
{
    /**
     * Returns an array of event names this subscriber wants to listen to.
     *
     * The array keys are event names and the value can be:
     *
     *  * The method name to call (priority defaults to 0)
     *  * An array composed of the method name to call and the priority
     *  * An array of arrays composed of the method names to call and respective
     *    priorities, or 0 if unset
     *
     * For instance:
     *
     *  * array('eventName' => 'methodName')
     *  * array('eventName' => array('methodName', $priority))
     *  * array('eventName' => array(array('methodName1', $priority), array('methodName2')))
     *
     * @return array The event names to listen to
     */
    public static function getSubscribedEvents()
    {
        return [FormEvents::PRE_SET_DATA => 'preSetData'];
    }

    /**
     * @param FormEvent $event
     */
    public function preSetData(FormEvent $event)
    {
        /* @var ProjectGeneralInfo $projectGeneralInfo */
        $projectGeneralInfo = $event->getData();

        /* @var Country $country */
        $country = $projectGeneralInfo->getCountries()->first(); // in Fellow Proposal we just need the first element, if exists

        $form = $event->getForm();

        if (null !== $country) {
            $form->add(
              'post',
              EntityType::class,
              [
                'class'       => 'AppBundle:Post',
                'choices'     => $country->getPosts(),
                'label'       => 'label.post',
                'placeholder' => 'Select a post',
                'required'    => false,
                'data'        => $projectGeneralInfo->getPosts()->first(),
                'mapped'      => false,
              ]
            );
        }
    }
}
