<?php

namespace AppBundle\Form;

use AppBundle\Entity\ContactPoint;
use AppBundle\Form\Type\CustomDateType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * ContactPointType form.
 *
 * @package AppBundle\Form
 * @author  Juan Obando <juan.obando@inqbation.com>
 */
class ContactPointType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
          ->add('firstName', null, ['label' => 'First name'])
          ->add('lastName', null, ['label' => 'Last name'])
          ->add(
            'title',
            ChoiceType::class,
            [
              'label'       => 'label.title',
              'choices'     => ContactPoint::getTitleChoices(),
              'placeholder' => 'Select an option',
              'required'    => false,
            ]
          )
          ->add('email', null, ['label' => 'label.email'])
          ->add('alternateEmail', null)
          ->add('anticipatedDepartureDate', CustomDateType::class);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
          ['data_class' => 'AppBundle\Entity\ContactPoint']
        );
    }
}
