<?php

namespace AppBundle\Form\Fellow;

use AppBundle\Entity\Country;
use AppBundle\Entity\Post;
use AppBundle\Form\BaseWizardStepType;
use AppBundle\Form\Type\CustomDateType;
use AppBundle\Form\Type\CustomTextareaType;
use AppBundle\Form\Type\YesNoType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\FormBuilderInterface;

/**
 * CreateProjectType form.
 *
 * @package AppBundle\Form\Fellow
 * @author  Juan Obando <juan.obando@inqbation.com>
 */
class CreateProjectType extends BaseWizardStepType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        $builder
          ->add(
            'region',
            null,
            ['label' => 'label.region', 'placeholder' => 'Select a region']
          )
          ->add(
            'country',
            EntityType::class,
            [
              'class'       => Country::class,
              'label'       => 'label.country',
              'placeholder' => 'Select a country',
              'mapped'      => false,
            ]
          )
          ->add(
            'post',
            EntityType::class,
            [
              'class'       => Post::class,
              'label'       => 'label.post',
              'placeholder' => 'Select a post',
              'mapped'      => false,
            ]
          )
          ->add(
            'proposedStartDate',
            CustomDateType::class,
            [
              'property_path' => 'fellowProject.proposedStartDate',
              'label'         => 'label.proposed_start_date',
            ]
          )
          ->add(
            'proposedStartDateFlexible',
            YesNoType::class,
            [
              'property_path' => 'fellowProject.proposedStartDateFlexible',
              'required'      => true,
            ]
          )
          ->add(
            'cycleSeasonDesc',
            CustomTextareaType::class,
            [
              'property_path' => 'fellowProject.cycleSeasonDesc',
              'label'         => 'label.cycle_season_desc',
              'required'      => false,
            ]
          );
    }
}
