<?php

namespace AppBundle\Form\Fellow;

use AppBundle\Form\Type\CustomTextareaType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * DutyActivityType entity.
 *
 * @package AppBundle\Form\Fellow
 */
class DutyActivityType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
          ->add(
            'description',
            CustomTextareaType::class,
            ['label' => 'label.activity_desc']
          )
          ->add(
            'hoursNumber',
            NumberType::class,
            [
              'label'           => 'label.hours_number',
              'required'        => false,
              'invalid_message' => 'Invalid',
            ]
          )
          ->add(
            'audience',
            CustomTextareaType::class,
            ['label' => 'label.audience']
          )
          ->add(
            'activitiesFocus',
            null,
            [
              'expanded' => true,
              'multiple' => true,
            ]
          );
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
          ['data_class' => 'AppBundle\Entity\Fellow\DutyActivityPrimary']
        );
    }
}
