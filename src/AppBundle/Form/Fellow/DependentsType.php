<?php

namespace AppBundle\Form\Fellow;

use AppBundle\Form\BaseWizardStepType;
use AppBundle\Form\Type\CustomTextareaType;
use AppBundle\Form\Type\YesNoType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * DependentsType form.
 *
 * @package AppBundle\Form\Fellow
 * @author  Juan Obando <juan.obando@inqbation.com>
 */
class DependentsType extends BaseWizardStepType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        $builder
          ->add(
            'bringDependents',
            YesNoType::class,
            ['label' => 'label.bring_dependents']
          )
          ->add('bringChildren', YesNoType::class)
          ->add('bringUnmarriedPartner', YesNoType::class)
          ->add('bringSameSexSpouse', YesNoType::class)
          ->add('bringSameSexPartner', YesNoType::class)
          ->add(
            'dependentRestrictionsDesc',
            CustomTextareaType::class,
            ['label' => 'label.dependent_restrictions_desc']
          )
          ->add(
            'housingDependents',
            YesNoType::class,
            ['label' => 'Is housing suitable for dependents?']
          )
          ->add('fellowFundsHousing', YesNoType::class)
          ->add('housingDependentInfo', CustomTextareaType::class);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
          ['data_class' => 'AppBundle\Entity\FellowProject']
        );
    }
}