<?php

namespace AppBundle\Form\Fellow;

use AppBundle\Entity\Fellow\DutyActivitySecondary;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class DutyActivitySecondaryType
 *
 * @package AppBundle\Form\Fellow
 */
class DutyActivitySecondaryType extends DutyActivityType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        $builder
          ->add(
            'hoursPeriod',
            ChoiceType::class,
            [
              'label'       => 'label.hours_period',
              'choices'     => DutyActivitySecondary::getHoursPeriodChoices(),
              'required'    => false,
              'placeholder' => 'Select a period'
            ]
          );
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
          ['data_class' => 'AppBundle\Entity\Fellow\DutyActivitySecondary']
        );
    }
}
