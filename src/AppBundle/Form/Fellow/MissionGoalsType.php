<?php

namespace AppBundle\Form\Fellow;

use AppBundle\Form\BaseWizardStepType;
use AppBundle\Form\Type\CustomTextareaType;
use Symfony\Component\Form\FormBuilderInterface;

/**
 * MissionGoalsSustainabilityType
 *
 * @package AppBundle\Form
 * @author  Juan Obando <juan.obando@inqbation.com>
 */
class MissionGoalsType extends BaseWizardStepType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        $builder
          ->add(
            'postMissionGoalsDesc',
            CustomTextareaType::class,
            ['label' => 'label.post_mission_goals_desc']
          );
    }
}
