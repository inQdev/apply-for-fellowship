<?php

namespace AppBundle\Form;

use AppBundle\Form\Type\CustomTextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;

class FellowProjectBudgetFundedType extends FellowProjectBudgetBaseType
{
	/**
	 * @param FormBuilderInterface $builder
	 * @param array $options
	 */
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		parent::buildForm($builder, $options);

		$builder
			->add('revision_id', HiddenType::class)
			->add('funded_by', ChoiceType::class, [
				'choices' => [
					'ECA' => 'ECA',
					'Post' => 'Post',
				],
				'label' => 'This proposal will be funded by:',
				'expanded' => true,
				'choice_attr' => function () {
					return ['class' => 'yes_no_choice'];
				}
			])
			->add(
				'postFundingSource', CustomTextareaType::class, [
					'label' => 'Post funding source:'
				]
			);

	}

}
