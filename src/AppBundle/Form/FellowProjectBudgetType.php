<?php

namespace AppBundle\Form;

use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class FellowProjectBudgetType extends FellowProjectBudgetBaseType
{

	/**
	 * @param FormBuilderInterface $builder
	 * @param array $options
	 */
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		parent::buildForm($builder, $options);

		$builder
			->add('revision_id', HiddenType::class)
			->add('budgetCostItems', CollectionType::class, [
				'entry_type' => FellowProjectBudgetCostItemType::class,
				'label_format' => ' ',
				'by_reference' => false,
				'required' => false,
				'error_bubbling' => true,
				'allow_add' => true,
			])
			->add('delete_id', HiddenType::class, [
				'mapped' => false,
			])
			->add('back', SubmitType::class, ['label' => 'Save and go back'])
			->add('next', SubmitType::class, ['label' => 'Save and continue'])
			->add('calculate1', SubmitType::class, ['label' => 'Calculate'])
			->add('calculate2', SubmitType::class, ['label' => 'Calculate'])
			->add('additional', SubmitType::class, ['label' => 'Add a cost'])
			->add('additional2', SubmitType::class, ['label' => 'Add an In-kind contribution']);
	}

}
