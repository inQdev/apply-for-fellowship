<?php

namespace AppBundle\Form;

use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\FormBuilderInterface;

class UserEmbassyType extends UserBaseType
{
	/**
	 * @param FormBuilderInterface $builder
	 * @param array $options
	 */
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		parent::buildForm($builder, $options);

		$builder
			->add('countries', EntityType::class, [
				'class' => 'AppBundle:Country',
				'multiple' => true,
				'expanded' => true,
				'query_builder' => function (EntityRepository $er) {
					return $er->createQueryBuilder('c')->orderBy('c.name', 'ASC');
				},
				'required' => false,
			]);
	}

}
