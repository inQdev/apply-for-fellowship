<?php

namespace AppBundle\Form\Common;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * ContactPointType form.
 *
 * @package AppBundle\Form
 * @author  Juan Obando <juan.obando@inqbation.com>
 */
class ContactPointType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
          ->add('firstName', null, ['label' => 'First name'])
          ->add('lastName', null, ['label' => 'Last name'])
          ->add('title', null, ['label' => 'label.title'])
          ->add('email', null, ['label' => 'label.email']);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
          ['data_class' => 'AppBundle\Entity\ContactPoint']
        );
    }
}
