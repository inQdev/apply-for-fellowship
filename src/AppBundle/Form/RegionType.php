<?php

namespace AppBundle\Form;

use Symfony\Component\Form\FormBuilderInterface;

/**
 * RegionType form.
 *
 * @package AppBundle\Form
 * @author  Juan Obando <juan.obando@inqbation.com>
 */
class RegionType extends BaseWizardStepType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        $builder->add(
          'region',
          null,
          [
            'label'       => 'label.region',
            'placeholder' => 'Select a region',
            'required'    => false,
          ]
        );
    }
}
