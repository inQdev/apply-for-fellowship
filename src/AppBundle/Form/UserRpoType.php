<?php

namespace AppBundle\Form;

use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\FormBuilderInterface;

/**
 * Class UserReloType
 *
 * @package AppBundle\Form
 */
class UserRpoType extends UserBaseType
{
	/**
	 * @param FormBuilderInterface $builder
	 * @param array $options
	 */
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		parent::buildForm($builder, $options);

		$builder->add(
			'regions',
			EntityType::class,
			[
				'class' => 'AppBundle:Region',
				'multiple' => true,
				'expanded' => true,
				'query_builder' => function (EntityRepository $er) {
					return $er->createQueryBuilder('r')->orderBy('r.name', 'ASC');
				},
				'required' => false
			]
		);
	}
}