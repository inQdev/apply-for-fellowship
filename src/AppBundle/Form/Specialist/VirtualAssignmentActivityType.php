<?php

namespace AppBundle\Form\Specialist;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use AppBundle\Form\Type\CustomTextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class VirtualAssignmentActivityType extends AbstractType
{
	/**
	 * @param FormBuilderInterface $builder
	 * @param array $options
	 */
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder
			->add('activityDesc', CustomTextareaType::class, [
				'required' => false,
			])
			->add('audience', CustomTextareaType::class, [
				'required' => false,
			])
			->add('participants', TextType::class, [
				'required' => false,
			])
			->add('hoursPerWeek', NumberType::class, [
				'required' => false,
			])
			->add('hoursTotal', NumberType::class, [
				'required' => false,
			])
		;
	}

	/**
	 * @param OptionsResolver $resolver
	 */
	public function configureOptions(OptionsResolver $resolver)
	{
		$resolver->setDefaults(array(
			'data_class' => 'AppBundle\Entity\VirtualAssignmentActivity'
		));
	}
}
