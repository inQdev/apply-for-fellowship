<?php

namespace AppBundle\Form\Specialist;

use AppBundle\Entity\ContributionTransportationCostItem;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * ContributionTransportationCostItemType form.
 *
 * @package AppBundle\Form\Specialist
 * @author  Juan Obando <juan.obando@inqbation.com>
 */
class ContributionTransportationCostItemType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
          ->add(
            'costItem',
            CostItemContributionTransportationDescriptionType::class
          )
          ->add('totalCost')
          ->add('hostContributionMonetary')
          ->add('hostContributionInKind')
          ->add('postHostTotalContribution', null, ['disabled' => true])
          ->addEventListener(
            FormEvents::PRE_SET_DATA,
            function (FormEvent $event) {
                /** @var ContributionTransportationCostItem $contributionTransportationCostItem */
                $contributionTransportationCostItem = $event->getData();

                $form = $event->getForm();

                if (null !== $contributionTransportationCostItem) {
                    $fundedBy = $contributionTransportationCostItem->getContributionTransportation()->getPhaseBudget()->getFundedBy();

                    if ('ECA' === $fundedBy) {
                        $form
                          ->add('postContribution')
                          ->add(
                            'ecaTotalContribution',
                            null,
                            ['disabled' => true]
                          );
                    } elseif ('Post' === $fundedBy) {
                        $form
                          ->add(
                            'postTotalContribution',
                            null,
                            ['disabled' => true]
                          );
                    }
                }
            }
          )
          ->addEventListener(
            FormEvents::PRE_SUBMIT,
            function (FormEvent $event) {
                $contributionTransportationCostItem = $event->getData();

                $contributionTransportationCostItem['totalCost'] = str_replace(
                  ['$', ','],
                  '',
                  $contributionTransportationCostItem['totalCost']
                );
                $contributionTransportationCostItem['hostContributionMonetary'] = str_replace(
                  ['$', ','],
                  '',
                  $contributionTransportationCostItem['hostContributionMonetary']
                );
                $contributionTransportationCostItem['hostContributionInKind'] = str_replace(
                  ['$', ','],
                  '',
                  $contributionTransportationCostItem['hostContributionInKind']
                );

                if (array_key_exists('postContribution', $contributionTransportationCostItem)) {
                    $contributionTransportationCostItem['postContribution'] = str_replace(
                      ['$', ','],
                      '',
                      $contributionTransportationCostItem['postContribution']
                    );
                }

                $event->setData($contributionTransportationCostItem);
            }
          );
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
          ['data_class' => 'AppBundle\Entity\ContributionTransportationCostItem']
        );
    }
}
