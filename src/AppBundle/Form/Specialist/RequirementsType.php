<?php

namespace AppBundle\Form\Specialist;

use AppBundle\Form\BaseWizardStepType;
use AppBundle\Form\Type\CustomTextareaType;
use AppBundle\Form\Type\YesNoType;
use Symfony\Component\Form\FormBuilderInterface;

/**
 * RequirementsType form.
 *
 * @package AppBundle\Form\Specialist
 * @author  Juan Obando <juan.obando@inqbation.com>
 */
class RequirementsType extends BaseWizardStepType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        $builder
          ->add(
            'visaPriorArrival',
            YesNoType::class,
            ['label' => 'label.visa_prior_arrival']
          )
          ->add(
            'visaAvgLength',
            null,
            [
              'label'       => 'label.visa_avg_length',
              'placeholder' => 'Select an option',
            ]
          )
          ->add(
            'visaRequirementsDesc',
            CustomTextareaType::class,
            ['label' => 'label.visa_requirements_desc']
          )
          ->add(
            'requiredMedicalConditions',
            YesNoType::class,
            ['label' => 'label.medical_access_issues']
          )
          ->add('medicalAccessIssues', YesNoType::class)
          ->add(
            'medicalRestrictionsDesc',
            CustomTextareaType::class,
            ['label' => 'label.medical_restrictions_desc']
          )
          ->add('requiredVaccinations', YesNoType::class)
          ->add('requiredVaccinationsDesc', CustomTextareaType::class)
          ->add('ageRestriction', YesNoType::class)
          ->add('ageRestrictionDesc', CustomTextareaType::class)
          ->add(
            'degreeRequirements',
            YesNoType::class,
            ['label' => 'label.degree_requirements']
          )
          ->add(
            'degreeRequirementsDesc',
            CustomTextareaType::class,
            ['label' => 'label.degree_requirements_desc',]
          )
          ->add(
            'securityInfoDesc',
            CustomTextareaType::class,
            ['label' => 'label.security_info_desc']
          )
          ->add(
            'additionalRequirementsDesc',
            CustomTextareaType::class,
            ['label' => 'label.additional_requirements_desc']
          );
    }
}
