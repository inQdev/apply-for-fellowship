<?php

namespace AppBundle\Form\Specialist;

//use AppBundle\Entity\InCountryAssignment;
use AppBundle\Form\BaseWizardStepType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class InCountryAssignmentsType extends BaseWizardStepType
{
	/**
	 * @param FormBuilderInterface $builder
	 * @param array $options
	 */
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		parent::buildForm($builder, $options);

		$builder
//			->add('addCity', SubmitType::class, ['label' => 'Add country'])
			->add(
				'inCountryAssignmentCity',
				CollectionType::class,
				[
					'entry_type' => InCountryAssignmentCityType::class,
					'label_format' => ' ',
					'by_reference' => false,
					'required' => false,
					'error_bubbling' => true,
					'allow_add' => true,
				])
			->add('delete_id', HiddenType::class, [
				'mapped' => false,
				'data' => '',
			]);
	}

	/**
	 * @param OptionsResolver $resolver
	 */
	public function configureOptions(OptionsResolver $resolver)
	{
		$resolver->setDefaults(array(
			'data_class' => null
		));
	}
}
