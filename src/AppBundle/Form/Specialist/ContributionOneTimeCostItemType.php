<?php

namespace AppBundle\Form\Specialist;

use AppBundle\Entity\ContributionOneTimeCostItem;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * ContributionOneTimeCostItemType form.
 *
 * @package AppBundle\Form\Specialist
 * @author  Juan Obando <juan.obando@inqbation.com>
 */
class ContributionOneTimeCostItemType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
          ->add(
            'costItem',
            CostItemContributionOneTimeDescriptionType::class
          )
          ->add('totalCost')
          ->add('hostContributionMonetary')
          ->add('hostContributionInKind')
          ->add('postHostTotalContribution', null, ['disabled' => true])
          ->addEventListener(
            FormEvents::PRE_SET_DATA,
            function (FormEvent $event) {
                /** @var ContributionOneTimeCostItem $contributionOneTimeCostItem */
                $contributionOneTimeCostItem = $event->getData();

                $form = $event->getForm();

                if (null !== $contributionOneTimeCostItem) {
                    $fundedBy = $contributionOneTimeCostItem->getContributionOneTime()->getPhaseBudget()->getFundedBy();

                    if ('ECA' === $fundedBy) {
                        $form
                          ->add('postContribution')
                          ->add(
                            'ecaTotalContribution',
                            null,
                            ['disabled' => true]
                          );
                    } elseif ('Post' === $fundedBy) {
                        $form
                          ->add(
                            'postTotalContribution',
                            null,
                            ['disabled' => true]
                          );
                    }
                }
            }
          )
          ->addEventListener(
            FormEvents::PRE_SUBMIT,
            function (FormEvent $event) {
                $contributionOneTimeCostItem = $event->getData();

                $contributionOneTimeCostItem['totalCost'] = str_replace(
                  ['$', ','],
                  '',
                  $contributionOneTimeCostItem['totalCost']
                );
                $contributionOneTimeCostItem['hostContributionMonetary'] = str_replace(
                  ['$', ','],
                  '',
                  $contributionOneTimeCostItem['hostContributionMonetary']
                );
                $contributionOneTimeCostItem['hostContributionInKind'] = str_replace(
                  ['$', ','],
                  '',
                  $contributionOneTimeCostItem['hostContributionInKind']
                );

                if (array_key_exists('postContribution', $contributionOneTimeCostItem)) {
                    $contributionOneTimeCostItem['postContribution'] = str_replace(
                      ['$', ','],
                      '',
                      $contributionOneTimeCostItem['postContribution']
                    );
                }

                $event->setData($contributionOneTimeCostItem);
            }
          );
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
          ['data_class' => 'AppBundle\Entity\ContributionOneTimeCostItem']
        );
    }
}
