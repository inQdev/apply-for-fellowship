<?php

namespace AppBundle\Form\Specialist;

use AppBundle\Form\BaseWizardStepType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * ContributionTransportationType
 *
 * @package AppBundle\Form\Specialist
 * @author  Juan Obando <juan.obando@inqbation.com>
 */
class ContributionTransportationType extends BaseWizardStepType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        $builder
          ->add(
            'contributionTransportationCostItems',
            CollectionType::class,
            [
              'entry_type' => ContributionTransportationCostItemType::class,
            ]
          )
          ->add('costItemToDeleteId', HiddenType::class, ['mapped' => false])
          ->add(
            'additional',
            SubmitType::class,
            ['label' => 'Add additional transportation cost']
          );
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
          ['data_class' => 'AppBundle\Entity\ContributionTransportation']
        );
    }
}
