<?php

namespace AppBundle\Form\Specialist;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;

/**
 * Class SubmitSpecialistProposalType
 *
 * @package AppBundle\Form
 */
class SubmitSpecialistProposalType extends AbstractType
{
    /**
     * @param \Symfony\Component\Form\FormBuilderInterface $builder
     * @param array                                        $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
          ->add(
            'submitSpecialistProposal',
            SubmitType::class,
            ['label' => 'Submit proposal']
          );
    }
}