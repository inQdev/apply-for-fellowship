<?php

namespace AppBundle\Form\Specialist;

use AppBundle\Form\Type\CustomTextareaType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class InCountryAssignmentActivityType extends AbstractType
{
	/**
	 * @param FormBuilderInterface $builder
	 * @param array $options
	 */
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder
			->add('activityDesc', CustomTextareaType::class, [
				'label' => 'Description of Activity',
				'required' => false,
			])
			->add('topic', null, [
				'label' => 'Topic',
				'required' => false,
			])
			->add('audienceDesc', CustomTextareaType::class, [
				'label' => 'Description of Audience',
				'required' => false,
			])
			->add('participants', TextType::class)
		;
	}

	/**
	 * @param OptionsResolver $resolver
	 */
	public function configureOptions(OptionsResolver $resolver)
	{
		$resolver->setDefaults(array(
			'data_class' => 'AppBundle\Entity\InCountryAssignmentActivity'
		));
	}
}
