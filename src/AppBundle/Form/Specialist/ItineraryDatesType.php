<?php

namespace AppBundle\Form\Specialist;

use AppBundle\Form\BaseWizardStepType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use AppBundle\Form\Type\CustomDateType;
use AppBundle\Form\Type\CustomTextareaType;

class ItineraryDatesType extends BaseWizardStepType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);
        
        $builder
            ->add('startDate', CustomDateType::class, [
                'label' => 'Start Date',
            ])
            ->add('endDate', CustomDateType::class, [
                'label' => 'End Date',
            ])
            ->add('dateLengthDesc', CustomTextareaType::class, [
                'label' => 'Date and length comment',
            ])
        ;
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\SpecialistProjectPhase'
        ));
    }
}
