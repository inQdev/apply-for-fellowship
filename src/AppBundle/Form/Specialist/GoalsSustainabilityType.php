<?php

namespace AppBundle\Form\Specialist;

use AppBundle\Form\BaseWizardStepType;
use AppBundle\Form\Type\CustomTextareaType;
use Symfony\Component\Form\FormBuilderInterface;

/**
 * GoalsSustainabilityType form.
 *
 * @package AppBundle\Form\Specialist
 * @author  Luke Torres <lucas.torres@inqbation.com>
 */
class GoalsSustainabilityType extends BaseWizardStepType
{
  /**
   * @param FormBuilderInterface $builder
   * @param array                $options
   */
  public function buildForm(FormBuilderInterface $builder, array $options)
  {
    parent::buildForm($builder, $options);

    $builder
      ->add('postMissionGoalsDesc',
        CustomTextareaType::class
      )
      ->add('sustainabilityByLocalsDesc',
        CustomTextareaType::class
      )
      ->add('specialistProjectPhase',
        PhaseSubsequentType::class
      );
  }

}