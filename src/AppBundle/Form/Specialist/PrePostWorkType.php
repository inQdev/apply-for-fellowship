<?php

namespace AppBundle\Form\Specialist;

use AppBundle\Form\BaseWizardStepType;
use AppBundle\Form\Type\CustomDateType;
use AppBundle\Form\Type\CustomTextareaType;
use AppBundle\Form\Type\YesNoType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * PrePostWorkType type.
 *
 * @package AppBundle\Form\Specialist
 * @author  Juan Obando <juan.obando@inqbation.com>
 */
class PrePostWorkType extends BaseWizardStepType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        $builder
          ->add('additionalPreWork', YesNoType::class)
          ->add(
            'additionalPreWorkDays',
            ChoiceType::class,
            [
              'choices'     => ['1' => 1, '2' => 2, '3' => 3],
              'placeholder' => 'Select number of days',
              'required'    => false,
            ]
          )
          ->add('additionalPreWorkStartDate', CustomDateType::class)
          ->add('additionalPreWorkDesc', CustomTextareaType::class)
          ->add('additionalPostWork', YesNoType::class)
          ->add(
            'additionalPostWorkDays',
            ChoiceType::class,
            [
              'choices'     => [
                '1' => 1,
                '2' => 2,
                '3' => 3,
                '4' => 4,
                '5' => 5,
              ],
              'placeholder' => 'Select number of days',
              'required'    => false,
            ]
          )
          ->add('additionalPostWorkStartDate', CustomDateType::class)
          ->add('additionalPostWorkDesc', CustomTextareaType::class);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
          ['data_class' => 'AppBundle\Entity\PrePostWork']
        );
    }
}
