<?php

namespace AppBundle\Form\Specialist;

use AppBundle\Form\BaseWizardStepType;
use AppBundle\Form\Type\CustomTextareaType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;

class VirtualAssignmentCountryType extends BaseWizardStepType
{
	/**
	 * @param FormBuilderInterface $builder
	 * @param array $options
	 */
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		parent::buildForm($builder, $options);

		$builder
			->add('otherCountries', CustomTextareaType::class, [
				'label' => 'If virtual Specialist project will incorporate more than one country, please describe here',
				'required' => false,
			]);

		$builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) {

			$virtualAssignment = $event->getData();
			$countries = $virtualAssignment->getSpecialistProjectPhase()->getProjectGeneralInfo()->getCountries();

			if (!$countries->count()) {
				$countries = [];
			}

			$form = $event->getForm();

			if (null !== $virtualAssignment) {
				$form->add(
					'country',
					EntityType::class,
					[
						'class' => 'AppBundle:Host',
						'choices' => $countries,
						'label' => 'Country',
						'required' => false,
						'placeholder' => 'Select a Country'
					]
				);
			}
		});

	}

	/**
	 * @param OptionsResolver $resolver
	 */
	public function configureOptions(OptionsResolver $resolver)
	{
		$resolver->setDefaults(array(
			'data_class' => 'AppBundle\Entity\VirtualAssignment'
		));
	}
}
