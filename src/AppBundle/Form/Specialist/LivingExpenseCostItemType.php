<?php

namespace AppBundle\Form\Specialist;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Form;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;

class LivingExpenseCostItemType extends AbstractType
{
	/**
	 * @param FormBuilderInterface $builder
	 * @param array $options
	 */
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder
			->add('costPerDay')
			->add('numberOfDays', NumberType::class)
			// Adds the Cost Total field disabled or enabled depending on the cost item
			->addEventListener(FormEvents::PRE_SET_DATA, function(FormEvent $event) {
				$livingExpensecostItem = $event->getData();
				$form = $event->getForm();

				if ($livingExpensecostItem->getCostItem()->getDescription() == 'Ground Transport') {
					$form->add('costTotal', null);
				} else {
					$form->add('costTotal', null, [
						'disabled' => true,
					]);
				}
			})
			// Checks and adjust format for submitted currency fields
			->addEventListener(FormEvents::PRE_SUBMIT, function (FormEvent $event) {
					$livingExpenseItem = $event->getData();

					if (!empty($livingExpenseItem['costPerDay'])) {
						$livingExpenseItem['costPerDay'] = str_replace(['$', ','], '', $livingExpenseItem['costPerDay']);
//						$livingExpenseItem['costTotal'] = $livingExpenseItem['costPerDay'] * $livingExpenseItem['numberOfDays'];
					}

					if (!empty($livingExpenseItem['costTotal'])) {
						$livingExpenseItem['costTotal'] = str_replace(['$', ','], '', $livingExpenseItem['costTotal']);
					}

					$event->setData($livingExpenseItem);
				}
			);
	}

	/**
	 * @param OptionsResolver $resolver
	 */
	public function configureOptions(OptionsResolver $resolver)
	{
		$resolver->setDefaults(array(
			'data_class' => 'AppBundle\Entity\LivingExpenseCostItem'
		));
	}
}
