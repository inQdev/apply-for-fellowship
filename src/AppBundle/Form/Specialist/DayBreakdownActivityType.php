<?php

namespace AppBundle\Form\Specialist;

use AppBundle\Form\Type\CustomDateType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
//use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;

class DayBreakdownActivityType extends AbstractType
{
	/**
	 * @param FormBuilderInterface $builder
	 * @param array $options
	 */
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder
			->add('activityDate', CustomDateType::class)
			->add('dayBreakdownActivityType', EntityType::class, [
				'class' => 'AppBundle:DayBreakdownActivityType',
				'placeholder' => 'Select an Activity type',
				'required' => false,
			])
//            ->add('inCountryAssignment', EntityType::class, [
//                'class' => 'AppBundle:InCountryAssignment',
//                'label' => 'label.primary_host_assignment',
//                'required' => false,
//                'placeholder' => 'Select a City'
//            ])
		;

		$builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) {

			$dayBreakdownActivity = $event->getData();
			$specialistProjectPhase = $dayBreakdownActivity->getSpecialistProjectPhase();

			$form = $event->getForm();

			$form->add(
				'inCountryAssignment',
				EntityType::class,
				[
					'class' => 'AppBundle:InCountryAssignment',
					'query_builder' => function (EntityRepository $er) use ($specialistProjectPhase) {
						$qb = $er->createQueryBuilder('ica');

						return $qb
							->join('ica.country', 'c')
							->where('ica.specialistProjectPhase = :phase')
							->setParameter('phase', $specialistProjectPhase)
							->orderBy('c.name', 'ASC')
							->addOrderBy('ica.city', 'ASC');
					},
					'label' => ' ',
					'required' => false,
					'choice_label' => function($inCountryAssignment) {
						return $inCountryAssignment->getCity() .' ('. $inCountryAssignment->getCountry()->getName() .')';
					},
					'placeholder' => 'Select a City'
				]
			);
		});

	}

	/**
	 * @param OptionsResolver $resolver
	 */
	public function configureOptions(OptionsResolver $resolver)
	{
		$resolver->setDefaults(array(
			'data_class' => 'AppBundle\Entity\DayBreakdownActivity'
		));
	}
}
