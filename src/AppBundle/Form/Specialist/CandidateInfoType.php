<?php

namespace AppBundle\Form\Specialist;

use AppBundle\Form\BaseWizardStepType;
use AppBundle\Form\Type\CustomTextareaType;
use AppBundle\Form\Type\YesNoType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * CandidateInfoType form.
 *
 * @package AppBundle\Form\Specialist
 * @author  Juan Obando <juan.obando@inqbation.com>
 */
class CandidateInfoType extends BaseWizardStepType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        $builder
          ->add(
            'areasOfExpertise',
            null,
            [
              'expanded' => true,
              'multiple' => true,
            ]
          )
          ->add('furtherDetailsDesc', CustomTextareaType::class)
          ->add('degreeRequirements', YesNoType::class)
          ->add('degreeRequirementsDesc', CustomTextareaType::class)
          ->add('proposedCandidate')
          ->add('proposedCandidateContacted', YesNoType::class)
          ->add('proposedCandidateEmail');
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
          ['data_class' => 'AppBundle\Entity\SpecialistProject']
        );
    }
}
