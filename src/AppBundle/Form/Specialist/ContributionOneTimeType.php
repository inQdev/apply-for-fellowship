<?php

namespace AppBundle\Form\Specialist;

use AppBundle\Form\BaseWizardStepType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class ContributionOneTimeType
 *
 * @package AppBundle\Form\Specialist
 */
class ContributionOneTimeType extends BaseWizardStepType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        $builder
          ->add(
            'contributionOneTimeCostItems',
            CollectionType::class,
            [
              'entry_type' => ContributionOneTimeCostItemType::class,
            ]
          )
          ->add('costItemToDeleteId', HiddenType::class, ['mapped' => false])
          ->add(
            'additional',
            SubmitType::class,
            ['label' => 'Add additional Settling in/one time cost']
          );
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
          ['data_class' => 'AppBundle\Entity\ContributionOneTime']
        );
    }
}