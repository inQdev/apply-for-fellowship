<?php

namespace AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class CustomDateType
 *
 * @package AppBundle\Form\Type
 */
class CustomDateType extends AbstractType
{
    /**
     * @param \Symfony\Component\OptionsResolver\OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
          [
            'widget'   => 'single_text',
            'required' => false,
            'html5'    => false,
            'format'   => 'MM/dd/yyyy'
          ]
        );
    }

    /**
     * @return mixed
     */
    public function getParent()
    {
        return DateType::class;
    }
}