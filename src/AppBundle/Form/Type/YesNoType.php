<?php

namespace AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class YesNoType
 *
 * @package AppBundle\Form\Type
 */
class YesNoType extends AbstractType
{
    /**
     * @param \Symfony\Component\OptionsResolver\OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
          [
            'choices'     => [
              'Yes' => true,
              'No'  => false
            ],
            'expanded'    => true,
            'multiple'    => false,
            'required'    => false,
            'placeholder' => false,
            'choice_attr' => function () {
                return ['class' => 'yes_no_choice'];
            },
          ]
        );
    }

    /**
     * @return mixed
     */
    public function getParent()
    {
        return ChoiceType::class;
    }
}