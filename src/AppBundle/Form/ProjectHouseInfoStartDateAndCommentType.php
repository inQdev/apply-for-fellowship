<?php

namespace AppBundle\Form;

use AppBundle\Form\Type\CustomDateType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class ProjectHouseInfoStartDateType
 *
 * @package AppBundle\Form
 */
class ProjectHouseInfoStartDateAndCommentType extends BaseWizardStepType
{
    /**
     * @param \Symfony\Component\Form\FormBuilderInterface $builder
     * @param array                                        $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        $builder
          ->add(
            'proposedStartDate',
            CustomDateType::class,
            ['label' => 'label.proposed_start_date']
          );
    }

    /**
     * @param \Symfony\Component\OptionsResolver\OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
          ['data_class' => 'AppBundle\Entity\FellowProject']
        );
    }
}