<?php

namespace AppBundle\Form;

use AppBundle\Entity\LocationHostInfo;
use AppBundle\EventListener\AddHostFieldSubscriber;
use AppBundle\Form\Type\CustomDateType;
use AppBundle\Form\Type\CustomTextareaType;
use AppBundle\Form\Type\YesNoType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * LocationHostInfoType form.
 *
 * @package AppBundle\Form
 * @author  Juan Obando <juan.obando@inqbation.com>
 */
class LocationHostInfoType extends BaseWizardStepType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        $builder
          ->add(
            'hostCustom',
            TextType::class,
            [
              'label'    => 'Or enter a new Host Institution',
              'required' => false,
              'mapped'   => false,
            ]
          )
          ->add(
            'hostCity',
            null,
            ['label' => 'label.host_city', 'required' => false]
          )
          ->add(
            'hostCityDescription',
            CustomTextareaType::class,
            ['label' => 'Description of Host city', 'required' => false]
          )
          ->add(
            'hostAcademicYear',
            YesNoType::class,
            ['label' => 'label.host_academic_year']
          )
          ->add(
            'hostAcademicYearStartDate',
            CustomDateType::class,
            ['label' => 'label.host_academic_year_start_date']
          )
          ->add(
            'hostAcademicYearEndDate',
            CustomDateType::class,
            ['label' => 'label.host_academic_year_end_date']
          )
          ->add('newHost', YesNoType::class, ['label' => 'label.new_host'])
          ->add(
            'fellowAtHost',
            ChoiceType::class,
            [
              'label'       => 'label.fellow_at_host',
              'choices'     => LocationHostInfo::getFellowAtHostChoices(),
              'placeholder' => 'Select an option',
              'required'    => false,
            ]
          )
          ->add(
            'currentFellowName',
            CustomTextareaType::class,
            ['label' => 'label.current_fellow_name']
          )
          ->add(
            'hostDesc',
            CustomTextareaType::class,
            ['label' => 'label.host_desc']
          )
          ->add(
            'additionalInfoDesc',
            CustomTextareaType::class,
            ['label' => 'Additional Host Institution/Location information']
          );

        $builder->addEventSubscriber(new AddHostFieldSubscriber());
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
          ['data_class' => 'AppBundle\Entity\LocationHostInfo']
        );
    }
}
