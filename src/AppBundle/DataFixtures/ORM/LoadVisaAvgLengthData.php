<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\VisaAvgLength;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

/**
 * Class LoadVisaAvgLengthData
 *
 * @package AppBundle\DataFixtures\ORM
 */
class LoadVisaAvgLengthData extends AbstractFixture implements OrderedFixtureInterface
{
    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $visaAvgLengths = [
          'Less than 1 month',
          '1 month',
          '2 months',
          '3 months',
          '4 months',
          '5 or more months',
        ];

        foreach ($visaAvgLengths as $visaAvgLengthName) {
            $visaAvgLength = new VisaAvgLength();

            $visaAvgLength->setName($visaAvgLengthName);

            $manager->persist($visaAvgLength);
        }

        $manager->flush();
    }

    /**
     * Get the order of this fixture
     *
     * @return integer
     */
    public function getOrder()
    {
        return 8;
    }
}