<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\ReloLocation;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

/**
 * Class LoadReloLocationData
 *
 * @package AppBundle\DataFixtures\ORM
 */
class LoadReloLocationData extends AbstractFixture implements OrderedFixtureInterface
{

    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $reloLocations = [
          'New Delhi, India'        => [
            'countries' => [
              'Afghanistan',
              'Bhutan',
              'India'
            ]
          ],
          'Budapest, Hungary'       => [
            'countries' => [
              'Albania',
              'Bosnia and Herzegovina',
              'Bulgaria',
              'Croatia',
              'Czech Republic',
              'Estonia',
              'Hungary',
              'Kosovo',
              'Latvia',
              'Lithuania',
              'Macedonia',
              'Montenegro',
              'Poland',
              'Romania',
              'Serbia',
              'Slovakia',
              'Slovenia'
            ]
          ],
          'Rabat, Morocco'          => [
            'countries' => [
              'Algeria',
              'Morocco',
              'Tunisia'
            ]
          ],
          'Pretoria, South Africa'  => [
            'countries' => [
              'Angola',
              'Botswana',
              'Comoros',
              'Congo, Democratic Republic of',
              'Congo, Republic of the',
              'Lesotho',
              'Madagascar',
              'Malawi',
              'Mauritius',
              'Mozambique',
              'Namibia',
              'Seychelles',
              'South Africa',
              'Swaziland',
              'Zambia',
              'Zimbabwe'
            ]
          ],
          'Mexico City, Mexico'     => [
            'countries' => [
              'Antigua and Barbuda',
              'Aruba',
              'Bahamas, The',
              'Barbados',
              'Belize',
              'Bermuda',
              'Cayman Islands',
              'Costa Rica',
              'Cuba',
              'Dominica',
              'El Salvador',
              'Grenada',
              'Guatemala',
              'Honduras',
              'Jamaica',
              'Mexico',
              'Nicaragua',
              'Panama',
              'Saint Lucia',
              'Saint Vincent and the Grenadines',
              'Samoa',
              'Trinidad and Tobago'
            ]
          ],
          'Lima, Peru'              => [
            'countries' => [
              'Argentina',
              'Bolivia',
              'Chile',
              'Colombia',
              'Ecuador',
              'Paraguay',
              'Peru',
              'Uruguay',
              'Venezuela'
            ]
          ],
          'Kyiv, Ukrain'            => [
            'countries' => [
              'Armenia',
              'Azerbaijan',
              'Belarus',
              'Georgia',
              'Moldova',
              'Ukraine'
            ]
          ],
          'Manama, Bahrain'         => [
            'countries' => [
              'Bahrain',
              'Iran',
              'Kuwait',
              'Oman',
              'Qatar',
              'United Arab Emirates',
              'Yemen'
            ]
          ],
          'Kathmandu, Nepal'        => [
            'countries' => [
              'Bangladesh',
              'Maldives',
              'Nepal',
              'Sri Lanka'
            ]
          ],
          'Dakar, Senegal'          => [
            'countries' => [
              'Benin',
              'Burkina Faso',
              'Cape Verde',
              'Cote d\'Ivoire',
              'Gabon',
              'Gambia, The',
              'Ghana',
              'Guinea',
              'Guinea-Bissau',
              'Liberia',
              'Mali',
              'Malta',
              'Niger',
              'Sao Tome and Principe',
              'Senegal',
              'Sierra Leone',
              'Togo'
            ]
          ],
          'Sao Paulo, Brazil'       => [
            'countries' => ['Brazil']
          ],
          'Hanoi, Vietnam'          => [
            'countries' => [
              'Brunei',
              'Cambodia',
              'Laos',
              'Vietnam'
            ]
          ],
          'Bangkok, Thailand'       => [
            'countries' => [
              'Burma',
              'Malaysia',
              'Singapore',
              'Thailand'
            ]
          ],
          'Dar es Salaam, Tanzania' => [
            'countries' => [
              'Burundi',
              'Cameroon',
              'Central African Republic',
              'Chad',
              'Djibouti',
              'Equatorial Guinea',
              'Eritrea',
              'Ethiopia',
              'Kenya',
              'Nigeria',
              'Rwanda',
              'Somalia',
              'South Sudan',
              'Tanzania',
              'Uganda'
            ]
          ],
          'Beijing, China'          => [
            'countries' => [
              'China, People’s Republic of',
              'Hong Kong',
              'Mongolia'
            ]
          ],
          'Jakarta, Indonesia'      => [
            'countries' => [
              'East Timor',
              'Indonesia'
            ]
          ],
          'Cairo, Egypt'            => [
            'countries' => [
              'Egypt',
              'Saudi Arabia',
              'Sudan'
            ]
          ],
          'Baghdad, Iraq'           => [
            'countries' => ['Iraq']
          ],
          'Amman, Jordan'           => [
            'countries' => [
              'Israel',
              'Jordan',
              'Lebanon',
              'Syria',
              'West Bank'
            ]
          ],
          'Manila, Philippines'     => [
            'countries' => [
              'Japan',
              'Papua New Guinea',
              'Philippines',
              'South Korea',
              'Taiwan'
            ]
          ],
          'Astana, Kazakhstan'      => [
            'countries' => [
              'Kazakhstan',
              'Kyrgyzstan',
              'Tajikistan',
              'Turkmenistan',
              'Uzbekistan'
            ]
          ],
          'Islamabad, Pakistan'     => [
            'countries' => ['Pakistan']
          ],
          'Moscow, Russia'          => [
            'countries' => ['Russia']
          ],
          'Ankara, Turkey'          => [
            'countries' => ['Turkey']
          ]
        ];

        foreach ($reloLocations as $reloLocationName => $reloLocationCountries) {
            $reloLocation = new ReloLocation();
            $reloLocation->setName($reloLocationName);

            foreach ($reloLocationCountries['countries'] as $countryName) {
                $reloLocation->addCountry($this->getReference($countryName));
            }

            $manager->persist($reloLocation);
        }

        $manager->flush();
        $manager->clear();
    }

    /**
     * Get the order of this fixture
     *
     * @return integer
     */
    public function getOrder()
    {
        return 3;
    }
}