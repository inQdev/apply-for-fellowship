<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\CostItemMonthly;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

/**
 * Class LoadCostItem
 *
 * @package AppBundle\DataFixtures\ORM
 */
class costItemMonthlyData extends AbstractFixture implements OrderedFixtureInterface {

	/**
	 * Load data fixtures with the passed EntityManager
	 *
	 * @param ObjectManager $manager
	 */
	public function load(ObjectManager $manager) {
		$costItems = [
			'Rent',
			'Utilities',
			'Food',
			'Local Transportation'
		];

		foreach ($costItems as $itemDescription) {
			$costItem = new CostItemMonthly();

			$costItem->setDescription($itemDescription);

			$manager->persist($costItem);
			$manager->flush();
		}
	}

	/**
	 * Get the order of this fixture
	 *
	 * @return integer
	 */
	public function getOrder() {
		return 10;
	}

}
