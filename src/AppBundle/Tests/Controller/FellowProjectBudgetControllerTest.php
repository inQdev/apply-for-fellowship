<?php

namespace AppBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class FellowProjectBudgetControllerTest extends WebTestCase
{
    public function testFundedby()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/fundedBy');
    }

    public function testCostitems()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/costItems');
    }

    public function testReviewcosts()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/reviewCosts');
    }

    public function testCompareversions()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/compareVersions');
    }

}
