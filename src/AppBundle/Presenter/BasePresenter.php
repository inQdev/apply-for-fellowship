<?php

namespace AppBundle\Presenter;

/**
 * Class BasePresenter
 *
 * @package Acme\DemoBundle\Presenter
 */
class BasePresenter
{
    protected $subject;

    /**
     * BasePresenter constructor.
     *
     * @param $subject
     */
    public function __construct($subject)
    {
        $this->subject = $subject;
    }

    /**
     * @param $methodName
     * @param $arguments
     *
     * @return mixed
     * @throws \Exception
     */
    public function __call($methodName, $arguments)
    {
        $methods = get_class_methods($this->subject);

        if (in_array($methodName, $methods)) {
            return call_user_func_array(
              [$this->subject, $methodName],
              $arguments
            );
        }
        else {
            throw new \Exception("No such method " . $methodName);
        }
    }
}