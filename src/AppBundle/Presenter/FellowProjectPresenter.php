<?php

namespace AppBundle\Presenter;

use AppBundle\Entity\FellowProject;
use AppBundle\Entity\ProjectGeneralInfo;
use AppBundle\Entity\WizardStepStatus;

/**
 * TODO: Refactor section status array; they can be handle as constants (static?) properties.
 *
 * FellowProjectPresenter class.
 *
 * @package AppBundle\Presenter
 * @author  Juan Obando <juan.obando@inqbation.com>
 */
class FellowProjectPresenter extends BasePresenter
{
    /**
     * @var array $notStartedSection
     */
    private static $notStartedSection = [
      'class'  => 'section-not-started',
      'status' => 'Not started',
    ];

    /**
     * @var array $inProgressSection
     */
    private static $inProgressSection = [
      'class'  => 'section-in-progress',
      'status' => 'In progress',
    ];

    /**
     * @var array $completeSection
     */
    private static $completedSection = [
      'class'  => 'section-complete',
      'status' => 'Completed',
    ];

    /**
     * @var array $fellowProjectWizardStepStatus
     */
    private $fellowProjectWizardStepStatus;

    /**
     * @var bool $isProposalReadyForSubmission
     */
    private $isProposalReadyForSubmission = true;

    /**
     * @var bool $isProposalSubmitted
     */
    private $isProposalSubmitted;

    /**
     * FellowProjectPresenter constructor.
     *
     * @param FellowProject $subject
     */
    public function __construct($subject)
    {
        parent::__construct($subject);

        $stepsStatuses = $this->getWizardStepsStatuses();

        /** @var WizardStepStatus $stepStatus */
        foreach ($stepsStatuses as $stepStatus) {
            $this->fellowProjectWizardStepStatus[$stepStatus->getStep()] = $stepStatus->getStatus();

            $isStepStatusCompleted = (WizardStepStatus::COMPLETED === $stepStatus->getStatus());

            $this->isProposalReadyForSubmission = $this->isProposalReadyForSubmission && $isStepStatusCompleted;
        }

        $this->isProposalSubmitted = ProjectGeneralInfo::PROJECT_SUBMITTED === $this->getProjectGeneralInfo()->getSubmissionStatus();
    }

    /**
     * @return array
     */
    public function getProjectGeneralInfoSectionStatus()
    {
        return $this->getSectionStatus(WizardStepStatus::GENERAL_INFO_STEP, 'project_fellow_region');
    }

    /**
     * @return array
     */
    public function getPointOfContactsSectionStatus()
    {
        return $this->getSectionStatus(WizardStepStatus::POINT_OF_CONTACT_STEP, 'project_fellow_contact_points');
    }

    /**
     * @return array
     */
    public function getProjectRequirementsSectionStatus()
    {
        return $this->getSectionStatus(WizardStepStatus::PROJECT_REQUIREMENTS_STEP, 'project_fellow_requirements');
    }

    /**
     * @return array
     */
    public function getHousingInfoSectionStatus()
    {
        return $this->getSectionStatus(WizardStepStatus::HOUSING_INFO_STEP, 'project_fellow_project_house_info');
    }

    /**
     * @return array
     */
    public function getCertificationsSectionStatus()
    {
        return $this->getSectionStatus(WizardStepStatus::CERTIFICATIONS_STEP, 'project_fellow_certifications');
    }

    /**
     * @return array
     */
    public function getLocationHostInfoSectionStatus()
    {
        return $this->getSectionStatus(WizardStepStatus::LOCATION_HOST_INFO_STEP, 'project_fellow_location_host_info');
    }

    /**
     * @return array
     */
    public function getFellowshipDutySectionStatus()
    {
        return $this->getSectionStatus(WizardStepStatus::FELLOWSHIP_DUTIES_STEP, 'fellow_project_fellowship_duties');
    }

    /**
     * @return array
     */
    public function getBudgetSectionStatus()
    {
        return $this->getSectionStatus(WizardStepStatus::BUDGET_STEP, 'fellow_project_budget_funded');
    }

    /**
     * @return bool
     */
    public function isProposalReadyForSubmission()
    {
        return $this->isProposalReadyForSubmission;
    }

    /**
     * Gets isProposalSubmitted
     *
     * @return bool
     */
    public function isProposalSubmitted()
    {
        return $this->isProposalSubmitted;
    }

    /**+
     * @param string $sectionKey
     * @param string $path
     *
     * @return array
     */
    private function getSectionStatus($sectionKey, $path)
    {
        $stepStatus = $this->fellowProjectWizardStepStatus[$sectionKey];

        if (WizardStepStatus::COMPLETED === $stepStatus) {
            $sectionStatus = self::$completedSection;
        }
        elseif (WizardStepStatus::IN_PROGRESS === $stepStatus) {
            $sectionStatus = self::$inProgressSection;
        }
        else {
            $sectionStatus = self::$notStartedSection;
        }

        $sectionStatus['path'] = $path;

        return $sectionStatus;
    }
}
