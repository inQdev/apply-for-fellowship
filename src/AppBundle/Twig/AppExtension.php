<?php
/**
 * Created by PhpStorm.
 * User: felipe
 * Date: 2/26/2016
 * Time: 2:13 PM
 */

namespace AppBundle\Twig;


class AppExtension extends \Twig_Extension
{

	public function getFilters()
	{
		return array(
			new \Twig_SimpleFilter('currency_format', array($this, 'currencyFormat')),
			new \Twig_SimpleFilter('unserialize', array($this, 'unserialize')),
		);
	}

	public function getTests() {
		return array(
			'instanceof' => new \Twig_Function_Method($this, 'isInstanceof'),
		);
	}


	/**
	 * A currency replacement for number_format
	 *
	 * @param int $amount
	 * @param int $decimal Number of decimal places
	 * @param string $decimalPoint What to use as the decimal point
	 * @param string
	 * @param string $currency Symbol ($) of currency
	 *
	 * @return string
	 */
	public function currencyFormat($amount, $decimal = 2, $decimalPoint = '.', $thousandSep = ',', $symbol = "$",
	                               $negativeParenthesis = true)
	{
		$working_amount = $amount;
		if ($negativeParenthesis !== false) {
			$working_amount = abs($working_amount);
		}

		$working_amount = number_format((float)$working_amount, $decimal, $decimalPoint, $thousandSep);
		$return = "{$symbol}$working_amount";

		return $negativeParenthesis !== false && $amount < 0 ? "($return)" : $return;
	}

	/**
	 * @param $var
	 * @param $instance
	 * @return bool
	 */
	public function isInstanceof($var, $instance) {
		return $var instanceof $instance;
	}



	public function unserialize($string) {
		return unserialize($string);
	}


	public function getName()
	{
		return 'app_extension';
	}
}