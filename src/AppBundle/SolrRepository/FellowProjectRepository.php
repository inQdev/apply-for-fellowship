<?php

namespace AppBundle\SolrRepository;

use AppBundle\Entity\FellowProject;
use FS\SolrBundle\Solr;

/**
 *
 */
class FellowProjectRepository
{
	public function __construct(Solr $fsSolr)
	{
		$this->solrClient = $fsSolr->getClient();
		$this->solrMapper = $fsSolr->getMapper();
	}

	public function getFacetedResultsForSearch(array $criteria, array $facetFields, array $allowedSorts, $page = 1, $sort = null, $sortDirection = null)
	{
		$query = $this->solrClient->createSelect(); // get a select query instance
		$facets = $query->getFacetSet(); // get access to the facetset options
		$helper = $query->getHelper();

		$query->setRows(50);

		if ($sort && in_array($sort, $allowedSorts)) {
			$direction = $query::SORT_ASC;
			if ($sortDirection === 'desc') {
				$direction = $query::SORT_DESC;
			}

			$query->addSort($sort, $direction);
		}

		foreach ($criteria as $key => $criterion) {
			if ($key === 'cycle_id_i') {
				$query->createFilterQuery('cycle_id_i')->setQuery('cycle_id_i:' . $helper->escapeTerm($criterion[0]));
			} else {
				$query->createFilterQuery($key)->setQuery($key . ':(' . implode(' OR ', array_map(function ($term) use ($helper) {
						return '"' . $helper->escapeTerm($term) . '"';
					}, $criterion)) . ')');
			}
		}

		foreach ($facetFields as $filter) {
			$facets->createFacetField($filter)->setField($filter);
		}

		$rs = $this->solrClient->select($query);

		return [
			'facet_set' => $rs->getFacetSet(),
			'fellow_projects' => $this->mapDocumentsToEntites($rs),
		];
	}

	protected function mapDocumentsToEntites($documents)
	{
		$blankEntity = (new \ReflectionClass(FellowProject::class))->newInstanceWithoutConstructor();

		$entities = [];
		foreach ($documents as $document) {
			$entities[] = $this->solrMapper->toEntity($document, $blankEntity);
		}

		return $entities;
	}
}
