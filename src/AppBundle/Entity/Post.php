<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Post entity.
 *
 * @package AppBundle\Entity
 * @author  Juan Obando <juan.obando@inqbation.com>
 *
 * @ORM\Entity()
 * @ORM\Table(name="post")
 */
class Post extends BaseSingleEntity
{
    /**
     * @var \AppBundle\Entity\Post $post
     *
     * @ORM\ManyToOne(
     *     targetEntity="Country",
     *     inversedBy="posts"
     * )
     */
    protected $country;

    /**
     * Set country
     *
     * @param Country $country
     *
     * @return Post
     */
    public function setCountry(Country $country = null)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Get country
     *
     * @return Country
     */
    public function getCountry()
    {
        return $this->country;
    }
}
