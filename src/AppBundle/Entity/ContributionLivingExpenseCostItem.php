<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ContributionLivingExpenseCostItem entity
 *
 * @package AppBundle\Entity
 *
 * @ORM\Entity()
 * @ORM\Table(name="contribution_living_expense_cost_item")
 */
class ContributionLivingExpenseCostItem extends ProjectContribution
{
    /**
     * @var CostItem $costItem
     *
     * @ORM\ManyToOne(targetEntity="CostItem")
     */
    protected $costItem;

    /**
     * @var ContributionLivingExpense $livingExpense
     *
     * @ORM\ManyToOne(
     *     targetEntity="ContributionLivingExpense",
     *     inversedBy="contributionLivingExpenseCostItems"
     * )
     */
    protected $contributionLivingExpense;

    /**
     * LivingExpenseCostItem constructor.
     *
     * @param ContributionLivingExpense $contributionLivingExpense
     */
    public function __construct(
      ContributionLivingExpense $contributionLivingExpense
    ) {
        $this->contributionLivingExpense = $contributionLivingExpense;
    }

    /**
     * TODO: Check calculation to make sure they are right for every type of budget.
     */
    public function calculateTotals()
    {
        $fundedBy = $this->contributionLivingExpense->getPhaseBudget()->getFundedBy();

        if ('ECA' === $fundedBy) {
            $this->calculateECATotals();
        } elseif ('Post' === $fundedBy) {
            $this->calculatePostTotals();
        }
    }

    /**
     * Set costItem
     *
     * @param CostItem $costItem
     *
     * @return ContributionLivingExpenseCostItem
     */
    public function setCostItem(CostItem $costItem = null)
    {
        $this->costItem = $costItem;

        return $this;
    }

    /**
     * Get costItem
     *
     * @return \AppBundle\Entity\CostItem
     */
    public function getCostItem()
    {
        return $this->costItem;
    }

    /**
     * Set contributionLivingExpense
     *
     * @param ContributionLivingExpense $contributionLivingExpense
     *
     * @return ContributionLivingExpenseCostItem
     */
    public function setContributionLivingExpense(
      ContributionLivingExpense $contributionLivingExpense = null
    ) {
        $this->contributionLivingExpense = $contributionLivingExpense;

        return $this;
    }

    /**
     * Get contributionLivingExpense
     *
     * @return ContributionLivingExpense
     */
    public function getContributionLivingExpense()
    {
        return $this->contributionLivingExpense;
    }
}
