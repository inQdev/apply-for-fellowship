<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * User entity
 *
 * @ORM\Table(name="user")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\UserRepository")
 */
class User extends BaseUser
{
    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string $name
     *
     * @ORM\Column(type="string", nullable=true)
     */
    protected $name;

    /**
     * @var integer
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    private $ciedId;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private $ciedAccessToken;

    /**
     * @var string $ciedProfileUri
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $ciedProfileUri;

    /**
     * @var $embassies
     *
     * @ORM\ManyToMany(targetEntity="Country", inversedBy="embassies")
     * @ORM\JoinTable(name="user_embassies_countries")
     *
     */
    private $countries;

    /**
     * @var $reloLocations
     *
     * @ORM\ManyToMany(targetEntity="ReloLocation", inversedBy="relos")
     * @ORM\JoinTable(name="user_relos_relo_locations")
     *
     */
    private $reloLocations;

    /**
     * @var $regions
     *
     * @ORM\ManyToMany(targetEntity="Region", inversedBy="rpos")
     * @ORM\JoinTable(name="user_rpos_regions")
     *
     */
    private $regions;

    /**
     * @var $projects
     *
     * @ORM\ManyToMany(targetEntity="ProjectGeneralInfo", inversedBy="authors")
     * @ORM\JoinTable(name="projects_user_authors")
     *
     */
    private $projects;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(
     *     targetEntity="ProjectReviewComment",
     *     mappedBy="commentBy"
     * )
     */
    protected $projectReviewComments;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     * @Gedmo\Timestampable(on="create")
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime")
     * @Gedmo\Timestampable(on="update")
     */
    private $updatedAt;

    /******************************* Methods Definition ***************************************/

    /**
     * @param $rolesArray
     *
     * @return array
     */
    public function parseRolesArray($rolesArray) {
        // TODO: This list should be retrieved (sync?) from the Application System via an endpoint or something
        $applicationRoles = [
          'ROLE_APPADMIN'                        => [
            'roleId'    => '3',
            'roleLabel' => 'AppAdmin',
          ],
          'ROLE_SUPER_ADMIN'                     => [
            'roleId'    => '5',
            'roleLabel' => 'Super Admin',
          ],
          'ROLE_ADMIN'                           => [
            'roleId'    => '6',
            'roleLabel' => 'Admin',
          ],
          'ROLE_ADMIN_CONFIRMER'                 => [
            'roleId'    => '15',
            'roleLabel' => 'Admin Confirmer',
          ],
          'ROLE_ELIGIBILITY_REVIEWER'            => [
            'roleId'    => '7',
            'roleLabel' => 'Eligibility Reviewer',
          ],
          'ROLE_FELLOW_REVIEWER'                 => [
            'roleId'    => '16',
            'roleLabel' => 'Fellow Reviewer',
          ],
          'ROLE_PANEL_REVIEWER'                  => [
            'roleId'    => '8',
            'roleLabel' => 'Panel Reviewer',
          ],
          'ROLE_STATE_DEPARTMENT'                => [
            'roleId'    => '10',
            'roleLabel' => 'State Department',
          ],
          'ROLE_RPO'                             => [
            'roleId'    => '18',
            'roleLabel' => 'RPO',
          ],
          'ROLE_RELO'                            => [
            'roleId'    => '11',
            'roleLabel' => 'RELO',
          ],
          'ROLE_SPECIALIST_ELIGIBILITY_REVIEWER' => [
            'roleId'    => '17',
            'roleLabel' => 'Specialist Eligibility Reviewer',
          ],
          'ROLE_EMBASSY'                         => [
            'roleId'    => '12',
            'roleLabel' => 'Embassy',
          ],
          'ROLE_SPECIALIST_REVIEWER'             => [
            'roleId'    => '9',
            'roleLabel' => 'Specialist Reviewer',
          ],
          'ROLE_REFERENCE'                       => [
            'roleId'    => '13',
            'roleLabel' => 'Reference',
          ],
          'ROLE_APPLICANT'                       => [
            'roleId'    => '14',
            'roleLabel' => 'Applicant',
          ],
        ];

        $roles = array_filter(
          $applicationRoles,
          function ($role) use ($rolesArray) {
              return in_array($role, $rolesArray);
          },
          ARRAY_FILTER_USE_KEY
        );

        return $roles;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return User
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set ciedId
     *
     * @param integer $ciedId
     *
     * @return User
     */
    public function setCiedId($ciedId)
    {
        $this->ciedId = $ciedId;

        return $this;
    }

    /**
     * Get ciedId
     *
     * @return integer
     */
    public function getCiedId()
    {
        return $this->ciedId;
    }

    /**
     * Set ciedAccessToken
     *
     * @param string $ciedAccessToken
     *
     * @return User
     */
    public function setCiedAccessToken($ciedAccessToken)
    {
        $this->ciedAccessToken = $ciedAccessToken;

        return $this;
    }

    /**
     * Get ciedAccessToken
     *
     * @return string
     */
    public function getCiedAccessToken()
    {
        return $this->ciedAccessToken;
    }

    /**
     * Set ciedProfileUri
     *
     * @param string $ciedProfileUri
     *
     * @return User
     */
    public function setCiedProfileUri($ciedProfileUri)
    {
        $this->ciedProfileUri = $ciedProfileUri;

        return $this;
    }

    /**
     * Get ciedProfileUri
     *
     * @return string
     */
    public function getCiedProfileUri()
    {
        return $this->ciedProfileUri;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return User
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return User
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Add country
     *
     * @param \AppBundle\Entity\Country $country
     *
     * @return User
     */
    public function addCountry(Country $country)
    {
//        $country->addEmbassy($this);
        $this->countries[] = $country;

        return $this;
    }

    /**
     * Remove country
     *
     * @param \AppBundle\Entity\Country $country
     */
    public function removeCountry(Country $country)
    {
        $this->countries->removeElement($country);
    }

    /**
     * Get countries
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCountries()
    {
        return $this->countries;
    }

    /**
     * Add reloLocation
     *
     * @param \AppBundle\Entity\ReloLocation $reloLocation
     *
     * @return User
     */
    public function addReloLocation(ReloLocation $reloLocation)
    {
        $this->reloLocations[] = $reloLocation;

        return $this;
    }

    /**
     * Remove reloLocation
     *
     * @param \AppBundle\Entity\ReloLocation $reloLocation
     */
    public function removeReloLocation(ReloLocation $reloLocation)
    {
        $this->reloLocations->removeElement($reloLocation);
    }

    /**
     * Get reloLocations
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getReloLocations()
    {
        return $this->reloLocations;
    }

    /**
     * Add region
     *
     * @param \AppBundle\Entity\Region $region
     *
     * @return User
     */
    public function addRegion(Region $region)
    {
        $this->regions[] = $region;

        return $this;
    }

    /**
     * Remove region
     *
     * @param \AppBundle\Entity\Region $region
     */
    public function removeRegion(Region $region)
    {
        $this->regions->removeElement($region);
    }

    /**
     * Get regions
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getRegions()
    {
        return $this->regions;
    }

    /**
     * Add project
     *
     * @param \AppBundle\Entity\ProjectGeneralInfo $project
     *
     * @return User
     */
    public function addProject(\AppBundle\Entity\ProjectGeneralInfo $project)
    {
        $this->projects[] = $project;

        return $this;
    }

    /**
     * Remove project
     *
     * @param \AppBundle\Entity\ProjectGeneralInfo $project
     */
    public function removeProject(\AppBundle\Entity\ProjectGeneralInfo $project)
    {
        $this->projects->removeElement($project);
    }

    /**
     * Get projects
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getProjects()
    {
        return $this->projects;
    }

    /**
     * Add projectReviewComment
     *
     * @param \AppBundle\Entity\ProjectReviewComment $projectReviewComment
     *
     * @return User
     */
    public function addProjectReviewComment(\AppBundle\Entity\ProjectReviewComment $projectReviewComment)
    {
        $this->projectReviewComments[] = $projectReviewComment;

        return $this;
    }

    /**
     * Remove projectReviewComment
     *
     * @param \AppBundle\Entity\ProjectReviewComment $projectReviewComment
     */
    public function removeProjectReviewComment(\AppBundle\Entity\ProjectReviewComment $projectReviewComment)
    {
        $this->projectReviewComments->removeElement($projectReviewComment);
    }

    /**
     * Get projectReviewComments
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getProjectReviewComments()
    {
        return $this->projectReviewComments;
    }
}
