<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * ReloLocation entity.
 *
 * @package AppBundle\Entity
 * @author  Juan Obando <juan.obando@inqbation.com>
 *
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ReloLocationRepository")
 * @ORM\Table(name="relo_location")
 */
class ReloLocation extends BaseSingleEntity
{
    /**
     * @var ArrayCollection $countries
     *
     * @ORM\ManyToMany(
     *     targetEntity="Country",
     *     inversedBy="reloLocations"
     * )
     * @ORM\JoinTable(name="relo_location_country")
     * @ORM\OrderBy({"name"="ASC"})
     */
    protected $countries;

    /**
     * @var ArrayCollection $relos
     *
     * @ORM\ManyToMany(targetEntity="User", mappedBy="reloLocations")
     *
     */
    private $relos;

    /**
     * ReloLocation constructor.
     */
    public function __construct()
    {
        $this->countries = new ArrayCollection();
    }

    /**
     * Add country
     *
     * @param Country $country
     *
     * @return ReloLocation
     */
    public function addCountry(Country $country)
    {
        $this->countries[] = $country;

        return $this;
    }

    /**
     * Remove country
     *
     * @param Country $country
     */
    public function removeCountry(Country $country)
    {
        $this->countries->removeElement($country);
    }

    /**
     * Get countries
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCountries()
    {
        return $this->countries;
    }

    /**
     * Add relo
     *
     * @param User $relo
     *
     * @return ReloLocation
     */
    public function addRelo(User $relo)
    {
        $this->relos[] = $relo;

        return $this;
    }

    /**
     * Remove relo
     *
     * @param User $relo
     */
    public function removeRelo(User $relo)
    {
        $this->relos->removeElement($relo);
    }

    /**
     * Get relos
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getRelos()
    {
        return $this->relos;
    }
}
