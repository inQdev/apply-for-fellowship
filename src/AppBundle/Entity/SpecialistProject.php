<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * SpecialistProject entity.
 *
 * @package AppBundle\Entity
 * @author  Juan Obando <juan.obando@inqbation.com>
 *
 * @ORM\Entity()
 * @ORM\Table(name="specialist_project")
 */
class SpecialistProject extends BaseEntity
{
    /**
     * @var string $title
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $title;

    /**
     * @var string $furtherDetailsDesc
     *
     * @ORM\Column(type="string", length=4294967295, nullable=true)
     */
    protected $furtherDetailsDesc;

    /**
     * @var bool $degreeRequirements
     *
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $degreeRequirements;

    /**
     * @var string $degreeRequirementsDesc
     *
     * @ORM\Column(type="string", length=4294967295, nullable=true)
     */
    protected $degreeRequirementsDesc;

    /**
     * @var string $proposedCandidate
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $proposedCandidate;

    /**
     * @var bool $proposedCandidateContacted
     *
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $proposedCandidateContacted;

    /**
     * @var string $proposedCandidateEmail
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $proposedCandidateEmail;

    /**
     * @var string $subsequentPhasesDesc
     *
     * @ORM\Column(type="string", length=4294967295, nullable=true)
     */
    protected $subsequentPhasesDesc;

    /**
     * @var ArrayCollection $areasOfExpertise
     *
     * @ORM\ManyToMany(targetEntity="AreaOfExpertise")
     * @ORM\JoinTable(name="specialist_projects_areas_of_expertise")
     */
    protected $areasOfExpertise;

    /**
     * @var ArrayCollection $phases
     *
     * @ORM\OneToMany(
     *     targetEntity="SpecialistProjectPhase",
     *     mappedBy="specialistProject"
     * )
     * @ORM\OrderBy({"createdAt"="ASC"})
     */
    protected $phases;

    /***************************** Methods Definition *****************************/

    /**
     * SpecialistProject constructor.
     */
    public function __construct()
    {
        $this->areasOfExpertise = new ArrayCollection();
        $this->phases = new ArrayCollection();
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return SpecialistProject
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set furtherDetailsDesc
     *
     * @param string $furtherDetailsDesc
     *
     * @return SpecialistProject
     */
    public function setFurtherDetailsDesc($furtherDetailsDesc)
    {
        $this->furtherDetailsDesc = $furtherDetailsDesc;

        return $this;
    }

    /**
     * Get furtherDetailsDesc
     *
     * @return string
     */
    public function getFurtherDetailsDesc()
    {
        return $this->furtherDetailsDesc;
    }

    /**
     * Set degreeRequirements
     *
     * @param boolean $degreeRequirements
     *
     * @return SpecialistProject
     */
    public function setDegreeRequirements($degreeRequirements)
    {
        $this->degreeRequirements = $degreeRequirements;

        return $this;
    }

    /**
     * Get degreeRequirements
     *
     * @return boolean
     */
    public function getDegreeRequirements()
    {
        return $this->degreeRequirements;
    }

    /**
     * Set degreeRequirementsDesc
     *
     * @param string $degreeRequirementsDesc
     *
     * @return SpecialistProject
     */
    public function setDegreeRequirementsDesc($degreeRequirementsDesc)
    {
        $this->degreeRequirementsDesc = $degreeRequirementsDesc;

        return $this;
    }

    /**
     * Get degreeRequirementsDesc
     *
     * @return string
     */
    public function getDegreeRequirementsDesc()
    {
        return $this->degreeRequirementsDesc;
    }

    /**
     * Set proposedCandidate
     *
     * @param string $proposedCandidate
     *
     * @return SpecialistProject
     */
    public function setProposedCandidate($proposedCandidate)
    {
        $this->proposedCandidate = $proposedCandidate;

        return $this;
    }

    /**
     * Get proposedCandidate
     *
     * @return string
     */
    public function getProposedCandidate()
    {
        return $this->proposedCandidate;
    }

    /**
     * Set proposedCandidateContacted
     *
     * @param boolean $proposedCandidateContacted
     *
     * @return SpecialistProject
     */
    public function setProposedCandidateContacted($proposedCandidateContacted)
    {
        $this->proposedCandidateContacted = $proposedCandidateContacted;

        return $this;
    }

    /**
     * Get proposedCandidateContacted
     *
     * @return boolean
     */
    public function getProposedCandidateContacted()
    {
        return $this->proposedCandidateContacted;
    }

    /**
     * Set proposedCandidateEmail
     *
     * @param string $proposedCandidateEmail
     *
     * @return SpecialistProject
     */
    public function setProposedCandidateEmail($proposedCandidateEmail)
    {
        $this->proposedCandidateEmail = $proposedCandidateEmail;

        return $this;
    }

    /**
     * Get proposedCandidateEmail
     *
     * @return string
     */
    public function getProposedCandidateEmail()
    {
        return $this->proposedCandidateEmail;
    }

    /**
     * Add areasOfExpertise
     *
     * @param AreaOfExpertise $areasOfExpertise
     *
     * @return SpecialistProject
     */
    public function addAreasOfExpertise(AreaOfExpertise $areasOfExpertise)
    {
        $this->areasOfExpertise[] = $areasOfExpertise;

        return $this;
    }

    /**
     * Remove areasOfExpertise
     *
     * @param AreaOfExpertise $areasOfExpertise
     */
    public function removeAreasOfExpertise(AreaOfExpertise $areasOfExpertise)
    {
        $this->areasOfExpertise->removeElement($areasOfExpertise);
    }

    /**
     * Get areasOfExpertise
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAreasOfExpertise()
    {
        return $this->areasOfExpertise;
    }

    /**
     * Add phase
     *
     * @param SpecialistProjectPhase $phase
     *
     * @return SpecialistProject
     */
    public function addPhase(SpecialistProjectPhase $phase)
    {
        $this->phases[] = $phase;

        return $this;
    }

    /**
     * Remove phase
     *
     * @param SpecialistProjectPhase $phase
     */
    public function removePhase(SpecialistProjectPhase $phase)
    {
        $this->phases->removeElement($phase);
    }

    /**
     * Get phases
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPhases()
    {
        return $this->phases;
    }

    /**
     * Set subsequentPhasesDesc
     *
     * @param string $subsequentPhasesDesc
     *
     * @return SpecialistProject
     */
    public function setSubsequentPhasesDesc($subsequentPhasesDesc)
    {
        $this->subsequentPhasesDesc = $subsequentPhasesDesc;

        return $this;
    }

    /**
     * Get subsequentPhasesDesc
     *
     * @return string
     */
    public function getSubsequentPhasesDesc()
    {
        return $this->subsequentPhasesDesc;
    }

}
