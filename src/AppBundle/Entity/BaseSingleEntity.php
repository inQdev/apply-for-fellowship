<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * BaseSingleEntity mapped superclass.
 *
 * @package AppBundle\Entity
 * @author  Juan Obando <juan.obando@inqbation.com>
 *
 * @ORM\MappedSuperclass()
 */
class BaseSingleEntity extends BaseEntity
{
    /**
     * @var string $name
     *
     * @ORM\Column(
     *     type="string",
     *     length=255,
     *     nullable=true
     * )
     */
    protected $name;

    /**
     * @return string
     */
    public function __toString()
    {
        return strval($this->name);
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return BaseSingleEntity
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
}
