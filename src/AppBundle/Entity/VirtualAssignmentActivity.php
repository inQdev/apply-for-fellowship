<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * VirtualAssignmentActivity
 *
 * @author Felipe Ceballos <felipe.ceballos@inqbation.com>
 *
 * @ORM\Entity()
 * @ORM\Table(name="virtual_assignment_activity")
 */
class VirtualAssignmentActivity extends BaseEntity
{
	/**
	 * @var string
	 *
	 * @ORM\Column(type="text", nullable=true)
	 */
	private $activityDesc;

	/**
	 * @var string
	 *
	 * @ORM\Column(type="text", nullable=true)
	 */
	private $audience;

	/**
	 * @var int
	 *
	 * @ORM\Column(type="integer", nullable=true)
	 */
	private $participants;

	/**
	 * @var string
	 *
	 * @ORM\Column(type="decimal", precision=10, scale=2, nullable=true)
	 */
	private $hoursPerWeek;

	/**
	 * @var string
	 *
	 * @ORM\Column(type="decimal", precision=10, scale=2, nullable=true)
	 */
	private $hoursTotal;

	/**
	 * @var int
	 *
	 * @ORM\ManyToOne(
	 *     targetEntity="VirtualAssignment",
	 *     inversedBy="virtualAssignmentActivities"
	 * )
	 */
	private $virtualAssignment;

	/************************** Methods ******************************/

	/**
	 * Set activityDesc
	 *
	 * @param string $activityDesc
	 *
	 * @return VirtualAssignmentActivity
	 */
	public function setActivityDesc($activityDesc)
	{
		$this->activityDesc = $activityDesc;

		return $this;
	}

	/**
	 * Get activityDesc
	 *
	 * @return string
	 */
	public function getActivityDesc()
	{
		return $this->activityDesc;
	}

	/**
	 * Set audience
	 *
	 * @param string $audience
	 *
	 * @return VirtualAssignmentActivity
	 */
	public function setAudience($audience)
	{
		$this->audience = $audience;

		return $this;
	}

	/**
	 * Get audience
	 *
	 * @return string
	 */
	public function getAudience()
	{
		return $this->audience;
	}

	/**
	 * Set participants
	 *
	 * @param integer $participants
	 *
	 * @return VirtualAssignmentActivity
	 */
	public function setParticipants($participants)
	{
		$this->participants = $participants;

		return $this;
	}

	/**
	 * Get participants
	 *
	 * @return int
	 */
	public function getParticipants()
	{
		return $this->participants;
	}

	/**
	 * Set hoursPerWeek
	 *
	 * @param string $hoursPerWeek
	 *
	 * @return VirtualAssignmentActivity
	 */
	public function setHoursPerWeek($hoursPerWeek)
	{
		$this->hoursPerWeek = $hoursPerWeek;

		return $this;
	}

	/**
	 * Get hoursPerWeek
	 *
	 * @return string
	 */
	public function getHoursPerWeek()
	{
		return $this->hoursPerWeek;
	}

	/**
	 * Set hoursTotal
	 *
	 * @param string $hoursTotal
	 *
	 * @return VirtualAssignmentActivity
	 */
	public function setHoursTotal($hoursTotal)
	{
		$this->hoursTotal = $hoursTotal;

		return $this;
	}

	/**
	 * Get hoursTotal
	 *
	 * @return string
	 */
	public function getHoursTotal()
	{
		return $this->hoursTotal;
	}

	/**
	 * Set virtualAssignment
	 *
	 * @param VirtualAssignment $virtualAssignment
	 *
	 * @return VirtualAssignmentActivity
	 */
	public function setVirtualAssignment($virtualAssignment)
	{
		$this->virtualAssignment = $virtualAssignment;

		return $this;
	}

	/**
	 * Get virtualAssignment
	 *
	 * @return VirtualAssignment
	 */
	public function getVirtualAssignment()
	{
		return $this->virtualAssignment;
	}
}
