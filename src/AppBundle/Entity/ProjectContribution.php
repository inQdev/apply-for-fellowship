<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ProjectContribution mapped superclass.
 *
 * @package AppBundle\Entity
 * @author  Juan Obando <juan.obando@inqbaiton.com>
 *
 * @ORM\MappedSuperclass()
 */
class ProjectContribution extends BaseEntity
{
    /**
     * @var float $totalCost
     *
     * @ORM\Column(
     *     type="decimal",
     *     precision=14,
     *     scale=2,
     *     nullable=true,
     *     options={"default": 0}
     * )
     */
    protected $totalCost = 0;

    /**
     * @var float $postContribution
     *
     * @ORM\Column(
     *     type="decimal",
     *     precision=14,
     *     scale=2,
     *     nullable=true,
     *     options={"default": 0}
     * )
     */
    protected $postContribution = 0;

    /**
     * @var float $hostContributionMonetary
     *
     * @ORM\Column(
     *     type="decimal",
     *     precision=14,
     *     scale=2,
     *     nullable=true,
     *     options={"default": 0}
     * )
     */
    protected $hostContributionMonetary = 0;

    /**
     * @var float $hostContributionInKind
     *
     * @ORM\Column(
     *     type="decimal",
     *     precision=14,
     *     scale=2,
     *     nullable=true,
     *     options={"default": 0}
     * )
     */
    protected $hostContributionInKind = 0;

    /**
     * @var float $postHostTotalContribution
     *
     * @ORM\Column(
     *     type="decimal",
     *     precision=14,
     *     scale=2,
     *     nullable=true,
     *     options={"default": 0}
     * )
     */
    protected $postHostTotalContribution = 0;

    /**
     * @var float $postTotalContribution
     *
     * @ORM\Column(
     *     type="decimal",
     *     precision=14,
     *     scale=2,
     *     nullable=true,
     *     options={"default": 0}
     * )
     */
    protected $postTotalContribution = 0;

    /**
     * @var float $ecaTotalContribution
     *
     * @ORM\Column(
     *     type="decimal",
     *     precision=14,
     *     scale=2,
     *     nullable=true,
     *     options={"default": 0}
     * )
     */
    protected $ecaTotalContribution = 0;

    /**
     * Calculate totals for ECA funded budget.
     */
    protected function calculateECATotals() {
        $this->postHostTotalContribution = $this->postContribution + $this->hostContributionMonetary + $this->hostContributionInKind;
        $this->postTotalContribution = $this->postContribution;
        $this->ecaTotalContribution = $this->totalCost - $this->postHostTotalContribution;
    }

    /**
     * Calculate totals for Post funded budget.
     */
    protected function calculatePostTotals() {
        $this->postHostTotalContribution = $this->hostContributionMonetary + $this->hostContributionInKind;
        $this->postTotalContribution = $this->totalCost - $this->postHostTotalContribution;
        $this->postContribution = $this->postTotalContribution;
        $this->ecaTotalContribution = 0;
    }

    /**
     * Set totalCost
     *
     * @param string $totalCost
     *
     * @return ContributionLivingExpenseCostItem
     */
    public function setTotalCost($totalCost)
    {
        $this->totalCost = $totalCost;

        return $this;
    }

    /**
     * Get totalCost
     *
     * @return float
     */
    public function getTotalCost()
    {
        return $this->totalCost;
    }

    /**
     * Set postContribution
     *
     * @param string $postContribution
     *
     * @return ContributionLivingExpenseCostItem
     */
    public function setPostContribution($postContribution)
    {
        $this->postContribution = $postContribution;

        return $this;
    }

    /**
     * Get postContribution
     *
     * @return float
     */
    public function getPostContribution()
    {
        return $this->postContribution;
    }

    /**
     * Set hostContributionMonetary
     *
     * @param string $hostContributionMonetary
     *
     * @return ContributionLivingExpenseCostItem
     */
    public function setHostContributionMonetary($hostContributionMonetary)
    {
        $this->hostContributionMonetary = $hostContributionMonetary;

        return $this;
    }

    /**
     * Get hostContributionMonetary
     *
     * @return float
     */
    public function getHostContributionMonetary()
    {
        return $this->hostContributionMonetary;
    }

    /**
     * Set hostContributionInKind
     *
     * @param string $hostContributionInKind
     *
     * @return ContributionLivingExpenseCostItem
     */
    public function setHostContributionInKind($hostContributionInKind)
    {
        $this->hostContributionInKind = $hostContributionInKind;

        return $this;
    }

    /**
     * Get hostContributionInKind
     *
     * @return float
     */
    public function getHostContributionInKind()
    {
        return $this->hostContributionInKind;
    }

    /**
     * Set postHostTotalContribution
     *
     * @param string $postHostTotalContribution
     *
     * @return ContributionLivingExpenseCostItem
     */
    public function setPostHostTotalContribution($postHostTotalContribution)
    {
        $this->postHostTotalContribution = $postHostTotalContribution;

        return $this;
    }

    /**
     * Get postHostTotalContribution
     *
     * @return float
     */
    public function getPostHostTotalContribution()
    {
        return $this->postHostTotalContribution;
    }

    /**
     * Set postTotalContribution
     *
     * @param string $postTotalContribution
     *
     * @return ContributionLivingExpenseCostItem
     */
    public function setPostTotalContribution($postTotalContribution)
    {
        $this->postTotalContribution = $postTotalContribution;

        return $this;
    }

    /**
     * Get postTotalContribution
     *
     * @return float
     */
    public function getPostTotalContribution()
    {
        return $this->postTotalContribution;
    }

    /**
     * Set ecaTotalContribution
     *
     * @param string $ecaTotalContribution
     *
     * @return ContributionLivingExpenseCostItem
     */
    public function setEcaTotalContribution($ecaTotalContribution)
    {
        $this->ecaTotalContribution = $ecaTotalContribution;

        return $this;
    }

    /**
     * Get ecaTotalContribution
     *
     * @return float
     */
    public function getEcaTotalContribution()
    {
        return $this->ecaTotalContribution;
    }
}
