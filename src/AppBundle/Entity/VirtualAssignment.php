<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * VirtualAssignment
 * @author Felipe Ceballos <felipe.ceballos@inqbation.com>
 *
 * @ORM\Table(name="virtual_assignment")
 * @ORM\Entity()
 */
class VirtualAssignment extends BaseEntity
{

	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(type="date", nullable=true)
	 */
	private $startDate;

	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(type="date", nullable=true)
	 */
	private $endDate;

	/**
	 * @var \AppBundle\Entity\Country
	 *
	 * @ORM\ManyToOne(
	 *     targetEntity="Country",
	 *     inversedBy="virtualAssignments"
	 * )
	 */
	private $country;

	/**
	 * @var string
	 *
	 * @ORM\Column(type="text", nullable=true)
	 */
	private $otherCountries;

	/**
	 * @var string
	 *
	 * @ORM\Column(type="text", nullable=true)
	 */
	private $platform;

	/**
	 * @var string
	 *
	 * @ORM\Column(type="text", nullable=true)
	 */
	private $deliverablesDesc;

	/**
	 * @var string
	 *
	 * @ORM\Column(type="text", nullable=true)
	 */
	private $additionalComments;

	/**
	 * @var SpecialistProjectPhase
	 *
	 * @ORM\ManyToOne(
	 *     targetEntity="SpecialistProjectPhase",
	 *     inversedBy="virtualAssignments"
	 * )
	 */
	private $specialistProjectPhase;

	/**
	 * @var Host
	 *
	 * @ORM\ManyToOne(targetEntity="Host")
	 */
	private $host;

	/**
	 * @var \Doctrine\ORM\Cache\Persister\Collection\
	 *
	 * @ORM\OneToMany(
	 *     targetEntity="VirtualAssignmentActivity",
	 *     mappedBy="virtualAssignment",
	 *     cascade={"persist", "remove"}
	 * )
	 */
	protected $virtualAssignmentActivities;

	/*************************** Methods *******************************/

	/**
	 * Constructor
	 */
	public function __construct()
	{
		$this->virtualAssignmentActivities = new \Doctrine\Common\Collections\ArrayCollection();
	}

	/**
	 * Set startDate
	 *
	 * @param \DateTime $startDate
	 *
	 * @return VirtualAssignment
	 */
	public function setStartDate($startDate)
	{
		$this->startDate = $startDate;

		return $this;
	}

	/**
	 * Get startDate
	 *
	 * @return \DateTime
	 */
	public function getStartDate()
	{
		return $this->startDate;
	}

	/**
	 * Set endDate
	 *
	 * @param \DateTime $endDate
	 *
	 * @return VirtualAssignment
	 */
	public function setEndDate($endDate)
	{
		$this->endDate = $endDate;

		return $this;
	}

	/**
	 * Get endDate
	 *
	 * @return \DateTime
	 */
	public function getEndDate()
	{
		return $this->endDate;
	}

	/**
	 * Set country
	 *
	 * @param Country $country
	 *
	 * @return VirtualAssignment
	 */
	public function setCountry($country)
	{
		$this->country = $country;

		return $this;
	}

	/**
	 * Get country
	 *
	 * @return Country
	 */
	public function getCountry()
	{
		return $this->country;
	}

	/**
	 * Set otherCountries
	 *
	 * @param string $otherCountries
	 *
	 * @return VirtualAssignment
	 */
	public function setOtherCountries($otherCountries)
	{
		$this->otherCountries = $otherCountries;

		return $this;
	}

	/**
	 * Get otherCountries
	 *
	 * @return string
	 */
	public function getOtherCountries()
	{
		return $this->otherCountries;
	}

	/**
	 * Set platform
	 *
	 * @param string $platform
	 *
	 * @return VirtualAssignment
	 */
	public function setPlatform($platform)
	{
		$this->platform = $platform;

		return $this;
	}

	/**
	 * Get platform
	 *
	 * @return string
	 */
	public function getPlatform()
	{
		return $this->platform;
	}

	/**
	 * Set deliverablesDesc
	 *
	 * @param string $deliverablesDesc
	 *
	 * @return VirtualAssignment
	 */
	public function setDeliverablesDesc($deliverablesDesc)
	{
		$this->deliverablesDesc = $deliverablesDesc;

		return $this;
	}

	/**
	 * Get deliverablesDesc
	 *
	 * @return string
	 */
	public function getDeliverablesDesc()
	{
		return $this->deliverablesDesc;
	}

	/**
	 * Set additionalComments
	 *
	 * @param string $additionalComments
	 *
	 * @return VirtualAssignment
	 */
	public function setAdditionalComments($additionalComments)
	{
		$this->additionalComments = $additionalComments;

		return $this;
	}

	/**
	 * Get additionalComments
	 *
	 * @return string
	 */
	public function getAdditionalComments()
	{
		return $this->additionalComments;
	}

	/**
	 * Set specialistProjectPhase
	 *
	 * @param SpecialistProjectPhase $specialistProjectPhase
	 *
	 * @return VirtualAssignment
	 */
	public function setSpecialistProjectPhase($specialistProjectPhase)
	{
		$this->specialistProjectPhase = $specialistProjectPhase;

		return $this;
	}

	/**
	 * Get specialistProjectPhase
	 *
	 * @return SpecialistProjectPhase
	 */
	public function getSpecialistProjectPhase()
	{
		return $this->specialistProjectPhase;
	}

	/**
	 * Set host
	 *
	 * @param Host $host
	 *
	 * @return VirtualAssignment
	 */
	public function setHost($host)
	{
		$this->host = $host;

		return $this;
	}

	/**
	 * Get host
	 *
	 * @return Host
	 */
	public function getHost()
	{
		return $this->host;
	}

	/**
	 * Add virtualAssignmentActivity
	 *
	 * @param \AppBundle\Entity\VirtualAssignmentActivity $virtualAssignmentActivity
	 *
	 * @return VirtualAssignment
	 */
	public function addVirtualAssignmentActivity(\AppBundle\Entity\VirtualAssignmentActivity $virtualAssignmentActivity)
	{
		$this->virtualAssignmentActivities[] = $virtualAssignmentActivity;

		return $this;
	}

	/**
	 * Remove virtualAssignmentActivity
	 *
	 * @param \AppBundle\Entity\VirtualAssignmentActivity $virtualAssignmentActivity
	 */
	public function removeVirtualAssignmentActivity(\AppBundle\Entity\VirtualAssignmentActivity $virtualAssignmentActivity)
	{
		$this->virtualAssignmentActivities->removeElement($virtualAssignmentActivity);
	}

	/**
	 * Get virtualAssignmentActivities
	 *
	 * @return \Doctrine\Common\Collections\Collection
	 */
	public function getVirtualAssignmentActivities()
	{
		return $this->virtualAssignmentActivities;
	}
}
