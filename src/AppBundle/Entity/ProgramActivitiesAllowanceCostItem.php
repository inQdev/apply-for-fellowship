<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ProgramActivitiesAllowanceCostItem entity.
 *
 * @package AppBundle\Entity
 * @author  Juan Obando <juan.obando@inqbation.com>
 *
 * @ORM\Entity()
 * @ORM\Table(name="program_activities_allowance_cost_item")
 */
class ProgramActivitiesAllowanceCostItem extends ProjectContribution
{
    /**
     * @var CostItemProgramActivityAllowance $costItem
     *
     * @ORM\ManyToOne(
     *     targetEntity="CostItemProgramActivityAllowance",
     *     cascade={"persist"}
     * )
     */
    protected $costItem;

    /**
     * @var ProgramActivitiesAllowance $programActivitiesAllowance
     *
     * @ORM\ManyToOne(
     *     targetEntity="ProgramActivitiesAllowance",
     *     inversedBy="programActivitiesAllowanceCostItems"
     * )
     */
    protected $programActivitiesAllowance;

    /**
     * ProgramActivitiesAllowanceCostItem constructor.
     *
     * @param ProgramActivitiesAllowance $programActivitiesAllowance
     */
    public function __construct(
      ProgramActivitiesAllowance $programActivitiesAllowance
    ) {
        $this->programActivitiesAllowance = $programActivitiesAllowance;
    }

    /**
     * TODO: Check calculation to make sure they are right for every type of budget.
     */
    public function calculateTotals()
    {
        $fundedBy = $this->programActivitiesAllowance->getPhaseBudget()->getFundedBy();

        if ('ECA' === $fundedBy) {
            $this->calculateECATotals();
        } 
        elseif ('Post' === $fundedBy) {
            $this->calculatePostTotals();
        }
    }

    /**
     * Set costItem
     *
     * @param CostItemProgramActivityAllowance $costItem
     *
     * @return ProgramActivitiesAllowanceCostItem
     */
    public function setCostItem(
      CostItemProgramActivityAllowance $costItem = null
    ) {
        $this->costItem = $costItem;

        return $this;
    }

    /**
     * Get costItem
     *
     * @return CostItemProgramActivityAllowance
     */
    public function getCostItem()
    {
        return $this->costItem;
    }

    /**
     * Set programActivitiesAllowance
     *
     * @param ProgramActivitiesAllowance $programActivitiesAllowance
     *
     * @return ProgramActivitiesAllowanceCostItem
     */
    public function setProgramActivitiesAllowance(
      ProgramActivitiesAllowance $programActivitiesAllowance = null
    ) {
        $this->programActivitiesAllowance = $programActivitiesAllowance;

        return $this;
    }

    /**
     * Get programActivitiesAllowance
     *
     * @return ProgramActivitiesAllowance
     */
    public function getProgramActivitiesAllowance()
    {
        return $this->programActivitiesAllowance;
    }
}
