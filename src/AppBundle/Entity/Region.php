<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class Region
 *
 * @package AppBundle\Entity
 * @author  Juan Obando <juan.obando@inqbation.com>
 * @author  Felipe Ceballos <felipe.ceballos@inqbation.com>
 *
 * @ORM\Entity()
 * @ORM\Table(name="region")
 */
class Region extends BaseSingleEntity
{
    /**
     * @var string $acronym
     *
     * @ORM\Column(type="string", length=16, nullable=true)
     */
    protected $acronym;

    /**
     * @var ArrayCollection $countries
     *
     * @ORM\OneToMany(
     *     targetEntity="Country",
     *     mappedBy="region",
     *     cascade={"persist"}
     * )
     */
    protected $countries;

    /**
     * @var ArrayCollection $rpos
     *
     * @ORM\ManyToMany(targetEntity="User", mappedBy="regions")
     *
     */
    private $rpos;

    /**
     * Region constructor.
     */
    public function __construct()
    {
        $this->countries = new ArrayCollection();
    }

    /**
     * Set acronym
     *
     * @param string $acronym
     *
     * @return Region
     */
    public function setAcronym($acronym)
    {
        $this->acronym = $acronym;

        return $this;
    }

    /**
     * Get acronym
     *
     * @return string
     */
    public function getAcronym()
    {
        return $this->acronym;
    }

    /**
     * Add country
     *
     * @param Country $country
     *
     * @return Region
     */
    public function addCountry(Country $country)
    {
        $this->countries[] = $country;

        return $this;
    }

    /**
     * Remove country
     *
     * @param Country $country
     */
    public function removeCountry(Country $country)
    {
        $this->countries->removeElement($country);
    }

    /**
     * Get countries
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCountries()
    {
        return $this->countries;
    }

    /**
     * Add rpo
     *
     * @param User $rpo
     *
     * @return Region
     */
    public function addRpo(User $rpo)
    {
        $this->rpos[] = $rpo;

        return $this;
    }

    /**
     * Remove rpo
     *
     * @param User $rpo
     */
    public function removeRpo(User $rpo)
    {
        $this->rpos->removeElement($rpo);
    }

    /**
     * Get rpos
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getRpos()
    {
        return $this->rpos;
    }
}
