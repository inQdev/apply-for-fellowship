<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * LocationHostInfo entity.
 *
 * @package AppBundle\Entity
 * @author  Juan Obando <juan.obando@inqbation.com>
 *
 * @ORM\Entity()
 * @ORM\Table(name="location_host_info")
 */
class LocationHostInfo extends BaseEntity
{
    /**
     * @var string $hostCity
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $hostCity;

    /**
     * @var string $hostCityDescription
     *
     * @ORM\Column(type="string", length=4294967295, nullable=true)
     */
    protected $hostCityDescription;

    /**
     * @var bool $hostAcademicYear
     *
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $hostAcademicYear;

    /**
     * @ORM\Column(type="date", nullable=true)
     * @Assert\Date()
     */
    protected $hostAcademicYearStartDate;

    /**
     * @ORM\Column(type="date", nullable=true)
     * @Assert\Date()
     */
    protected $hostAcademicYearEndDate;

    /**
     * @var bool $newHost
     *
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $newHost;

    /**
     * @var string $fellowAtHost
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\Choice(callback="getFellowAtHostChoices")
     */
    protected $fellowAtHost;

    /**
     * @var string $currentFellowName
     *
     * @ORM\Column(type="string", length=4294967295, nullable=true)
     */
    protected $currentFellowName;

    /**
     * @var string $hostDesc
     *
     * @ORM\Column(type="string", length=4294967295, nullable=true)
     */
    protected $hostDesc;

    /**
     * @var string $additionalInfoDesc
     *
     * @ORM\Column(type="string", length=4294967295, nullable=true)
     */
    protected $additionalInfoDesc;

    /**
     * @var Country $country
     *
     * @ORM\ManyToOne(targetEntity="Country")
     */
    protected $country;

    /**
     * @var Host $host
     *
     * @ORM\ManyToOne(targetEntity="Host")
     */
    protected $host;

    /**
     * @var FellowProject $fellowProject
     *
     * @ORM\OneToOne(
     *     targetEntity="FellowProject",
     *     inversedBy="locationHostInfo"
     * )
     * @ORM\JoinColumn(
     *     name="fellow_project_id",
     *     referencedColumnName="id",
     *     onDelete="cascade"
     * )
     */
    protected $fellowProject;

    /************************ Methods Definitions ***************************/

    /**
     * LocationHostInfo constructor.
     *
     * @param FellowProject $fellowProject
     */
    public function __construct(FellowProject $fellowProject)
    {
        $this->fellowProject = $fellowProject;
    }

    /**
     * @return array
     */
    public static function getFellowAtHostChoices()
    {
        return [
          'No'                      => 'no',
          'Yes, first year Fellow'  => 'first_year',
          'Yes, second year Fellow' => 'second_year',
          'Other'                   => 'other',
        ];
    }

    /**
     * Set hostCity
     *
     * @param string $hostCity
     *
     * @return LocationHostInfo
     */
    public function setHostCity($hostCity)
    {
        $this->hostCity = $hostCity;

        return $this;
    }

    /**
     * Get hostCity
     *
     * @return string
     */
    public function getHostCity()
    {
        return $this->hostCity;
    }

    /**
     * Set hostAcademicYear
     *
     * @param boolean $hostAcademicYear
     *
     * @return LocationHostInfo
     */
    public function setHostAcademicYear($hostAcademicYear)
    {
        $this->hostAcademicYear = $hostAcademicYear;

        return $this;
    }

    /**
     * Get hostAcademicYear
     *
     * @return boolean
     */
    public function getHostAcademicYear()
    {
        return $this->hostAcademicYear;
    }

    /**
     * Set hostAcademicYearStartDate
     *
     * @param \DateTime $hostAcademicYearStartDate
     *
     * @return LocationHostInfo
     */
    public function setHostAcademicYearStartDate($hostAcademicYearStartDate)
    {
        $this->hostAcademicYearStartDate = $hostAcademicYearStartDate;

        return $this;
    }

    /**
     * Get hostAcademicYearStartDate
     *
     * @return \DateTime
     */
    public function getHostAcademicYearStartDate()
    {
        return $this->hostAcademicYearStartDate;
    }

    /**
     * Set hostAcademicYearEndDate
     *
     * @param \DateTime $hostAcademicYearEndDate
     *
     * @return LocationHostInfo
     */
    public function setHostAcademicYearEndDate($hostAcademicYearEndDate)
    {
        $this->hostAcademicYearEndDate = $hostAcademicYearEndDate;

        return $this;
    }

    /**
     * Get hostAcademicYearEndDate
     *
     * @return \DateTime
     */
    public function getHostAcademicYearEndDate()
    {
        return $this->hostAcademicYearEndDate;
    }

    /**
     * Set newHost
     *
     * @param boolean $newHost
     *
     * @return LocationHostInfo
     */
    public function setNewHost($newHost)
    {
        $this->newHost = $newHost;

        return $this;
    }

    /**
     * Get newHost
     *
     * @return boolean
     */
    public function getNewHost()
    {
        return $this->newHost;
    }

    /**
     * Set fellowAtHost
     *
     * @param string $fellowAtHost
     *
     * @return LocationHostInfo
     */
    public function setFellowAtHost($fellowAtHost)
    {
        $this->fellowAtHost = $fellowAtHost;

        return $this;
    }

    /**
     * Get fellowAtHost
     *
     * @return string
     */
    public function getFellowAtHost()
    {
        return $this->fellowAtHost;
    }

    /**
     * Set currentFellowName
     *
     * @param string $currentFellowName
     *
     * @return LocationHostInfo
     */
    public function setCurrentFellowName($currentFellowName)
    {
        $this->currentFellowName = $currentFellowName;

        return $this;
    }

    /**
     * Get currentFellowName
     *
     * @return string
     */
    public function getCurrentFellowName()
    {
        return $this->currentFellowName;
    }

    /**
     * Set hostDesc
     *
     * @param string $hostDesc
     *
     * @return LocationHostInfo
     */
    public function setHostDesc($hostDesc)
    {
        $this->hostDesc = $hostDesc;

        return $this;
    }

    /**
     * Get hostDesc
     *
     * @return string
     */
    public function getHostDesc()
    {
        return $this->hostDesc;
    }

    /**
     * Set additionalInfoDesc
     *
     * @param string $additionalInfoDesc
     *
     * @return LocationHostInfo
     */
    public function setAdditionalInfoDesc($additionalInfoDesc)
    {
        $this->additionalInfoDesc = $additionalInfoDesc;

        return $this;
    }

    /**
     * Get additionalInfoDesc
     *
     * @return string
     */
    public function getAdditionalInfoDesc()
    {
        return $this->additionalInfoDesc;
    }

    /**
     * Set country
     *
     * @param Country $country
     *
     * @return LocationHostInfo
     */
    public function setCountry(Country $country = null)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Get country
     *
     * @return Country
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Set host
     *
     * @param Host $host
     *
     * @return LocationHostInfo
     */
    public function setHost(Host $host = null)
    {
        $this->host = $host;

        return $this;
    }

    /**
     * Get host
     *
     * @return Host
     */
    public function getHost()
    {
        return $this->host;
    }

    /**
     * Set fellowProject
     *
     * @param FellowProject $fellowProject
     *
     * @return LocationHostInfo
     */
    public function setFellowProject(FellowProject $fellowProject = null)
    {
        $this->fellowProject = $fellowProject;

        return $this;
    }

    /**
     * Get fellowProject
     *
     * @return FellowProject
     */
    public function getFellowProject()
    {
        return $this->fellowProject;
    }

    /**
     * Set hostCityDescription
     *
     * @param string $hostCityDescription
     *
     * @return LocationHostInfo
     */
    public function setHostCityDescription($hostCityDescription)
    {
        $this->hostCityDescription = $hostCityDescription;

        return $this;
    }

    /**
     * Get hostCityDescription
     *
     * @return string
     */
    public function getHostCityDescription()
    {
        return $this->hostCityDescription;
    }
}
