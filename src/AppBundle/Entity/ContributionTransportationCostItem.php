<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ContributionTransportationCostItem entity.
 *
 * @package AppBundle\Entity
 * @author  Juan Obando <juan.obando@inqbation.com>
 *
 * @ORM\Entity()
 * @ORM\Table(name="contribution_transportation_cost_item")
 */
class ContributionTransportationCostItem extends ProjectContribution
{
    /**
     * @var CostItem $costItem
     *
     * @ORM\ManyToOne(targetEntity="CostItem", cascade={"persist"})
     */
    protected $costItem;

    /**
     * @var ContributionTransportation $contributionTransportation
     *
     * @ORM\ManyToOne(
     *     targetEntity="ContributionTransportation",
     *     inversedBy="contributionTransportationCostItems"
     * )
     */
    protected $contributionTransportation;

    /**
     * ContributionTransportationCostItem constructor.
     *
     * @param ContributionTransportation $contributionTransportation
     */
    public function __construct(
      ContributionTransportation $contributionTransportation
    ) {
        $this->contributionTransportation = $contributionTransportation;
    }

    /**
     * TODO: Check calculation to make sure they are right for every type of budget.
     */
    public function calculateTotals()
    {
        $fundedBy = $this->contributionTransportation->getPhaseBudget()->getFundedBy();

        if ('ECA' === $fundedBy) {
            $this->calculateECATotals();
        } elseif ('Post' === $fundedBy) {
            $this->calculatePostTotals();
        }
    }

    /**
     * Set costItem
     *
     * @param CostItem $costItem
     *
     * @return ContributionTransportationCostItem
     */
    public function setCostItem(CostItem $costItem = null)
    {
        $this->costItem = $costItem;

        return $this;
    }

    /**
     * Get costItem
     *
     * @return CostItem
     */
    public function getCostItem()
    {
        return $this->costItem;
    }

    /**
     * Set contributionTransportation
     *
     * @param ContributionTransportation $contributionTransportation
     *
     * @return ContributionTransportationCostItem
     */
    public function setContributionTransportation(
      ContributionTransportation $contributionTransportation = null
    ) {
        $this->contributionTransportation = $contributionTransportation;

        return $this;
    }

    /**
     * Get contributionTransportation
     *
     * @return ContributionTransportation
     */
    public function getContributionTransportation()
    {
        return $this->contributionTransportation;
    }
}
