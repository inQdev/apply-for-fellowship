<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class DayBreakdownActivityType
 * @author Felipe Ceballos <felipe.ceballos@inqbation.com>
 *
 * @package AppBundle\Entity
 * @ORM\Entity()
 * 
 */
class DayBreakdownActivityType extends BaseTerm
{
}
