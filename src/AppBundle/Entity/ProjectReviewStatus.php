<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ProjectReviewStatus entity.
 *
 * @package AppBundle\Entity
 * @author  Juan Obando <juan.obando@inqbation.com>
 *
 * @ORM\Entity()
 */
class ProjectReviewStatus extends BaseSingleEntity
{
    /**
     *
     */
    const UNDER_RELO_REVIEW = 0;

    /**
     *
     */
    const UNDER_POST_RE_REVIEW = 1;

    /**
     *
     */
    const UNDER_ECA_REVIEW = 2;

    /**
     *
     */
    const UNDER_RPO_REVIEW = 3;

    /**
     *
     */
    const REVIEW_COMPLETE = 4;

    /**
     *
     */
    const POST_REQUIRE_ALTERNATE_FUNDING = 5;

    /**
     * @var integer $id
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    protected $id;

    /********************* Methods Definitions *******************/

    /**
     * Set id
     *
     * @param integer $id
     *
     * @return ProjectOutcome
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }
}
