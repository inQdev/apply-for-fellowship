<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ProjectOutcome entity.
 *
 * @package AppBundle\Entity
 * @author  Juan Obando <juan.obando@inqbation.com>
 *
 * @ORM\Entity()
 */
class ProjectOutcome extends BaseSingleEntity
{
    /**
     *
     */
    const REJECTED_BY_RELO = 0;

    /**
     *
     */
    const REJECTED_BY_ECA = 1;

    /**
     *
     */
    const APPROVED_BY_ECA = 2;

    /**
     *
     */
    const WITHDRAWN = 4;

    /**
     *
     */
    const ALTERNATE_FUNDING_UNAVAILABLE = 5;

    /**
     * @var integer $id
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    protected $id;

    /************************** Methods Definition *************************/

    public function __construct($id=null) {
        $this->id = $id;
    }

    /**
     * Set id
     *
     * @param integer $id
     *
     * @return ProjectOutcome
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }
}
