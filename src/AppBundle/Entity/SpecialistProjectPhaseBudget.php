<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * SpecialistProjectPhaseBudget entity
 *
 * @package AppBundle\Entity
 * @author  Juan Obando <juan.obando@inqbation.com>
 * @author  Felipe Ceballos <felipe.ceballos@inqbation.com>
 *
 * @ORM\Entity
 * @ORM\Table(name="specialist_project_phase_budget")
 */
class SpecialistProjectPhaseBudget extends BaseEntity
{
    /**
     * @var int $revisionId
     *
     * @ORM\Column(type="integer")
     */
    protected $revisionId = 1;

    /**
     * @var string $fundedBy
     *
     * @ORM\Column(type="string", nullable=true)
     */
    protected $fundedBy;

    /**
     * @var string $postFundingSource
     *
     * @ORM\Column(type="string", length=4294967295, nullable=true)
     */
    protected $postFundingSource;

    /**
     * @var SpecialistProjectPhase $phase
     *
     * @ORM\OneToOne(targetEntity="SpecialistProjectPhase", inversedBy="budget")
     */
    protected $phase;

    /**
     * @var ArrayCollection $contributionLivingExpenses
     *
     * @ORM\OneToMany(
     *     targetEntity="ContributionLivingExpense",
     *     mappedBy="phaseBudget",
     *     cascade={"persist"}
     * )
     */
    protected $contributionLivingExpenses;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(
     *     targetEntity="LivingExpense",
     *     mappedBy="phaseBudget",
     *     cascade={"persist"}
     * )
     */
    protected $livingExpenseEstimates;

    /**
     * @var ContributionTransportation $contributionTransportation
     *
     * @ORM\OneToOne(
     *     targetEntity="ContributionTransportation",
     *     mappedBy="phaseBudget"
     * )
     */
    protected $contributionTransportation;

    /**
     * @var ContributionOneTime $contributionOneTime
     *
     * @ORM\OneToOne(targetEntity="ContributionOneTime", mappedBy="phaseBudget")
     */
    protected $contributionOneTime;

    /**
     * @var ProgramActivitiesAllowance $programActivitiesAllowance
     *
     * @ORM\OneToOne(
     *     targetEntity="ProgramActivitiesAllowance",
     *     mappedBy="phaseBudget"
     * )
     */
    protected $programActivitiesAllowance;

    /************************** Methods ***************************/

    /**
     * SpecialistProjectPhaseBudget constructor.
     *
     * @param SpecialistProjectPhase $phase
     */
    public function __construct(SpecialistProjectPhase $phase)
    {
        $this->phase = $phase;

        $this->contributionLivingExpenses = new ArrayCollection();
    }

    /**
     * Set revisionId
     *
     * @param integer $revisionId
     *
     * @return SpecialistProjectPhaseBudget
     */
    public function setRevisionId($revisionId)
    {
        $this->revisionId = $revisionId;

        return $this;
    }

    /**
     * Get revisionId
     *
     * @return integer
     */
    public function getRevisionId()
    {
        return $this->revisionId;
    }

    /**
     * Set fundedBy
     *
     * @param string $fundedBy
     *
     * @return SpecialistProjectPhaseBudget
     */
    public function setFundedBy($fundedBy)
    {
        $this->fundedBy = $fundedBy;

        return $this;
    }

    /**
     * Get fundedBy
     *
     * @return string
     */
    public function getFundedBy()
    {
        return $this->fundedBy;
    }

    /**
     * Set postFundingSource
     *
     * @param string $postFundingSource
     *
     * @return SpecialistProjectPhaseBudget
     */
    public function setPostFundingSource($postFundingSource)
    {
        $this->postFundingSource = $postFundingSource;

        return $this;
    }

    /**
     * Get postFundingSource
     *
     * @return string
     */
    public function getPostFundingSource()
    {
        return $this->postFundingSource;
    }

    /**
     * Set phase
     *
     * @param SpecialistProjectPhase $phase
     *
     * @return SpecialistProjectPhaseBudget
     */
    public function setPhase(SpecialistProjectPhase $phase = null)
    {
        $this->phase = $phase;

        return $this;
    }

    /**
     * Get phase
     *
     * @return SpecialistProjectPhase
     */
    public function getPhase()
    {
        return $this->phase;
    }

    /**
     * Add contributionLivingExpense
     *
     * @param ContributionLivingExpense $contributionLivingExpense
     *
     * @return SpecialistProjectPhaseBudget
     */
    public function addContributionLivingExpense(
      ContributionLivingExpense $contributionLivingExpense
    ) {
        $this->contributionLivingExpenses[] = $contributionLivingExpense;

        return $this;
    }

    /**
     * Remove contributionLivingExpense
     *
     * @param ContributionLivingExpense $contributionLivingExpense
     */
    public function removeContributionLivingExpense(
      ContributionLivingExpense $contributionLivingExpense
    ) {
        $this->contributionLivingExpenses->removeElement(
          $contributionLivingExpense
        );
    }

    /**
     * Get contributionLivingExpenses
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getContributionLivingExpenses()
    {
        return $this->contributionLivingExpenses;
    }

    /**
     * Add livingExpenseEstimate
     *
     * @param LivingExpense $livingExpenseEstimate
     *
     * @return SpecialistProjectPhaseBudget
     */
    public function addLivingExpenseEstimate(
      LivingExpense $livingExpenseEstimate
    ) {
        $this->livingExpenseEstimates[] = $livingExpenseEstimate;

        return $this;
    }

    /**
     * Remove livingExpenseEstimate
     *
     * @param LivingExpense $livingExpenseEstimate
     */
    public function removeLivingExpenseEstimate(
      LivingExpense $livingExpenseEstimate
    ) {
        $this->livingExpenseEstimates->removeElement($livingExpenseEstimate);
    }

    /**
     * Get livingExpenseEstimates
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getLivingExpenseEstimates()
    {
        return $this->livingExpenseEstimates;
    }

    /**
     * Set contributionTransportation
     *
     * @param ContributionTransportation $contributionTransportation
     *
     * @return SpecialistProjectPhaseBudget
     */
    public function setContributionTransportation(
      ContributionTransportation $contributionTransportation = null
    ) {
        $this->contributionTransportation = $contributionTransportation;
    }

    /**
     * Get contributionTransportation
     *
     * @return ContributionTransportation
     */
    public function getContributionTransportation()
    {
        return $this->contributionTransportation;
    }

    /**
     * Set contributionOneTime
     *
     * @param ContributionOneTime $contributionOneTime
     *
     * @return SpecialistProjectPhaseBudget
     */
    public function setContributionOneTime(
      ContributionOneTime $contributionOneTime = null
    ) {
        $this->contributionOneTime = $contributionOneTime;

        return $this;
    }

    /**
     * Get contributionOneTime
     *
     * @return ContributionOneTime
     */
    public function getContributionOneTime()
    {
        return $this->contributionOneTime;
    }

    /**
     * Set programActivitiesAllowance
     *
     * @param ProgramActivitiesAllowance $programActivitiesAllowance
     *
     * @return SpecialistProjectPhaseBudget
     */
    public function setProgramActivitiesAllowance(
      ProgramActivitiesAllowance $programActivitiesAllowance = null
    ) {
        $this->programActivitiesAllowance = $programActivitiesAllowance;

        return $this;
    }

    /**
     * Get programActivitiesAllowance
     *
     * @return ProgramActivitiesAllowance
     */
    public function getProgramActivitiesAllowance()
    {
        return $this->programActivitiesAllowance;
    }
}
