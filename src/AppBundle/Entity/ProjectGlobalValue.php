<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * ProjectGlobalValue
 *
 * @ORM\Table(name="project_global_value")
 * @ORM\Entity()
 */
class ProjectGlobalValue
{
	/**
	 * @var int
	 *
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	private $id;

	/**
	 * @var \AppBundle\Entity\GlobalValue $globalValue
	 *
	 * @ORM\ManyToOne(targetEntity="GlobalValue")
	 *
	 */
	private $globalValue;

	/**
	 * @var \AppBundle\Entity\FellowProjectBudget $fellowProjectBudget
	 *
	 * @ORM\ManyToOne(targetEntity="FellowProjectBudget", inversedBy="projectGlobalValues")
	 * @ORM\JoinColumn(
	 *     name="fellow_project_budget_id",
	 *     referencedColumnName="id",
	 *     onDelete="cascade"
	 * )
	 */
	private $fellowProjectBudget;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="value", type="decimal", precision=10, scale=2)
	 */
	private $actualValue;

	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="created_at", type="datetime")
	 * @Gedmo\Timestampable(on="create")
	 */
	private $createdAt;

	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="updated_at", type="datetime")
	 * @Gedmo\Timestampable(on="update")
	 */
	private $updatedAt;

	/************************** Methods Definitions ***************************/

	/**
	 * Get id
	 *
	 * @return int
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * Set globalValue
	 *
	 * @param \AppBundle\Entity\GlobalValue $globalValue
	 *
	 * @return ProjectGlobalValue
	 */
	public function setGlobalValue(GlobalValue $globalValue = null)
	{
		$this->globalValue = $globalValue;

		return $this;
	}

	/**
	 * Get globalValue
	 *
	 * @return \AppBundle\Entity\GlobalValue
	 */
	public function getGlobalValue()
	{
		return $this->globalValue;
	}

	/**
	 * Set fellowProjectBudget
	 *
	 * @param \AppBundle\Entity\FellowProjectBudget $fellowProjectBudget
	 *
	 * @return ProjectGlobalValue
	 */
	public function setFellowProjectBudget(FellowProjectBudget $fellowProjectBudget = null)
	{
		$this->fellowProjectBudget = $fellowProjectBudget;

		return $this;
	}

	/**
	 * Get fellowProjectBudget
	 *
	 * @return \AppBundle\Entity\FellowProjectBudget
	 */
	public function getFellowProjectBudgetId()
	{
		return $this->fellowProjectBudgetId;
	}

	/**
	 * Set value
	 *
	 * @param string $value
	 *
	 * @return ProjectGlobalValue
	 */
	public function setActualValue($value)
	{
		$this->actualValue = $value;

		return $this;
	}

	/**
	 * Get value
	 *
	 * @return string
	 */
	public function getActualValue()
	{
		return $this->actualValue;
	}

	/**
	 * Set createdAt
	 *
	 * @param \DateTime $createdAt
	 *
	 * @return ProjectGlobalValue
	 */
	public function setCreatedAt($createdAt)
	{
		$this->createdAt = $createdAt;

		return $this;
	}

	/**
	 * Get createdAt
	 *
	 * @return \DateTime
	 */
	public function getCreatedAt()
	{
		return $this->createdAt;
	}

	/**
	 * Set updatedAt
	 *
	 * @param \DateTime $updatedAt
	 *
	 * @return ProjectGlobalValue
	 */
	public function setUpdatedAt($updatedAt)
	{
		$this->updatedAt = $updatedAt;

		return $this;
	}

	/**
	 * Get updatedAt
	 *
	 * @return \DateTime
	 */
	public function getUpdatedAt()
	{
		return $this->updatedAt;
	}
}

