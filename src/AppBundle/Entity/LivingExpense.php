<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * LivingExpense
 * @author Felipe Ceballos <felipe.ceballos@inqbation.com>
 *
 * @ORM\Table(name="living_expense")
 * @ORM\Entity()
 */
class LivingExpense extends BaseEntity
{

	/**
	 * @var int
	 *
	 * @ORM\Column(type="integer", nullable=true)
	 */
	protected $revisionId;

	/**
	 * @var int
	 *
	 * @ORM\ManyToOne(
	 *     targetEntity="SpecialistProjectPhaseBudget",
	 *     inversedBy="livingExpenseEstimates"
	 * )
	 */
	protected $phaseBudget;

	/**
	 * @var int
	 *
	 * @ORM\ManyToOne(
	 *     targetEntity="InCountryAssignment",
	 *     inversedBy="livingExpenseEstimates"
	 * )
	 */
	protected $inCountryAssignment;

	/**
	 * @var ArrayCollection
	 *
	 * @ORM\OneToMany(
	 *     targetEntity="LivingExpenseCostItem",
	 *     mappedBy="livingExpense",
	 *     cascade={"persist"}
	 * )
	 */
	protected $livingExpenseCostItems;


	/**************************** Methods ****************************/

	/**
	 * Constructor
	 */
	public function __construct()
	{
		$this->livingExpenseCostItems = new \Doctrine\Common\Collections\ArrayCollection();
	}

	/**
	 * Set specialistProjectPhaseBudget
	 *
	 * @param SpecialistProjectPhaseBudget $specialistProjectPhaseBudget
	 *
	 * @return LivingExpense
	 */
	public function setPhaseBudget($specialistProjectPhaseBudget)
	{
		$this->phaseBudget = $specialistProjectPhaseBudget;

		return $this;
	}

	/**
	 * Get specialistProjectPhaseBudget
	 *
	 * @return SpecialistProjectPhaseBudget
	 */
	public function getPhaseBudget()
	{
		return $this->phaseBudget;
	}

	/**
	 * Set inCountryAssignment
	 *
	 * @param InCountryAssignment $inCountryAssignment
	 *
	 * @return LivingExpense
	 */
	public function setInCountryAssignment($inCountryAssignment)
	{
		$this->inCountryAssignment = $inCountryAssignment;

		return $this;
	}

	/**
	 * Get inCountryAssignment
	 *
	 * @return InCountryAssignment
	 */
	public function getInCountryAssignment()
	{
		return $this->inCountryAssignment;
	}

	/**
	 * Set revisionId
	 *
	 * @param integer $revisionId
	 *
	 * @return LivingExpense
	 */
	public function setRevisionId($revisionId)
	{
		$this->revisionId = $revisionId;

		return $this;
	}

	/**
	 * Get revisionId
	 *
	 * @return int
	 */
	public function getRevisionId()
	{
		return $this->revisionId;
	}

    /**
     * Add livingExpenseCostItem
     *
     * @param \AppBundle\Entity\LivingExpenseCostItem $livingExpenseCostItem
     *
     * @return LivingExpense
     */
    public function addLivingExpenseCostItem(\AppBundle\Entity\LivingExpenseCostItem $livingExpenseCostItem)
    {
        $this->livingExpenseCostItems[] = $livingExpenseCostItem;

        return $this;
    }

    /**
     * Remove livingExpenseCostItem
     *
     * @param \AppBundle\Entity\LivingExpenseCostItem $livingExpenseCostItem
     */
    public function removeLivingExpenseCostItem(\AppBundle\Entity\LivingExpenseCostItem $livingExpenseCostItem)
    {
        $this->livingExpenseCostItems->removeElement($livingExpenseCostItem);
    }

    /**
     * Get livingExpenseCostItems
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getLivingExpenseCostItems()
    {
        return $this->livingExpenseCostItems;
    }
}
