<?php

namespace AppBundle\Entity;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * Class FellowProjectBudget
 *
 * @package AppBundle\Entity
 *
 * @ORM\Entity(repositoryClass="AppBundle\Repository\FellowProjectBudgetRepository")
 * @ORM\Table(name="fellow_project_budget")
 */
class FellowProjectBudget {

	/**
	 * @var integer $id
	 *
	 * @ORM\Column(type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	protected $id;

	/**
	 * @var integer $revision_id
	 *
	 * @ORM\Column(type="integer")
	 */
	protected $revision_id;

	/**
	 * @var string $funded_by
	 *
	 * @ORM\Column(type="string")
	 */
	protected $funded_by;

	/**
	 * @var string $postFundingSource
	 *
	 * @ORM\Column(
	 *     type="string",
	 *     length=4294967295,
	 *     nullable=true
	 * )
	 */
	protected $postFundingSource;

	/**
	 * @var \DateTime $created_at
	 *
	 * @ORM\Column(type="datetime")
	 * @Gedmo\Timestampable(on="create")
	 *
	 */
	protected $created_at;

	/**
	 * @var \DateTime $updated_at
	 *
	 * @ORM\Column(type="datetime")
	 * @Gedmo\Timestampable(on="update")
	 */
	protected $updated_at;

	/**
	 * @var \AppBundle\Entity\FellowProject $fellowProject
	 *
	 * @ORM\OneToOne(targetEntity="FellowProject",
	 *     inversedBy="fellowProjectBudget"
	 * )
	 * @ORM\JoinColumn(
	 *     name="fellow_project_id",
	 *     referencedColumnName="id",
	 *     onDelete="cascade"
	 * )
	 */
	protected $fellowProject;

	/**
	 * Cascade deletion defined in FellowProjectBudgetCostItem
	 * @var \AppBundle\Entity\FellowProjectBudgetCostItem $fellowProjectBudgetCostItem
	 *
	 * @ORM\OneToMany(
	 *	targetEntity = "FellowProjectBudgetCostItem",
	 *	mappedBy = "fellowProjectBudget",
	 *	cascade = {"persist"}
	 * )
	 * @ORM\OrderBy({"revision_id" = "DESC", "id" = "ASC"})
	 */
	protected $budgetCostItems;

	/**
	 * Cascade deletion defined in ProjectGlobalValue
	 * @var \AppBundle\Entity\ProjectGlobalValue $projectGlobalValues
	 *
	 * @ORM\OneToMany(
	 *	targetEntity = "ProjectGlobalValue",
	 *	mappedBy = "fellowProjectBudget",
	 *	cascade = {"persist"}
	 * )
	 */
	protected $projectGlobalValues;

	/**
	 * Cascade deletion defined in FellowProjectBudgetTotal
	 * @var FellowProjectBudgetTotals
	 *
	 * @ORM\OneToMany (
	 *     targetEntity = "FellowProjectBudgetTotals",
	 *     mappedBy = "fellowProjectBudget",
	 *     cascade = {"persist"}
	 * )
	 */
	protected $fellowProjectBudgetTotals;


	protected $budgetCostItemsCustom;


	/**
	 * Constructor
	 */
	public function __construct() {
		$this->budgetCostItems = new \Doctrine\Common\Collections\ArrayCollection();
		$this->projectGlobalValues = new \Doctrine\Common\Collections\ArrayCollection();
	}

	/**
	 * Get id
	 *
	 * @return integer
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 * Set revisionId
	 *
	 * @param integer $revisionId
	 *
	 * @return FellowProjectBudget
	 */
	public function setRevisionId($revisionId) {
		$this->revision_id = $revisionId;

		return $this;
	}

	/**
	 * Get revisionId
	 *
	 * @return integer
	 */
	public function getRevisionId() {
		return $this->revision_id;
	}

	/**
	 * Set createdAt
	 *
	 * @param \DateTime $createdAt
	 *
	 * @return FellowProjectBudget
	 */
	public function setCreatedAt($createdAt) {
		$this->created_at = $createdAt;

		return $this;
	}

	/**
	 * Get createdAt
	 *
	 * @return \DateTime
	 */
	public function getCreatedAt() {
		return $this->created_at;
	}

	/**
	 * Set updatedAt
	 *
	 * @param \DateTime $updatedAt
	 *
	 * @return FellowProjectBudget
	 */
	public function setUpdatedAt($updatedAt) {
		$this->updated_at = $updatedAt;

		return $this;
	}

	/**
	 * Get updatedAt
	 *
	 * @return \DateTime
	 */
	public function getUpdatedAt() {
		return $this->updated_at;
	}

	/**
	 * Set fellowProject
	 *
	 * @param \AppBundle\Entity\FellowProject $fellowProject
	 *
	 * @return FellowProjectBudget
	 */
	public function setFellowProject(FellowProject $fellowProject = null) {
		$this->fellowProject = $fellowProject;

		return $this;
	}

	/**
	 * Get fellowProject
	 *
	 * @return \AppBundle\Entity\FellowProject
	 */
	public function getFellowProject() {
		return $this->fellowProject;
	}

	/**
	 * Set fundedBy
	 *
	 * @param string $fundedBy
	 *
	 * @return FellowProjectBudget
	 */
	public function setFundedBy($fundedBy) {
		$this->funded_by = $fundedBy;

		return $this;
	}

	/**
	 * Get fundedBy
	 *
	 * @return string
	 */
	public function getFundedBy() {
		return $this->funded_by;
	}

	/**
	 * Set postFundingSource
	 *
	 * @param string $postFundingSource
	 *
	 * @return FellowProjectBudget
	 */
	public function setPostFundingSource($postFundingSource)
	{
		$this->postFundingSource = $postFundingSource;

		return $this;
	}

	/**
	 * Get postFundingSource
	 *
	 * @return string
	 */
	public function getPostFundingSource()
	{
		return $this->postFundingSource;
	}

	/**
	 * Add budgetCostItem
	 *
	 * @param \AppBundle\Entity\FellowProjectBudgetCostItem $budgetCostItem
	 *
	 * @return FellowProjectBudget
	 */
	public function addBudgetCostItem(FellowProjectBudgetCostItem $budgetCostItem) {
		$this->budgetCostItems[] = $budgetCostItem;

		return $this;
	}

	/**
	 * Remove budgetCostItem
	 *
	 * @param \AppBundle\Entity\FellowProjectBudgetCostItem $budgetCostItem
	 */
	public function removeBudgetCostItem(FellowProjectBudgetCostItem $budgetCostItem) {
		$this->budgetCostItems->removeElement($budgetCostItem);
	}

	/**
	 * Get budgetCostItems
	 *
	 * @return \Doctrine\Common\Collections\Collection
	 */
	public function getBudgetCostItems() {
		return $this->budgetCostItems;
	}


    /**
     * Add projectGlobalValue
     *
     * @param \AppBundle\Entity\ProjectGlobalValue $projectGlobalValue
     *
     * @return FellowProjectBudget
     */
    public function addProjectGlobalValue(ProjectGlobalValue $projectGlobalValue)
    {
        $this->projectGlobalValues[] = $projectGlobalValue;

        return $this;
    }

    /**
     * Remove projectGlobalValue
     *
     * @param \AppBundle\Entity\ProjectGlobalValue $projectGlobalValue
     */
    public function removeProjectGlobalValue(ProjectGlobalValue $projectGlobalValue)
    {
        $this->projectGlobalValues->removeElement($projectGlobalValue);
    }

    /**
     * Get projectGlobalValues
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getProjectGlobalValues()
    {
        return $this->projectGlobalValues;
    }


	/**
	 * @param \AppBundle\Entity\FellowProject $fellowProject
	 * @param \Doctrine\ORM\EntityManager     $em
	 * @return int $status
	 */
	public function getBudgetStepStatus($fellowProject, $em)
	{
		$budgetStep = $em->getRepository('AppBundle:WizardStepStatus')
			->findStepStatusByProject($fellowProject, WizardStepStatus::BUDGET_STEP);

		return $budgetStep->getStatus();
	}

	/**
	 * @param \AppBundle\Entity\FellowProject $fellowProject
	 * @param \Doctrine\ORM\EntityManager     $em
	 * @param int $status
	 */
	public function setBudgetStepStatus($fellowProject, $em, $status)
	{
		$budgetStep = $em->getRepository('AppBundle:WizardStepStatus')
			->findStepStatusByProject($fellowProject, WizardStepStatus::BUDGET_STEP);

		$budgetStep->setStatus($status);
		$em->persist($budgetStep);
		$em->flush();

	}


	/**
	 * Calculates the totals that will be shown in the summary tables
	 *
	 * @return array $totals
	 */
	public function getSummaryTotals() {
		// Calculates totals (values that are not in the DB table)
		$totals = array(
			'monthly' => array(
				'fellowship' => 0,
				'post' => 0,
				'host_monetary' => 0,
				'host_kind' => 0,
				'post_total' => 0,
				'eca' => 0,
			),
			'onetime' => array(
				'fellowship' => 0,
				'post' => 0,
				'host_monetary' => 0,
				'host_kind' => 0,
				'post_total' => 0,
				'eca' => 0,
			),
			'shared' => 0,
		);

		foreach ($this->getBudgetCostItems() as $item) {
			if (is_a($item->getCostItem(), 'AppBundle\Entity\CostItemMonthly')) { //monthly

				$totals['monthly']['fellowship'] += $item->getFullCost();
				$totals['monthly']['post'] += $item->getPostContribution();
				$totals['monthly']['host_monetary'] += $item->getHostContributionMonetary();
				$totals['monthly']['host_kind'] += $item->getHostContributionInKind();
				$totals['monthly']['post_total'] += $item->getPostTotalContribution();
				$totals['monthly']['eca'] += $item->getECATotalContribution();

			} else { // one time cost

				$totals['onetime']['fellowship'] += $item->getFullCost();
				$totals['onetime']['post'] += $item->getPostContribution();
				$totals['onetime']['host_monetary'] += $item->getHostContributionMonetary();
				$totals['onetime']['host_kind'] += $item->getHostContributionInKind();
				$totals['onetime']['post_total'] += $item->getPostTotalContribution();
				$totals['onetime']['eca'] += $item->getECATotalContribution();

			}
		}

		$fellowProjectBudgetTotals = $this->getFellowProjectBudgetTotals()->first();

		if ($this->getFundedBy() == 'ECA') {
			$totals['shared'] = $fellowProjectBudgetTotals->getHostContributionMonetary() + $fellowProjectBudgetTotals->getPostContribution() + $fellowProjectBudgetTotals->getHostContributionInKind();
		} else {
			$totals['shared'] = $fellowProjectBudgetTotals->getTotal();
		}

		return $totals;
	}


	/**
	 * Calculates and saves totals (Summary) to avoid some recalculations and to expedite searches
	 * @param EntityManager $em
	 * @param boolean $save - to control if the settings must be saved
	 * @return array
	 */
	public function setSummaryTotals($em=null, $save=false) {

		$budgetTotals = $this->getFellowProjectBudgetTotals();

		if (!count($budgetTotals)) {
			$budgetTotals = new FellowProjectBudgetTotals();
			$budgetTotals->setRevisionId($this->getRevisionId());
			$budgetTotals->setFellowProjectBudget($this);
			$this->addFellowProjectBudgetTotal($budgetTotals);
		} else {
			$budgetTotals = $budgetTotals->first();
		}


		// Calculates totals (values that are not in the DB table)
		$totals = array(
			'monthly' => array(
				'fellowship' => 0,
				'post' => 0,
				'host_monetary' => 0,
				'host_kind' => 0,
				'post_total' => 0,
				'eca' => 0,
			),
			'onetime' => array(
				'fellowship' => 0,
				'post' => 0,
				'host_monetary' => 0,
				'host_kind' => 0,
				'post_total' => 0,
				'eca' => 0,
			),
			'global' => 0,
		);

		foreach ($this->getBudgetCostItems() as $item) {
			if (is_a($item->getCostItem(), 'AppBundle\Entity\CostItemMonthly')) { //monthly

				$totals['monthly']['fellowship'] += $item->getFullCost();
				$totals['monthly']['post'] += $item->getPostContribution();
				$totals['monthly']['host_monetary'] += $item->getHostContributionMonetary();
				$totals['monthly']['host_kind'] += $item->getHostContributionInKind();
				$totals['monthly']['post_total'] += $item->getPostTotalContribution();
				$totals['monthly']['eca'] += $item->getECATotalContribution();

			} else { // one time cost

				$totals['onetime']['fellowship'] += $item->getFullCost();
				$totals['onetime']['post'] += $item->getPostContribution();
				$totals['onetime']['host_monetary'] += $item->getHostContributionMonetary();
				$totals['onetime']['host_kind'] += $item->getHostContributionInKind();
				$totals['onetime']['post_total'] += $item->getPostTotalContribution();
				$totals['onetime']['eca'] += $item->getECATotalContribution();

			}
		}

		// calculates the global values total
		foreach ($this->getProjectGlobalValues() as $projectGlobalValue) {
			$totals['global'] += $projectGlobalValue->getActualValue();
		}
		$budgetTotals->setGlobal($totals['global']);

		$budgetTotals->setPostContribution($totals['monthly']['post'] + $totals['onetime']['post']);
		$budgetTotals->setPostTotalContribution($totals['monthly']['post_total'] + $totals['onetime']['post_total']);
		$budgetTotals->setHostContributionMonetary($totals['monthly']['host_monetary'] + $totals['onetime']['host_monetary']);
		$budgetTotals->setHostContributionInKind($totals['monthly']['host_kind'] + $totals['onetime']['host_kind']);
		// Full cost includes the costs (monetary) and in-kind contributions by host
		$budgetTotals->setFullCost($totals['monthly']['fellowship'] + $totals['onetime']['fellowship'] + $budgetTotals->getHostContributionInKind());
		$budgetTotals->setTotal($budgetTotals->getFullCost() + $budgetTotals->getGlobal());

		if ($this->getFundedBy() == 'ECA') {
			$budgetTotals->setEcaTotalContribution($totals['monthly']['eca'] + $totals['onetime']['eca'] + $totals['global']);
//			$budgetTotals->setHostContributionInKind($totals['monthly']['host_kind'] + $totals['onetime']['host_kind']);
		} else {
			$budgetTotals->setEcaTotalContribution(0);
			$budgetTotals->setPostTotalContribution($budgetTotals->getPostTotalContribution() + $totals['global']);
		}

		if ($save) {
			$em->persist($budgetTotals);
			$em->flush();
		}

		return $totals;
	}


    /**
     * Add fellowProjectBudgetTotal
     *
     * @param \AppBundle\Entity\FellowProjectBudgetTotals $fellowProjectBudgetTotal
     *
     * @return FellowProjectBudget
     */
    public function addFellowProjectBudgetTotal(\AppBundle\Entity\FellowProjectBudgetTotals $fellowProjectBudgetTotal)
    {
        $this->fellowProjectBudgetTotals[] = $fellowProjectBudgetTotal;

        return $this;
    }

    /**
     * Remove fellowProjectBudgetTotal
     *
     * @param \AppBundle\Entity\FellowProjectBudgetTotals $fellowProjectBudgetTotal
     */
    public function removeFellowProjectBudgetTotal(\AppBundle\Entity\FellowProjectBudgetTotals $fellowProjectBudgetTotal)
    {
        $this->fellowProjectBudgetTotals->removeElement($fellowProjectBudgetTotal);
    }

    /**
     * Get fellowProjectBudgetTotals
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getFellowProjectBudgetTotals()
    {
        return $this->fellowProjectBudgetTotals;
    }
}
