<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Assignment Entity Class
 * 
 * @author Felipe Ceballos <felipe.ceballos@inqbation.com>
 *
 * @ORM\Entity()
 * @ORM\Table(name="in_country_assignment")
 */
class InCountryAssignment extends BaseEntity
{
	/**
	 * @var string
	 *
	 * @ORM\Column(type="string", length=255)
	 */
	protected $city;

	/**
	 * @var string
	 *
	 * @ORM\Column(type="text", nullable=true)
	 */
	protected $deliverablesDesc;

	/**
	 * @var Country $country
	 *
	 * @ORM\ManyToOne(
	 *     targetEntity="Country",
	 *     inversedBy="inCountryAssignments"
	 * )
	 */
	protected $country;

	/**
	 * @var SpecialistProjectPhase $specialistProjectPhase
	 *
	 * @ORM\ManyToOne(
	 *     targetEntity="SpecialistProjectPhase",
	 *     inversedBy="inCountryAssignments"
	 * )
	 */
	protected $specialistProjectPhase;

	/**
	 * @var Host $host
	 *
	 * @ORM\ManyToOne(targetEntity="Host")
	 */
	protected $host;

	/**
	 * @var string
	 *
	 * @ORM\Column(type="text", nullable=true)
	 */
	protected $hostDesc;


	/**
	 * @var ArrayCollection
	 *
	 * @ORM\OneToMany(
	 *     targetEntity="DayBreakdownActivity",
	 *     mappedBy="inCountryAssignment",
     *     cascade={"persist", "remove"}
	 * )
	 */
	protected $dayBreakdownActivities;

	/**
	 * @var ArrayCollection
	 *
	 * @ORM\OneToMany(
	 *     targetEntity="InCountryAssignmentActivity",
	 *     mappedBy="inCountryAssignment",
	 *     cascade={"persist", "remove"}
	 * )
	 */
	protected $assignmentActivities;

	/**
	 * @var ArrayCollection
	 *
	 * @ORM\OneToMany(
	 *     targetEntity="LivingExpense",
	 *     mappedBy="inCountryAssignment",
	 * )
	 */
	protected $livingExpenseEstimates;

	/****************************  Methods **********************************/

	public function __construct() {
		$this->dayBreakdownActivities = new ArrayCollection();
		$this->assignmentActivities = new ArrayCollection();
	}

	/**
     * Set city
     *
     * @param string $city
     *
     * @return InCountryAssignment
     */
    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city
     *
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set deliverablesDesc
     *
     * @param string $deliverablesDesc
     *
     * @return InCountryAssignment
     */
    public function setDeliverablesDesc($deliverablesDesc)
    {
        $this->deliverablesDesc = $deliverablesDesc;

        return $this;
    }

    /**
     * Get deliverablesDesc
     *
     * @return string
     */
    public function getDeliverablesDesc()
    {
        return $this->deliverablesDesc;
    }

    /**
     * Set country
     *
     * @param integer $country
     *
     * @return InCountryAssignment
     */
    public function setCountry($country)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Get country
     *
     * @return integer
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Set specialistProjectPhase
     *
     * @param SpecialistProjectPhase $specialistProjectPhase
     *
     * @return InCountryAssignment
     */
    public function setSpecialistProjectPhase($specialistProjectPhase)
    {
        $this->specialistProjectPhase = $specialistProjectPhase;

        return $this;
    }

    /**
     * Get specialistProjectPhase
     *
     * @return SpecialistProjectPhase
     */
    public function getSpecialistProjectPhase()
    {
        return $this->specialistProjectPhase;
    }

    /**
     * Set host
     *
     * @param integer $host
     *
     * @return InCountryAssignment
     */
    public function setHost($host)
    {
        $this->host = $host;

        return $this;
    }

    /**
     * Get host
     *
     * @return integer
     */
    public function getHost()
    {
        return $this->host;
    }

    /**
     * Set hostDesc
     *
     * @param string $hostDesc
     *
     * @return InCountryAssignment
     */
    public function setHostDesc($hostDesc)
    {
        $this->hostDesc = $hostDesc;

        return $this;
    }

    /**
     * Get hostDesc
     *
     * @return string
     */
    public function getHostDesc()
    {
        return $this->hostDesc;
    }

    /**
     * Add dayBreakdownActivity
     *
     * @param \AppBundle\Entity\DayBreakdownActivity $dayBreakdownActivity
     *
     * @return InCountryAssignment
     */
    public function addDayBreakdownActivity(\AppBundle\Entity\DayBreakdownActivity $dayBreakdownActivity)
    {
        $this->dayBreakdownActivities[] = $dayBreakdownActivity;

        return $this;
    }

    /**
     * Remove dayBreakdownActivity
     *
     * @param \AppBundle\Entity\DayBreakdownActivity $dayBreakdownActivity
     */
    public function removeDayBreakdownActivity(\AppBundle\Entity\DayBreakdownActivity $dayBreakdownActivity)
    {
        $this->dayBreakdownActivities->removeElement($dayBreakdownActivity);
    }

    /**
     * Get dayBreakdownActivities
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDayBreakdownActivities()
    {
        return $this->dayBreakdownActivities;
    }


    /**
     * Add assignmentActivity
     *
     * @param \AppBundle\Entity\InCountryAssignmentActivity $assignmentActivity
     *
     * @return InCountryAssignment
     */
    public function addAssignmentActivity(\AppBundle\Entity\InCountryAssignmentActivity $assignmentActivity)
    {
        $this->assignmentActivities[] = $assignmentActivity;

        return $this;
    }

    /**
     * Remove assignmentActivity
     *
     * @param \AppBundle\Entity\InCountryAssignmentActivity $assignmentActivity
     */
    public function removeAssignmentActivity(\AppBundle\Entity\InCountryAssignmentActivity $assignmentActivity)
    {
        $this->assignmentActivities->removeElement($assignmentActivity);
    }

    /**
     * Get assignmentActivities
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAssignmentActivities()
    {
        return $this->assignmentActivities;
    }


	public function getName() {
		return sprinf("%s (%s)", $this->city, $this->getCountry()->getName());
	}


    /**
     * Add livingExpenseEstimate
     *
     * @param \AppBundle\Entity\LivingExpense $livingExpenseEstimate
     *
     * @return InCountryAssignment
     */
    public function addLivingExpenseEstimate(\AppBundle\Entity\LivingExpense $livingExpenseEstimate)
    {
        $this->livingExpenseEstimates[] = $livingExpenseEstimate;

        return $this;
    }

    /**
     * Remove livingExpenseEstimate
     *
     * @param \AppBundle\Entity\LivingExpense $livingExpenseEstimate
     */
    public function removeLivingExpenseEstimate(\AppBundle\Entity\LivingExpense $livingExpenseEstimate)
    {
        $this->livingExpenseEstimates->removeElement($livingExpenseEstimate);
    }

    /**
     * Get livingExpenseEstimates
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getLivingExpenseEstimates()
    {
        return $this->livingExpenseEstimates;
    }
}
