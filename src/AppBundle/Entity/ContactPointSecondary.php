<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class ContactPointSecondary
 *
 * @package AppBundle\Entity
 * @author  Juan Obando <juan.obando@inqbation.com>
 *
 * @ORM\Entity()
 */
class ContactPointSecondary extends ContactPoint
{
    /**
     * Get the type
     *
     * @return int
     */
    public function getType()
    {
        return self::SECONDARY_TYPE;
    }
}
