<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * SpecialistProjectPhase entity.
 *
 * @package AppBundle\Entity
 * @author  Juan Obando <juan.obando@inqbation.com>
 * @author  Felipe Ceballos <felipe.ceballos@inqbation.com>
 *
 * @ORM\Entity(repositoryClass="AppBundle\Repository\SpecialistProjectPhaseRepository")
 * @ORM\Table(name="specialist_project_phase")
 */
class SpecialistProjectPhase extends BaseEntity
{
    /**
     * @var ProjectGeneralInfo $projectGeneralInfo
     *
     * @ORM\OneToOne(
     *     targetEntity="ProjectGeneralInfo",
     *     inversedBy="specialistProjectPhase"
     * )
     */
    protected $projectGeneralInfo;

    /**
     * @var SpecialistProject $specialistProject
     *
     * @ORM\ManyToOne(
     *     targetEntity="SpecialistProject",
     *     inversedBy="phases"
     * )
     */
    protected $specialistProject;

    /**
     * @var string $type
     *
     * @ORM\Column(
     *     type="string",
     *     length=45,
     *     nullable=true
     * )
     * @Assert\Choice(choices = {"in-country", "virtual", "mixed"})
     */
    protected $type;

    /**
     * @var \DateTime $startDate
     *
     * @ORM\Column(type="date", nullable=true)
     */
    protected $startDate;

    /**
     * @var \DateTime $endDate
     *
     * @ORM\Column(type="date", nullable=true)
     */
    protected $endDate;

    /**
     * @var string $dateLengthDesc
     *
     * @ORM\Column(type="text", nullable=true)
     */
    protected $dateLengthDesc;

    /**
     * @var PrePostWork $prePostWork
     *
     * @ORM\OneToOne(targetEntity="PrePostWork", mappedBy="phase")
     */
    protected $prePostWork;

    /**
     * @var ArrayCollection
     * @ORM\OneToMany(
     *     targetEntity="InCountryAssignment",
     *     mappedBy="specialistProjectPhase"
     * )
     * @ORM\OrderBy({"country" = "ASC", "city" = "ASC"})
     */
    protected $inCountryAssignments;

    /**
     * @var ArrayCollection
     * @ORM\OneToMany(
     *     targetEntity="VirtualAssignment",
     *     mappedBy="specialistProjectPhase"
     * )
     */
    protected $virtualAssignments;

    /**
     * @var ArrayCollection $dayBreakdownActivities
     *
     * @ORM\OneToMany(
     *     targetEntity="DayBreakdownActivity",
     *     mappedBy="specialistProjectPhase",
     *     cascade={"persist"}
     * )
     */
    protected $dayBreakdownActivities;
    
    /**
     * @var ArrayCollection $specialistProjectReviews
     *
     * @ORM\ManyToMany(targetEntity="ProjectReview", mappedBy="specialistProjects")
     */
    protected $specialistProjectReviews;

    /******************************** Methods ****************************/

    /**
     * @var SpecialistProjectPhaseBudget $budget
     *
     * @ORM\OneToOne(
     *     targetEntity="SpecialistProjectPhaseBudget",
     *     mappedBy="phase"
     * )
     */
    protected $budget;

    /******************************** Methods ****************************/

    /**
     * SpecialistProjectPhase constructor.
     *
     * @param SpecialistProject  $specialistProject
     * @param ProjectGeneralInfo $projectGeneralInfo
     */
    public function __construct(
      SpecialistProject $specialistProject,
      ProjectGeneralInfo $projectGeneralInfo
    ) {
        $this->specialistProject = $specialistProject;
        $this->projectGeneralInfo = $projectGeneralInfo;

        $this->dayBreakdownActivities = new ArrayCollection();
    }

    /**
     * @return array Type of choices.
     */
    public static function getTypeChoices()
    {
        return [
          'Entirely In-Country'                                => 'in-country',
          'Entirely Virtual'                                   => 'virtual',
          'Mixed (in-country component and virtual component)' => 'mixed',
        ];
    }

    /**
     * Set projectGeneralInfo
     *
     * @param ProjectGeneralInfo $projectGeneralInfo
     *
     * @return SpecialistProjectPhase
     */
    public function setProjectGeneralInfo(
      ProjectGeneralInfo $projectGeneralInfo = null
    ) {
        $this->projectGeneralInfo = $projectGeneralInfo;

        return $this;
    }

    /**
     * Get projectGeneralInfo
     *
     * @return ProjectGeneralInfo
     */
    public function getProjectGeneralInfo()
    {
        return $this->projectGeneralInfo;
    }

    /**
     * Set specialistProject
     *
     * @param SpecialistProject $specialistProject
     *
     * @return SpecialistProjectPhase
     */
    public function setSpecialistProject(
      SpecialistProject $specialistProject = null
    ) {
        $this->specialistProject = $specialistProject;

        return $this;
    }

    /**
     * Get specialistProject
     *
     * @return SpecialistProject
     */
    public function getSpecialistProject()
    {
        return $this->specialistProject;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return SpecialistProjectPhase
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set startDate
     *
     * @param \DateTime $startDate
     *
     * @return SpecialistProjectPhase
     */
    public function setStartDate($startDate)
    {
        $this->startDate = $startDate;

        return $this;
    }

    /**
     * Get startDate
     *
     * @return \DateTime
     */
    public function getStartDate()
    {
        return $this->startDate;
    }

    /**
     * Set endDate
     *
     * @param \DateTime $endDate
     *
     * @return SpecialistProjectPhase
     */
    public function setEndDate($endDate)
    {
        $this->endDate = $endDate;

        return $this;
    }

    /**
     * Get endDate
     *
     * @return \DateTime
     */
    public function getEndDate()
    {
        return $this->endDate;
    }

    /**
     * Set dateLengthDesc
     *
     * @param string $dateLengthDesc
     *
     * @return SpecialistProjectPhase
     */
    public function setDateLengthDesc($dateLengthDesc)
    {
        $this->dateLengthDesc = $dateLengthDesc;

        return $this;
    }

    /**
     * Get dateLengthDesc
     *
     * @return string
     */
    public function getDateLengthDesc()
    {
        return $this->dateLengthDesc;
    }

    /**
     * Set prePostWork
     *
     * @param PrePostWork $prePostWork
     *
     * @return SpecialistProjectPhase
     */
    public function setPrePostWork(PrePostWork $prePostWork = null)
    {
        $this->prePostWork = $prePostWork;

        return $this;
    }

    /**
     * Get prePostWork
     *
     * @return PrePostWork
     */
    public function getPrePostWork()
    {
        return $this->prePostWork;
    }

    /**
     * Add inCountryAssignment
     *
     * @param InCountryAssignment $inCountryAssignment
     *
     * @return SpecialistProjectPhase
     */
    public function addInCountryAssignment(
      InCountryAssignment $inCountryAssignment
    ) {
        $this->inCountryAssignments[] = $inCountryAssignment;

        return $this;
    }

    /**
     * Remove inCountryAssignment
     *
     * @param InCountryAssignment $inCountryAssignment
     */
    public function removeInCountryAssignment(
      InCountryAssignment $inCountryAssignment
    ) {
        $this->inCountryAssignments->removeElement($inCountryAssignment);
    }

    /**
     * Get inCountryAssignments
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getInCountryAssignments()
    {
        return $this->inCountryAssignments;
    }

    /**
     * Add dayBreakdownActivity
     *
     * @param DayBreakdownActivity $dayBreakdownActivity
     *
     * @return SpecialistProjectPhase
     */
    public function addDayBreakdownActivity(
      DayBreakdownActivity $dayBreakdownActivity
    ) {
        $this->dayBreakdownActivities[] = $dayBreakdownActivity;

        return $this;
    }

    /**
     * Remove dayBreakdownActivity
     *
     * @param DayBreakdownActivity $dayBreakdownActivity
     */
    public function removeDayBreakdownActivity(
      DayBreakdownActivity $dayBreakdownActivity
    ) {
        $this->dayBreakdownActivities->removeElement($dayBreakdownActivity);
    }

    /**
     * Get dayBreakdownActivities
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDayBreakdownActivities()
    {
        return $this->dayBreakdownActivities;
    }

    /**
     * Add virtualAssignment
     *
     * @param VirtualAssignment $virtualAssignment
     *
     * @return SpecialistProjectPhase
     */
    public function addVirtualAssignment(VirtualAssignment $virtualAssignment)
    {
        $this->virtualAssignments[] = $virtualAssignment;

        return $this;
    }

    /**
     * Remove virtualAssignment
     *
     * @param VirtualAssignment $virtualAssignment
     */
    public function removeVirtualAssignment(VirtualAssignment $virtualAssignment)
    {
        $this->virtualAssignments->removeElement($virtualAssignment);
    }

    /**
     * Get virtualAssignments
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getVirtualAssignments()
    {
        return $this->virtualAssignments;
    }

    /**
     * Set budget
     *
     * @param SpecialistProjectPhaseBudget $budget
     *
     * @return SpecialistProjectPhase
     */
    public function setBudget(SpecialistProjectPhaseBudget $budget = null)
    {
        $this->budget = $budget;

        return $this;
    }

    /**
     * Get budget
     *
     * @return SpecialistProjectPhaseBudget
     */
    public function getBudget()
    {
        return $this->budget;
    }
}
