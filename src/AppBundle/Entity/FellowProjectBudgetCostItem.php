<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * Class FellowProjectBudgetCostItem
 *
 * @package AppBundle\Entity
 *
 * @ORM\Entity
 * @ORM\Table(name="fellow_project_budget_cost_item")
 */
class FellowProjectBudgetCostItem
{
	/**
	 * @var integer $id
	 *
	 * @ORM\Column(type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	protected $id;

	/**
	 * @var integer $revision_id
	 *
	 * @ORM\Column(type="integer")
	 */
	protected $revision_id;

	/**
	 * @var float $full_cost
	 *
	 * @ORM\Column(
	 *     type="decimal",
	 *     precision=14,
	 *     scale=2,
	 *     nullable=true,
	 *     options={"default": 0}
	 * )
	 */
	protected $full_cost;

	/**
	 * @var float $post_contribution
	 *
	 * @ORM\Column(
	 *     type="decimal",
	 *     precision=14,
	 *     scale=2,
	 *     nullable=true,
	 *     options={"default": 0}
	 * )
	 */
	protected $post_contribution;

	/**
	 * @var float $host_contribution_monetary
	 *
	 * @ORM\Column(
	 *     type="decimal",
	 *     precision=14,
	 *     scale=2,
	 *     nullable=true,
	 *     options={"default": 0}
	 * )
	 */
	protected $host_contribution_monetary;

	/**
	 * @var float $host_contribution_in_kind
	 *
	 * @ORM\Column(
	 *     type="decimal",
	 *     precision=14,
	 *     scale=2,
	 *     nullable=true,
	 *     options={"default": 0}
	 * )
	 */
	protected $host_contribution_in_kind;

	/**
	 * @var float $post_host_total_contribution
	 *
	 * @ORM\Column(
	 *     type="decimal",
	 *     precision=14,
	 *     scale=2,
	 *     nullable=true,
	 *     options={"default": 0}
	 * )
	 */
	protected $post_host_total_contribution;

	/**
	 * @var float $post_total_contribution
	 *
	 * @ORM\Column(
	 *     type="decimal",
	 *     precision=14,
	 *     scale=2,
	 *     nullable=true,
	 *     options={"default": 0}
	 * )
	 */
	protected $post_total_contribution;

	/**
	 * @var float $eca_total_contribution
	 *
	 * @ORM\Column(
	 *     type="decimal",
	 *     precision=14,
	 *     scale=2,
	 *     nullable=true,
	 *     options={"default": 0}
	 * )
	 */
	protected $eca_total_contribution;

	/**
	 * @var \DateTime $created_at
	 *
	 * @ORM\Column(type="datetime")
	 * @Gedmo\Timestampable(on="create")
	 */
	protected $created_at;

	/**
	 * @var \AppBundle\Entity\FellowProjectBudget $fellowProjectBudget
	 *
	 * @ORM\ManyToOne(targetEntity="FellowProjectBudget", inversedBy="budgetCostItems")
	 * @ORM\JoinColumn(
	 *     name="fellow_project_budget_id",
	 *     referencedColumnName="id",
	 *     onDelete="cascade"
	 * )
	 */
	protected $fellowProjectBudget;

	/**
	 * TODO: Custom cost items should be removed if the budget is removed
	 * @var \AppBundle\Entity\CostItemOneTime $costItem
	 *
	 * @ORM\ManyToOne(targetEntity="CostItem",
	 *	cascade = {"persist"}
	 * )
	 */
	protected $costItem;

	/***************************** Methods definitions *******************************/
	
	/**
	 * Get id
	 *
	 * @return integer
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * Set revisionId
	 *
	 * @param integer $revisionId
	 *
	 * @return FellowProjectBudgetCostItem
	 */
	public function setRevisionId($revisionId)
	{
		$this->revision_id = $revisionId;

		return $this;
	}

	/**
	 * Get revisionId
	 *
	 * @return integer
	 */
	public function getRevisionId()
	{
		return $this->revision_id;
	}

	/**
	 * Set fullCost
	 *
	 * @param string $fullCost
	 *
	 * @return FellowProjectBudgetCostItem
	 */
	public function setFullCost($fullCost)
	{
		$this->full_cost = $fullCost;

		return $this;
	}

	/**
	 * Get fullCost
	 *
	 * @return string
	 */
	public function getFullCost()
	{
		return $this->full_cost;
	}

	/**
	 * Set postContribution
	 *
	 * @param string $postContribution
	 *
	 * @return FellowProjectBudgetCostItem
	 */
	public function setPostContribution($postContribution)
	{
		$this->post_contribution = $postContribution;

		return $this;
	}

	/**
	 * Get postContribution
	 *
	 * @return string
	 */
	public function getPostContribution()
	{
		return $this->post_contribution;
	}

	/**
	 * Set hostContributionMonetary
	 *
	 * @param string $hostContributionMonetary
	 *
	 * @return FellowProjectBudgetCostItem
	 */
	public function setHostContributionMonetary($hostContributionMonetary)
	{
		$this->host_contribution_monetary = $hostContributionMonetary;

		return $this;
	}

	/**
	 * Get hostContributionMonetary
	 *
	 * @return string
	 */
	public function getHostContributionMonetary()
	{
		return $this->host_contribution_monetary;
	}

	/**
	 * Set hostContributionInKind
	 *
	 * @param string $hostContributionInKind
	 *
	 * @return FellowProjectBudgetCostItem
	 */
	public function setHostContributionInKind($hostContributionInKind)
	{
		$this->host_contribution_in_kind = $hostContributionInKind;

		return $this;
	}

	/**
	 * Get hostContributionInKind
	 *
	 * @return string
	 */
	public function getHostContributionInKind()
	{
		return $this->host_contribution_in_kind;
	}

	/**
	 * Set postHostTotalContribution
	 *
	 * @param string $postHostTotalContribution
	 *
	 * @return FellowProjectBudgetCostItem
	 */
	public function setPostHostTotalContribution($postHostTotalContribution)
	{
		$this->post_host_total_contribution = $postHostTotalContribution;

		return $this;
	}

	/**
	 * Get postHostTotalContribution
	 *
	 * @return string
	 */
	public function getPostHostTotalContribution()
	{
		return $this->post_host_total_contribution;
	}

	/**
	 * Set postTotalContribution
	 *
	 * @param string $postTotalContribution
	 *
	 * @return FellowProjectBudgetCostItem
	 */
	public function setPostTotalContribution($postTotalContribution)
	{
		$this->post_total_contribution = $postTotalContribution;

		return $this;
	}

	/**
	 * Get postTotalContribution
	 *
	 * @return string
	 */
	public function getPostTotalContribution()
	{
		return $this->post_total_contribution;
	}

	/**
	 * Set ecaTotalContribution
	 *
	 * @param string $ecaTotalContribution
	 *
	 * @return FellowProjectBudgetCostItem
	 */
	public function setEcaTotalContribution($ecaTotalContribution)
	{
		$this->eca_total_contribution = $ecaTotalContribution;

		return $this;
	}

	/**
	 * Get ecaTotalContribution
	 *
	 * @return string
	 */
	public function getEcaTotalContribution()
	{
		return $this->eca_total_contribution;
	}

	/**
	 * Set createdAt
	 *
	 * @param \DateTime $createdAt
	 *
	 * @return FellowProjectBudgetCostItem
	 */
	public function setCreatedAt($createdAt)
	{
		$this->created_at = $createdAt;

		return $this;
	}

	/**
	 * Get createdAt
	 *
	 * @return \DateTime
	 */
	public function getCreatedAt()
	{
		return $this->created_at;
	}

	/**
	 * Set fellowProjectBudget
	 *
	 * @param \AppBundle\Entity\FellowProjectBudget $fellowProjectBudget
	 *
	 * @return FellowProjectBudgetCostItem
	 */
	public function setFellowProjectBudget(FellowProjectBudget $fellowProjectBudget = null)
	{
		$this->fellowProjectBudget = $fellowProjectBudget;

		return $this;
	}

	/**
	 * Get fellowProjectBudget
	 *
	 * @return \AppBundle\Entity\FellowProjectBudget
	 */
	public function getFellowProjectBudget()
	{
		return $this->fellowProjectBudget;
	}

	/**
	 * Set costItem
	 *
	 * @param \AppBundle\Entity\CostItem $costItem
	 *
	 * @return FellowProjectBudgetCostItem
	 */
	public function setCostItem(CostItem $costItem = null)
	{
		$this->costItem = $costItem;

		return $this;
	}

	/**
	 * Get costItem
	 *
	 * @return \AppBundle\Entity\CostItem
	 */
	public function getCostItem()
	{
		return $this->costItem;
	}
}
