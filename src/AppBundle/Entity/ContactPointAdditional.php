<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class ContactPointAdditional
 *
 * @package AppBundle\Entity
 * @author  Juan Obando <juan.obando@inqbation.com>
 *
 * @ORM\Entity()
 */
class ContactPointAdditional extends ContactPoint
{
    /**
     * Get the type
     *
     * @return int
     */
    public function getType()
    {
        return self::ADDITIONAL_TYPE;
    }
}
