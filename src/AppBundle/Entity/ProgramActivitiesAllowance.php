<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * ProgramActivitiesAllowance entity.
 *
 * @package AppBundle\Entity
 * @author  Juan Obando <juan.obando@inqbation.com>
 *
 * @ORM\Entity()
 * @ORM\Table(name="program_activities_allowance")
 */
class ProgramActivitiesAllowance extends BaseEntity
{
    /**
     * @var SpecialistProjectPhaseBudget $phaseBudget
     *
     * @ORM\OneToOne(
     *     targetEntity="SpecialistProjectPhaseBudget",
     *     inversedBy="programActivitiesAllowance"
     * )
     */
    protected $phaseBudget;

    /**
     * @var ArrayCollection $programActivitiesAllowanceCostItems
     *
     * @ORM\OneToMany(
     *     targetEntity="ProgramActivitiesAllowanceCostItem",
     *     mappedBy="programActivitiesAllowance",
     *     cascade={"persist", "remove"}
     * )
     */
    protected $programActivitiesAllowanceCostItems;

    /**
     * ProgramActivitiesAllowance constructor.
     *
     * @param SpecialistProjectPhaseBudget $phaseBudget
     */
    public function __construct(SpecialistProjectPhaseBudget $phaseBudget)
    {
        $this->phaseBudget = $phaseBudget;

        $this->programActivitiesAllowanceCostItems = new ArrayCollection();
    }

    /**
     *
     */
    public function calculateSummaryTotals() {
        $summaryTotals = [
          'cost'          => 0,
          'post'          => 0,
          'hostMonetary'  => 0,
          'hostInKind'    => 0,
          'postHostTotal' => 0,
          'postTotal'     => 0,
          'ecaTotal'      => 0,
        ];

        /** @var ProgramActivitiesAllowanceCostItem $paaCostItem */
        foreach ($this->programActivitiesAllowanceCostItems as $paaCostItem) {
            $summaryTotals['cost'] += $paaCostItem->getTotalCost();
            $summaryTotals['post'] += $paaCostItem->getPostContribution();
            $summaryTotals['hostMonetary'] += $paaCostItem->getHostContributionMonetary();
            $summaryTotals['hostInKind'] += $paaCostItem->getHostContributionInKind();
            $summaryTotals['postHostTotal'] += $paaCostItem->getPostHostTotalContribution();
            $summaryTotals['postTotal'] += $paaCostItem->getPostTotalContribution();
            $summaryTotals['ecaTotal'] += $paaCostItem->getEcaTotalContribution();
        }

        return $summaryTotals;
    }

    /**
     * Set phaseBudget
     *
     * @param SpecialistProjectPhaseBudget $phaseBudget
     *
     * @return ProgramActivitiesAllowance
     */
    public function setPhaseBudget(
      SpecialistProjectPhaseBudget $phaseBudget = null
    ) {
        $this->phaseBudget = $phaseBudget;

        return $this;
    }

    /**
     * Get phaseBudget
     *
     * @return SpecialistProjectPhaseBudget
     */
    public function getPhaseBudget()
    {
        return $this->phaseBudget;
    }

    /**
     * Add programActivitiesAllowanceCostItem
     *
     * @param ProgramActivitiesAllowanceCostItem $programActivitiesAllowanceCostItem
     *
     * @return ProgramActivitiesAllowance
     */
    public function addProgramActivitiesAllowanceCostItem(
      ProgramActivitiesAllowanceCostItem $programActivitiesAllowanceCostItem
    ) {
        $this->programActivitiesAllowanceCostItems[] = $programActivitiesAllowanceCostItem;

        return $this;
    }

    /**
     * Remove programActivitiesAllowanceCostItem
     *
     * @param ProgramActivitiesAllowanceCostItem $programActivitiesAllowanceCostItem
     */
    public function removeProgramActivitiesAllowanceCostItem(
      ProgramActivitiesAllowanceCostItem $programActivitiesAllowanceCostItem
    ) {
        $this->programActivitiesAllowanceCostItems->removeElement(
          $programActivitiesAllowanceCostItem
        );
    }

    /**
     * Get programActivitiesAllowanceCostItems
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getProgramActivitiesAllowanceCostItems()
    {
        return $this->programActivitiesAllowanceCostItems;
    }
}
