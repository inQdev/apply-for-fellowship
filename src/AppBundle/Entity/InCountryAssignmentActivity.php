<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use AppBundle\Entity\BaseEntity;

/**
 * InCountryAssignmentActivity
 *
 * @author Felipe Ceballos <felipe.ceballos@inqbation.com>
 *
 * @ORM\Entity()
 * @ORM\Table(name="in_country_assignment_activity")
 */
class InCountryAssignmentActivity extends BaseEntity
{

	/**
	 * @var string
	 *
	 * @ORM\Column(type="text", nullable=true)
	 */
	protected $activityDesc;

	/**
	 * @var string
	 *
	 * @ORM\Column(type="string", length=255, nullable=true)
	 */
	protected $topic;

	/**
	 * @var string
	 *
	 * @ORM\Column(type="string", length=255, nullable=true)
	 */
	protected $audienceDesc;

	/**
	 * @var int
	 *
	 * @ORM\Column(type="smallint", nullable=true)
	 */
	protected $participants;

	/**
	 * @var InCountryAssignment $inCountryAssignment
	 *
	 * @ORM\ManyToOne(
	 *     targetEntity="InCountryAssignment",
	 *     inversedBy="assignmentActivities"
	 * )
	 */
	protected $inCountryAssignment;

	/*********************** Methods ***************************/

    /**
     * Set activityDesc
     *
     * @param string $activityDesc
     *
     * @return InCountryAssignmentActivity
     */
    public function setActivityDesc($activityDesc)
    {
        $this->activityDesc = $activityDesc;

        return $this;
    }

    /**
     * Get activityDesc
     *
     * @return string
     */
    public function getActivityDesc()
    {
        return $this->activityDesc;
    }

    /**
     * Set topic
     *
     * @param string $topic
     *
     * @return InCountryAssignmentActivity
     */
    public function setTopic($topic)
    {
        $this->topic = $topic;

        return $this;
    }

    /**
     * Get topic
     *
     * @return string
     */
    public function getTopic()
    {
        return $this->topic;
    }

    /**
     * Set audienceDesc
     *
     * @param string $audienceDesc
     *
     * @return InCountryAssignmentActivity
     */
    public function setAudienceDesc($audienceDesc)
    {
        $this->audienceDesc = $audienceDesc;

        return $this;
    }

    /**
     * Get audienceDesc
     *
     * @return string
     */
    public function getAudienceDesc()
    {
        return $this->audienceDesc;
    }

    /**
     * Set participants
     *
     * @param integer $participants
     *
     * @return InCountryAssignmentActivity
     */
    public function setParticipants($participants)
    {
        $this->participants = $participants;

        return $this;
    }

    /**
     * Get participants
     *
     * @return integer
     */
    public function getParticipants()
    {
        return $this->participants;
    }

    /**
     * Set inCountryAssignment
     *
     * @param integer $inCountryAssignment
     *
     * @return InCountryAssignmentActivity
     */
    public function setInCountryAssignment($inCountryAssignment)
    {
        $this->inCountryAssignment = $inCountryAssignment;

        return $this;
    }

    /**
     * Get inCountryAssignment
     *
     * @return integer
     */
    public function getInCountryAssignment()
    {
        return $this->inCountryAssignment;
    }
}
