<?php

namespace AppBundle\Entity;

use AppBundle\AppBundle;
use Doctrine\ORM\Mapping as ORM;
use AppBundle\Entity\BaseEntity;

/**
 * DayBreakdownActivity
 * @author Felipe Ceballos <felipe.ceballos@inqbation.com>
 *
 * @ORM\Entity()
 * @ORM\Table(name="day_breakdown_activity")
 */
class DayBreakdownActivity extends BaseEntity
{

	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(type="date")
	 */
	protected $activityDate;

	/**
	 * @var \AppBundle\Entity\DayBreakdownActivityType $dayBreakdownActivityType
	 *
	 * @ORM\ManyToOne(targetEntity="DayBreakdownActivityType")
	 */
	protected $dayBreakdownActivityType;

	/**
	 * @var InCountryAssignment $inCountryAssignment
	 *
	 * @ORM\ManyToOne(
	 *     targetEntity="InCountryAssignment",
	 *     inversedBy="dayBreakdownActivities"
	 * )
	 */
	protected $inCountryAssignment;


	/**
	 * @var SpecialistProjectPhase $specialistProjectPhase
	 *
	 * @ORM\ManyToOne(
	 *     targetEntity="SpecialistProjectPhase",
	 *     inversedBy="dayBreakdownActivities"
	 * )
	 */
	protected $specialistProjectPhase;

	/*********************** Methods *****************************/

	/**
	 * Set activityDate
	 *
	 * @param \DateTime $activityDate
	 *
	 * @return DayBreakdownActivity
	 */
	public function setActivityDate($activityDate)
	{
		$this->activityDate = $activityDate;

		return $this;
	}

	/**
	 * Get activityDate
	 *
	 * @return \DateTime
	 */
	public function getActivityDate()
	{
		return $this->activityDate;
	}

	/**
	 * Set setDayBreakdownActivityType
	 *
	 * @param integer $dayBreakdownActivityType
	 *
	 * @return DayBreakdownActivity
	 */
	public function setDayBreakdownActivityType($dayBreakdownActivityType)
	{
		$this->dayBreakdownActivityType = $dayBreakdownActivityType;

		return $this;
	}

	/**
	 * Get getDayBreakdownActivityType
	 *
	 * @return \AppBundle\Entity\DayBreakdownActivityType
	 */
	public function getDayBreakdownActivityType()
	{
		return $this->dayBreakdownActivityType;
	}

	/**
	 * Set inCountryAssignment
	 *
	 * @param integer $inCountryAssignment
	 *
	 * @return DayBreakdownActivity
	 */
	public function setInCountryAssignment($inCountryAssignment)
	{
		$this->inCountryAssignment = $inCountryAssignment;

		return $this;
	}

	/**
	 * Get inCountryAssignment
	 *
	 * @return \AppBundle\Entity\InCountryAssignment
	 */
	public function getInCountryAssignment()
	{
		return $this->inCountryAssignment;
	}

    /**
     * Set specialistProjectPhase
     *
     * @param \AppBundle\Entity\SpecialistProjectPhase $specialistProjectPhase
     *
     * @return DayBreakdownActivity
     */
    public function setSpecialistProjectPhase(\AppBundle\Entity\SpecialistProjectPhase $specialistProjectPhase = null)
    {
        $this->specialistProjectPhase = $specialistProjectPhase;

        return $this;
    }

    /**
     * Get specialistProjectPhase
     *
     * @return \AppBundle\Entity\SpecialistProjectPhase
     */
    public function getSpecialistProjectPhase()
    {
        return $this->specialistProjectPhase;
    }
}
