<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class CostItemCustomInKind
 *
 * @package AppBundle\Entity
 *
 * @ORM\Entity
 */
class CostItemCustomInKind extends CostItem
{

	public function __construct()
	{
		$this->description = '';
	}
}