<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * ContributionTransportation entity.
 *
 * @package AppBundle\Entity
 * @author  Juan Obando <juan.obando@inqbation.com>
 *
 * @ORM\Entity()
 * @ORM\Table(name="contribution_transportation")
 */
class ContributionTransportation extends BaseEntity
{
    /**
     * @var SpecialistProjectPhaseBudget $phaseBudget
     *
     * @ORM\OneToOne(
     *     targetEntity="SpecialistProjectPhaseBudget",
     *     inversedBy="contributionTransportation"
     * )
     */
    protected $phaseBudget;

    /**
     * @var ArrayCollection $contributionTransportationCostItems
     *
     * @ORM\OneToMany(
     *     targetEntity="ContributionTransportationCostItem",
     *     mappedBy="contributionTransportation",
     *     cascade={"persist"}
     * )
     * @ORM\OrderBy({"createdAt"="ASC"})
     */
    protected $contributionTransportationCostItems;

    /**
     * ContributionTransportation constructor.
     *
     * @param SpecialistProjectPhaseBudget $phaseBudget
     */
    public function __construct(SpecialistProjectPhaseBudget $phaseBudget)
    {
        $this->phaseBudget = $phaseBudget;

        $this->contributionTransportationCostItems = new ArrayCollection();
    }

    /**
     *
     */
    public function calculateSummaryTotals() {
        $summaryTotals = [
          'cost'          => 0,
          'post'          => 0,
          'hostMonetary'  => 0,
          'hostInKind'    => 0,
          'postHostTotal' => 0,
          'postTotal'     => 0,
          'ecaTotal'      => 0,
        ];

        /** @var ContributionTransportationCostItem $costItem */
        foreach ($this->contributionTransportationCostItems as $costItem) {
            $summaryTotals['cost'] += $costItem->getTotalCost();
            $summaryTotals['post'] += $costItem->getPostContribution();
            $summaryTotals['hostMonetary'] += $costItem->getHostContributionMonetary();
            $summaryTotals['hostInKind'] += $costItem->getHostContributionInKind();
            $summaryTotals['postHostTotal'] += $costItem->getPostHostTotalContribution();
            $summaryTotals['postTotal'] += $costItem->getPostTotalContribution();
            $summaryTotals['ecaTotal'] += $costItem->getEcaTotalContribution();
        }

        return $summaryTotals;
    }

    /**
     * Set phaseBudget
     *
     * @param SpecialistProjectPhaseBudget $phaseBudget
     *
     * @return ContributionTransportation
     */
    public function setPhaseBudget(
      SpecialistProjectPhaseBudget $phaseBudget = null
    ) {
        $this->phaseBudget = $phaseBudget;

        return $this;
    }

    /**
     * Get phaseBudget
     *
     * @return SpecialistProjectPhaseBudget
     */
    public function getPhaseBudget()
    {
        return $this->phaseBudget;
    }

    /**
     * Add contributionTransportationCostItem
     *
     * @param ContributionTransportationCostItem $contributionTransportationCostItem
     *
     * @return ContributionTransportation
     */
    public function addContributionTransportationCostItem(
      ContributionTransportationCostItem $contributionTransportationCostItem
    ) {
        $this->contributionTransportationCostItems[] = $contributionTransportationCostItem;

        return $this;
    }

    /**
     * Remove contributionTransportationCostItem
     *
     * @param ContributionTransportationCostItem $contributionTransportationCostItem
     */
    public function removeContributionTransportationCostItem(
      ContributionTransportationCostItem $contributionTransportationCostItem
    ) {
        $this->contributionTransportationCostItems->removeElement(
          $contributionTransportationCostItem
        );
    }

    /**
     * Get contributionTransportationCostItems
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getContributionTransportationCostItems()
    {
        return $this->contributionTransportationCostItems;
    }
}
