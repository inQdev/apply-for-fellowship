<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Consecutive
 *
 * @ORM\Table(name="sequence")
 * @ORM\Entity()
 */
class Sequence
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=100, unique=true)
     */
    private $name;

    /**
     * @var int
     *
     * @ORM\Column(name="nextValue", type="integer")
     */
    private $nextValue;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Consecutive
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set nextValue
     *
     * @param integer $nextValue
     *
     * @return Consecutive
     */
    public function setNextValue($nextValue)
    {
        $this->nextValue = $nextValue;

        return $this;
    }

    /**
     * Get nextValue
     *
     * @return int
     */
    public function getNextValue()
    {
        return $this->nextValue;
    }
}

