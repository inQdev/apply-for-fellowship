<?php

namespace AppBundle\Entity\Fellow;

use AppBundle\Entity\FellowProject;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * DutyActivityPrimary entity.
 *
 * @package AppBundle\Entity\Fellow
 * @author  Juan Obando <juan.obando@inqbation.com>
 *
 * @ORM\Entity()
 */
class DutyActivityPrimary extends AbstractDutyActivity
{
    /**
     * @var ArrayCollection $activitiesFocus
     *
     * @ORM\ManyToMany(targetEntity="DutyActivityFocus")
     * @ORM\JoinTable(name="fellowship_duties_primary_activities_focus")
     */
    protected $activitiesFocus;

    /**
     * @var FellowProject $fellowProject
     *
     * @ORM\ManyToOne(
     *     targetEntity="AppBundle\Entity\FellowProject",
     *     inversedBy="dutyPrimaryActivities"
     * )
     * @ORM\JoinColumn(
     *     name="fellow_project_id",
     *     referencedColumnName="id",
     *     onDelete="cascade"
     * )
     */
    protected $fellowProject;

    /******************************* Methods Definitions ************************/

    /**
     * DutyActivityPrimary constructor.
     *
     * @param FellowProject $fellowProject
     */
    public function __construct(FellowProject $fellowProject)
    {
        parent::__construct($fellowProject);

        $this->activitiesFocus = new ArrayCollection();
    }

    /**
     * Add activitiesFocus
     *
     * @param DutyActivityFocus $activitiesFocus
     *
     * @return DutyActivityPrimary
     */
    public function addActivitiesFocus(DutyActivityFocus $activitiesFocus)
    {
        $this->activitiesFocus[] = $activitiesFocus;

        return $this;
    }

    /**
     * Remove activitiesFocus
     *
     * @param DutyActivityFocus $activitiesFocus
     */
    public function removeActivitiesFocus(DutyActivityFocus $activitiesFocus)
    {
        $this->activitiesFocus->removeElement($activitiesFocus);
    }

    /**
     * Get activitiesFocus
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getActivitiesFocus()
    {
        return $this->activitiesFocus;
    }
}
