<?php

namespace AppBundle\Entity\Fellow;

use AppBundle\Entity\BaseTerm;
use Doctrine\ORM\Mapping as ORM;

/**
 * DutyActivityFocus entity.
 *
 * @package AppBundle\Entity\Fellow
 * @author  Juan Obando <juan.obando@inqbation.com>
 *
 * @ORM\Entity()
 */
class DutyActivityFocus extends BaseTerm
{
}
