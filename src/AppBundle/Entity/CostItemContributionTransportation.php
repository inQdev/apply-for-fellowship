<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CostItemContributionTransportation entity.
 *
 * @package AppBundle\Entity
 * @author  Juan Obando <juan.obando@inqbation.com>
 *
 * @ORM\Entity()
 */
class CostItemContributionTransportation extends CostItem
{
}
