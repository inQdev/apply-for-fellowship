<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Host entity.
 *
 * @package AppBundle\Entity
 * @author  Juan Obando <juan.obando@inqbation.com>
 *
 * @ORM\Entity()
 * @ORM\Table(name="host")
 */
class Host extends BaseSingleEntity
{
    /**
     * @var Country $country
     *
     * @ORM\ManyToOne(targetEntity="Country", inversedBy="hosts")
     */
    protected $country;

    /**
     * Set country
     *
     * @param Country $country
     *
     * @return Host
     */
    public function setCountry(Country $country = null)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Get country
     *
     * @return Country
     */
    public function getCountry()
    {
        return $this->country;
    }
}
