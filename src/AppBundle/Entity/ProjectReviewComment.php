<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ProjectReviewComment
 * @author Felipe Ceballos <felipe.ceballos@inqbation.com>
 *
 * @ORM\Table(name="project_review_comment")
 * @ORM\Entity()
 */
class ProjectReviewComment extends BaseEntity
{

	/**
	 * @var ProjectReview
	 *
	 * @ORM\ManyToOne(
	 *     targetEntity="ProjectReview",
	 *     inversedBy="projectReviewComments"
	 * )
	 * @ORM\JoinColumn(
	 *     name="project_review_id",
	 *     referencedColumnName="id",
	 *     onDelete="cascade"
	 * )
	 */
	protected $projectReview;

	/**
	 * @var $actionTaken
	 *
	 * @ORM\Column(type="string", length=40)
	 */
	protected $actionTaken;

	/**
	 * @var string
	 *
	 * @ORM\Column(type="text", nullable=true)
	 */
	protected $comment;

	/**
	 * @var User
	 *
	 * @ORM\ManyToOne(
	 *     targetEntity="user",
	 *     inversedBy="projectReviewComments"
	 * )
	 */
	protected $commentBy;

	/**
	 * @var string
	 *
	 * @ORM\Column(type="string", length=40)
	 */
	protected $commentByRole;


	/************************ Methods ************************/


	/**
	 * Set comment
	 *
	 * @param string $comment
	 *
	 * @return ProjectReviewComment
	 */
	public function setComment($comment)
	{
		$this->comment = $comment;

		return $this;
	}

	/**
	 * Get comment
	 *
	 * @return string
	 */
	public function getComment()
	{
		return $this->comment;
	}

	/**
	 * Set commentByRole
	 *
	 * @param string $commentByRole
	 *
	 * @return ProjectReviewComment
	 */
	public function setCommentByRole($commentByRole)
	{
		$this->commentByRole = $commentByRole;

		return $this;
	}

	/**
	 * Get commentByRole
	 *
	 * @return string
	 */
	public function getCommentByRole()
	{
		return $this->commentByRole;
	}


    /**
     * Set projectReview
     *
     * @param \AppBundle\Entity\ProjectReview $projectReview
     *
     * @return ProjectReviewComment
     */
    public function setProjectReview(\AppBundle\Entity\ProjectReview $projectReview = null)
    {
        $this->projectReview = $projectReview;

        return $this;
    }

    /**
     * Get projectReview
     *
     * @return \AppBundle\Entity\ProjectReview
     */
    public function getProjectReview()
    {
        return $this->projectReview;
    }

    /**
     * Set actionTaken
     *
     * @param string
     *
     * @return ProjectReviewComment
     */
    public function setActionTaken($actionTaken)
    {
        $this->actionTaken = $actionTaken;

        return $this;
    }

    /**
     * Get actionTaken
     *
     * @return string
     */
    public function getActionTaken()
    {
        return $this->actionTaken;
    }

    /**
     * Set commentBy
     *
     * @param \AppBundle\Entity\user $commentBy
     *
     * @return ProjectReviewComment
     */
    public function setCommentBy(\AppBundle\Entity\user $commentBy = null)
    {
        $this->commentBy = $commentBy;

        return $this;
    }

    /**
     * Get commentBy
     *
     * @return \AppBundle\Entity\user
     */
    public function getCommentBy()
    {
        return $this->commentBy;
    }
}
