<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Country entity.
 *
 * @package AppBundle\Entity
 * @author  Juan Obando <juan.obando@inqbation.com>
 * @author  Felipe Ceballos <felipe.ceballos@inqbation.com>
 *
 * @ORM\Entity()
 * @ORM\Table(name="country")
 */
class Country extends BaseSingleEntity
{
    /**
     * @var string $iso2Code
     *
     * @ORM\Column(
     *     name="iso2_code",
     *     type="string",
     *     length=2,
     *     unique=true
     * )
     * @Assert\Country()
     */
    protected $iso2Code;

    /**
     * @var \AppBundle\Entity\Region $region
     *
     * @ORM\ManyToOne(
     *     targetEntity="Region",
     *     inversedBy="countries"
     * )
     */
    protected $region;

    /**
     * @var ArrayCollection $posts
     *
     * @ORM\OneToMany(
     *     targetEntity="Post",
     *     mappedBy="country"
     * )
     */
    protected $posts;

    /**
     * @var ArrayCollection $hosts
     *
     * @ORM\OneToMany(
     *     targetEntity="Host",
     *     mappedBy="country"
     * )
     */
    protected $hosts;

    /**
     * @var ArrayCollection $reloLocations
     *
     * @ORM\ManyToMany(
     *     targetEntity="ReloLocation",
     *     mappedBy="countries"
     * )
     * @ORM\OrderBy({"createdAt"="DESC"})
     */
    protected $reloLocations;

    /**
     * @var ArrayCollection $projects
     *
     * @ORM\ManyToMany(
     *     targetEntity="ProjectGeneralInfo",
     *     mappedBy="countries"
     * )
     * @ORM\OrderBy({"createdAt"="DESC"})
     */
    protected $projects;

    /**
     * @var ArrayCollection $embassies
     *
     * @ORM\ManyToMany(targetEntity="User", mappedBy="countries")
     */
    protected $embassies;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="InCountryAssignment", mappedBy="country")
     */
    protected $inCountryAssignments;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="VirtualAssignment", mappedBy="country")
     */
    protected $virtualAssignments;

    /*************************** Methods *******************************/

    /**
     * Country constructor.
     */
    public function __construct()
    {
        $this->posts = new ArrayCollection();
        $this->hosts = new ArrayCollection();
        $this->reloLocations = new ArrayCollection();
        $this->projects = new ArrayCollection();
    }

    /**
     * Set iso2Code
     *
     * @param string $iso2Code
     *
     * @return Country
     */
    public function setIso2Code($iso2Code)
    {
        $this->iso2Code = $iso2Code;

        return $this;
    }

    /**
     * Get iso2Code
     *
     * @return string
     */
    public function getIso2Code()
    {
        return $this->iso2Code;
    }

    /**
     * Set region
     *
     * @param Region $region
     *
     * @return Country
     */
    public function setRegion(Region $region = null)
    {
        $this->region = $region;

        return $this;
    }

    /**
     * Get region
     *
     * @return Region
     */
    public function getRegion()
    {
        return $this->region;
    }

    /**
     * Add post
     *
     * @param Post $post
     *
     * @return Country
     */
    public function addPost(Post $post)
    {
        $this->posts[] = $post;

        return $this;
    }

    /**
     * Remove post
     *
     * @param Post $post
     */
    public function removePost(Post $post)
    {
        $this->posts->removeElement($post);
    }

    /**
     * Get posts
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPosts()
    {
        return $this->posts;
    }

    /**
     * Add host
     *
     * @param Host $host
     *
     * @return Country
     */
    public function addHost(Host $host)
    {
        $this->hosts[] = $host;

        return $this;
    }

    /**
     * Remove host
     *
     * @param \AppBundle\Entity\Host $host
     */
    public function removeHost(Host $host)
    {
        $this->hosts->removeElement($host);
    }

    /**
     * Get hosts
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getHosts()
    {
        return $this->hosts;
    }

    /**
     * Add reloLocation
     *
     * @param ReloLocation $reloLocation
     *
     * @return Country
     */
    public function addReloLocation(ReloLocation $reloLocation)
    {
        $this->reloLocations[] = $reloLocation;

        return $this;
    }

    /**
     * Remove reloLocation
     *
     * @param ReloLocation $reloLocation
     */
    public function removeReloLocation(ReloLocation $reloLocation)
    {
        $this->reloLocations->removeElement($reloLocation);
    }

    /**
     * Get reloLocations
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getReloLocations()
    {
        return $this->reloLocations;
    }

    /**
     * Add project
     *
     * @param ProjectGeneralInfo $project
     *
     * @return Country
     */
    public function addProject(ProjectGeneralInfo $project)
    {
        $this->projects[] = $project;

        return $this;
    }

    /**
     * Remove project
     *
     * @param ProjectGeneralInfo $project
     */
    public function removeProject(ProjectGeneralInfo $project)
    {
        $this->projects->removeElement($project);
    }

    /**
     * Get projects
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getProjects()
    {
        return $this->projects;
    }

    /**
     * Add embassy
     *
     * @param User $embassy
     *
     * @return Country
     */
    public function addEmbassy(User $embassy)
    {
        $this->embassies[] = $embassy;

        return $this;
    }

    /**
     * Remove embassy
     *
     * @param User $embassy
     */
    public function removeEmbassy(User $embassy)
    {
        $this->embassies->removeElement($embassy);
    }

    /**
     * Get embassies
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getEmbassies()
    {
        return $this->embassies;
    }

    /**
     * Add inCountryAssignment
     *
     * @param \AppBundle\Entity\InCountryAssignment $inCountryAssignment
     *
     * @return Country
     */
    public function addInCountryAssignment(\AppBundle\Entity\InCountryAssignment $inCountryAssignment)
    {
        $this->inCountryAssignments[] = $inCountryAssignment;

        return $this;
    }

    /**
     * Remove inCountryAssignment
     *
     * @param \AppBundle\Entity\InCountryAssignment $inCountryAssignment
     */
    public function removeInCountryAssignment(\AppBundle\Entity\InCountryAssignment $inCountryAssignment)
    {
        $this->inCountryAssignments->removeElement($inCountryAssignment);
    }

    /**
     * Get inCountryAssignments
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getInCountryAssignments()
    {
        return $this->inCountryAssignments;
    }

    /**
     * Add virtualAssignment
     *
     * @param \AppBundle\Entity\VirtualAssignment $virtualAssignment
     *
     * @return Country
     */
    public function addVirtualAssignment(\AppBundle\Entity\VirtualAssignment $virtualAssignment)
    {
        $this->virtualAssignments[] = $virtualAssignment;

        return $this;
    }

    /**
     * Remove virtualAssignment
     *
     * @param \AppBundle\Entity\VirtualAssignment $virtualAssignment
     */
    public function removeVirtualAssignment(\AppBundle\Entity\VirtualAssignment $virtualAssignment)
    {
        $this->virtualAssignments->removeElement($virtualAssignment);
    }

    /**
     * Get virtualAssignments
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getVirtualAssignments()
    {
        return $this->virtualAssignments;
    }
}
