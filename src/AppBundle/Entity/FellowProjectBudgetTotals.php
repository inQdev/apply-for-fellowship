<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * FellowProjectBudgetTotals
 *
 * @ORM\Table(name="fellow_project_budget_totals")
 * @ORM\Entity()
 */
class FellowProjectBudgetTotals extends BaseEntity
{

	/**
	 * @var FellowProjectBudget
	 *
	 * @ORM\ManyToOne(
	 *     targetEntity="FellowProjectBudget",
	 *     inversedBy="fellowProjectBudgetTotals",
	 * )
	 * @ORM\JoinColumn(
	 *     name="fellow_project_budget_id",
	 *     referencedColumnName="id",
	 *     onDelete="cascade"
	 * )
	 */
	private $fellowProjectBudget;

	/**
	 * @var int
	 *
	 * @ORM\Column(type="integer")
	 */
	private $revisionId;

	/**
	 * @var string
	 *
	 * @ORM\Column(type="decimal", precision=10, scale=2)
	 */
	private $fullCost;

	/**
	 * @var string
	 *
	 * @ORM\Column(type="decimal", precision=10, scale=2)
	 */
	private $postContribution;

	/**
	 * @var string
	 *
	 * @ORM\Column(type="decimal", precision=10, scale=2)
	 */
	private $hostContributionMonetary;

	/**
	 * @var string
	 *
	 * @ORM\Column(type="decimal", precision=10, scale=2)
	 */
	private $hostContributionInKind;

	/**
	 * @var string
	 *
	 * @ORM\Column(type="decimal", precision=10, scale=2)
	 */
	private $postTotalContribution;

	/**
	 * @var string
	 *
	 * @ORM\Column(type="decimal", precision=10, scale=2)
	 */
	private $ecaTotalContribution;

	/**
	 * @var string
	 *
	 * @ORM\Column(type="decimal", precision=10, scale=2)
	 */
	private $global;

	/**
	 * @var string
	 *
	 * @ORM\Column(type="decimal", precision=10, scale=2)
	 */
	private $total;

	/********************** Methods Definitions *******************************/

	/**
	 * Set fellowProjectBudget
	 *
	 * @param FellowProjectBudget $fellowProjectBudget
	 *
	 * @return FellowProjectBudgetTotals
	 */
	public function setFellowProjectBudget($fellowProjectBudget)
	{
		$this->fellowProjectBudget = $fellowProjectBudget;

		return $this;
	}

	/**
	 * Get fellowProjectBudget
	 *
	 * @return FellowProjectBudget
	 */
	public function getFellowProjectBudget()
	{
		return $this->fellowProjectBudget;
	}

	/**
	 * Set revisionId
	 *
	 * @param integer $revisionId
	 *
	 * @return FellowProjectBudgetTotals
	 */
	public function setRevisionId($revisionId)
	{
		$this->revisionId = $revisionId;

		return $this;
	}

	/**
	 * Get revisionId
	 *
	 * @return int
	 */
	public function getRevisionId()
	{
		return $this->revisionId;
	}

	/**
	 * Set fullCost
	 *
	 * @param string $fullCost
	 *
	 * @return FellowProjectBudgetTotals
	 */
	public function setFullCost($fullCost)
	{
		$this->fullCost = $fullCost;

		return $this;
	}

	/**
	 * Get fullCost
	 *
	 * @return string
	 */
	public function getFullCost()
	{
		return $this->fullCost;
	}

	/**
	 * Set postContribution
	 *
	 * @param string $postContribution
	 *
	 * @return FellowProjectBudgetTotals
	 */
	public function setPostContribution($postContribution)
	{
		$this->postContribution = $postContribution;

		return $this;
	}

	/**
	 * Get postContribution
	 *
	 * @return string
	 */
	public function getPostContribution()
	{
		return $this->postContribution;
	}

	/**
	 * Set hostContributionMonetary
	 *
	 * @param string $hostContributionMonetary
	 *
	 * @return FellowProjectBudgetTotals
	 */
	public function setHostContributionMonetary($hostContributionMonetary)
	{
		$this->hostContributionMonetary = $hostContributionMonetary;

		return $this;
	}

	/**
	 * Get hostContributionMonetary
	 *
	 * @return string
	 */
	public function getHostContributionMonetary()
	{
		return $this->hostContributionMonetary;
	}

	/**
	 * Set hostContributionInKind
	 *
	 * @param string $hostContributionInKind
	 *
	 * @return FellowProjectBudgetTotals
	 */
	public function setHostContributionInKind($hostContributionInKind)
	{
		$this->hostContributionInKind = $hostContributionInKind;

		return $this;
	}

	/**
	 * Get hostContributionInKind
	 *
	 * @return string
	 */
	public function getHostContributionInKind()
	{
		return $this->hostContributionInKind;
	}

	/**
	 * Set postTotalContribution
	 *
	 * @param string $postTotalContribution
	 *
	 * @return FellowProjectBudgetTotals
	 */
	public function setPostTotalContribution($postTotalContribution)
	{
		$this->postTotalContribution = $postTotalContribution;

		return $this;
	}

	/**
	 * Get postTotalContribution
	 *
	 * @return string
	 */
	public function getPostTotalContribution()
	{
		return $this->postTotalContribution;
	}

	/**
	 * Set ecaTotalContribution
	 *
	 * @param string $ecaTotalContribution
	 *
	 * @return FellowProjectBudgetTotals
	 */
	public function setEcaTotalContribution($ecaTotalContribution)
	{
		$this->ecaTotalContribution = $ecaTotalContribution;

		return $this;
	}

	/**
	 * Get ecaTotalContribution
	 *
	 * @return string
	 */
	public function getEcaTotalContribution()
	{
		return $this->ecaTotalContribution;
	}

	/**
	 * Set global
	 *
	 * @param string $global
	 *
	 * @return FellowProjectBudgetTotals
	 */
	public function setGlobal($global)
	{
		$this->global = $global;

		return $this;
	}

	/**
	 * Get global
	 *
	 * @return string
	 */
	public function getGlobal()
	{
		return $this->global;
	}

	/**
	 * Set total
	 *
	 * @param string $total
	 *
	 * @return FellowProjectBudgetTotals
	 */
	public function setTotal($total)
	{
		$this->total = $total;

		return $this;
	}

	/**
	 * Get total
	 *
	 * @return string
	 */
	public function getTotal()
	{
		return $this->total;
	}
}

