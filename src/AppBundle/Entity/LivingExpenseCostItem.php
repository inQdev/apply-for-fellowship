<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * LivingExpense
 * @author Felipe Ceballos <felipe.ceballos@inqbation.com>
 *
 * @ORM\Table(name="living_expense_cost_item")
 * @ORM\Entity()
 */
class LivingExpenseCostItem extends BaseEntity
{
	/**
	 * @var float
	 *
	 * @ORM\Column(
	 *     type="decimal", 
	 *     precision=10, 
	 *     scale=2, 
	 *     nullable=true,
	 *     options={"default": 0}
	 * )
	 */
	protected $costPerDay = 0;

	/**
	 * @var int
	 *
	 * @ORM\Column(type="integer", nullable=true)
	 */
	protected $numberOfDays;

	/**
	 * @var float
	 *
	 * @ORM\Column(
	 *     type="decimal", 
	 *     precision=10, 
	 *     scale=2, 
	 *     nullable=true, 
	 *     options={"default": 0}
	 * )
	 */
	protected $costTotal = 0;

	/**
	 * @var LivingExpense
	 *
	 * @ORM\ManyToOne(
	 *     targetEntity="LivingExpense",
	 *     inversedBy="livingExpenseCostItems"
	 * )
	 */
	protected $livingExpense;

	/**
	 * @var CostItem
	 *
	 * @ORM\ManyToOne(targetEntity="CostItem")
	 */
	protected $costItem;


	/**************************** Methods ****************************/
	/**
	 * Set costPerDay
	 *
	 * @param string $costPerDay
	 *
	 * @return LivingExpense
	 */
	public function setCostPerDay($costPerDay)
	{
		$this->costPerDay = $costPerDay;

		return $this;
	}

	/**
	 * Get costPerDay
	 *
	 * @return string
	 */
	public function getCostPerDay()
	{
		return $this->costPerDay;
	}

	/**
	 * Set numberOfDays
	 *
	 * @param integer $numberOfDays
	 *
	 * @return LivingExpense
	 */
	public function setNumberOfDays($numberOfDays)
	{
		$this->numberOfDays = $numberOfDays;

		return $this;
	}

	/**
	 * Get numberOfDays
	 *
	 * @return int
	 */
	public function getNumberOfDays()
	{
		return $this->numberOfDays;
	}

	/**
	 * Set costTotal
	 *
	 * @param string $costTotal
	 *
	 * @return LivingExpense
	 */
	public function setCostTotal($costTotal)
	{
		$this->costTotal = $costTotal;

		return $this;
	}

	/**
	 * Get costTotal
	 *
	 * @return string
	 */
	public function getCostTotal()
	{
		return $this->costTotal;
	}

	/**
	 * Set costItem
	 *
	 * @param integer $costItem
	 *
	 * @return LivingExpense
	 */
	public function setCostItem($costItem)
	{
		$this->costItem = $costItem;

		return $this;
	}

	/**
	 * Get costItem
	 *
	 * @return CostItem
	 */
	public function getCostItem()
	{
		return $this->costItem;
	}


    /**
     * Set livingExpense
     *
     * @param \AppBundle\Entity\LivingExpense $livingExpense
     *
     * @return LivingExpenseCostItem
     */
    public function setLivingExpense(\AppBundle\Entity\LivingExpense $livingExpense = null)
    {
        $this->livingExpense = $livingExpense;

        return $this;
    }

    /**
     * Get livingExpense
     *
     * @return \AppBundle\Entity\LivingExpense
     */
    public function getLivingExpense()
    {
        return $this->livingExpense;
    }

	/**
	 * Calculate the total Cost based on cost per day and number of days
	 */
	public function calculateTotals() {
		$this->costTotal = $this->costPerDay * $this->numberOfDays;
	}

}
