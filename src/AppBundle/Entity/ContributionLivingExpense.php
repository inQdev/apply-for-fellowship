<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * ContributionLivingExpense entity.
 *
 * @package AppBundle\Entity
 * @author  Juan Obando <juan.obando@inqbation.com>
 *
 * @ORM\Entity()
 * @ORM\Table(name="contribution_living_expense")
 */
class ContributionLivingExpense extends BaseEntity
{
    /**
     * @var SpecialistProjectPhaseBudget $phaseBudget
     *
     * @ORM\ManyToOne(
     *     targetEntity="SpecialistProjectPhaseBudget",
     *     inversedBy="contributionLivingExpenses"
     * )
     */
    protected $phaseBudget;

    /**
     * @var Country $country
     *
     * @ORM\ManyToOne(targetEntity="Country")
     */
    protected $country;

    /**
     * @var ArrayCollection $livingExpenseCostItems
     *
     * @ORM\OneToMany(
     *     targetEntity="AppBundle\Entity\ContributionLivingExpenseCostItem",
     *     mappedBy="contributionLivingExpense"
     * )
     */
    protected $contributionLivingExpenseCostItems;

    /**
     * LivingExpense constructor.
     *
     * @param SpecialistProjectPhaseBudget $phaseBudget
     * @param Country                      $country
     */
    public function __construct(
      SpecialistProjectPhaseBudget $phaseBudget,
      Country $country
    ) {
        $this->phaseBudget = $phaseBudget;
        $this->country = $country;

        $this->contributionLivingExpenseCostItems = new ArrayCollection();
    }

    /**
     *
     */
    public function calculateSummaryTotals() {
        $summaryTotals = [
          'cost'          => 0,
          'post'          => 0,
          'hostMonetary'  => 0,
          'hostInKind'    => 0,
          'postHostTotal' => 0,
          'postTotal'     => 0,
          'ecaTotal'      => 0,
        ];

        /** @var ContributionLivingExpenseCostItem $costItem */
        foreach ($this->contributionLivingExpenseCostItems as $costItem) {
            $summaryTotals['cost'] += $costItem->getTotalCost();
            $summaryTotals['post'] += $costItem->getPostContribution();
            $summaryTotals['hostMonetary'] += $costItem->getHostContributionMonetary();
            $summaryTotals['hostInKind'] += $costItem->getHostContributionInKind();
            $summaryTotals['postHostTotal'] += $costItem->getPostHostTotalContribution();
            $summaryTotals['postTotal'] += $costItem->getPostTotalContribution();
            $summaryTotals['ecaTotal'] += $costItem->getEcaTotalContribution();
        }

        return $summaryTotals;
    }

    /**
     * Set phaseBudget
     *
     * @param SpecialistProjectPhaseBudget $phaseBudget
     *
     * @return ContributionLivingExpense
     */
    public function setPhaseBudget(
      SpecialistProjectPhaseBudget $phaseBudget = null
    ) {
        $this->phaseBudget = $phaseBudget;

        return $this;
    }

    /**
     * Get phaseBudget
     *
     * @return SpecialistProjectPhaseBudget
     */
    public function getPhaseBudget()
    {
        return $this->phaseBudget;
    }

    /**
     * Set country
     *
     * @param Country $country
     *
     * @return ContributionLivingExpense
     */
    public function setCountry(Country $country = null)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Get country
     *
     * @return \AppBundle\Entity\Country
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Add contributionLivingExpenseCostItem
     *
     * @param ContributionLivingExpenseCostItem $contributionLivingExpenseCostItem
     *
     * @return ContributionLivingExpense
     */
    public function addContributionLivingExpenseCostItem(
      ContributionLivingExpenseCostItem $contributionLivingExpenseCostItem
    ) {
        $this->contributionLivingExpenseCostItems[] = $contributionLivingExpenseCostItem;

        return $this;
    }

    /**
     * Remove contributionLivingExpenseCostItem
     *
     * @param ContributionLivingExpenseCostItem $contributionLivingExpenseCostItem
     */
    public function removeContributionLivingExpenseCostItem(
      ContributionLivingExpenseCostItem $contributionLivingExpenseCostItem
    ) {
        $this->contributionLivingExpenseCostItems->removeElement(
          $contributionLivingExpenseCostItem
        );
    }

    /**
     * Get contributionLivingExpenseCostItems
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getContributionLivingExpenseCostItems()
    {
        return $this->contributionLivingExpenseCostItems;
    }
}
