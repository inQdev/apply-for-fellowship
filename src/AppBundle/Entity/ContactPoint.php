<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * ContactPoint entity.
 *
 * @package AppBundle\Entity
 * @author  Juan Obando <juan.obando@inqbation.com>
 *
 * @ORM\Entity()
 * @ORM\Table(name="contact_point")
 * @ORM\InheritanceType("SINGLE_TABLE")
 * @ORM\DiscriminatorColumn(name="type", type="integer")
 * @ORM\DiscriminatorMap({
 *     "0" = "ContactPoint",
 *     "1" = "ContactPointSecondary",
 *     "2" = "ContactPointAdditional",
 * })
 */
class ContactPoint extends BaseEntity
{
    /**
     * Constants
     */

    /**
     *
     */
    const PRIMARY_TYPE = 0;

    /**
     *
     */
    const SECONDARY_TYPE = 1;

    /**
     *
     */
    const ADDITIONAL_TYPE = 2;

    /**
     * @var string $firstName
     *
     * @ORM\Column(type="string", length=128, nullable=true)
     */
    protected $firstName;

    /**
     * @var string $firstName
     *
     * @ORM\Column(type="string", length=128, nullable=true)
     */
    protected $lastName;

    /**
     * @var string $title
     *
     * @ORM\Column(type="string", length=128, nullable=true)
     * @Assert\Choice(callback="getTitleChoices")
     */
    protected $title;

    /**
     * @var string $email
     *
     * @ORM\Column(type="string", length=128, nullable=true)
     * @Assert\Email()
     */
    protected $email;

    /**
     * @var string $alternateEmail
     *
     * @ORM\Column(type="string", length=128, nullable=true)
     * @Assert\Email()
     */
    protected $alternateEmail;

    /**
     * @var \DateTime $anticipatedDepartureDate
     *
     * @ORM\Column(type="date", nullable=true)
     * @Assert\Date()
     */
    protected $anticipatedDepartureDate;

    /**
     * @var ProjectGeneralInfo $projectGeneralInfo
     *
     * @ORM\ManyToOne(
     *     targetEntity="ProjectGeneralInfo",
     *     inversedBy="contactPoints"
     * )
     * @ORM\JoinColumn(
     *     name="project_general_info_id",
     *     referencedColumnName="id",
     *     onDelete="cascade"
     * )
     */
    protected $projectGeneralInfo;

    /******************************* Methods Definitions ********************************/

    /**
     * ContactPoint constructor.
     *
     * @param ProjectGeneralInfo $projectGeneralInfo
     */
    public function __construct(ProjectGeneralInfo $projectGeneralInfo)
    {
        $this->projectGeneralInfo = $projectGeneralInfo;
    }

    /**
     * @return array
     */
    public static function getTitleChoices()
    {
        return [
          'PAO'            => 'PAO',
          'APAO'           => 'APAO',
          'CAO'            => 'CAO',
          'ACAO'           => 'ACAO',
          'LE Staff'       => 'LE-Staff',
          'RELO'           => 'RELO',
          'RELO Assistant' => 'RELO-Assistant',
        ];
    }

    /**
     * Get the type
     *
     * @return int
     */
    public function getType()
    {
        return self::PRIMARY_TYPE;
    }

    /**
     * Set firstName
     *
     * @param string $firstName
     *
     * @return ContactPoint
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * Get firstName
     *
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * Set lastName
     *
     * @param string $lastName
     *
     * @return ContactPoint
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * Get lastName
     *
     * @return string
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return ContactPoint
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return ContactPoint
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set alternateEmail
     *
     * @param string $alternateEmail
     *
     * @return ContactPoint
     */
    public function setAlternateEmail($alternateEmail)
    {
        $this->alternateEmail = $alternateEmail;

        return $this;
    }

    /**
     * Get alternateEmail
     *
     * @return string
     */
    public function getAlternateEmail()
    {
        return $this->alternateEmail;
    }

    /**
     * Set anticipatedDepartureDate
     *
     * @param \DateTime $anticipatedDepartureDate
     *
     * @return ContactPoint
     */
    public function setAnticipatedDepartureDate($anticipatedDepartureDate)
    {
        $this->anticipatedDepartureDate = $anticipatedDepartureDate;

        return $this;
    }

    /**
     * Get anticipatedDepartureDate
     *
     * @return \DateTime
     */
    public function getAnticipatedDepartureDate()
    {
        return $this->anticipatedDepartureDate;
    }

    /**
     * Set projectGeneralInfo
     *
     * @param ProjectGeneralInfo $projectGeneralInfo
     *
     * @return ContactPoint
     */
    public function setProjectGeneralInfo(
      ProjectGeneralInfo $projectGeneralInfo = null
    ) {
        $this->projectGeneralInfo = $projectGeneralInfo;

        return $this;
    }

    /**
     * Get projectGeneralInfo
     *
     * @return ProjectGeneralInfo
     */
    public function getProjectGeneralInfo()
    {
        return $this->projectGeneralInfo;
    }
}
