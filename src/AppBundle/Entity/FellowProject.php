<?php

namespace AppBundle\Entity;

use AppBundle\Entity\Fellow\CycleFellow;
use AppBundle\Entity\Fellow\DutyActivityPrimary;
use AppBundle\Entity\Fellow\DutyActivitySecondary;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use FS\SolrBundle\Doctrine\Annotation as Solr;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * FellowProject entity.
 *
 * @package AppBundle\Entity
 * @author  Juan Obando <juan.obando@inqbation.com>
 * @author  Felipe Ceballos <felipe.ceballos@inqbation.com>
 *
 * @ORM\Entity(repositoryClass="AppBundle\Repository\FellowProjectRepository")
 * @ORM\Table(name="fellow_project")
 * @ORM\HasLifecycleCallbacks()
 * @Solr\Document()
 */
class FellowProject extends BaseEntity
{
    /**
     * Constants
     */

    /**
     *
     */
    const CYCLE_SEASON_REGULAR = 'regular';

    /**
     *
     */
    const CYCLE_SEASON_OFF_CYCLE = 'off-cycle';

    /**
     * @var bool $bringDependents
     *
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $bringDependents;

    /**
     * @var bool $bringChildren
     *
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $bringChildren;

    /**
     * @var bool $bringUnmarriedPartner
     *
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $bringUnmarriedPartner;

    /**
     * @var bool $bringSameSexSpouse
     *
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $bringSameSexSpouse;

    /**
     * @var bool $bringSameSexPartner
     *
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $bringSameSexPartner;

    /**
     * @var string $dependentRestrictionsDesc
     *
     * @ORM\Column(type="string", length=4294967295, nullable=true)
     */
    protected $dependentRestrictionsDesc;

    /**
     * @var \DateTime $proposedStartDate
     *
     * @ORM\Column(type="date", nullable=true)
     * @Assert\Date()
     */
    protected $proposedStartDate;

    /**
     * @var bool $proposedStartDateFlexible
     *
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $proposedStartDateFlexible;

    /**
     * @var \DateTime $proposedEndDate
     *
     * @ORM\Column(type="date", nullable=true)
     * @Assert\Date()
     */
    protected $proposedEndDate;

    /**
     * @var string $cycleSeason
     *
     * @ORM\Column(
     *     type="string",
     *     length=32,
     *     nullable=true,
     *     options={"default"="regular"}
     * )
     * @Assert\Choice(
     *     choices = {"regular", "off-cycle"}
     * )
     * @Solr\Field(type="string")
     */
    protected $cycleSeason;

    /**
     * @var string $cycleSeasonDesc
     *
     * @ORM\Column(type="string", length=4294967295, nullable=true)
     * )
     */
    protected $cycleSeasonDesc;

    /**
     * @var boolean $housingDependents
     *
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $housingDependents;

    /**
     * @var boolean $fellowFundsHousing
     *
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $fellowFundsHousing;

    /**
     * @var string $housingDependentInfo
     *
     * @ORM\Column(type="string", length=4294967295, nullable=true)
     */
    protected $housingDependentInfo;

    /**
     * @var ProjectGeneralInfo $projectGeneralInfo
     *
     * @see http://symfony.com/doc/current/cookbook/form/form_collections.html#allowing-new-tags-with-the-prototype
     *      Documentation of Embed a Collections of Forms
     *
     * @ORM\OneToOne(
     *     targetEntity="ProjectGeneralInfo",
     *     inversedBy="fellowProject",
     *     cascade={"persist", "remove"}
     * )
     * @ORM\JoinColumn(
     *     name="project_general_info_id",
     *     referencedColumnName="id",
     *     onDelete="cascade"
     * )
     * @Solr\Field(name="reference_number", type="string", getter="getReferenceNumber")
     * @Solr\Field(name="proposal_status", type="string", getter="getProposalStatusText")
     * @Solr\Field(name="proposal_outcome", type="string", getter="getProjectOutcome")
     * @Solr\Field(name="region", type="string", getter="getRegionAcronym")
     * @Solr\Field(name="rpo_ranking", type="integer", getter="getRpoRanking")
     * @Solr\Field(name="relo_location", type="string", getter="getReloLocation")
     * @Solr\Field(name="relo_ranking", type="integer", getter="getReloRanking")
     * @Solr\Field(name="country", type="string", getter="getLastCountry")
     * @Solr\Field(name="cycle_id", type="integer", getter="getCycleId")
     */
    protected $projectGeneralInfo;

    /**
     * Cascade deletion defined in LocationHostInfo
     * @var LocationHostInfo $locationHostInfo
     *
     * @ORM\OneToOne(
     *     targetEntity="LocationHostInfo",
     *     mappedBy="fellowProject",
     *     cascade={"persist"}
     * )
     * @Solr\Field(name="host_institution", type="string", getter="getHost")
     */
    protected $locationHostInfo;

    /**
     * Cascade deletion defined in DutyActivityPrimary
     * @var ArrayCollection $dutyPrimaryActivities
     *
     * @ORM\OneToMany(
     *     targetEntity="AppBundle\Entity\Fellow\DutyActivityPrimary",
     *     mappedBy="fellowProject",
     *     cascade={"persist"}
     * )
     */
    protected $dutyPrimaryActivities;

    /**
     * Cascade deletion defined in DutyActivitySecondary
     * @var ArrayCollection $dutySecondaryActivities
     *
     * @ORM\OneToMany(
     *     targetEntity="AppBundle\Entity\Fellow\DutyActivitySecondary",
     *     mappedBy="fellowProject",
     *     cascade={"persist"}
     * )
     */
    protected $dutySecondaryActivities;

    /**
     * Cascade deletion defined in FellowProjectBudget
     * @ORM\OneToOne(
     *     targetEntity="FellowProjectBudget",
     *     mappedBy="fellowProject",
     *     cascade={"persist"}
     * )
     */
    protected $fellowProjectBudget;

    /**
     * Cascade deletion defined in WizardStepStatus
     * @var ArrayCollection $wizardStepsStatuses
     *
     * @ORM\OneToMany(
     *     targetEntity="WizardStepStatus",
     *     mappedBy="fellowProject",
     *     cascade={"persist"}
     * )
     */
    protected $wizardStepsStatuses;

    /**
     * @var ArrayCollection $fellowProjectReviews
     *
     * @ORM\ManyToMany(targetEntity="ProjectReview", mappedBy="fellowProjects")
     */
    protected $fellowProjectReviews;

    /***************************** Methods Definition *****************************/

    /**
     * FellowProject constructor.
     *
     * @param ProjectGeneralInfo $projectGeneralInfo
     */
    public function __construct(ProjectGeneralInfo $projectGeneralInfo)
    {
        $this->projectGeneralInfo = $projectGeneralInfo;

        $this->locationHostInfo = new LocationHostInfo($this);

        $this->dutyPrimaryActivities = new ArrayCollection();
        $this->dutySecondaryActivities = new ArrayCollection();
        $this->wizardStepsStatuses = new ArrayCollection();
        $this->fellowProjectReviews = new ArrayCollection();

        // TODO: there must be a better way for this :(
        $generalInfoStepStatus = new WizardStepStatus(
            WizardStepStatus::GENERAL_INFO_STEP, WizardStepStatus::NOT_STARTED
        );
        $generalInfoStepStatus->setFellowProject($this);
        $this->addWizardStepsStatus($generalInfoStepStatus);

        $pointsOfContactStepStatus = new WizardStepStatus(
            WizardStepStatus::POINT_OF_CONTACT_STEP, WizardStepStatus::NOT_STARTED
        );
        $pointsOfContactStepStatus->setFellowProject($this);
        $this->addWizardStepsStatus($pointsOfContactStepStatus);

        $projectRequirementsStepStatus = new WizardStepStatus(
            WizardStepStatus::PROJECT_REQUIREMENTS_STEP,
            WizardStepStatus::NOT_STARTED
        );
        $projectRequirementsStepStatus->setFellowProject($this);
        $this->addWizardStepsStatus($projectRequirementsStepStatus);

        $housingInfoStepStatus = new WizardStepStatus(
            WizardStepStatus::HOUSING_INFO_STEP, WizardStepStatus::NOT_STARTED
        );
        $housingInfoStepStatus->setFellowProject($this);
        $this->addWizardStepsStatus($housingInfoStepStatus);

        $certificationsStepStatus = new WizardStepStatus(
            WizardStepStatus::CERTIFICATIONS_STEP, WizardStepStatus::NOT_STARTED
        );
        $certificationsStepStatus->setFellowProject($this);
        $this->addWizardStepsStatus($certificationsStepStatus);

        $locationHostInfoStepStatus = new WizardStepStatus(
            WizardStepStatus::LOCATION_HOST_INFO_STEP,
            WizardStepStatus::NOT_STARTED
        );
        $locationHostInfoStepStatus->setFellowProject($this);
        $this->addWizardStepsStatus($locationHostInfoStepStatus);

        $fellowshipDutiesStepStatus = new WizardStepStatus(
            WizardStepStatus::FELLOWSHIP_DUTIES_STEP,
            WizardStepStatus::NOT_STARTED
        );
        $fellowshipDutiesStepStatus->setFellowProject($this);
        $this->addWizardStepsStatus($fellowshipDutiesStepStatus);

        $budgetStepStatus = new WizardStepStatus(
            WizardStepStatus::BUDGET_STEP, WizardStepStatus::NOT_STARTED
        );
        $budgetStepStatus->setFellowProject($this);
        $this->addWizardStepsStatus($budgetStepStatus);
    }

    /**
     * @ORM\PrePersist()
     */
    public function doOnPrePersist()
    {
        $this->setProposedEndDateAndCycleSeason();
    }

    /**
     * @ORM\PreUpdate()
     */
    public function doOnPreUpdate()
    {
        $this->setProposedEndDateAndCycleSeason();
    }

    /**
     *
     */
    private function setProposedEndDateAndCycleSeason()
    {
        if (null !== $this->proposedStartDate) {
            /** @var CycleFellow $cycle */
            $cycle = $this->projectGeneralInfo->getCycle();

            $this->proposedEndDate = clone $this->proposedStartDate;

            // TODO: the number of months should be entered by the app admin from an admin panel
            $this->proposedEndDate->add(new \DateInterval('P10M'));

            // Here is check what kind of Cycle is going to be the project.
            if (($this->proposedStartDate >= $cycle->getRegularCyclePeriodStartDate())
                && ($this->proposedStartDate <= $cycle->getRegularCyclePeriodEndDate())
            ) {
                $this->cycleSeason = self::CYCLE_SEASON_REGULAR;
            } elseif (($this->proposedStartDate >= $cycle->getOffCyclePeriodStartDate())
                && ($this->proposedStartDate <= $cycle->getOffCyclePeriodEndDate())
            ) {
                $this->cycleSeason = self::CYCLE_SEASON_OFF_CYCLE;
            }
        }
    }

    /**
     * Get the Fellowship Duties section status and persist it.
     *
     * @param \Doctrine\Common\Persistence\ObjectManager $em
     */
   public function setFellowshipDutiesAndMissiongGoalsSectionStatus($em)
   {
       $isFellowshipDutiesStepComplete = true;

       /** @var DutyActivityPrimary $primaryDuty */
       foreach ($this->dutyPrimaryActivities as $primaryDuty) {
           if ((null !== $primaryDuty->getDescription())
             && !$primaryDuty->getActivitiesFocus()->isEmpty()
             && (null !== $primaryDuty->getHoursNumber())
             && (null !== $primaryDuty->getAudience())
           ) {
               $isFellowshipDutiesStepComplete = $isFellowshipDutiesStepComplete && true;
           } elseif ((null !== $primaryDuty->getDescription())
             || !$primaryDuty->getActivitiesFocus()->isEmpty()
             || (null !== $primaryDuty->getHoursNumber())
             || (null !== $primaryDuty->getAudience())
           ) {
               $isFellowshipDutiesStepComplete = false;
           } else {
               $isFellowshipDutiesStepComplete = false;
           }
       }

       if ($isFellowshipDutiesStepComplete && (null != $this->projectGeneralInfo->getPostMissionGoalsDesc())) {
           $isFellowshipDutiesStepComplete = $isFellowshipDutiesStepComplete && true;
       }
       elseif ($isFellowshipDutiesStepComplete || (null != $this->projectGeneralInfo->getPostMissionGoalsDesc())) {
           $isFellowshipDutiesStepComplete = false;
       }

       $newStepStatus = $isFellowshipDutiesStepComplete
         ? WizardStepStatus::COMPLETED
         : WizardStepStatus::IN_PROGRESS;

       /** @var WizardStepStatus $fellowshipDutiesAndMissionGoalsSectionStatus */
       $fellowshipDutiesAndMissionGoalsSectionStatus = $em->getRepository('AppBundle:WizardStepStatus')
         ->findStepStatusByProject(
           $this,
           WizardStepStatus::FELLOWSHIP_DUTIES_STEP
         );

       if ($newStepStatus !== $fellowshipDutiesAndMissionGoalsSectionStatus->getStatus()) {
           $fellowshipDutiesAndMissionGoalsSectionStatus->setStatus($newStepStatus);

           $em->persist($fellowshipDutiesAndMissionGoalsSectionStatus);
           $em->flush();
       }
   }

    /**
     * Set bringDependents
     *
     * @param boolean $bringDependents
     *
     * @return FellowProject
     */
    public function setBringDependents($bringDependents)
    {
        $this->bringDependents = $bringDependents;

        return $this;
    }

    /**
     * Get bringDependents
     *
     * @return boolean
     */
    public function getBringDependents()
    {
        return $this->bringDependents;
    }

    /**
     * Set bringChildren
     *
     * @param boolean $bringChildren
     *
     * @return FellowProject
     */
    public function setBringChildren($bringChildren)
    {
        $this->bringChildren = $bringChildren;

        return $this;
    }

    /**
     * Get bringChildren
     *
     * @return boolean
     */
    public function getBringChildren()
    {
        return $this->bringChildren;
    }

    /**
     * Set bringUnmarriedPartner
     *
     * @param boolean $bringUnmarriedPartner
     *
     * @return FellowProject
     */
    public function setBringUnmarriedPartner($bringUnmarriedPartner)
    {
        $this->bringUnmarriedPartner = $bringUnmarriedPartner;

        return $this;
    }

    /**
     * Get bringUnmarriedPartner
     *
     * @return boolean
     */
    public function getBringUnmarriedPartner()
    {
        return $this->bringUnmarriedPartner;
    }

    /**
     * Set bringSameSexSpouse
     *
     * @param boolean $bringSameSexSpouse
     *
     * @return FellowProject
     */
    public function setBringSameSexSpouse($bringSameSexSpouse)
    {
        $this->bringSameSexSpouse = $bringSameSexSpouse;

        return $this;
    }

    /**
     * Get bringSameSexSpouse
     *
     * @return boolean
     */
    public function getBringSameSexSpouse()
    {
        return $this->bringSameSexSpouse;
    }

    /**
     * Set bringSameSexPartner
     *
     * @param boolean $bringSameSexPartner
     *
     * @return FellowProject
     */
    public function setBringSameSexPartner($bringSameSexPartner)
    {
        $this->bringSameSexPartner = $bringSameSexPartner;

        return $this;
    }

    /**
     * Get bringSameSexPartner
     *
     * @return boolean
     */
    public function getBringSameSexPartner()
    {
        return $this->bringSameSexPartner;
    }

    /**
     * Set dependentRestrictionsDesc
     *
     * @param string $dependentRestrictionsDesc
     *
     * @return FellowProject
     */
    public function setDependentRestrictionsDesc($dependentRestrictionsDesc)
    {
        $this->dependentRestrictionsDesc = $dependentRestrictionsDesc;

        return $this;
    }

    /**
     * Get dependentRestrictionsDesc
     *
     * @return string
     */
    public function getDependentRestrictionsDesc()
    {
        return $this->dependentRestrictionsDesc;
    }

    /**
     * Set proposedStartDate
     *
     * @param \DateTime $proposedStartDate
     *
     * @return FellowProject
     */
    public function setProposedStartDate($proposedStartDate)
    {
        $this->proposedStartDate = $proposedStartDate;

        return $this;
    }

    /**
     * Get proposedStartDate
     *
     * @return \DateTime
     */
    public function getProposedStartDate()
    {
        return $this->proposedStartDate;
    }

    /**
     * Set proposedStartDateFlexible
     *
     * @param boolean $proposedStartDateFlexible
     *
     * @return FellowProject
     */
    public function setProposedStartDateFlexible($proposedStartDateFlexible)
    {
        $this->proposedStartDateFlexible = $proposedStartDateFlexible;

        return $this;
    }

    /**
     * Get proposedStartDateFlexible
     *
     * @return boolean
     */
    public function isProposedStartDateFlexible()
    {
        return $this->proposedStartDateFlexible;
    }

    /**
     * Set proposedEndDate
     *
     * @param \DateTime $proposedEndDate
     *
     * @return FellowProject
     */
    public function setProposedEndDate($proposedEndDate)
    {
        $this->proposedEndDate = $proposedEndDate;

        return $this;
    }

    /**
     * Get proposedEndDate
     *
     * @return \DateTime
     */
    public function getProposedEndDate()
    {
        return $this->proposedEndDate;
    }

    /**
     * Set cycleSeason
     *
     * @param string $cycleSeason
     *
     * @return FellowProject
     */
    public function setCycleSeason($cycleSeason)
    {
        $this->cycleSeason = $cycleSeason;

        return $this;
    }

    /**
     * Get cycleSeason
     *
     * @return string
     */
    public function getCycleSeason()
    {
        return $this->cycleSeason;
    }

    /**
     * Set cycleSeasonDesc
     *
     * @param string $cycleSeasonDesc
     *
     * @return FellowProject
     */
    public function setCycleSeasonDesc($cycleSeasonDesc)
    {
        $this->cycleSeasonDesc = $cycleSeasonDesc;

        return $this;
    }

    /**
     * Get cycleSeasonDesc
     *
     * @return string
     */
    public function getCycleSeasonDesc()
    {
        return $this->cycleSeasonDesc;
    }

    /**
     * Set housingDependents
     *
     * @param boolean $housingDependents
     *
     * @return FellowProject
     */
    public function setHousingDependents($housingDependents)
    {
        $this->housingDependents = $housingDependents;

        return $this;
    }

    /**
     * Get housingDependents
     *
     * @return boolean
     */
    public function getHousingDependents()
    {
        return $this->housingDependents;
    }

    /**
     * Set fellowFundsHousing
     *
     * @param boolean $fellowFundsHousing
     *
     * @return FellowProject
     */
    public function setFellowFundsHousing($fellowFundsHousing)
    {
        $this->fellowFundsHousing = $fellowFundsHousing;

        return $this;
    }

    /**
     * Get fellowFundsHousing
     *
     * @return boolean
     */
    public function getFellowFundsHousing()
    {
        return $this->fellowFundsHousing;
    }

    /**
     * Set housingDependentInfo
     *
     * @param string $housingDependentInfo
     *
     * @return FellowProject
     */
    public function setHousingDependentInfo($housingDependentInfo)
    {
        $this->housingDependentInfo = $housingDependentInfo;

        return $this;
    }

    /**
     * Get housingDependentInfo
     *
     * @return string
     */
    public function getHousingDependentInfo()
    {
        return $this->housingDependentInfo;
    }

    /**
     * Set projectGeneralInfo
     *
     * @param ProjectGeneralInfo $projectGeneralInfo
     *
     * @return FellowProject
     */
    public function setProjectGeneralInfo(
        ProjectGeneralInfo $projectGeneralInfo = null
    ) {
        $this->projectGeneralInfo = $projectGeneralInfo;

        return $this;
    }

    /**
     * Get projectGeneralInfo
     *
     * @return ProjectGeneralInfo
     */
    public function getProjectGeneralInfo()
    {
        return $this->projectGeneralInfo;
    }

    /**
     * Set locationHostInfo
     *
     * @param LocationHostInfo $locationHostInfo
     *
     * @return FellowProject
     */
    public function setLocationHostInfo(
        LocationHostInfo $locationHostInfo = null
    ) {
        $this->locationHostInfo = $locationHostInfo;

        return $this;
    }

    /**
     * Get locationHostInfo
     *
     * @return LocationHostInfo
     */
    public function getLocationHostInfo()
    {
        return $this->locationHostInfo;
    }

    /**
     * Add dutyPrimaryActivity
     *
     * @param DutyActivityPrimary $dutyPrimaryActivity
     *
     * @return FellowProject
     */
    public function addDutyPrimaryActivity(
        DutyActivityPrimary $dutyPrimaryActivity
    ) {
        $this->dutyPrimaryActivities[] = $dutyPrimaryActivity;

        return $this;
    }

    /**
     * Remove dutyPrimaryActivity
     *
     * @param DutyActivityPrimary $dutyPrimaryActivity
     */
    public function removeDutyPrimaryActivity(
        DutyActivityPrimary $dutyPrimaryActivity
    ) {
        $this->dutyPrimaryActivities->removeElement($dutyPrimaryActivity);
    }

    /**
     * Get dutyPrimaryActivities
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDutyPrimaryActivities()
    {
        return $this->dutyPrimaryActivities;
    }

    /**
     * Add dutySecondaryActivity
     *
     * @param DutyActivitySecondary $dutySecondaryActivity
     *
     * @return FellowProject
     */
    public function addDutySecondaryActivity(
        DutyActivitySecondary $dutySecondaryActivity
    ) {
        $this->dutySecondaryActivities[] = $dutySecondaryActivity;

        return $this;
    }

    /**
     * Remove dutySecondaryActivity
     *
     * @param DutyActivitySecondary $dutySecondaryActivity
     */
    public function removeDutySecondaryActivity(
        DutyActivitySecondary $dutySecondaryActivity
    ) {
        $this->dutySecondaryActivities->removeElement($dutySecondaryActivity);
    }

    /**
     * Get dutySecondaryActivities
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDutySecondaryActivities()
    {
        return $this->dutySecondaryActivities;
    }

    /**
     * Set fellowProjectBudget
     *
     * @param FellowProjectBudget $fellowProjectBudget
     *
     * @return FellowProject
     */
    public function setFellowProjectBudget(
        FellowProjectBudget $fellowProjectBudget = null
    ) {
        $this->fellowProjectBudget = $fellowProjectBudget;

        return $this;
    }

    /**
     * Get fellowProjectBudget
     *
     * @return FellowProjectBudget
     */
    public function getFellowProjectBudget()
    {
        return $this->fellowProjectBudget;
    }

    /**
     * Add wizardStepsStatus
     *
     * @param WizardStepStatus $wizardStepsStatus
     *
     * @return FellowProject
     */
    public function addWizardStepsStatus(WizardStepStatus $wizardStepsStatus)
    {
        $this->wizardStepsStatuses[] = $wizardStepsStatus;

        return $this;
    }

    /**
     * Remove wizardStepsStatus
     *
     * @param WizardStepStatus $wizardStepsStatus
     */
    public function removeWizardStepsStatus(WizardStepStatus $wizardStepsStatus)
    {
        $this->wizardStepsStatuses->removeElement($wizardStepsStatus);
    }

    /**
     * Get wizardStepsStatuses
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getWizardStepsStatuses()
    {
        return $this->wizardStepsStatuses;
    }

    /**
     * Add fellowProjectReview
     *
     * @param ProjectReview $fellowProjectReview
     *
     * @return FellowProject
     */
    public function addFellowProjectReview(ProjectReview $fellowProjectReview)
    {
        $this->fellowProjectReviews[] = $fellowProjectReview;

        return $this;
    }

    /**
     * Remove fellowProjectReview
     *
     * @param ProjectReview $fellowProjectReview
     */
    public function removeFellowProjectReview(
        ProjectReview $fellowProjectReview
    ) {
        $this->fellowProjectReviews->removeElement($fellowProjectReview);
    }

    /**
     * Get fellowProjectReviews
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getFellowProjectReviews()
    {
        return $this->fellowProjectReviews;
    }

    /**
     * Get proposedStartDateFlexible
     *
     * @return boolean
     */
    public function getProposedStartDateFlexible()
    {
        return $this->proposedStartDateFlexible;
    }
}
