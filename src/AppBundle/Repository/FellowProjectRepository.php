<?php

namespace AppBundle\Repository;

use AppBundle\Entity\Fellow\CycleFellow;
use AppBundle\Entity\ProjectGeneralInfo;
use AppBundle\Entity\ProjectOutcome;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\ORM\QueryBuilder;

/**
 * FellowProjectRepository entity repository.
 *
 * @package AppBundle\Repository
 * @author  Juan Obando <juan.obando@inqbation.com>
 */
class FellowProjectRepository extends EntityRepository
{
    /**
     * @param Collection  $reloLocations
     * @param CycleFellow $cycleFellow
     *
     * @return int
     */
    public function countAllByRELOLocationInCycle(Collection $reloLocations, CycleFellow $cycleFellow)
    {
        $qb = $this->findAllByRELOLocationsInCycle($reloLocations, $cycleFellow);

        if (null === $qb) {
            return 0;
        }

        return ((int) $qb->select($qb->expr()->count('fp'))->getQuery()->getSingleScalarResult());
    }

    /**
     * Find all Fellow projects associated to pass RELO Locations.
     *
     * @param Collection  $reloLocations RELO locations collection.
     * @param CycleFellow $cycleFellow   Fellow cycle.
     *
     * @return array Fellow projects associated to RELO Locations.
     */
    public function findAllByRELOLocationsAndCycle(Collection $reloLocations, CycleFellow $cycleFellow)
    {
        $qb = $this->findAllByRELOLocationsInCycle($reloLocations, $cycleFellow);

        if (null === $qb) {
            return [];
        }

        return $qb->getQuery()->getResult();
    }

    /**
     * Find all Fellow projects associated to pass regions.
     *
     * @param Collection $regions Regions collection.
     *
     * @return array Fellow projects associated to regions.
     */
    public function findAllByRegions(Collection $regions)
    {
        $fellowProjects = [];

        // Do not perform the query if regions collection is empty.
        if (!$regions->isEmpty()) {
            $qb = $this->createQueryBuilder('fp');

            $fellowProjects = $qb
              ->innerJoin('fp.projectGeneralInfo', 'pgi', Join::WITH)
              ->where($qb->expr()->in('pgi.region', ':regions'))
              ->setParameter(':regions', $regions)
              ->orderBy('pgi.updatedAt', 'ASC')
              ->getQuery()
              ->getResult();
        }

        return $fellowProjects;
    }

    /**
     * @param CycleFellow $cycleFellow The Fellow Cycle
     * @param bool        $results
     *
     * @return array|QueryBuilder Return Fellow Proposals approved in given Fellow cycle if $results is true.  Otherwise, return the QueryBuilder.
     */
    public function findAllApprovedInCycle(CycleFellow $cycleFellow, $results = true)
    {
        /** @var ProjectOutcome $outcome */
        $outcome = $this->getEntityManager()->getRepository('AppBundle:ProjectOutcome')->find(ProjectOutcome::APPROVED_BY_ECA);

        $qb = $this->findAllByOutcomeInCycle($outcome, $cycleFellow);

        return $results
          ? $qb->getQuery()->getResult()
          : $qb;
    }

    /**
     * Find all Fellow projects associated to a given Outcome and Fellow cycle.
     *
     * @param ProjectOutcome $outcome
     * @param CycleFellow    $cycleFellow The Fellow Cycle
     *
     * @return QueryBuilder
     */
    private function findAllByOutcomeInCycle(ProjectOutcome $outcome, CycleFellow $cycleFellow)
    {
        $qb = $this->createQueryBuilder('fp');

        return $qb
          ->innerJoin('fp.projectGeneralInfo', 'pgi', Join::WITH)
          // Join with countries to sort the result by the country name
          ->innerJoin('pgi.countries', 'c', Join::WITH)
          ->where(
            $qb->expr()->andX(
              $qb->expr()->eq('pgi.projectOutcome', ':outcome'),
              $qb->expr()->eq('pgi.cycle', ':cycle')
            )
          )
          ->setParameters(
            [
              'outcome' => $outcome,
              'cycle'   => $cycleFellow,
            ]
          )
          ->orderBy('c.name', 'ASC');
    }

    /**
     * Query to retrieve all Proposals in a RELO Location in a given Fellow Cycle.
     *
     * @param Collection  $reloLocations The RELO Locations.
     * @param CycleFellow $cycleFellow   The Fellow Cycle.
     *
     * @return QueryBuilder|null
     */
    private function findAllByRELOLocationsInCycle(Collection $reloLocations, CycleFellow $cycleFellow)
    {
        if ($reloLocations->isEmpty()) {
            return null;
        }

        $qb = $this->findAllSubmittedInCycle($cycleFellow);

//        $x = $qb->getQuery()->getResult();

        if (null === $qb) {
            return null;
        }

        return $qb
          ->andWhere($qb->expr()->in('pgi.reloLocation', ':reloLocations'))
          ->setParameter(':reloLocations', $reloLocations)
          ->orderBy('pgi.reloRanking', 'ASC');
    }

    /**
     * @param CycleFellow $cycleFellow The Fellow cycle.
     *
     * @return QueryBuilder
     */
    private function findAllSubmittedInCycle(CycleFellow $cycleFellow)
    {
        $qb = $this->createQueryBuilder('fp');

        return $qb
          ->innerJoin('fp.projectGeneralInfo', 'pgi', Join::WITH)
          ->where(
            $qb->expr()->andX(
              $qb->expr()->eq('pgi.submissionStatus', ':submissionStatus'),
              $qb->expr()->eq('pgi.cycle', ':fellowCycle')
            )
          )
          ->setParameters(
            [
              ':submissionStatus' => ProjectGeneralInfo::PROJECT_SUBMITTED,
              ':fellowCycle'      => $cycleFellow,
            ]
          );
    }
}
