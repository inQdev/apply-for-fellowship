<?php

namespace AppBundle\Repository;

use AppBundle\Entity\Region;
use AppBundle\Entity\ReloLocation;
use AppBundle\Entity\User;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr\Join;

/**
 * ProjectGeneralInfoRepository repository
 *
 * @package AppBundle\Repository
 * @author  Juan Obando <juan.obando@inqbation.com>
 */
class ProjectGeneralInfoRepository extends EntityRepository
{
    /**
     * Find all Fellow Proposals associated to current authenticated user.
     *
     * @param User $user
     *
     * @return \Doctrine\ORM\QueryBuilder
     */
    public function findAllFellowProposalsByUser(User $user)
    {
        $qb = $this->createQueryBuilder('pgi');

        $fellowProposals = $qb
          ->innerJoin('pgi.fellowProject', 'fp', Join::WITH)
          ->where($qb->expr()->eq('pgi.createdBy', ':user'))
          ->setParameter(':user', $user)
          ->orderBy('pgi.createdAt', 'ASC')
          ->getQuery()
          ->getResult();

        return $fellowProposals;
    }

    /**
     * Find all Specialist Proposals associated to current authenticated user.
     *
     * @param User $user
     *
     * @return \Doctrine\ORM\QueryBuilder
     */
    public function findAllSpecialistProposalsByUser(User $user)
    {
        $qb = $this->createQueryBuilder('pgi');

        $fellowProposals = $qb
          ->innerJoin('pgi.specialistProjectPhase', 'spp', Join::WITH)
          ->where($qb->expr()->eq('pgi.createdBy', ':user'))
          ->setParameter(':user', $user)
          ->orderBy('pgi.createdAt', 'ASC')
          ->getQuery()
          ->getResult();

        return $fellowProposals;
    }
}
