<?php

namespace AppBundle\Repository;
use Symfony\Component\Validator\Constraints\Collection;
use Doctrine\ORM\EntityRepository;

/**
 * SpecialistProjectPhaseRepository
 *
 */
class SpecialistProjectPhaseRepository extends EntityRepository
{

	/**
	 * @return Collection
	 */
//	 * @param int $id
	public function getInCountryAssignmentOptions() {

		$query = $this->getEntityManager()->createQuery(
			'SELECT ICA.id, CONCAT(ICA.city,"(", C.name, ")") AS name
				FROM AppBundle:InCountryAssignment ICA
				JOIN AppBundle:Country C
				ON ICA.country_id = C.id
				WHERE ICA.specialist_project_phase_id = :phase_id'
		)->setParameters([
			'phase_id' => $this->getId(),
		]);

		$items = $query->getResult();

		return $items;
	}

}
