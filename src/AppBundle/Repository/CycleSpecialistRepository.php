<?php

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * CycleSpecialistRepository repository.
 *
 * @package AppBundle\Repository
 * @author  Luke Torres <lucas.torres@inqbation.com>
 * @author  Juan Obando <juan.obando@inqbation.com>
 */
class CycleSpecialistRepository extends EntityRepository
{
    /**
     * @return mixed
     * @throws \Doctrine\ORM\NoResultException
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getCurrentCycle()
    {
        $now = new \DateTime();
        $qb = $this->createQueryBuilder('c');

        $cycleSpecialist = $qb
          ->where(
            $qb->expr()->andX(
              $qb->expr()->lte('c.openPeriodStartDate', ':now'),
              $qb->expr()->gte('c.openPeriodEndDate', ':now')
            )
          )
          ->setParameter('now', $now->format('Y-m-d'))
          ->getQuery()
          ->getSingleResult();

        return $cycleSpecialist;
    }
}
