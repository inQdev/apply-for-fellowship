<?php

namespace AppBundle\Repository;

use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\EntityRepository;

/**
 * FellowProjectRepository entity repository.
 *
 * @package AppBundle\Repository
 * @author  Juan Obando <juan.obando@inqbation.com>
 */
class RoleStructureRepository extends EntityRepository
{

	/**
	 * Finds all the records in the table and returns them in simple array format to be used by the RoleHierarchy class
	 * @return array
	 */
	public function getRolesArray()
	{
		$roleStructure = $this->findAll();

		$rolesArray = array();

		foreach ($roleStructure as $role) {
			$rolesArray[$role->getName()] = json_decode($role->getRoles());
		}

		return $rolesArray;
	}

}
