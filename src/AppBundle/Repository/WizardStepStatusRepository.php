<?php

namespace AppBundle\Repository;

/**
 * WizardStepStatusRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 *
 */
class WizardStepStatusRepository extends \Doctrine\ORM\EntityRepository
{

	/**
	 * @param \AppBundle\Entity\FellowProject $fellowProject
	 * @param int $step
	 * @return int
	 */
	public function findStepStatusByProject(\AppBundle\Entity\FellowProject $fellowProject, $step) {
		$query = $this->getEntityManager()->createQuery(
				'SELECT W
				FROM AppBundle:WizardStepStatus W
				WHERE W.fellowProject = :project
				AND W.step = :step'
			)->setParameters([
				'project' => $fellowProject,
				'step' => $step
			]);

		$status = $query->getResult();

		if ($status) {
			return ($status[0]);
		}

		return false;
	}

}
